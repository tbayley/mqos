/*
 * mqosMessage.c - mqosMessage module source file.
 *
 * The mqosMessage module implements functions to dynamically allocate and free
 * blocks of memory that store MQOS messages. The message allocation heap is
 * a static tMqosMessage array, whose size is defined in mqosConfig.h.
 *
 * Copyright (C) 2021 Antony Bayley.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <stdlib.h>
#include "mqosMessage.h"
#include "mqosPort.h"

/* PRIVATE VARIABLE DEFINITIONS **********************************************/

/*! MQOS message heap */
static tMqosMessageContainer heap[MQOS_CONFIG_HEAP_SIZE] = {
    {
        {
            mqosConfig_MSG_NONE,
#if MQOS_CONFIG_MSG_CONTAINS_PAYLOAD
            {0},
#endif  /* #if MQOS_CONFIG_MSG_CONTAINS_PAYLOAD */
        },
        0,
#if MQOS_CONFIG_MSG_SCHEDULER_FIFO_ORDER
        NULL
#endif  /* #if MQOS_CONFIG_MSG_SCHEDULER_FIFO_ORDER */
    },
};


/*! MQOS message statistics */
static tMqosMessage_Mallinfo mallinfo = {0, 0, 0};


#ifdef TEST

/*! Empty MQOS message structure, to reset state in unit tests */
static const tMqosMessageContainer emptyMessageContainer = {
    {
        mqosConfig_MSG_NONE,
#if MQOS_CONFIG_MSG_CONTAINS_PAYLOAD
        {0},
#endif  /* #if MQOS_CONFIG_MSG_CONTAINS_PAYLOAD */
    },
    0,
#if MQOS_CONFIG_MSG_SCHEDULER_FIFO_ORDER
    NULL
#endif  /* #if MQOS_CONFIG_MSG_SCHEDULER_FIFO_ORDER */
};


/*! Empty mallinfo structure, to reset state in unit tests */
static const tMqosMessage_Mallinfo emptyMallinfo = {0, 0, 0};

#endif  /* #ifdef TEST */


/* PUBLIC FUNCTION DEFINITIONS ***********************************************/

tMqosMessageContainer* mqosMessage_Malloc(void)
{
    tMqosMessageContainer* msgHandle = NULL;

    /* check if sufficient memory is available on the message heap */
    if (MQOS_CONFIG_HEAP_SIZE > mallinfo.allocCount)
    {
        /* there is space left on the heap */
        /* find the first unallocated message block */
        msgHandle = heap;
        while(mqosConfig_MSG_NONE != msgHandle->msg.msgId)
        {
            msgHandle++;
        }

        /* set the msgId to mark the message as allocated */
        msgHandle->msg.msgId = mqos_MSG_MAX_ID;

        /* update the message scheduler's state variables */
        mallinfo.allocCount++;  /* increment the queued message count */
        if (mallinfo.allocCount > mallinfo.maxAllocCount)
        {
            /* run-time monitoring of maximum heap usage */
            mallinfo.maxAllocCount = mallinfo.allocCount;
        }    
    }
    else
    {
        /* increment the message allocation fault count */
        mallinfo.allocFaults++;
    }
    return msgHandle;
}


void mqosMessage_Free(tMqosMessageContainer* msgHandle)
{
    if(msgHandle != NULL)
    {
        if(mqosConfig_MSG_NONE != msgHandle->msg.msgId)
        {
            msgHandle->msg.msgId = mqosConfig_MSG_NONE;  /* mark memory block as free */
            msgHandle->delay = 0;   /* set delay to zero, so scheduler will not waste processor cycles decrementing it */
            mallinfo.allocCount--;  /* decrement the queued message count */
        }
    }
}


tMqosMessage_Mallinfo mqosMessage_Mallinfo(void)
{
    return mallinfo;
}


tMqosMessageContainer* mqosMessage_GetHeapPointer(void)
{
    return heap;
}


#ifdef TEST

void mqosMessage_Reset(void)
{
    /* reset all messages on the heap */
    for(tMqosConfig_MessageIndex i = 0; i < MQOS_CONFIG_HEAP_SIZE; i++)
    {
        heap[i] = emptyMessageContainer;
    }

    /* reset message statistics */
    mallinfo = emptyMallinfo;
}

#endif  /* #ifdef TEST */
