/*
 * mqosMessage.h - mqosMessage module header file.
 *
 * The mqosMessage module implements functions to dynamically allocate and free
 * blocks of memory that store MQOS messages. The message allocation heap is
 * a static tMqosMessage array, whose size is defined in mqosConfig.h.
 *
 * Copyright (C) 2021 Antony Bayley.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef MQOSMESSAGE_H
#define	MQOSMESSAGE_H

#ifdef __cplusplus
extern "C" {
#endif

#include "mqosConfig.h"     /* application-specific MQOS configuration */

/* MACROS ********************************************************************/


/* TYPE DEFINITIONS **********************************************************/

/*! MQOS message class */
typedef struct
{
    tMqosConfig_MessageId msgId;            //!< Message ID
#if MQOS_CONFIG_MSG_CONTAINS_PAYLOAD
    tMqosConfig_MessagePayload payload;     //!< Message payload
#endif  /* #if MQOS_CONFIG_MSG_CONTAINS_PAYLOAD */
} tMqosMessage;


/*! MQOS message container class 
 *
 *  The MQOS message container consists of the message along with additional
 *  data used for message queuing and delays.
 */
typedef struct _tMqosMessageContainer
{
    tMqosMessage msg;                       //!< MQOS Message
    tMqosConfig_TimerCount delay;           //!< Delay before message is ready for delivery
#if MQOS_CONFIG_MSG_SCHEDULER_FIFO_ORDER
    struct _tMqosMessageContainer* nextMsg; //!< Next message in the queue
#endif  /* #if MQOS_CONFIG_MSG_SCHEDULER_FIFO_ORDER */
} tMqosMessageContainer;


/*! MQOS message allocation information class
 *
 *  This type stores statistics that are useful to debug memory allocation.
 *  To verify that you have configured an adequately large heap you can check
 *  that, after a period of normal use, mallinfo.allocFaults == 0 and
 *  mallinfo.maxAllocCount < MQOS_CONFIG_HEAP_SIZE.
 */
typedef struct
{
    tMqosConfig_MessageIndex allocCount;    //!< number of currently allocated messages.
    tMqosConfig_MessageIndex maxAllocCount; //!< greatest number of allocated messages since boot-up.
    tMqosConfig_MessageIndex allocFaults;   //!< number of message allocation faults since boot-up.
} tMqosMessage_Mallinfo;


/* PUBLIC FUNCTION DECLARATIONS **********************************************/

/*!
 *  Allocate memory for one MQOS message container from the heap.
 *
 *  If successful the message container pointer is returned. If there is no
 *  free memory on the message scheduler's heap, NULL is returned.
 *
 *  IMPORTANT: mqosMessage_Malloc() may only be called from within a critical
 *             code block: i.e. between the macros mqosPort_ENTER_CRITICAL()
 *             and mqosPort_EXIT_CRITICAL().
 *
 *  @return             Message container pointer
 */
tMqosMessageContainer* mqosMessage_Malloc(void);


/*!
 *  Return MQOS message memory to the heap.
 *
 *  To return an MQOS message to the heap and make its memory available for
 *  re-use, the message's msgId field is set to mqosConfig_MSG_NONE.
 *
 *  IMPORTANT: mqosMessage_Free() may only be called from within a critical code
 *             block: i.e. between the macros mqosPort_ENTER_CRITICAL() and
 *             mqosPort_EXIT_CRITICAL().
 *
 *  @param msgHandle    Message container pointer.
 */
void mqosMessage_Free(tMqosMessageContainer* msgHandle);


/*!
 *  Return message allocation information.
 *
 *  The message allocation information struct contains data that are useful to
 *  verify that the optimum heap size has been defined, or debugging message
 *  allocation faults.
 *
 *  @return             Message allocation information
 */
tMqosMessage_Mallinfo mqosMessage_Mallinfo(void);


/*!
 *  Get a pointer to the first message container in the MQOS message heap.
 *
 *  IMPORTANT: mqosMessage_GetHeapPointer() may only be called from within a
 *             critical code block: i.e. between the mqosPort_ENTER_CRITICAL()
 *             and mqosPort_EXIT_CRITICAL() macros.
 *
 *  @return             Message container pointer
 */
tMqosMessageContainer* mqosMessage_GetHeapPointer(void);


#ifdef TEST

/*!
 *  Reset the message heap and message allocation statistics to their initial
 *  power-up states: i.e. all bytes set to zero.
 *
 *  In embedded systems the C variable definitions initialise the heap and
 *  message allocation statistics at system reset, and their values are never
 *  normally reset at run-time.  This function is only intended to be used for
 *  unit testing.  It is omitted from release builds to save memory.
 */
void mqosMessage_Reset(void);

#endif  /* #ifdef TEST */


#ifdef __cplusplus
}
#endif

#endif  /* MQOSMESSAGE_H */
