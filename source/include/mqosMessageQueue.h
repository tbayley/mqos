/*
 * mqosMessageQueue.h - mqosMessageQueue module header file.
 *
 * Copyright (C) 2021 Antony Bayley.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef MQOSMESSAGEQUEUE_H
#define	MQOSMESSAGEQUEUE_H

#ifdef	__cplusplus
extern "C" {
#endif

#include "mqosConfig.h"  /* application-specific MQOS configuration */
#include "mqosMessage.h"


/* MACROS ********************************************************************/


/* TYPE DEFINITIONS **********************************************************/
    
typedef struct {
    tMqosMessageContainer* front;   //!< Front of the queue: the next message to be delivered
    tMqosMessageContainer* back;    //!< Back of the queue: the last message added
} tMqosMessageQueue;


/* PUBLIC FUNCTION DECLARATIONS **********************************************/

#if MQOS_CONFIG_MSG_SCHEDULER_FIFO_ORDER

/*! Send a message to the back of the queue.
 * 
 *  IMPORTANT: mqosMessageQueue_SendToBack() may only be called from within a
 *             critical code block: i.e. between the mqosPort_ENTER_CRITICAL()
 *             and mqosPort_EXIT_CRITICAL() macros.
 *
 *  @param  queue       Queue to which the message will be appended.
 *  @param  msgHandle   Message container to be appended.
 */
void mqosMessageQueue_SendToBack(tMqosMessageQueue* queue, tMqosMessageContainer* msgHandle);


/*! Receive the message that is at the front of the queue.
 *
 *  Remove the message that is at the front of the queue and return its message
 *  pointer. If the queue is empty return NULL. It is the responsibility of the
 *  caller to free the message if it is no longer required.
 * 
 *  IMPORTANT: mqosMessageQueue_ReceiveFromFront() may only be called from
 *             within a critical code block: i.e. between the macros
 *             mqosPort_ENTER_CRITICAL() and mqosPort_EXIT_CRITICAL().
 *
 *  @param  queue   Queue from which to receive the message.
 *
 *  @return         Message received from the front of the queue, or NULL
 *                  if the queue is empty.
 */
tMqosMessageContainer* mqosMessageQueue_ReceiveFromFront(tMqosMessageQueue* queue);


#endif  /* #if MQOS_CONFIG_MSG_SCHEDULER_FIFO_ORDER */

#ifdef	__cplusplus
}
#endif

#endif	/* MQOSMESSAGEQUEUE_H */
