/*
 * mqos.h - mqos module header file.
 *
 * Copyright (C) 2021 Antony Bayley.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef MQOS_H
#define	MQOS_H

#ifdef __cplusplus
extern "C" {
#endif

#include "mqosConfig.h"     /* application-specific MQOS configuration */
#include "mqosPort.h"       /* MQOS port for specific compiler and device */
#include "mqosMessage.h"

/* CONSTANTS *****************************************************************/


/* TYPE DEFINITIONS **********************************************************/


/* MACROS ********************************************************************/

/*!
 *  Convert time in microseconds to MQOS system clock ticks.
 *
 *  @param  uSec    time in microseconds
 *
 *  @return         MQOS system clock ticks
 */
#define mqos_uSecToTicks(uSec) (uSec / MQOS_CONFIG_TICK_PERIOD_US)


/*!
 *  Convert time in milliseconds to MQOS system clock ticks.
 *
 *  @param  mSec    time in milliseconds
 *
 *  @return         MQOS system clock ticks
 */
#define mqos_mSecToTicks(mSec) ((tMqosConfig_TimerCount) ((mSec * 1000UL) / MQOS_CONFIG_TICK_PERIOD_US))


/*!
 *  Convert time in seconds to MQOS system clock ticks.
 *
 *  @param  sec     time in seconds
 *
 *  @return         MQOS system clock ticks
 */
#define mqos_secToTicks(sec) ((tMqosConfig_TimerCount) ((sec * 1000000UL) / MQOS_CONFIG_TICK_PERIOD_US))


/*!
 *  Convert MQOS system clock ticks to time in microseconds.
 *
 *  @param  ticks   MQOS system clock ticks
 *
 *  @return         time in microseconds
 */
#define mqos_TicksToMicrosec(ticks) (ticks * MQOS_CONFIG_TICK_PERIOD_US)


/*!
 *  Convert MQOS system clock ticks to time in milliseconds.
 *
 *  @param  ticks   MQOS system clock ticks
 *
 *  @return         time in milliseconds
 */
#define mqos_TicksToMillisec(ticks) ((ticks * MQOS_CONFIG_TICK_PERIOD_US) / 1000UL)


/*!
 *  Convert MQOS system clock ticks to time in seconds.
 *
 *  @param  ticks   MQOS system clock ticks
 *
 *  @return         time in seconds
 */
#define mqos_TicksToSec(ticks) ((ticks * MQOS_CONFIG_TICK_PERIOD_US) / 1000000UL)


#if MQOS_CONFIG_MSG_CONTAINS_PAYLOAD

/*!
 *  Post a message to the ready message queue, for immediate delivery.
 *
 *  @param  msgId           Message ID
 *  @param  payload         Message payload
 *
 *  @return                 Message container handle if the message was successfully posted.
 *                          NULL if a new message container could not be allocated from the message heap.
 */
#define mqos_MessageSend(msgId, payload) \
        mqos_MessageSendLater(msgId, payload, 0)

#else   /* #if MQOS_CONFIG_MSG_CONTAINS_PAYLOAD */

/*!
 *  Post a message to the ready message queue, for immediate delivery.
 *
 *  @param  msgId           Message ID
 *
 *  @return                 Message container handle if the message was successfully posted.
 *                          NULL if a new message container could not be allocated from the message heap.
 */
#define mqos_MessageSend(msgId) \
        mqos_MessageSendLater(msgId, 0)

#endif  /* #if MQOS_CONFIG_MSG_CONTAINS_PAYLOAD */


/*!
 * Null message payload macro.
 */
#define MQOS_NULL_MESSAGE_PAYLOAD    ((tMqosConfig_MessagePayload) {0})


/* PUBLIC FUNCTION DECLARATIONS **********************************************/

#if MQOS_CONFIG_MSG_CONTAINS_PAYLOAD

/*!
 *  Post a message to the delayed message queue, for delivery later.
 *
 *  The message is delivered when the specified number of system timer ticks
 *  has elapsed.
 *
 *  @param  msgId           Message ID
 *  @param  payload         Message payload
 *  @param  delay           Delay time, in system timer ticks
 *
 *  @return                 Message container handle if the message was successfully posted.
 *                          NULL if a new message container could not be allocated from the message heap.
 */
tMqosMessageContainer* mqos_MessageSendLater(tMqosConfig_MessageId msgId, tMqosConfig_MessagePayload payload, tMqosConfig_TimerCount delay);

#else   /* #if MQOS_CONFIG_MSG_CONTAINS_PAYLOAD */

/*!
 *  Post a message to the delayed message queue, for delivery later.
 *
 *  The message is delivered when the specified number of system timer ticks
 *  has elapsed.
 *
 *  @param  msgId           Message ID
 *  @param  delay           Delay time, in system timer ticks
 *
 *  @return                 Message container handle if the message was successfully posted.
 *                          NULL if a new message container could not be allocated from the message heap.
 */
tMqosMessageContainer* mqos_MessageSendLater(tMqosConfig_MessageId msgId, tMqosConfig_TimerCount delay);

#endif  /* #if MQOS_CONFIG_MSG_CONTAINS_PAYLOAD */


/*!
 *  Cancel all messages that have the specified message ID.
 *
 *  The cancelled messages are removed from the delayed message queue and their
 *  memory blocks are returned to the scheduler's message heap.
 *
 *  @param  msgId           Message ID
 */
void mqos_MessageCancelAll(tMqosConfig_MessageId msgId);


/*!
 *  Cancel a specified message.
 *
 *  The message with the specified message container handle is removed from the
 *  delayed message queue and its memory block is returned to the scheduler's
 *  message heap.  Messages that are already on the ready queue cannot be
 *  cancelled.
 *
 *  @param  msgHandle           Message container handle
 */
void mqos_MessageCancel(tMqosMessageContainer* msgHandle);


/*!
 *  Retrieve the first message from the ready message queue.
 *
 *  Deliver the message at the head of the ready message queue and return its
 *  allocated memory to the scheduler's message heap. If no message is ready,
 *  return a message with message ID set to mqosConfig_MSG_NONE.
 *
 *  @return                 Message.
 */
tMqosMessage mqos_MessageReceive(void);


/*!
 *  MQOS system timer tick function.
 *
 * The host system must call this function periodically, at the time period
 * defined by MQOS_CONFIG_TICK_PERIOD_US.
 *
 * The function decrements the delay value of every message in the delayed
 * message queue. When a message's delay value reaches zero, the message is
 * transferred to the ready message queue.
 */
void mqos_Tick(void);


/*!
*  Read the MQOS system timer tick count.
*
*  Return the cumulative number of system ticks since system reset. The system
*  tick period is set by the value MQOS_CONFIG_TICK_PERIOD_US that is defined
*  in the file mqosConfig.h.
*
*  @return      System tick count.
*/
tMqosConfig_TimerCount mqos_getSystemTickCount(void);

#ifdef TEST

/*!
 *  Reset the ready queue, message heap and message allocation statistics to
 *  their initial power-up states: i.e. all bytes set to zero or NULL.
 *
 *  In embedded systems the C variable definitions initialise the ready queue,
 *  heap and message allocation statistics at system reset, and their values
 *  are never normally reset at run-time.  This function is only intended to be
 *  used for unit testing.  It is omitted from release builds to save memory.
 */
void mqos_Reset(void);

#endif  /* #ifdef TEST */


#ifdef __cplusplus
}
#endif

#endif  /* MQOS_H */
