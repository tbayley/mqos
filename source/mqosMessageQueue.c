/*
 * mqosMessageQueue.c - mqosMessageQueue module source file.
 *
 * Copyright (C) 2021 Antony Bayley.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <stdlib.h>
#include "mqosMessageQueue.h"

/* PRIVATE VARIABLE DEFINITIONS **********************************************/


/* PUBLIC FUNCTION DEFINITIONS ***********************************************/

#if MQOS_CONFIG_MSG_SCHEDULER_FIFO_ORDER

void mqosMessageQueue_SendToBack(tMqosMessageQueue* queue, tMqosMessageContainer* msgHandle)
{
    /* run-time asserts: check that pointers are not NULL */
    MQOS_CONFIG_ASSERT(NULL != queue);
    MQOS_CONFIG_ASSERT(NULL != msgHandle);

    msgHandle->nextMsg = NULL;

    if(NULL == queue->back)
    {
        /* queue is empty: new message is both back and front of the queue */
        queue->back = msgHandle;
        queue->front = msgHandle;
    }
    else
    {
        /* queue is not empty: add new message to back of queue */
        queue->back->nextMsg = msgHandle;
        queue->back = msgHandle;
    }
}


tMqosMessageContainer* mqosMessageQueue_ReceiveFromFront(tMqosMessageQueue* queue)
{
    /* run-time assert: check that pointer is not NULL */
    MQOS_CONFIG_ASSERT(NULL != queue);

    tMqosMessageContainer* msgHandle = queue->front;
    if(NULL != msgHandle)
    {
        queue->front = msgHandle->nextMsg;
        if(NULL == queue->front)
        {
            /* queue is empty */
            queue->back = NULL;
        }
    }
    return msgHandle;
}

#endif  /* #if MQOS_CONFIG_MSG_SCHEDULER_FIFO_ORDER */
