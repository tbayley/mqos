/*
 * mqosPort.c - MQOS port source file for Linux (GCC)
 *
 * This port enables MQOS applications to be built on Linux using GCC compiler.
 *
 * MQOS is intended for use on resource-constrained embedded systems. The
 * Linux port enables developers that do not have access to an embedded
 * microprocessor development board to evaluate MQOS. It is not envisaged that
 * MQOS will be used to develop Linux applications!
 *
 * Copyright (C) 2021 Antony Bayley.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "mqosPort.h"
#include "mqos.h"
#include <stdlib.h>
#include <stdbool.h>
#include <stdio.h>

#include <signal.h>
#include <time.h>
#include <pthread.h>


/* PRIVATE VARIABLE DEFINITIONS **********************************************/

static timer_t linuxTimer;
static struct itimerspec ts;
static struct sigevent sev;


/* PUBLIC VARIABLE DEFINITIONS ***********************************************/

pthread_mutex_t mqosPort_CsMutex = PTHREAD_MUTEX_INITIALIZER;


/* PRIVATE FUNCTION DECLARATIONS *********************************************/

static void mqosPort_LinuxTimerCallback(union sigval sv);


/* PRIVATE FUNCTION DEFINITIONS **********************************************/

/*!
 *  Callback function for Linux timer.
 *
 *  @param  sv      Signal value.
 */ 
static void mqosPort_LinuxTimerCallback(union sigval sv)
{
    /* signal value parameter is not used */
    (void)sv;

    /* Thread-safe call to the MQOS system tick function.
     * Critical section management is handled inside the mqos_Tick() function.
     */
    mqos_Tick();
}


/* PUBLIC FUNCTION DEFINITIONS ***********************************************/

void mqosPort_Init(void)
{
    /* set the timer period from the value defined in mqosConfig.h */
    double period = MQOS_CONFIG_TICK_PERIOD_US * USEC_TO_SEC;
    ts.it_value.tv_sec = (time_t) period;
    ts.it_value.tv_nsec = (period - (time_t) period) * SEC_TO_NSEC;
    ts.it_interval.tv_sec = (time_t) period;
    ts.it_interval.tv_nsec = (period - (time_t) period) * SEC_TO_NSEC;

    /* Create a periodic Linux timer */
    sev.sigev_notify = SIGEV_THREAD;                           /* notify via thread */
    sev.sigev_notify_function = mqosPort_LinuxTimerCallback;   /* thread start function */
    sev.sigev_notify_attributes = NULL;
    if(timer_create(CLOCK_REALTIME, &sev, &linuxTimer) == -1)
    {
        printf("timer_create() failed\n");
        exit(1);
    }
    if(timer_settime(&linuxTimer, 0, &ts, NULL) == -1)
    {
        printf("timer_settime() failed\n");
        exit(1);
    }
}


void mqosPort_Teardown(void)
{
    /* disarm and delete the Linux timer */
    if(timer_delete(linuxTimer) == -1)
    {
        printf("timer_delete() failed\n");
        exit(1);
    }
    
    /* Destroy the critical section mutex */
    pthread_mutex_destroy(&mqosPort_CsMutex);
}

