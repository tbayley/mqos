/*
 * mqosPort.h - MQOS port header file for Linux (GCC)
 *
 * This port enables MQOS applications to be built on Linux using GCC compiler.
 *
 * MQOS is intended for use on resource-constrained embedded systems. The
 * Linux port enables developers that do not have access to an embedded
 * microprocessor development board to evaluate MQOS. It is not envisaged that
 * MQOS will be used to develop Linux applications!
 *
 * Copyright (C) 2021 Antony Bayley.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef MQOSPORT_H
#define	MQOSPORT_H

#ifdef	__cplusplus
extern "C" {
#endif

#include <pthread.h>


/** PUBLIC MACRO DEFINITIONS *************************************************/

/*! microseconds to seconds conversion factor */
#define USEC_TO_SEC 0.000001

/*! seconds to nanoseconds conversion factor */
#define SEC_TO_NSEC 1000000000L


/* Critical section management macros */

/*! Enter critical section macro */
#define mqosPort_ENTER_CRITICAL()   pthread_mutex_lock(&mqosPort_CsMutex)


 /*! Exit critical section macro */
#define mqosPort_EXIT_CRITICAL()    pthread_mutex_unlock(&mqosPort_CsMutex)


/* PUBLIC FUNCTION DECLARATIONS **********************************************/

/*!
 *  Perform platform-specific initialisation.
 *
 *  Perform any platform-specific system initialisation that is needed before
 *  MQOS can start, such as configuring the system tick timer.
 */
void mqosPort_Init(void);


/*!
 *  Perform platform-specific tear down.
 *
 *  Free any system resources that are no longer required after MQOS stops
 *  running.
 */
void mqosPort_Teardown(void);


/** PUBLIC VARIABLE DECLARATIONS *********************************************/

/*! mutex that controls access to critical code sections. */
extern pthread_mutex_t mqosPort_CsMutex;

/*! condition variable that signals timer callback thread invocation */
//extern pthread_cond_t mqosPort_LinuxTimerCondition;


#ifdef	__cplusplus
}
#endif

#endif	/* MQOSPORT_H */
