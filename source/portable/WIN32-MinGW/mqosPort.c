/*
 * mqosPort.c - MQOS port source file for Windows (MinGW)
 *
 * This port enables MQOS applications to be built using MinGW compiler.
 *
 * MQOS is intended for use on resource-constrained embedded systems. The
 * Windows port enables developers that do not have access to an embedded
 * microprocessor development board to evaluate MQOS. It is not envisaged that
 * MQOS will be used to develop Windows applications!
 *
 * Copyright (C) 2021 Antony Bayley.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "mqosPort.h"
#include "mqos.h"
#include <stdlib.h>
#include <stdbool.h>
#include <stdio.h>


/* PRIVATE VARIABLE DEFINITIONS **********************************************/

static HANDLE hTimer = NULL;
static HANDLE hTimerQueue = NULL;


/* PRIVATE FUNCTION DECLARATIONS *********************************************/

static VOID CALLBACK mqosPort_WindowsTimerCallback(PVOID lpParam, BOOLEAN TimerOrWaitFired);


/* PRIVATE FUNCTION DEFINITIONS **********************************************/

/*!
 *  Callback function for Windows timer.
 *
 *  @param  lpParam            The thread data passed to the function using a
 *                             parameter of the CreateTimerQueueTimer function.
 *
 *  @param  TimerOrWaitFired   This parameter is always TRUE for timer
 *                             callbacks.
 */
static VOID CALLBACK mqosPort_WindowsTimerCallback(PVOID lpParam, BOOLEAN TimerOrWaitFired)
{
    /* Call the MQOS system tick function */
    mqos_Tick();
}


/* PUBLIC VARIABLE DEFINITIONS ***********************************************/

/* variable used to control access to critical code sections. */
CRITICAL_SECTION CriticalSection;


/* PUBLIC FUNCTION DEFINITIONS ***********************************************/

void mqosPort_Init(void)
{
    // Initialize the critical section one time only.
    (void)InitializeCriticalSectionAndSpinCount(&CriticalSection, 0x00000400);

    // Create the timer queue
    hTimerQueue = CreateTimerQueue();
    if (NULL == hTimerQueue)
    {
        /* print error message and close the application */
        printf("CreateTimerQueue() failed: error = %d\n", GetLastError());
        exit(1);
    }

    // Set a timer to generate a tick event at the specified interval
    DWORD tickPeriod = MQOS_CONFIG_TICK_PERIOD_US / 1000;
    bool timerCreated = CreateTimerQueueTimer(
        &hTimer,        // handle
        hTimerQueue,    // timer queue
        (WAITORTIMERCALLBACK)mqosPort_WindowsTimerCallback,  // callback
        NULL,           // parameter pointer
        tickPeriod,     // due time
        tickPeriod,     // period
        0);             // flags
    if(!timerCreated)
    {
        /* print error message and close the application */
        printf("CreateTimerQueueTimer failed: error = %d\n", GetLastError());
        exit(1);
    }
}


void mqosPort_Teardown(void)
{
    // Delete all timers in the timer queue.
    if (!DeleteTimerQueue(hTimerQueue))
        printf("DeleteTimerQueue failed (%d)\n", GetLastError());

    // Release resources used by the critical section object.
    DeleteCriticalSection(&CriticalSection);
}
