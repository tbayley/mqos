/*
 * mqosPort.h - MQOS port header file for Windows (Microsoft Visual Studio)
 *
 * This port enables MQOS applications to be built using Microsoft Visual
 * Studio 2017 or later.
 *
 * MQOS is intended for use on resource-constrained embedded systems. The
 * Windows port enables developers that do not have access to an embedded
 * microprocessor development board to evaluate MQOS. It is not envisaged that
 * MQOS will be used to develop Windows applications!
 *
 * Copyright (C) 2021 Antony Bayley.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef MQOSPORT_H
#define	MQOSPORT_H

#ifdef	__cplusplus
extern "C" {
#endif


#include <Windows.h>

/* MACROS ********************************************************************/

/* Critical section management. */


/*! Enter critical section macro.
 *
 *  See Windows documentation "Using Critical Section Objects":
 *  https://docs.microsoft.com/en-gb/windows/desktop/Sync/using-critical-section-objects
 */
#define mqosPort_ENTER_CRITICAL()   EnterCriticalSection(&CriticalSection)


 /*! Exit critical section macro.
  *
  *  See Windows documentation "Using Critical Section Objects":
  *  https://docs.microsoft.com/en-gb/windows/desktop/Sync/using-critical-section-objects
  */
#define mqosPort_EXIT_CRITICAL()    LeaveCriticalSection(&CriticalSection)


/* PUBLIC FUNCTION DECLARATIONS **********************************************/

/*!
 *  Perform platform-specific initialisation.
 *
 *  Perform any platform-specific system initialisation that is needed before
 *  MQOS can start, such as configuring the system tick timer.
 */
void mqosPort_Init(void);


/*!
 *  Perform platform-specific tear down.
 *
 *  Free any system resources that are no longer required after MQOS stops
 *  running.
 */
void mqosPort_Teardown(void);


/** PUBLIC VARIABLE DECLARATIONS *********************************************/

/* Declare the variable used to control access to critical code sections. */
extern CRITICAL_SECTION CriticalSection;


#ifdef	__cplusplus
}
#endif

#endif	/* MQOSPORT_H */
