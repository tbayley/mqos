/*
 * mqosPort.h - MQOS port for XC8 compiler and PIC18F devices
 *
 * The PIC18 core uses a 16-bit-wide instruction set and 31 level hardware
 * stack.  The architecture is optimised for compiled C code applications.
 *
 * Copyright (C) 2021 Antony Bayley.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef MQOSPORT_H
#define MQOSPORT_H

#ifdef __cplusplus
extern "C" {
#endif

#include <xc.h>     // Microchip PIC device-specific headers
#include <stdint.h>


#pragma warning disable 520     /* ignore warning: (520) function "_foo" is never called */


/* MACROS ********************************************************************/

/*!
 *  Interrupt control register.
 *
 *  The GIE/GIEH and GIEL high and low priority interrupt enable bits are in the
 *  INTCON register in some PIC18F family devices, but are in the INTCON0
 *  register in other PIC18F devices.
 *
 *  This macro enables the critical section macros to work with both classes of
 *  PIC18F device. The GIE/GIEH and GIEL bits are at the same bit position in
 *  both INTCON and INTCON0 registers.
 */
#ifdef INTCON0
#define INTCON INTCON0
#define INTCONbits INTCON0bits
#endif


/*! Bit mask for high-priority global interrupt GIE/GIEH bit in INTCON0 register */
#define GIEH_BIT_MASK 0x80


/*! Bit mask for low priority global interrupt GIEL bit in INTCON0 register */
#define GIEL_BIT_MASK 0x40


/*! Enter critical section macro.
 *
 *  Save the high and low priority global interrupt enable state (GIEH and GIEL)
 *  on entry to the critical section, to enable the initial state to be restored
 *  when exiting the critical section. This allows the critical section
 *  management macros to be used safely in normal program code and in interrupt
 *  handlers.
 *
 *  Critical section macros cannot be nested. After mqosPort_ENTER_CRITICAL()
 *  has been called, mqosPort_EXIT_CRITICAL() must be called before calling
 *  mqosPort_ENTER_CRITICAL() again. If you nest critical section macros,
 *  you may inadvertently change the Global Interrupt Enable (GIEH and/or GIEL)
 *  state.
 */
#define mqosPort_ENTER_CRITICAL()                   \
        uint8_t localCachedINTCON = INTCON;         \
        if(INTCONbits.GIEH) {INTCONbits.GIEH = 0;}  \
        if(INTCONbits.GIEL) {INTCONbits.GIEL = 0;}


/*! Exit critical section macro.
 *
 *  Only enable interrupts if they were enabled when entering the critical
 *  section.
 *
 *  When mqosPort_EXIT_CRITICAL() is called within the application context, it
 *  sets the INTCON0bits.GIEH bit and/or the INTCON0bits.GIEL bit to re-enable
 *  high and low priority global interrupts respectively.  However when
 *  mqosPort_EXIT_CRITICAL() is called within an interrupt handler function it
 *  must not set the global interrupt enable bits. That is because when an
 *  interrupt handler function returns the RETFIE instruction (return from
 *  interrupt) is called, which automatically sets the INTCON0bits.GIEH bit or
 *  the INTCON0bits.GIEL bit to re-enable interrupts. Re-enabling interrupts
 *  before the interrupt handler has exited will cause corruption of the call
 *  stack.
 *
 *  Critical section macros cannot be nested. After mqosPort_ENTER_CRITICAL()
 *  has been called, mqosPort_EXIT_CRITICAL() must be called before calling
 *  mqosPort_ENTER_CRITICAL() again. If you nest critical section macros,
 *  you may inadvertently change the Global Interrupt Enable (GIEH and/or GIEL)
 *  state.
 */
#define mqosPort_EXIT_CRITICAL()                                          \
        do{                                                               \
            if(localCachedINTCON & GIEH_BIT_MASK) {INTCONbits.GIEH = 1;}  \
            if(localCachedINTCON & GIEL_BIT_MASK) {INTCONbits.GIEL = 1;}  \
        } while(0)


#ifdef __cplusplus
}
#endif

#endif  /* MQOSPORT_H */
