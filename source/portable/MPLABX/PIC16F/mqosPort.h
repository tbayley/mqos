/*
 * mqosPort.h - MQOS port for XC8 compiler and PIC16F mid-range devices
 *
 * The mid-range core uses a 14-bit-wide instruction set that includes more
 * instructions than the baseline core. It has larger data memory banks and
 * program memory pages, as well. It is available in PIC12, PIC14 and PIC16 part
 * numbers.
 *
 * The Enhanced mid-range core also uses a 14-bit-wide instruction set but
 * incorporates additional instructions and features. There are both PIC12 and
 * PIC16 part numbers that are based on the Enhanced mid-range core.
 *
 * Copyright (C) 2021 Antony Bayley.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef MQOSPORT_H
#define	MQOSPORT_H

#ifdef __cplusplus
extern "C" {
#endif

#include <xc.h>     // Microchip PIC device-specific headers
#include <stdint.h>


#pragma warning disable 520     /* ignore warning: (520) function "_foo" is never called */


/* MACROS ********************************************************************/

/*! Bit mask for global interrupt enable bit GIE in INTCON register */
#define GIE_BIT_MASK 0x80


/*! Enter critical section macro.
 *
 *  Save the GIE state on entry to the critical section to enable the initial
 *  state to be restored when exiting the critical section. This allows the
 *  critical section management macros to be used safely in normal program code
 *  and in interrupt handlers.
 *
 *  Critical section macros cannot be nested. After mqosPort_ENTER_CRITICAL()
 *  has been called, mqosPort_EXIT_CRITICAL() must be called before calling
 *  mqosPort_ENTER_CRITICAL() again. If you nest critical section macros,
 *  you may inadvertently change the Global Interrupt Enable (GIE) state.
 */
#define mqosPort_ENTER_CRITICAL()                   \
        uint8_t localCachedINTCON = INTCON;         \
        if(INTCONbits.GIE) {INTCONbits.GIE = 0;}    \


/*! Exit critical section macro.
 *
 *  Only enable interrupts if they were enabled when entering the critical
 *  section.
 *
 *  When mqosPort_EXIT_CRITICAL() is called within the application context, it
 *  sets the INTCONbits.GIE bit to re-enable interrupts.  However when
 *  mqosPort_EXIT_CRITICAL() is called within an interrupt handler function it
 *  must not set INTCONbits.GIE. That is because when an interrupt handler
 *  function returns the RETFIE instruction (return from interrupt) is called,
 *  which automatically sets the INTCONbits.GIE bit to re-enable interrupts.
 *  Re-enabling interrupts before the interrupt handler has exited will cause
 *  corruption of the call stack.
 *
 *  Critical section macros cannot be nested. After mqosPort_ENTER_CRITICAL()
 *  has been called, mqosPort_EXIT_CRITICAL() must be called before calling
 *  mqosPort_ENTER_CRITICAL() again. If you nest critical section macros,
 *  you may inadvertently change the Global Interrupt Enable (GIE) state.
 */
#define mqosPort_EXIT_CRITICAL()                                        \
        do{                                                             \
            if(localCachedINTCON & GIE_BIT_MASK) {INTCONbits.GIE = 1;}  \
        } while(0)


#ifdef __cplusplus
}
#endif

#endif  /* MQOSPORT_H */
