/*
 * mqosPort.h - MQOS port for XC16 compiler and PIC24F / dsPIC devices
 *
 * The PIC24 / dsPIC core is a 16-bit CPU that uses a 24-bit-wide instruction
 * set and software stack in RAM. The architecture is optimised for compiled C
 * code applications.
 *
 * Copyright (C) 2021 Antony Bayley.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef MQOSPORT_H
#define	MQOSPORT_H

#ifdef	__cplusplus
extern "C" {
#endif

#include <xc.h>     // Microchip PIC device-specific headers
#include <stdint.h>


/* MACROS ********************************************************************/

/*! Maximum permissible interrupt disable period is 16383 instruction cycles */
#define MAX_INTERRUPT_DISABLE_PERIOD    0x3FFF


/*! Enter critical section macro.
 *
 *  Critical section management for PIC24 / dsPIC uses the DISI instruction,
 *  which disables interrupts for a specified number of cycles. Critical
 *  sections macros can safely be used both in the main application code and in
 *  interrupt service routines.
 * 
 *  Call DISI instruction with the maximum period of 16383 instruction cycles.
 *
 *  Critical section macros cannot be nested. After mqosPort_ENTER_CRITICAL()
 *  has been called, mqosPort_EXIT_CRITICAL() must be called before calling
 *  mqosPort_ENTER_CRITICAL() again. If you nest critical section macros,
 *  interrupts will be enabled when the innermost mqosPort_EXIT_CRITICAL() macro
 *  is called.
 */
#define mqosPort_ENTER_CRITICAL()   __builtin_disi(MAX_INTERRUPT_DISABLE_PERIOD)


/*! Exit critical section macro.
 *
 *  Critical section management for PIC24 / dsPIC uses the DISI instruction,
 *  which disables interrupts for a specified number of cycles. Critical
 *  sections macros can safely be used both in the main application code and in
 *  interrupt service routines.
 * 
 *  Call DISI instruction to set the interrupt disable period to zero
 *  instruction cycles, which re-enables any interrupts that were enabled when
 *  mqosPort_ENTER_CRITICAL() was called.
 *
 *  Critical section macros cannot be nested. After mqosPort_ENTER_CRITICAL()
 *  has been called, mqosPort_EXIT_CRITICAL() must be called before calling
 *  mqosPort_ENTER_CRITICAL() again. If you nest critical section macros,
 *  interrupts will be enabled when the innermost mqosPort_EXIT_CRITICAL() macro
 *  is called.
 */
#define mqosPort_EXIT_CRITICAL()    __builtin_disi(0)


#ifdef  __cplusplus
}
#endif

#endif  /* MQOSPORT_H */
