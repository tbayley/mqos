/*
 * mqos.c - mqos module source file.
 *
 * This file implements message scheduler functions using an unordered message
 * delivery algorithm, which uses less RAM than FIFO message delivery. Messages
 * are delivered in the order that they appear in the heap, which may not
 * correspond to the order in which the messages were posted.   
 *
 * Copyright (C) 2021 Antony Bayley.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */


#include <stdlib.h>
#include "mqos.h"
#include "mqosMessageQueue.h"


/* PRIVATE VARIABLE DEFINITIONS **********************************************/

/*! Number of system ticks since system reset */
static volatile tMqosConfig_TimerCount systemTickCount = 0;

/*! Previous system tick count, used by the message scheduler */
static tMqosConfig_TimerCount previousTickCount = 0;


#if MQOS_CONFIG_MSG_SCHEDULER_FIFO_ORDER

/*! Queue containing messages that are ready for delivery */
static tMqosMessageQueue readyMessageQueue = {NULL, NULL};

/*! Empty message queue for reset */
static const tMqosMessageQueue emptyMessageQueue = {NULL, NULL};

#endif  /* #if MQOS_CONFIG_MSG_SCHEDULER_FIFO_ORDER */

    
/* PRIVATE FUNCTION DECLARATIONS *********************************************/

static void mqos_RunScheduler(void);


/* PRIVATE FUNCTION DEFINITIONS **********************************************/

/*!
 *  Process any system ticks that have occurred since the scheduler last ran.
 * 
 *  IMPORTANT: mqos_RunScheduler() may only be called from within a critical
 *             code block: i.e. between the macros mqosPort_ENTER_CRITICAL()
 *             and mqosPort_EXIT_CRITICAL().
 * 
 */
static void mqos_RunScheduler(void)
{
    /* find how may system ticks have occurred since the scheduler last ran */
    tMqosConfig_TimerCount unprocessedSystemTicks = systemTickCount - previousTickCount;
    previousTickCount = systemTickCount;

    /* process system ticks one at a time, to ensure that delayed messages are delivered in order */
    while(0 != unprocessedSystemTicks)
    {
        mqosConfig_profilerStart();

        /* decrement the unprocessed system ticks */
        unprocessedSystemTicks--;

        /* decrement delay for each active message, if not already 0 */
        tMqosMessageContainer* msgHandle = mqosMessage_GetHeapPointer();
        for(tMqosConfig_MessageIndex i = 0; i < MQOS_CONFIG_HEAP_SIZE; i++)
        {
            if(0 != msgHandle->delay)
            {
                msgHandle->delay--;
#if MQOS_CONFIG_MSG_SCHEDULER_FIFO_ORDER
                if(0 == msgHandle->delay) 
                {
                    if(mqosConfig_MSG_NONE != msgHandle->msg.msgId)
                    {
                        /* if message is allocated, when delay times out add it to the ready queue */
                        mqosMessageQueue_SendToBack(&readyMessageQueue, msgHandle);
                    }
                }
#endif /* #if MQOS_CONFIG_MSG_SCHEDULER_FIFO_ORDER */
            }    
            msgHandle++;
        }
    }
    mqosConfig_profilerStop();
}


/* PUBLIC FUNCTION DEFINITIONS ***********************************************/

tMqosMessageContainer* mqos_MessageSendLater(tMqosConfig_MessageId msgId,
#if MQOS_CONFIG_MSG_CONTAINS_PAYLOAD
                                             tMqosConfig_MessagePayload payload,
#endif  /* #if MQOS_CONFIG_MSG_CONTAINS_PAYLOAD */
                                             tMqosConfig_TimerCount delay)
{
    mqosPort_ENTER_CRITICAL();  /* mask interrupts */

    tMqosMessageContainer* msgHandle = mqosMessage_Malloc();
    if(msgHandle != NULL)
    {
        /* create the new message */
        msgHandle->msg.msgId = msgId;
#if MQOS_CONFIG_MSG_CONTAINS_PAYLOAD
        msgHandle->msg.payload = payload;
#endif  /* #if MQOS_CONFIG_MSG_CONTAINS_PAYLOAD */
        msgHandle->delay = delay;

#if MQOS_CONFIG_MSG_SCHEDULER_FIFO_ORDER
        if(0 == delay)
        {
            mqosMessageQueue_SendToBack(&readyMessageQueue, msgHandle);
        }
#endif  /* #if MQOS_CONFIG_MSG_SCHEDULER_FIFO_ORDER */

    }

    mqosPort_EXIT_CRITICAL();  /* unmask interrupts */

    return msgHandle;
}


void mqos_MessageCancelAll(tMqosConfig_MessageId msgId)
{
    mqosPort_ENTER_CRITICAL();  /* mask interrupts */

    tMqosMessageContainer* msgHandle = mqosMessage_GetHeapPointer();
    for(tMqosConfig_MessageIndex i = 0; i < MQOS_CONFIG_HEAP_SIZE; i++)
    {
        if((msgId == msgHandle->msg.msgId) && (0 != msgHandle->delay))
        {
            mqosMessage_Free(msgHandle);
        }    
        msgHandle++;
    }

    mqosPort_EXIT_CRITICAL();  /* unmask interrupts */
}


void mqos_MessageCancel(tMqosMessageContainer* msgHandle)
{
    /* run-time assert: check that pointer is not NULL */
    MQOS_CONFIG_ASSERT(NULL != msgHandle);

    mqosPort_ENTER_CRITICAL();  /* mask interrupts */

    if(0 != msgHandle->delay)   /* message is in the delayed queue */
    {
        /* delete the message with the specified message handle */
        mqosMessage_Free(msgHandle);
    }

    mqosPort_EXIT_CRITICAL();  /* unmask interrupts */
}


void mqos_Tick(void)
{
    mqosPort_ENTER_CRITICAL();  /* mask interrupts */

    systemTickCount++;

    mqosPort_EXIT_CRITICAL();  /* unmask interrupts */
}


tMqosConfig_TimerCount mqos_getSystemTickCount(void)
{
    mqosPort_ENTER_CRITICAL();  /* mask interrupts */

    tMqosConfig_TimerCount count = systemTickCount;

    mqosPort_EXIT_CRITICAL();  /* unmask interrupts */

    return count;
}


#if MQOS_CONFIG_MSG_SCHEDULER_UNORDERED

/*----------------------------------------------------------------------------
 *  Message scheduler for unordered message delivery
 *---------------------------------------------------------------------------*/

tMqosMessage mqos_MessageReceive(void)
{
#if MQOS_CONFIG_MSG_CONTAINS_PAYLOAD
    tMqosMessage result = {mqosConfig_MSG_NONE, {0}};
#else   /* #if MQOS_CONFIG_MSG_CONTAINS_PAYLOAD */
    tMqosMessage result = {mqosConfig_MSG_NONE};
#endif  /* #if MQOS_CONFIG_MSG_CONTAINS_PAYLOAD */

    mqosPort_ENTER_CRITICAL();  /* mask interrupts */

    /* update message delay counts */
    mqos_RunScheduler();

    /* get the first message that is ready for delivery */
    tMqosMessageContainer* msgHandle = mqosMessage_GetHeapPointer();
    for(tMqosConfig_MessageIndex i = 0; i < MQOS_CONFIG_HEAP_SIZE; i++)
    {
        if (mqosConfig_MSG_NONE != msgHandle->msg.msgId)
        {
            if (0 == msgHandle->delay)
            {
                result = msgHandle->msg;  /* deliver the message */
                mqosMessage_Free(msgHandle);
                break;
            }    
        }
        msgHandle++;
    }

    mqosPort_EXIT_CRITICAL();  /* unmask interrupts */

    return result;    
}


#elif MQOS_CONFIG_MSG_SCHEDULER_FIFO_ORDER

/*----------------------------------------------------------------------------
 *  Message scheduler for FIFO order message delivery
 *---------------------------------------------------------------------------*/

tMqosMessage mqos_MessageReceive(void)
{
#if MQOS_CONFIG_MSG_CONTAINS_PAYLOAD
    tMqosMessage result = {mqosConfig_MSG_NONE, {0}};
#else   /* #if MQOS_CONFIG_MSG_CONTAINS_PAYLOAD */
    tMqosMessage result = {mqosConfig_MSG_NONE};
#endif  /* #if MQOS_CONFIG_MSG_CONTAINS_PAYLOAD */

    mqosPort_ENTER_CRITICAL();  /* mask interrupts */

    /* update message delay counts and move ready messages to the ready queue */
    mqos_RunScheduler();

    /* receive the message that is at the front of the ready message queue */
    tMqosMessageContainer* msgHandle = mqosMessageQueue_ReceiveFromFront(&readyMessageQueue);
    if(msgHandle != NULL)
    {
        result = msgHandle->msg;  /* deliver the message */
        mqosMessage_Free(msgHandle);
    }

    mqosPort_EXIT_CRITICAL();  /* unmask interrupts */

    return result;
}


#else   /* #if MQOS_CONFIG_MSG_SCHEDULER_UNORDERED */

#error Message scheduler algorithm is not defined

#endif  /* #if MQOS_CONFIG_MSG_SCHEDULER_UNORDERED */


#ifdef TEST

void mqos_Reset(void)
{
#if MQOS_CONFIG_MSG_SCHEDULER_FIFO_ORDER
    readyMessageQueue = emptyMessageQueue;
#endif  /* #if MQOS_CONFIG_MSG_SCHEDULER_FIFO_ORDER */
    mqosMessage_Reset();
    systemTickCount = 0;
    previousTickCount = 0;
}

#endif  /* #ifdef TEST */
