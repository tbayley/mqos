/*
 * mqosCliString.c - mqosCliString module source file.
 *
 * This module implements a small selection of the standard library <string.h>
 * module functions that are used by the MQOS CLI. The aim is to achieve a
 * smaller memory footprint than would be achieved using the full <string.h>
 * library, and to enable MQOS CLI to be used on systems that do not implement
 * the <string.h> standard library.
 *
 * This module differs from the standard <string.h> library in that all
 * functions check for NULL pointer parameter values, and avoid dereferencing
 * them. Of course you should never pass a NULL parameter value but, if you do
 * mess up, these string functions will not cause segmentation faults that are
 * usually hard to debug and fix in embedded systems!
 *
 * Copyright (C) 2021 Antony Bayley.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "mqosString.h"

#if(MQOS_CONFIG_STRING_LIBRARY == MQOS_CONFIG_MQOS_STRING_LIBRARY)


/* PUBLIC FUNCTION DEFINITIONS ***********************************************/

int memcmp(const void* ptr1, const void* ptr2, size_t n)
{
    /* run-time asserts: check that pointers are not NULL */
    MQOS_CONFIG_ASSERT(NULL != ptr1);
    MQOS_CONFIG_ASSERT(NULL != ptr2);

    int result = 0;

    for(size_t i = 0; i < n; i++)
    {
        if(((unsigned char*)ptr1)[i] < ((unsigned char*)ptr2)[i])
        {
            result = -1;
            break;
        }
        else if(((unsigned char*)ptr1)[i] > ((unsigned char*)ptr2)[i])
        {
            result = 1;
            break;
        }
    }

    return result;
}


void* memcpy(void* dest, const void* src, size_t n)
{
    /* run-time asserts: check that pointers are not NULL */
    MQOS_CONFIG_ASSERT(NULL != dest);
    MQOS_CONFIG_ASSERT(NULL != src);

    for(size_t i = 0; i < n; i++)
    {
        ((unsigned char*)dest)[i] = ((unsigned char*)src)[i];
    }
    return dest;
}


void* memset(void* dest, int value, size_t n)
{
    /* run-time assert: check that pointer is not NULL */
    MQOS_CONFIG_ASSERT(NULL != dest);

    for(size_t i = 0; i < n; i++)
    {
        ((unsigned char*)dest)[i] = (unsigned char)value;
    }
    return dest;
}


size_t strlen(const char* str)
{
    /* run-time assert: check that pointer is not NULL */
    MQOS_CONFIG_ASSERT(NULL != str);

    size_t len = 0;
    while ('\0' != str[len])
    {
        len += 1;
    }
    return len;
}


int strncmp(const char* str1, const char* str2, size_t n)
{
    /* run-time asserts: check that pointers are not NULL */
    MQOS_CONFIG_ASSERT(NULL != str1);
    MQOS_CONFIG_ASSERT(NULL != str2);

    int result = 0;

    for(size_t i = 0; i < n; i++)
    {
        if(str1[i] < str2[i])
        {
            result = -1;
            break;
        }
        else if(str1[i] > str2[i])
        {
            result = 1;
            break;
        }
        if('\0' == str1[i])
        {
            /* end of string character found in both strings */
            break;
        }
    }

    return result;
}


char* strncpy(char* dest, const char* src, size_t n)
{
    /* NULL pointer checks are done in strlen(), memcpy() and memset() */
    size_t copylen = strlen(src);
    if(n < copylen)
    {
        copylen = n;
    }
    memcpy(dest, src, copylen);
    memset(dest + copylen, '\0', n - copylen);
    return dest;
}

#endif  /* #if(MQOS_CONFIG_STRING_LIBRARY == MQOS_CONFIG_MQOS_STRING_LIBRARY) */
