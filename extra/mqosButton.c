/*
 * mqosButton.c - mqosButton module source file.
 *
 * The MQOS button module implements a push-button user interface by polling
 * GPIO inputs. It debounces the switch and generates, press, release, and long
 * press event messages. The preprocessor definition MQOS_BUTTON_MAX_BUTTONS
 * sets the maximum number of buttons that are supported.
 *
 * Copyright (C) 2021 Antony Bayley.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <stdlib.h>
#include "mqosButton.h"
#include "mqos.h"


/* PRIVATE MACRO DEFINITIONS *************************************************/

#ifndef MQOS_BUTTON_POLL_PERIOD
#define MQOS_BUTTON_POLL_PERIOD  5  //!< Button polling period in milliseconds.
#endif

#ifndef MQOS_BUTTON_LONG_PRESS_TIME_MS
#define MQOS_BUTTON_LONG_PRESS_TIME_MS   1000  //!< Button long-press duration in milliseconds.
#endif

#define BUTTON_LONG_PRESS_TICKS mqos_mSecToTicks(MQOS_BUTTON_LONG_PRESS_TIME_MS)


/* PRIVATE TYPE DEFINITIONS **************************************************/

/** Button object type. */
struct MqosButton
{
    tMqosGpio_PortPin portPin;                    //!< IO pin connected to button.
    volatile uint8_t rawStateBuffer : 4;          //!< previous four raw switch states, stored in 4 least-significant bits.
    bool isActiveLow : 1;                         //!< Button polarity: true if button is active low (GND when button pressed).
    volatile bool isEnabled : 1;                  //!< Button enabled status: true if button is enabled and being polled.
    volatile bool isPressed : 1;                  //!< True if the button is currently being pressed.
    volatile bool longPress : 1;                  //!< Button long press has occurred.
    volatile tMqosConfig_TimerCount pressedTime;  //!< system tick count when the button was pressed.
};


/*! Button pressed state, each time that it is polled. */
typedef enum
{
    NO_CHANGE,
    PRESSED,
    RELEASED
} tButton_StateChange;


/* PRIVATE VARIABLE DEFINITIONS **********************************************/

/*! Button instances array */
static struct MqosButton instances[MQOS_BUTTON_MAX_BUTTONS];

/*! The number of allocated button instances */
static tMqosConfig_UBaseType allocatedButtonCount = 0;


/* PRIVATE FUNCTION DECLARATIONS *********************************************/

static tMqosButton_Button getInstance(void);
static bool allButtonsAreDisabled(void);
static tButton_StateChange getButtonStateChange(tMqosButton_Button button);


/* PUBLIC FUNCTION DEFINITIONS ***********************************************/

tMqosButton_Button mqosButton_Create(const tMqosGpio_PortPin portPin, const bool activeLow)
{
    tMqosButton_Button button = getInstance();
    if(NULL != button)
    {
        button->portPin = portPin;
        button->isActiveLow = activeLow;
        button->isEnabled = false;
        button->isPressed = false;
    }

    /* Each button event message contains a pointer to the button instance that
     * generated the event. Therefore the MQOS message payload size must be big
     * enough to hold a pointer to a button instance.
     * 
     * C has no compile-time assert, so a run-time assert is used to check that
     * the message payload size is big enough.
     */
    MQOS_CONFIG_ASSERT(sizeof(tMqosConfig_MessagePayloadData) >= sizeof(void*));

    return button;
}


void mqosButton_Enable(tMqosButton_Button button)
{
    if(allButtonsAreDisabled())
    {
        /* send a message to start polling the buttons */
        (void) mqos_MessageSendLater(mqosConfig_MSG_POLL_BUTTONS, MQOS_NULL_MESSAGE_PAYLOAD, mqos_mSecToTicks(MQOS_BUTTON_POLL_PERIOD));
    }

    /* initialise the button's raw state buffer */
    if(0 == mqosGpio_Read(button->portPin))
    {
        /* all bits 0 */
        button->isPressed = button->isActiveLow ? true : false;
        button->rawStateBuffer = 0x00U;
    }
    else
    {
        /* all bits 1 */
        button->isPressed = button->isActiveLow ? false : true;
        button->rawStateBuffer = 0x0FU;
    }

    /* if button is pressed at boot time, set longPress flag to prevent
     * a BUTTON_LONG_PRESS message being sent 1 second after boot up.
     */
    button->longPress = button->isPressed;

    /* enable the button */
    button->isEnabled = true;
}


void mqosButton_Disable(tMqosButton_Button button)
{
    button->isEnabled = false;
}


void mqosButton_EnableAll(void)
{
    for(tMqosConfig_UBaseType i = 0; i < allocatedButtonCount; i++)
    {
        mqosButton_Enable(&instances[i]);
    }
}


void mqosButton_DisableAll(void)
{
    for(tMqosConfig_UBaseType i = 0; i < allocatedButtonCount; i++)
    {
        mqosButton_Disable(&instances[i]);
    }
}


void mqosButton_Poll(void)
{
    for(tMqosConfig_UBaseType i = 0; i < allocatedButtonCount; i++)
    {
        if(instances[i].isEnabled)
        {
            tButton_StateChange stateChange = getButtonStateChange(&instances[i]);
            tMqosConfig_MessagePayload payload = {(tMqosConfig_MessagePayloadData) (&instances[i])};

            if(PRESSED == stateChange)
            {
                instances[i].pressedTime = mqos_getSystemTickCount();
                instances[i].isPressed = true;
                instances[i].longPress = false;
                (void) mqos_MessageSend(mqosConfig_MSG_BUTTON_PRESSED, payload);
            }
            else if(RELEASED == stateChange)
            {
                instances[i].isPressed = false;
                (void) mqos_MessageSend(mqosConfig_MSG_BUTTON_RELEASED, payload);
            }
            else
            {
                /* no change */
            }

            if( mqosButton_IsPressed(&instances[i]) &&
                !(instances[i].longPress) &&
                (BUTTON_LONG_PRESS_TICKS <= (mqos_getSystemTickCount() - instances[i].pressedTime)) )
            {
                instances[i].longPress = true;
                (void) mqos_MessageSend(mqosConfig_MSG_BUTTON_LONG_PRESS, payload);
            }
        }
    }

    if(!allButtonsAreDisabled())
    {
        /* send a message to schedule the next button poll */
        (void) mqos_MessageSendLater(mqosConfig_MSG_POLL_BUTTONS, MQOS_NULL_MESSAGE_PAYLOAD, mqos_mSecToTicks(MQOS_BUTTON_POLL_PERIOD));
    }
}


bool mqosButton_IsPressed(tMqosButton_Button button)
{
    return button->isPressed;
}


#ifdef TEST
void mqosButton_Init(void)
{
    /* Reset the allocated button count */
    allocatedButtonCount = 0;
}
#endif


/* PRIVATE FUNCTION DEFINITIONS **********************************************/

/**
 * Get an instance of the Button class.
 *
 * @return The Button class instance.
 */
static tMqosButton_Button getInstance(void)
{
    tMqosButton_Button button = NULL;
    if(MQOS_BUTTON_MAX_BUTTONS > allocatedButtonCount)
    {
        button = &(instances[allocatedButtonCount++]);
    }
    return button;
}


/**
 * Determine if all buttons are currently disabled.
 * 
 * @return     true:  All buttons are disabled
 *             false: One button or more is enabled. 
 */
static bool allButtonsAreDisabled(void)
{
    bool allDisabled = true;
    
    for(uint8_t i = 0; i < allocatedButtonCount; i++)
    {
        if(instances[i].isEnabled)
        {
            allDisabled = false;
            break;
        }
    }
    
    return allDisabled;
}


/*! 
 * Return the debounced button-press state change
 *
 * This function is intended to be called repeatedly, with a 5ms period. The
 * value returned indicates whether the debounced button state has changed
 * since the last time the function was called.
 *
 * For an active low button, the GPIO pin value is logic 1 when the button is
 * not pressed and is logic 0 when pressed. This function returns PRESSED when
 * the state buffer contains a single 1 sample followed by 3 consecutive 0
 * samples. It returns RELEASED when the state buffer contains a single 0
 * sample followed by 3 consecutive 1 samples.
 *
 * For an active-high button, the function returns PRESSED when the state
 * buffer contains a single 0 sample followed by 3 consecutive 1 samples and
 * RELEASED when the state buffer contains a single 1 sample followed by 3
 * consecutive 1 samples.
 *
 * The requirement for three consecutive identical samples to trigger a state
 * change removes switch bounce and makes the buttons resistant to noise.
 * 
 * @return  PRESSED   : Button press has occurred
 *          RELEASED  : Button release has occurred
 *          NO_CHANGE : Button state has not changed
 */
static tButton_StateChange getButtonStateChange(tMqosButton_Button button)
{
    tButton_StateChange state = NO_CHANGE;
    
    /* Bit shift operations on struct bit-fields can cause unexpected results
     * (e.g. with Microchip XC8 Compile v2.10), so copy the raw state buffer to
     * a local uint8_t variable. This also reduces program memory usage.
     */
    uint8_t rawStateBuffer = button->rawStateBuffer;
    rawStateBuffer = ( (rawStateBuffer << 1) | ((uint8_t)mqosGpio_Read(button->portPin)) | ((uint8_t)0xF0U) );
    button->rawStateBuffer = rawStateBuffer;

    if(0xF8U == rawStateBuffer)
    {
        /* detected the bit sequence [1, 0, 0, 0] */
        state = button->isActiveLow ? PRESSED : RELEASED;
    }
    else if(0xF7U == rawStateBuffer)
    {
        /* detected the bit sequence [0, 1, 1, 1] */
        state = button->isActiveLow ? RELEASED : PRESSED;
    }
    return state;
}
