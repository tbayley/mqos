/*
 * mqosStdio.c - mqosStdio module source file.
 *
 * This module implements a small subset of the <stdio.h> standard library's
 * functions. The MQOSn printf() function has a restricted range of format
 * specifiers. It typically uses less program memory and RAM than the <stdio.h>
 * implementation, so may be a useful alternative for some applications on
 * memory-constrained embedded systems.
 *
 * The application developer must implement the function mqosPutch() that writes
 * a single character to the standard output and returns 1 if successful, or 0
 * on failure. The application developer can choose any peripheral for standard
 * output: a hardware UART, USB CDC, LCD display, etc. The MQOS printf()
 * function writes characters using mqosPutch().
 * 
 * The version of printf() implemented in the Microchip PIC <stdio.h> library
 * also relies on the application developer implementing a function to print a
 * single character to stdout: putch(). The difference is that the PIC putch()
 * function returns void, so there is no way to detect whether printing has
 * failed due to print buffer overflow. By contrast the MQOS printf() function,
 * based on mqosPutch(), correctly reports the number of characters printed so
 * that appropriate action can be taken if the print buffer overflows.
 *
 * Copyright (C) 2021 Antony Bayley.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <stdlib.h>
#include <stdint.h>
#include <limits.h>
#include <stdarg.h>
#include "mqosStdio.h"

#if(MQOS_CONFIG_STDIO_LIBRARY == MQOS_CONFIG_MQOS_STDIO_LIBRARY)


/** PRIVATE MACRO DEFINITIONS ************************************************/

#if MQOS_CONFIG_STDIO_LIBRARY_LONG_PRINTF_ENABLED

typedef unsigned long       tMqosStdio_uint;
typedef long                tMqosStdio_int;
#define XTOA_BUFFER_SIZE    21  //!< Buffer used for long integer formatting (up to int64_t)

#else   /* #if MQOS_CONFIG_STDIO_LIBRARY_LONG_PRINTF_ENABLED */

typedef unsigned int        tMqosStdio_uint;
typedef int                 tMqosStdio_int;
#define XTOA_BUFFER_SIZE    11  //!< Buffer used for long integer formatting (up to int32_t)

#endif  /* #if MQOS_CONFIG_STDIO_LIBRARY_LONG_PRINTF_ENABLED */

#define NOT_A_DIGIT                 -1      //!< Flag used when parsing the format string.

#define FLAG_NEGATIVE               (1U << 0)   //!< Negative integer value
#define FLAG_HEXADECIMAL            (1U << 1)   //!< Hexadecimal format
#define FLAG_HEXADECIMAL_UPPERCASE  (1U << 2)   //!< Hexadecimal format uses upper case A-F characters
#define FLAG_ZERO_PADDING           (1U << 3)   //!< Zero-padding is specified


/** PRIVATE TYPE DEFINITIONS *************************************************/

/*! Type for the result of unsigned integer division by 10 or 16 */
typedef struct
{
    tMqosStdio_uint q;  //!< quotient
    char r;             //!< remainder
} tMqosStdio_uDivResult;


/* PRIVATE FUNCTION DECLARATIONS *********************************************/

static int mqosPutchToStringOrStdio(char** dest, char c);
static int puts(char** dest, const char* src);
static tMqosStdio_uDivResult uDiv10(tMqosStdio_uint val);
static tMqosStdio_uDivResult uDiv16(tMqosStdio_uint val);
static int xtoa(char** dest, tMqosStdio_uint val, uint8_t flags, int fieldWidth);
static int getint(const char** str);
static int printfAndSprintfHandler(char** dest, const char *format, va_list args);


/* PRIVATE FUNCTION DEFINITIONS **********************************************/

/*!
 *  Write the character c to the specified string or standard output (stdout).
 *
 *  If the str parameter is NULL, the character is written to stdout. If str is
 *  not NULL, the character is written to str.
 *
 *  @param[out] dest    The destination string, or NULL to write to stdout.
 *  @param      c       The character to be written.
 *  @return             Return the number of characters written: 1 if successful, 0 on failure.
 */
static int mqosPutchToStringOrStdio(char** dest, char c)
{
    int n = 1;
    if(NULL == dest)
    {
        n = mqosPutch(c);
    }
    else
    {
        **dest = c;
        *dest += 1;
    }
    return n;
}


/*! Write the C string pointed by src to the C string dest or to standard output (stdout).
 *
 *  The function begins copying from the address specified (src) until it
 *  reaches the terminating null character '\0'. The terminating null-character
 *  is not copied to the destination.
 *
 *  If the dest is NULL, the src string is written to stdout. If dest in not
 *  NULL, the src string is written to dest.
 *
 *  @param[out] dest    The destination string, or NULL to write to stdout.
 *  @param[in]  str     The string to be printed.
 *  @return             The number of characters printed.
 */
static int puts(char** dest, const char* src)
{
    char c;
    int n = 0;
    while('\0' != (c = *src++))
    {
        if(0 == mqosPutchToStringOrStdio(dest, c))
        {
            break;
        }
        n += 1;
    }
    return n;
}


/*!
 *  Unsigned integer division by 10.
 *
 *  Originally, this function used the method described in Figure 10.12 of the
 *  book "Hacker's Delight, Second Edition" by Henry S. Warren Jr. Unsigned
 *  integer division by 10 was implemented by a series of right-shift (divide by
 *  power of 2) and add operations.
 *
 *  However it became apparent that it is hard to implement this algorithm in a
 *  way that compiles and works reliably on processors using different int word
 *  lengths: 8-bit, 16-bit and 32-bit. After extensive unit testing on a 32-bit
 *  desktop system, bugs became apparent when running on an 8-bit PIC 16F series
 *  processor. Because of this, the function has been modified to use the native
 *  C language division and mod operators. When building with Microchip XC-8
 *  compiler for an 8-bit PIC processor, this implementation uses an additional
 *  67 bytes of program memory and 2 bytes of RAM relative to the previous
 *  shift-and-add implementation. A price worth paying for reliable operation
 *  on all target processors!
 *
 *  @param  val     Value to be divided by 10.
 *  @return         Result of division, as quotient and remainder.
 */
static tMqosStdio_uDivResult uDiv10(tMqosStdio_uint val)
{
    tMqosStdio_uDivResult result;
    result.q = val / 10;
    result.r = (uint8_t) (val % 10);
    return result;
}


/*!
 *  Unsigned integer division by 16.
 *
 *  Unsigned integer division by a constant power of 2 is implemented by a
 *  right-shift operation. This method is suitable for processors that do not
 *  have a hardware multiplier and do not support long long data type. The
 *  calculations being performed are:
 * 
 *      result.q = val / 16;
 *      result.r = val - (16 * result.q)
 *
 *  @param  val     Value to be divided by 16.
 *  @return         Result of division, as quotient and remainder.
 */
static tMqosStdio_uDivResult uDiv16(tMqosStdio_uint val)
{
    tMqosStdio_uDivResult result;
    result.q = val >> 4U;
    result.r = (char) (val - (result.q << 4U));
    return result;
}


/*! Render the unsigned integer "val" to a decimal numeric string and write it
 *  to the specified destination string or to standard output (stdout).
 *
 *  @param[out] dest        The destination string, or NULL to write to stdout.
 *
 *  @param  val             Unsigned integer value to be printed.
 *
 *  @param  flags           Boolean flags that set print formatting options.
 *                            Bit 0: true for hexadecimal, false for decimal.
 *                            Bit 1: true for uppercase hexadecimal, false for lowercase.
 *                            Bit 2: true for zero padding, false for spaces.
 *
 *  @param  fieldWidth      Minimum number of characters to be printed. If the
 *                          value to be printed is shorter than this number, the
 *                          result is padded with blank spaces. The value is not
 *                          truncated even if the result is larger.
 *
 *  @return                 The number of characters printed.
 */
static int xtoa(char** dest, tMqosStdio_uint val, uint8_t flags, int fieldWidth)
{
    char buffer[XTOA_BUFFER_SIZE];  // temporary buffer
    int buflen = 0;                 // number of characters written to buffer
    int n = 0;                      // number of characters written to stdout

    char hexcase = (0 != (flags & FLAG_HEXADECIMAL_UPPERCASE)) ? 'A' : 'a';

    tMqosStdio_uDivResult result = {val, 0};

    do
    {
        /* print digits to a temporary character buffer in reverse order */
        if(0 != (flags & FLAG_HEXADECIMAL))
        {
            result = uDiv16(result.q);
        }
        else
        {
            result = uDiv10(result.q);
        }

        char digitChar;
        if(10 > result.r)
        {
            digitChar = '0' + result.r;
        }
        else
        {
            digitChar = hexcase + (result.r - 10);
        }
        buffer[buflen++] = digitChar;
    } while ((result.q != 0) && (XTOA_BUFFER_SIZE > buflen));

    /* add field width padding and negative sign, if required */
    int negativeSignLen = (0 == (flags & FLAG_NEGATIVE)) ? 0 : 1;
    char paddingChar = (0 == (flags & FLAG_ZERO_PADDING)) ? ' ' : '0';
    int paddingLen = fieldWidth - buflen - negativeSignLen;
    if(0 < paddingLen)
    {
        if(('0' == paddingChar) && (0 != negativeSignLen))
        {
            /* if zero-padding, write the negative sign before padding */
            n += mqosPutchToStringOrStdio(dest, '-');
        }
        for(int i = 0; i < paddingLen; i++)
        {
            if(0 == mqosPutchToStringOrStdio(dest, paddingChar))
            {
                break;
            }
            n += 1;
        }
    }
    if((0 != (flags & FLAG_NEGATIVE)) && ((0 >= paddingLen) || (' ' == paddingChar)))
    {
        /* Write negative sign now for negative integer with no padding, or
         * for negative integer with whitespace-padding.
         */
        n += mqosPutchToStringOrStdio(dest, '-');
    }

    /* print data to stdout from temporary character buffer */
    for(uint8_t i = buflen; i > 0; i--)
    {
        if(0 == mqosPutchToStringOrStdio(dest, buffer[i - 1]))
        {
            break;
        }
        n += 1;
    }

    return n;
}


/*! Return the numeric value of characters in the format string.
 *
 *  Parse decimal digit characters in the format string to read an integer
 *  value. Stop parsing on the first character that is not a decimal digit.
 *
 *  @param[in,out]  format  Pointer to the format string pointer.
 *  @return                 The parsed integer value.
 */
static int getint(const char** format)
{
    int val = 0;
    int digitVal;
    do
    {
        digitVal = NOT_A_DIGIT;
        char c = **format;
        if(('0' <= c) && ('9' >= c))
        {
            digitVal = c - '0';
            val = ((val << 3) + (val << 1)) + digitVal;  // (10 * val) + digitVal
            *format += 1;
        }
    } while(NOT_A_DIGIT != digitVal);
    return val;
}


/*!
 *  Handler function for printf and sprintf.
 *
 *  This function handles string formatting and output for both printf and
 *  sprintf functions. If the str parameter is NULL, output is written to
 *  stdout. If the str parameter is not NULL, output is written to str.
 *
 *  Specifier   Output                                              Example
 *  ---------   -------------------------------------------------   -------
 *  d or i      Signed decimal integer                             -359
 *  u           Unsigned decimal integer                            7235
 *  ld or li    Signed decimal long integer                        -1250047
 *  lu          Unsigned decimal long integer                       824126
 *  x           Unsigned hexadecimal integer, lowercase a-f         7fa
 *  X           Unsigned hexadecimal integer, uppercase A-F         F876
 *  lx          Unsigned hexadecimal long integer, lowercase a-f    7faf1230
 *  lX          Unsigned hexadecimal long integer, uppercase A-F    FACE0FF5
 *  c           Character                                           k
 *  s           String of characters	                            Hello world
 *
 *  Field width specifiers and zero-padding specifiers are supported for all
 *  integer data types, rendered in decimal or hexadecimal format. Values are
 *  always right-justified. The left-justify specifier is not supported.
 *
 *  @param[out] dest    The destination string, or NULL to write to stdout.
 *  @param      format  Format string, containing text and string specifiers.
 *  @param      args    Variable parameters: one for each format specifier.
 *
 *  @return             Number of characters printed.
 */
static int printfAndSprintfHandler(char** dest, const char *format, va_list args)
{
    char c;                 //!< character to print
    tMqosStdio_uint uVal;   //!< unsigned integer value
    tMqosStdio_int val;     //!< signed integer to print
    uint8_t flags = 0;      //!< Boolean flags that set print formatting options.
    int n = 0;              //!< Number of characters printed, or error code
    int fieldWidth;         //!< field width

    while((c = *format++))
    {
        if(c == '%')
        {
            flags = 0;  /* reset flags for each new format specifier */
            if('0' == *format)
            {
                flags |= FLAG_ZERO_PADDING;
                format += 1;
            }
            fieldWidth = getint(&format);

            switch(c = *format++)
            {
                case 's':               /* string */
                    n += puts(dest, va_arg(args, char*));
                    break;

                case 'c':               /* char */
                    n += mqosPutchToStringOrStdio(dest, va_arg(args, int));
                    break;

                case 'd':               /* signed int */
                case 'i':               /* signed int */
                    val = va_arg(args, int);
                    if(val < 0)
                    {
#if !MQOS_CONFIG_STDIO_LIBRARY_LONG_PRINTF_ENABLED
                        if(val != INT_MIN)  // negating the minimum signed integer value is undefined behaviour
#endif
                        {
                            val = - val;
                        }
                        flags |= FLAG_NEGATIVE;
                    }
                    n += xtoa(dest, (tMqosStdio_uint)val, flags, fieldWidth);
                    break;

                case 'u':               /* unsigned int */
                    uVal = va_arg(args, unsigned int);
                    n += xtoa(dest, uVal, flags, fieldWidth);
                    break;

#if MQOS_CONFIG_STDIO_LIBRARY_LONG_PRINTF_ENABLED
                case 'l':
                    c = *format++;
                    if(('d' == c) ||    /* signed long */
                       ('i' == c))
                    {
                        val = va_arg(args, long);
                        if(val < 0)
                        {
                            if(val != LONG_MIN)  // negating the minimum signed long integer value is undefined behaviour
                            {
                                val = - val;
                            }
                            flags |= FLAG_NEGATIVE;
                        }
                        n += xtoa(dest, (tMqosStdio_uint)val, flags, fieldWidth);
                    }
                    else if('u' == c)   /* unsigned long */
                    {
                        uVal = va_arg(args, unsigned long);
                        n += xtoa(dest, uVal, flags, fieldWidth);
                    }
                    else if(('x' == c) || ('X' == c))   /* hexadecimal unsigned long */
                    {
                        uVal = va_arg(args, unsigned long);
                        flags |= FLAG_HEXADECIMAL;
                        if('X' == c)
                        {
                            flags |= FLAG_HEXADECIMAL_UPPERCASE;
                        }
                        n += xtoa(dest, uVal, flags, fieldWidth);
                    }
                    break;
#endif  /* #if MQOS_CONFIG_STDIO_LIBRARY_LONG_PRINTF_ENABLED */

                case 'x':               /* hexadecimal int, lower case */
                case 'X':               /* hexadecimal int, upper case */
                    uVal = (tMqosStdio_uint) (va_arg(args, unsigned int));
                    flags |= FLAG_HEXADECIMAL;
                    if('X' == c)
                    {
                        flags |= FLAG_HEXADECIMAL_UPPERCASE;
                    }
                    n += xtoa(dest, uVal, flags, fieldWidth);
                    break;

                case '\0':
                default:
                    return n;
            }
        }
        else
        {
            n += mqosPutchToStringOrStdio(dest, c);
        }
    }
    return n;
}


/* PUBLIC FUNCTION DEFINITIONS ***********************************************/

#ifdef TEST
/* The Catch2 unit test framework uses functions from the standard <stdio.h> 
 * library in its test macros, including printf(). To test the mqosPrintf
 * module's printf() function, the function must be renamed differently to its
 * counterpart in <stdio.h>.
 */
int test_printf(const char *format, ...)
#else   /* #ifdef TEST */
int printf(const char *format, ...)
#endif  /* #ifdef TEST */
{
    va_list args;
    va_start(args, format);
    int n = printfAndSprintfHandler(NULL, format, args);
    va_end(args);
    return n;
}


#ifdef TEST
/* The Catch2 unit test framework uses functions from the standard <stdio.h> 
 * library in its test macros, including sprintf(). To test the mqosPrintf
 * module's sprintf() function, the function must be renamed differently to its
 * counterpart in <stdio.h>.
 */
int test_sprintf(char* str, const char *format, ...)
#else   /* #ifdef TEST */
int sprintf(char* str, const char *format, ...)
#endif  /* #ifdef TEST */
{
    va_list args;
    va_start(args, format);
    char* dest = str;
    int n = printfAndSprintfHandler(&dest, format, args);
    *dest = '\0';  // append a terminating null character after the content
    va_end(args);
    return n;
}

#endif  /* #if(MQOS_CONFIG_STDIO_LIBRARY == MQOS_CONFIG_MQOS_STDIO_LIBRARY) */
