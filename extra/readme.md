# MQOS extra - General purpose utility modules #

The MQOS _extra_ subdirectory contains various utility modules that provide
functionality that is commonly used in embedded systems. They are not part of
the MQOS kernel. They are optional extras that can be used if required. The
modules are described below:


## Circular Buffer ##

The files _mqosCbuffer.h_ and _mqosCbuffer.c_ implement a configurable circular
buffer.  A new circular buffer is defined using the macro _MQOS\_CBUFFER()_.
For example, the following macro statement creates a circular buffer variable
with the name _myCbuffer_ that can hold up to 80 data items of type _uint8\_t_:

```c
MQOS_BUFFER(myCbuffer, uint8_t, 80);
```


## Command Line Interface ##

The files _mqosCli.h_ and _mqosCli.c_ implement a command line interface using
a terminal that is connected to one of the processor's UART serial ports. 

The application must implement the configuration file _mqosCliConfig.h_ that
defines macros that read a single byte from the _stdin_ input stream and write
a single byte to the _stdout_ output stream.  In an embedded system the _stdin_
and _stdout_ streams are normally implemented as UART Tx and Rx functions
respectively, that communicate with a terminal emulator application running on 
a PC.

The application must also implement the array _mqosCliCommands\_CommandList_
that is declared in _mqosCli.h_ and the associated command functions.  Normally
this is done by copying and extending the file _mqosCliCommands.c_ that
implements just a single command: _help_, which displays a list of all available
commands.
