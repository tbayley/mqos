/*
 * mqosGpio.c - mqosGpio module source file.
 *
 * The MQOS GPIO module implements a platform-independent abstraction layer
 * for memory-mapped general purpose I/O ports. Peripheral drivers that are
 * implemented using the MQOS GPIO abstraction layer for pin I/O can be more
 * easily ported to a different processor than drivers that directly use the
 * GPIO driver functions supplied by the microprocessor vendor.
 *
 * Copyright (C) 2021 Antony Bayley.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <mqos.h>
#include "mqosGpio.h"


/* PUBLIC FUNCTION DEFINITIONS ***********************************************/


tMqosConfig_UBaseType mqosGpio_Read(const tMqosGpio_PortPin portPin)
{
    tMqosConfig_UBaseType bitmask = (tMqosConfig_UBaseType)(0x01 << portPin.bit);

    tMqosConfig_UBaseType pinValue = 1;
    if(0 == (*(portPin.port) & bitmask))
    {
        pinValue = 0;
    }
    return pinValue;
}


void mqosGpio_Write(const tMqosGpio_PortPin portPin, tMqosConfig_UBaseType value)
{
    tMqosConfig_UBaseType bitmask = (tMqosConfig_UBaseType)(0x01 << portPin.bit);
    if(0 == value)
    {
        /* clear the bit */
        *(portPin.port) &= ~bitmask;
    }
    else
    {
        /* set the bit */
        *(portPin.port) |= bitmask;
    }
}
