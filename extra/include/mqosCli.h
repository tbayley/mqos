/*
 * mqosCli.h - mqosCli module header file.
 *
 * Copyright (C) 2021 Antony Bayley.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef MQOSCLI_H
#define MQOSCLI_H

#ifdef __cplusplus
extern "C" {
#endif

#include "mqosConfig.h"
#include "mqosCbuffer.h"


/* CONSTANTS *****************************************************************/

/* MQOS CLI function return values */
#define MQOS_CLI_OK                     0
#define MQOS_CLI_INSUFFICIENT_SPACE     1
#define MQOS_CLI_CMD_NOT_FOUND          2
#define MQOS_CLI_INVALID_CMD            3
#define MQOS_CLI_DELIMITER_NOT_FOUND    4
#define MQOS_CLI_TOKEN_NOT_FOUND        5
#define MQOS_CLI_TOKEN_TRUNCATED        6
#define MQOS_CLI_CBUFFER_WRITE_FAILED   7
#define MQOS_CLI_EOL_NOT_FOUND          8
#define MQOS_CLI_CMD_NOT_FINISHED       9

#define MQOS_CLI_HELP_SPACING           4   //!< Minimum space between command name and help string in formatted help output


/* TYPE DEFINITIONS **********************************************************/

/*! Command function prototype.
 * 
 *  All command functions must conform to this prototype.
 * 
 *  @param[in]  inCbuffer   Pointer to input Cbuffer containing the command
 *                          line.
 *
 *  @param      startIndex  Command parser starting index, relative to
 *                          inCbuffer's read index. On entry, index points
 *                          to the first character after the command keyword
 *                          in the command line.
 *
 *  @param      eolIndex    End-of-line index, relative to inCbuffer's
 *                          read index.
 *
 *  @param[out] outBuffer   Pointer to output Cbuffer.
 * 
 *  @return                 Result code:
 *                            MQOS_CLI_OK: Success.
 *                            MQOS_CLI_CMD_NOT_FINISHED: Success - the command
 *                              expects to be called again to finish its work.
 *                            Non-zero fault code on failure.
 */
typedef tMqosConfig_BaseType (*tMqosCli_CmdFunction)(tMqosCbuffer* inCbuffer, tMqosCbufferIndex startIndex, tMqosCbufferIndex eolIndex, tMqosCbuffer* outCbuffer);


/*! Command definition 
 *
 *  All commands supported by the command line interface are defined in an array
 *  of command definition structures.  When a command is entered, the CLI uses
 *  the array to look up the command format and the command function that is to
 *  be called.
 */
typedef struct
{
    const char* cmdString;              //!< The command
    const char* helpString;             //!< Help string: describes what the command does
    tMqosCli_CmdFunction cmdFunction;   //!< Function called when the command is invoked
} tMqosCli_CmdDefinition;


/*! Position and length of a token, or token delimiter, within the Cbuffer. */
typedef struct
{
    tMqosCbufferIndex index;    //!< Start index of token, relative to Cbuffer read index.
    tMqosCbufferIndex len;      //!< Length of token, in bytes.
} tMqosCliToken;


/* PUBLIC VARIABLE DECLARATIONS **********************************************/

/*! Default token parsing delimiters: whitespace characters and null-character. */
extern const char* defaultTokenDelimiters;


/* PUBLIC FUNCTION DECLARATIONS **********************************************/

/*! Invoke a command that was entered by the user.
 *
 *  Read one line from the input Cbuffer, terminated by newline character \n.
 *  If the line contains a correctly formatted command, call the corresponding
 *  command function and write the command output to the output Cbuffer. When
 *  the function exits, one line has been deleted from the front of the input
 *  Cbuffer.
 *
 *  If the input Cbuffer contains no end-of-line character, the function returns
 *  fault code FA MQOS_CLI_EOL_NOT_FOUND. If the first line does not contain a
 *  valid command keyword the function returns MQOS_CLI_CMD_NOT_FOUND. If the
 *  command line contains a syntax error a different fault code is returned.
 *
 *  @param[in]  inCbuffer    Pointer to input Cbuffer.
 *  @param[out] outCbuffer   Pointer to output Cbuffer.
 *
 *  @return                 Result code:
 *                            MQOS_CLI_OK: Success.
 *                            Non-zero fault code on failure.
 */
tMqosConfig_BaseType mqosCli_InvokeCommand(tMqosCbuffer* inCbuffer, tMqosCbuffer* outCbuffer);


/*! Find the next token or block of delimiter characters.
 *
 *  The delimiter characters are specified in the C string pointed to by delim.
 *  If delim is an empty string (""), or NULL, the default delimiter characters
 *  are used. The default delimiter characters are all the whitespace characters
 *  and the null character (\0).
 *
 *  A token is defined as a block of contiguous characters, none of which is a
 *  delimiter character.
 *
 *  @param[in]  cbuf    Pointer to the input Cbuffer.
 *  @param      index   Index at which to start searching, relative to the read
 *                      pointer.
 *  @param      token   If true search for a token, else search for a delimiter.
 *  @param      delim   Pointer to a string containing delimiter characters. If
 *                      an empty string ("") or NULL is passed, then the default
 *                      delimiters " \n\r\t\f\v\0" are used.
 *
 *  @return             Index and length of the first block of contiguous token
 *                      or delimiter characters, relative to the read pointer.
 *                      The value returned is {MQOS_CBUFFER_NOT_FOUND, 0} if no
 *                      token or delimiter was not found.
 */
tMqosCliToken mqosCli_NextTokenOrDelimiter(tMqosCbuffer* cbuf, tMqosCbufferIndex index, bool token, const char* delim);


/*! Test if a character is a token delimiter.
 *
 *  Return true if the character is a valid token delimiter: i.e. whitespace,
 *  end of line or null-character.
 * 
 *  @param  c   Character to be tested.
 * 
 *  @return     Result: true if character is a token delimiter, else false.
 */
bool mqosCli_CharIsTokenDelimiter(char c, const char* delim);


/*!
 *  Convert a token that contains a numeric string to the corresponding integer
 * 
 *  The conversion is performed using the <stdlib.h> function atoi() or atol(),
 *  so the rules for those functions apply. An optional minus sign at the start
 *  of the string, and the following continuous sequence of decimal digits, are
 *  converted to the corresponding 32-bit integer. The number is terminated by
 *  whitespace or any non-numeric character.
 * 
 *  @param[in]  inCbuffer   Pointer to the input Cbuffer.
 *  @param      token       Token to be converted.
 * 
 *  @return                 The integer value.
 */
int32_t mqosCli_TokenToInt(tMqosCbuffer* inCbuffer, tMqosCliToken token);


/*!
 *  Write an integer value to the circular buffer as a numeric string.
 *
 *  If the circular buffer becomes full before the full integer has been written
 *  the function returns a non-zero fault code.
 *
 *  @param[out]  outCbuffer     Pointer to the output Cbuffer.
 *  @param       val            Integer value.
 * 
 *  @return                     Result code:
 *                                MQOS_CLI_OK: Success.
 *                                Non-zero fault code on failure.
 */
tMqosConfig_BaseType mqosCli_WriteIntToCbuffer(tMqosCbuffer* outCbuffer, int32_t val);


/*! MQOS CLI Help command handler function.
 *
 *  This function is called when the user enters the help command. On each call
 *  one line of help is written, for one of the supported commands. The return
 *  value is MQOS_CLI_CMD_NOT_FINISHED for every call, until help has been
 *  printed for the last supported command (the help command). At this point
 *  the value MQOS_CLI_OK is returned.
 * 
 *  If there is insufficient space in the output Cbuffer to write the line of
 *  help, the function returns the fault code MQOS_CLI_INSUFFICIENT_SPACE.
 * 
 *  @param[in]  inCbuffer   Pointer to input Cbuffer containing the command
 *                          line.
 *
 *  @param      startIndex  Command parser starting index, relative to
 *                          inCbuffer's read index. On entry, index points
 *                          to the first character after the command keyword
 *                          in the command line.
 *
 *  @param      eolIndex    End-of-line index, relative to inCbuffer's
 *                          read index.
 *
 *  @param[out] outBuffer   Pointer to output Cbuffer.
 * 
 *  @return                 Result code:
 *                            MQOS_CLI_OK: Success.
 *                            MQOS_CLI_CMD_NOT_FINISHED: Success - the command
 *                              expects to be called again to finish its work.
 *                            Non-zero fault code on failure.
 */
tMqosConfig_BaseType mqosCli_Help(tMqosCbuffer* inCbuffer, tMqosCbufferIndex startIndex, tMqosCbufferIndex eolIndex, tMqosCbuffer* outCbuffer);


#ifdef __cplusplus
}
#endif

#endif  /* MQOSCLI_H */
