/*
 * mqosStdio.h - mqosStdio module header file.
 *
 * This module implements a small subset of the <stdio.h> standard library's
 * functions. The MQOSn printf() function has a restricted range of format
 * specifiers. It typically uses less program memory and RAM than the <stdio.h>
 * implementation, so may be a useful alternative for some applications on
 * memory-constrained embedded systems.
 *
 * The application developer must implement the function mqosPutch() that writes
 * a single character to the standard output and returns 1 if successful, or 0
 * on failure. The application developer can choose any peripheral for standard
 * output: a hardware UART, USB CDC, LCD display, etc. The MQOS printf()
 * function writes characters using mqosPutch().
 * 
 * The version of printf() implemented in the Microchip PIC <stdio.h> library
 * also relies on the application developer implementing a function to print a
 * single character to stdout: putch(). The difference is that the PIC putch()
 * function returns void, so there is no way to detect whether printing has
 * failed due to print buffer overflow. By contrast the MQOS printf() function,
 * based on mqosPutch(), correctly reports the number of characters printed so
 * that appropriate action can be taken if the print buffer overflows.
 *
 * Copyright (C) 2021 Antony Bayley.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef PRINTF_H
#define PRINTF_H

#ifdef __cplusplus
extern "C" {
#endif

#include "mqosConfig.h"


/* PUBLIC MACRO DEFINITIONS **************************************************/

#define MQOS_PUTCH_ERROR    -1      //!< Failed to write a character to stdout.


#if(MQOS_CONFIG_STDIO_LIBRARY == MQOS_CONFIG_MQOS_STDIO_LIBRARY)

/* PUBLIC FUNCTION DECLARATIONS **********************************************/

#ifdef TEST

/* The Catch2 unit test framework uses functions from the standard <stdio.h> 
 * libary in its test macros, including printf(). To test the mqosPrintf
 * module's printf() function, the function must be renamed differently to its
 * counterpart in <stdio.h>.
 */
int test_printf(const char *format, ...);
int test_sprintf(char* str, const char *format, ...);

#else   /* #ifdef TEST */

/*!
 *  Print formatted data to stdout.
 *
 *  Write the C string pointed to by format to the standard output (stdout).
 *  If format includes format specifiers (subsequences beginning with %), the
 *  additional arguments are formatted and inserted in the resulting string to
 *  replace their respective specifiers.
 *
 *  The MQOS implementation of printf supports only a small subset of the
 *  standard printf format specifiers:
 *
 *  Specifier   Output                                              Example
 *  ---------   -------------------------------------------------   -------
 *  d or i      Signed decimal integer                             -359
 *  u           Unsigned decimal integer                            7235
 *  ld or li    Signed decimal long integer                        -1250047
 *  lu          Unsigned decimal long integer                       824126
 *  x           Unsigned hexadecimal integer, lowercase a-f         7fa
 *  X           Unsigned hexadecimal integer, uppercase A-F         F876
 *  lx          Unsigned hexadecimal long integer, lowercase a-f    7faf1230
 *  lX          Unsigned hexadecimal long integer, uppercase A-F    FACE0FF5
 *  c           Character                                           k
 *  s           String of characters	                            Hello world
 *
 *  Field width specifiers and zero-padding specifiers are supported for all
 *  integer data types, rendered in decimal or hexadecimal format. Values are
 *  always right-justified. The left-justify specifier is not supported.
 *
 *  @param  format      Format string, containing text and string specifiers.
 *  @param  ...         Variable parameters: one for each format specifier.
 *
 *  @return             Number of characters printed.
 */
int printf(const char *format, ...);


/*!
 *  Print formatted data to a string.
 *
 *  Write the C string pointed to by format to the C string str. If format
 *  includes format specifiers (subsequences beginning with %), the additional
 *  arguments are formatted and inserted in the resulting string to replace
 *  their respective specifiers. A terminating null character is automatically
 *  appended after the content.
 *
 *  The size of the destination buffer should be large enough to contain the
 *  entire resulting string.
 *
 *  The MQOS implementation of sprintf supports only a small subset of the
 *  standard printf format specifiers:
 *
 *  Specifier   Output                                              Example
 *  ---------   -------------------------------------------------   -------
 *  d or i      Signed decimal integer                             -359
 *  u           Unsigned decimal integer                            7235
 *  ld or li    Signed decimal long integer                        -1250047
 *  lu          Unsigned decimal long integer                       824126
 *  x           Unsigned hexadecimal integer, lowercase a-f         7fa
 *  X           Unsigned hexadecimal integer, uppercase A-F         F876
 *  lx          Unsigned hexadecimal long integer, lowercase a-f    7faf1230
 *  lX          Unsigned hexadecimal long integer, uppercase A-F    FACE0FF5
 *  c           Character                                           k
 *  s           String of characters	                            Hello world
 *
 *  Field width specifiers and zero-padding specifiers are supported for all
 *  integer data types, rendered in decimal or hexadecimal format. Values are
 *  always right-justified. The left-justify specifier is not supported.
 *
 *  @param  str         Destination string to which formatted output is written.
 *  @param  format      Format string, containing text and string specifiers.
 *  @param  ...         Variable parameters: one for each format specifier.
 *
 *  @return             Number of characters printed. This count does not
 *                      include the additional null-character automatically
 *                      appended at the end of the string.
 */
int sprintf(char* str, const char *format, ...);

#endif  /* #ifdef TEST */


/*!
 *  Write the character c to the standard output (stdout).
 *
 *  This is an application-specific function that must be implemented in the
 *  application code. It is not implemented in mqosStdio.c.
 * 
 *  @param  c      The character to be written.
 *  @return        Return the number of characters written: 1 if successful, 0 on failure.
 */
int mqosPutch(char c);


#endif  /* #if(MQOS_CONFIG_STDIO_LIBRARY == MQOS_CONFIG_MQOS_STDIO_LIBRARY) */

#ifdef __cplusplus
}
#endif

#endif  /* PRINTF_H */
