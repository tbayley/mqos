/*
 * mqosCliCommands.h - mqosCliCommands module header file.
 *
 * Copyright (C) 2021 Antony Bayley.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef MQOSCLICOMMANDS_H
#define MQOSCLICOMMANDS_H

#ifdef __cplusplus
extern "C" {
#endif


#include "mqos.h"
#include "mqosCli.h"


/* PUBLIC FUNCTION DECLARATIONS **********************************************/

/*! Return the number of commands impemented by the CLI.
 *
 *  @return     The number of commands implemented by the CLI.
 */
tMqosConfig_UBaseType mqosCliCommands_NumberOfCommands(void);


/*! Return a pointer to the specified CLI command definition.
 *
 *  This function returns a pointer to the specified CLI command definition
 *  struct, if it exists. If there is no command definition at the specified
 *  index, the function returns NULL.
 *
 *  @param  index   Index of the required CLI command.
 *
 *  @return         Pointer to the specified CLI command definition, or NULL.
 */
const tMqosCli_CmdDefinition* mqosCliCommands_GetCmdDefinition(tMqosConfig_UBaseType index);


#ifdef __cplusplus
}
#endif

#endif  /* MQOSCLICOMMANDS_H */
