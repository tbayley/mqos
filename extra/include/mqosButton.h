/*
 * mqosButton.h - mqosButton module header file.
 *
 * The MQOS button module implements a push-button user interface by polling
 * GPIO inputs. It debounces the switch and generates, press, release, and long
 * press event messages. The preprocessor definition MQOS_BUTTON_MAX_BUTTONS
 * sets the maximum number of buttons that are supported.
 *
 * Copyright (C) 2021 Antony Bayley.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef MQOS_BUTTON_H
#define MQOS_BUTTON_H

#ifdef __cplusplus
extern "C" {
#endif


#include <stdint.h>
#include <stdbool.h>
#include "mqosGpio.h"


/* PUBLIC MACRO DEFINITIONS **************************************************/

#ifndef MQOS_BUTTON_MAX_BUTTONS
#define MQOS_BUTTON_MAX_BUTTONS     4   //!< Maximum number of BUTTON class instances.
#endif


/* PUBLIC TYPE DEFINITIONS ***************************************************/

/** Pointer to Button class instance.
 *
 *  The button struct definition is private, encapsulated in mqosButton.c.
 */
typedef struct MqosButton* tMqosButton_Button;


/* PUBLIC FUNCTION DECLARATIONS **********************************************/


/**
 * Button constructor.
 *
 * This function creates an instance of the BUTTON class, which is a driver for
 * a single user interface button that uses polling to determine changes to the
 * button state.
 *
 * Before calling this function, the application code must have configured the
 * selected I/O pin as a digital input during system initialisation.
 *
 * @param  portPin      The port pin to which the button is connected.
 * @param  activeLow    Button polarity.
 *                        true:  The button is active low (GND when pressed).
 *                        false: The button is active high (VDD when pressed).
 *
 * @return              The button handle, if successful, or NULL on failure.
 *                      Button creation will fail if MQOS_BUTTON_MAX_BUTTONS
 *                      instances have already been created. 
 * 
 * The following code snippet instantiates an active-low button on bit 1 of
 * PORT B then polls the button, using the default polling period of 5
 * milliseconds, and handles the BUTTON_PRESSED event.
 *
 * @code
 * extern volatile unsigned char    PORTB   __at(0x00D);
 * static tMqosButton_Button button1;
 *
 * void main(void)
 * {
 *     // initialise the button
 *     tMqosGpio_PortPin button1PortPin = {&PORTB, 1};
 *     button1 = mqosButton_Create(button1PortPin, true);
 *     if(NULL != button1)
 *     {
 *         mqosButton_Enable(button1);
 *     }
 *
 *     ...
 *
 *     // MQOS message handler loop
 *     while(1)
 *     {
 *         tMqosMessage msg = mqos_MessageReceive();
 *         switch(msg.msgId)
 *         {
 *             case mqosConfig_MSG_POLL_BUTTONS:
 *             mqosButton_Poll();
 *             break;
 *
 *             case mqosConfig_MSG_BUTTON_PRESSED:
 *             if(button1 == (tMqosButton_Button) msg.payload.data)
 *             {
 *                 // handle the button-press event for button1
 *             }
 *             break;
 *
 *             ...
 *         }
 *     }
 * }
 * @endcode
 */
tMqosButton_Button mqosButton_Create(const tMqosGpio_PortPin portPin, const bool activeLow);


/**
 * Enable the specified button.
 *
 * @param button   Button instance to be enabled.
 */
void mqosButton_Enable(tMqosButton_Button button);


/**
 * Disable the specified button.
 *
 * @param button   Button instance to be disabled.
 */
void mqosButton_Disable(tMqosButton_Button button);


/**
 * Enable all buttons.
 */
void mqosButton_EnableAll(void);


/**
 * Disable the specified button.
 */
void mqosButton_DisableAll(void);


/**
 * Poll all enabled buttons.
 * 
 * The mqosButton_Poll() function despatches button event messages to the MQOS
 * message queue when buttons are pressed or released.
 */
void mqosButton_Poll(void);


/*!
 * Determine whether the specified button is currently being pressed.
 * 
 * @return      true if the button is being pressed, else false.
 */
bool mqosButton_IsPressed(tMqosButton_Button button);


#ifdef TEST
    /* Reset the allocated button count.
     *
     * There is no need to call mqosButton_Init() in embedded applications,
     * because the button count is initialised to zero at boot-time. However it
     * is useful to restore boot-time state in unit tests.
     */
void mqosButton_Init(void);
#endif


#ifdef __cplusplus
}
#endif

#endif  /* MQOS_BUTTON_H */
