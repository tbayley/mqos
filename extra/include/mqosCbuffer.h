/*
 * mqosCbuffer.h - mqosCbuffer module header file.
 * 
 * A circular buffer containing uint8_t data.
 *
 * Copyright (C) 2021 Antony Bayley.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef MQOSCBUFFER_H
#define MQOSCBUFFER_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>
#include <stdbool.h>
#include "mqosConfig.h"


/* PUBLIC MACROS *************************************************************/

/* MQOS circular buffer function return values */
#define MQOS_CBUFFER_OK         0   //!< Cbuffer operation succeeded.
#define MQOS_CBUFFER_FULL       1   //!< Cbuffer is full.
#define MQOS_CBUFFER_EMPTY      2   //!< Cbuffer is empty.
#define MQOS_CBUFFER_NULL       3   //!< Cbuffer pointer is NULL.
#define MQOS_CBUFFER_DATA_NULL  4   //!< Cbuffer data pointer is NULL.
#define MQOS_CBUFFER_NOT_FOUND  ((tMqosCbufferIndex) - 1)  //!< Specified data byte was not found in Cbuffer.


/*!
 *  Macro to define a circular buffer struct and its associated data array.
 *
 *  To make it easier to differentiate between buffer empty and buffer full
 *  conditions, the data array length must be one more than the maximum number
 *  of bytes that can be contained in the circular buffer.
 *
 *  @param  cbuf    Name of the circular buffer variable to be created.
 *  @param  size    Maximum number of bytes that circular buffer can contain.
 */
#define MQOS_CBUFFER_DEFINE(cbuf, size)     \
    static uint8_t cbuf##_array[size + 1];  \
    static tMqosCbuffer cbuf = {            \
        .data = cbuf##_array,               \
        .writeIndex = 0,                    \
        .readIndex = 0,                     \
        .dataArrayLen = size + 1            \
    };


/*!
 *  Test if the buffer contains a specified data byte.
 * 
 *  @param[in]  cbuf   Circular buffer struct pointer.
 *  @param      index  Index at which to start search, relative to read pointer.
 *  @param      data   The data byte to be searched for.
 *
 *  @return            true if the specified data byte is found, else false.
 */
#define mqosCbuffer_Contains(cbuf, index, data)    \
    (MQOS_CBUFFER_NOT_FOUND != mqosCbuffer_IndexOf(cbuf, index, data))


/*!
 *  Copy the source C string src to the destination circular buffer cbuf.
 * 
 *  If the space available in cbuf is less than n characters, the copied string
 *  is truncated to fit the available space. The null character that terminates
 *  the source C string is not never copied to the destination cbuf.
 *
 *  @param[out] cbuf    Pointer to the destination circular buffer to which 
 *                      characters are copied.
 *  @param[in]  src     Pointer to the source C string, from which characters
 *                      are copied.
 * 
 *  @return             The number of characters copied.
 */
#define mqosCbuffer_StrcpyToCbuffer(cbuf, src)  \
    mqosCbuffer_StrncpyToCbuffer((cbuf), (src), (cbuf)->dataArrayLen)


/* TYPE DEFINITIONS **********************************************************/

/*!
 *  Circular buffer index type
 * 
 *  The default index type is uint8_t.  If your application uses circular
 *  buffers of length more than 254 bytes, you must change the index type to
 *  uint16_t.
 *
 *  The largest integer (i.e. 255 for uint8_t or 65535 for uint16_t) is reserved
 *  for use as a MQOS_CBUFFER_NOT_FOUND return value for the function
 *  mqosCbuffer_IndexOf(), which returns the index in a circular buffer at
 *  which a specified data byte occurs.
 */
typedef uint8_t tMqosCbufferIndex;


/*! Circular buffer type
 *
 *  Circular buffers are often used to transfer data between the main program
 *  context and a peripheral driver that runs in an interrupt context. Therefore
 *  the writeIndex and readIndex member variables are declared volatile.
 */
typedef struct
{
    uint8_t* const data;                    //!< start address of data array.
    volatile tMqosCbufferIndex writeIndex;  //!< index of the next byte to be written to the cbuffer.
    volatile tMqosCbufferIndex readIndex;   //!< index of the next byte to be read from the cbuffer.
    const tMqosCbufferIndex dataArrayLen;   //!< data array length.
} tMqosCbuffer;


/* PUBLIC FUNCTION DECLARATIONS **********************************************/

/*!
 *  Write one byte to the circular buffer.
 *
 *  @param[out] cbuf    Circular buffer struct pointer.
 *  @param      data    The data byte to be written.
 *
 *  @return             return value: 0 if byte was successfully written,
 *                                    non-zero fault code on failure.
 */
tMqosConfig_BaseType  mqosCbuffer_Put(tMqosCbuffer* cbuf, uint8_t data);


/*!
 *  Write one byte to the circular buffer, with no run-time assert checks.
 *
 *  Typically an embedded toolchain will use mqosCbuffer_Put() to implement the
 *  putch() function that is the basis of the <stdio.h> library's printf()
 *  function.
 *
 *  If a run-time assert function is configured to print debug messages, that
 *  creates a recursive function call because the mqosCbuffer_Put() function
 *  itself contains run-time asserts. Recursive function calls are not supported
 *  on low-end Microchip PIC devices because they have a limited-depth hardware
 *  call stack. The presence of recursive function calls causes a compiler
 *  error.
 * 
 *  The solution is to use this modified version of mqosCbuffer_Put() to
 *  implement the system's putch() function. mqosCbuffer_Put_NoAssert() omits
 *  run-time assert checks, but is otherwise identical to the normal
 *  mqosCbuffer_Put() function.
 *
 *  @param[out] cbuf    Circular buffer struct pointer.
 *  @param      data    The data byte to be written.
 *
 *  @return             return value: 0 if byte was successfully written,
 *                                    non-zero fault code on failure.
 */
tMqosConfig_BaseType mqosCbuffer_Put_NoAssert(tMqosCbuffer* cbuf, uint8_t data);


/*!
 *  Read one byte from the circular buffer.
 *
 *  Remove one byte from the front of circular buffer and transfer it to the
 *  uint8_t variable pointed to by data.  If data is a NULL pointer, one byte
 *  is removed from the circular buffer and discarded.
 *
 *  @param[in]  cbuf    Circular buffer struct pointer.
 *  @param[out] data    Pointer to where the read data byte is to be copied.
 *
 *  @return             return value: 0 if byte was successfully read,
 *                                    non-zero fault code on failure.
 */
tMqosConfig_BaseType mqosCbuffer_Get(tMqosCbuffer* cbuf, uint8_t* data);


/*!
 *  Return a pointer to the byte at the specified index, relative to the
 *  circular buffer's read pointer. If index is 0 the read pointer is returned,
 *  if index is 1 a pointer to the byte after the read pointer is returned, and
 *  so on.
 *
 *  If index exceeds the number of bytes contained in the circular buffer then
 *  the function returns NULL.
 *
 *  The circular buffer contents remain unchanged.
 *
 *  @param[in]  cbuf    Circular buffer struct pointer.
 *  @param      index   Index of required data byte, relative to read pointer.
 *
 *  @return             Pointer to the required byte
 */
const uint8_t* mqosCbuffer_Peek(tMqosCbuffer* cbuf, tMqosCbufferIndex index);


/*!
 *  Reset the circular buffer and discard all data.
 *
 *  @param[in]  cbuf    Circular buffer struct pointer.
 */
void mqosCbuffer_Reset(tMqosCbuffer* cbuf);


/*!
 *  Test if the circular buffer is full.
 * 
 *  @param[in]  cbuf    Circular buffer struct pointer.
 * 
 *  @return             true if the buffer is full, else false.
 */
bool mqosCbuffer_IsFull(tMqosCbuffer* cbuf);


/*!
 *  Test if the circular buffer is empty.
 * 
 *  @param[in]  cbuf    Circular buffer struct pointer.
 * 
 *  @return             true if the buffer is empty, else false.
 */
bool mqosCbuffer_IsEmpty(tMqosCbuffer* cbuf);


/*!
 *  Return the circular buffer length.
 * 
 *  @param[in]  cbuf    Circular buffer struct pointer.
 * 
 *  @return             The number of bytes contained in the circular buffer.
 */
tMqosCbufferIndex mqosCbuffer_Length(tMqosCbuffer* cbuf);


/*!
 *  Return the amount of space left in the circular buffer.
 * 
 *  @param[in]  cbuf    Circular buffer struct pointer.
 * 
 *  @return             The number of unused bytes in the circular buffer.
 */
tMqosCbufferIndex mqosCbuffer_Space(tMqosCbuffer* cbuf);


/*!
 *  Return the circular buffer index at which a specified data byte value occurs.
 *
 *  The search begins at a specified index, relative to the current read index.
 *
 *  The return value is the index of the specified data byte value relative to
 *  the current read index.
 *
 *  All relative index values are zero-based. i.e. the value 0 is returned for
 *  the data byte at the current read index, the value 1 is returned for the
 *  byte at (readIndex + 1), and so on.
 *
 *  If the specified data byte does not occur in the circular buffer, after the
 *  specified start index, the value MQOS_CBUFFER_NOT_FOUND is returned.
 *  MQOS_CBUFFER_NOT_FOUND is the maximum value that can be represented by the
 *  tMqosCbufferIndex type. For example if tMqosCbufferIndex is defined as
 *  uint8_t, the value would be 255 (0xFF). 
 *
 *  @param[in]  cbuf    Circular buffer struct pointer.
 *  @param      index   Index at which to start search, relative to read pointer.
 *  @param      data    The data byte whose index is required.
 *
 *  @return             Index of the specified data byte, if found.
 *                      MQOS_CBUFFER_NOT_FOUND if the specified data byte is not found.
 */
tMqosCbufferIndex mqosCbuffer_IndexOf(tMqosCbuffer* cbuf, tMqosCbufferIndex index, const uint8_t data);


/*!
 *  Compare a C string to the contents of the Cbuffer at the specified index.
 *
 *  This function starts by comparing the first character of C string str with
 *  the character at the specified index relative to the circular buffer's read
 *  pointer. If they are equal to each other, it continues with the following
 *  pairs of characters until n characters have been compared or until the
 *  characters differ, or until the C string's terminating null-character is
 *  reached, or until the end of the circular buffer has been reached.
 *
 *  @param[in]  cbuf    Circular buffer struct pointer.
 *  @param      index   Index of required data byte, relative to read pointer.
 *  @param[in]  str     C string to be compared.
 *  @param      n       Number of characters to be compared.
 *
 *  @return             < 0: The first character that does not match has a lower 
 *                           value in cbuf than in str, or the end of the
 *                           circular buffer has been reached before comparing
 *                           the specified number of characters.
 *                        0: The contents of both strings are equal.
 *                      > 0: The first character that does not match has a
 *                           greater value in cbuf than in str.
 */
tMqosConfig_BaseType mqosCbuffer_Strncmp(tMqosCbuffer* cbuf, tMqosCbufferIndex index, const char* str, tMqosCbufferIndex n);


/*!
 *  Copy the first n characters from the source C string src to the destination
 *  circular buffer cbuf.
 * 
 *  If src contains m characters (less than n characters) then only m characters
 *  are copied to the destination cbuf.  If src contains more than n characters,
 *  the copied string is truncated to n characters. If the space available in
 *  cbuf is less than n characters, the copied string is truncated to fit the
 *  available space. The null character that terminates the source C string is
 *  never copied to the destination cbuf.
 *
 *  @param[out] cbuf    Pointer to the destination circular buffer to which 
 *                      characters are copied.
 *  @param[in]  src     Pointer to the source C string, from which characters
 *                      are copied.
 *  @param      n       The maximum number of characters to be copied from str.
 * 
 *  @return             The number of characters copied.
 */
tMqosCbufferIndex mqosCbuffer_StrncpyToCbuffer(tMqosCbuffer* cbuf, const char* src, tMqosCbufferIndex n);


/*!
 *  Copy at most n bytes from the source circular buffer cbuf to the destination
 *  memory block dest.
 *
 *  If cbuf contains m bytes (less than n characters) then only m bytes are
 *  copied to the destination memory block. If cbuf contains more than n bytes,
 *  the first n bytes are copied. The size of the destination memory block must
 *  be at least n bytes.
 *
 *  @param[out] dest    Pointer to the destination memory block to which bytes
 *                      are copied.
 *  @param[in]  cbuf    Pointer to the source circular buffer, from which bytes
 *                      are copied.
 *  @param      index   Start index of copy, relative to the read pointer.
 *  @param      n       The maximum number of bytes to be copied from cbuf.
 *
 *  @return             The number of characters copied.
 */
tMqosCbufferIndex mqosCbuffer_MemcpyFromCbuffer(uint8_t* dest, tMqosCbuffer* cbuf, tMqosCbufferIndex index, tMqosCbufferIndex n);


#ifdef __cplusplus
}
#endif

#endif  /* MQOSCBUFFER_H */
