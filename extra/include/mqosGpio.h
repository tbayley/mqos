/*
 * mqosGpio.h - mqosGpio module header file.
 *
 * The MQOS GPIO module implements a platform-independent abstraction layer
 * for memory-mapped general purpose I/O ports. Peripheral drivers that are
 * implemented using the MQOS GPIO abstraction layer for pin I/O can be more
 * easily ported to a different processor than drivers that directly use the
 * GPIO driver functions supplied by the microprocessor vendor.
 *
 * Copyright (C) 2021 Antony Bayley.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef MQOS_GPIO_H
#define MQOS_GPIO_H

#ifdef __cplusplus
extern "C" {
#endif

#include "mqosConfig.h"


/* PUBLIC TYPE DEFINITIONS ***************************************************/

    /*! Port pin structure that represents a single IO port pin */
typedef struct
{
    volatile tMqosConfig_UBaseType* port; //!< Pointer to the IO port.
    uint8_t bit;                          //!< The port pin bit (0 is the least significant bit).
} tMqosGpio_PortPin;


/* PUBLIC FUNCTION DECLARATIONS **********************************************/


/**
 * Read an IO port pin.
 * 
 * @param portPin   The port pin to be read.
 * @return          The port pin value. 1 if the pin is at logic 1 (VDD), or 0 if at logic 0 (GND).
 */
tMqosConfig_UBaseType mqosGpio_Read(const tMqosGpio_PortPin portPin);


/**
 * Write to an IO port pin.
 * 
 * @param portPin   The port pin to be written.
 * @param value     The value to be written. If 0 the pin is set to logic 0.
 *                  For any non-zero value the pin is set to logic 1.
 */
void mqosGpio_Write(const tMqosGpio_PortPin portPin, tMqosConfig_UBaseType value);


#ifdef __cplusplus
}
#endif

#endif  /* MQOS_GPIO_H */
