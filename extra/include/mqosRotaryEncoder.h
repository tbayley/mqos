/*
 * mqosRotaryEncoder.h - mqosRotaryEncoder module header file.
 *
 * The rotary encoder module implements an interrupt-driven rotary encoder user
 * interface for a Bourns PEC12R series incremental encoder. It should be easy
 * to adapt for use with any other rotary encoder that has two mechanical or
 * optical switch outputs that generate quadrature square-wave waveforms.
 * 
 * The processing is based on the following web article by Ling Dian Miao:
 * https://tutorial.cytron.io/2012/01/17/quadrature-encoder/
 *
 * Copyright (C) 2021 Antony Bayley.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef MQOS_ROTARY_ENCODER_H
#define MQOS_ROTARY_ENCODER_H

#ifdef __cplusplus
extern "C" {
#endif


#include <stdint.h>
#include <stdbool.h>
#include "mqosGpio.h"


/* PUBLIC MACRO DEFINITIONS **************************************************/

#ifndef MQOS_ROTARY_ENCODER_MAX_INSTANCES
/*! Maximum number of ROTARY ENCODER class instances */
#define MQOS_ROTARY_ENCODER_MAX_INSTANCES       1
#endif


#ifndef MQOS_ROTARY_ENCODER_COUNTS_PER_CYCLE
/*! Number of output counts per full cycle of the encoder quadrature waveform: 4, 2 or 1 */
#define MQOS_ROTARY_ENCODER_COUNTS_PER_CYCLE    1
#endif


#ifndef MQOS_ROTARY_ENCODER_FAST_COUNT_ENABLE
/*! Fast count mode enable setting: 1 = fast counting enabled, 0 = disabled */
#define MQOS_ROTARY_ENCODER_FAST_COUNT_ENABLE   1
#endif
    
#ifndef MQOS_ROTARY_ENCODER_FAST_COUNT_CYCLES_THRESHOLD
/*! 
 * Fast count cycle threshold.
 * 
 * The number of quadrature waveform cycles that must occur within a sliding
 * window of duration encoder->fastCountTimeThresholdMs to trigger entry into
 * fast count mode.
 * 
 * When the rotary encoder's knob is turned fast, triggering entry into fast
 * count mode, the count increments or decrements in steps of size
 * encoder->fastCountStepSize, instead of steps of size 1.
 */
#define MQOS_ROTARY_ENCODER_FAST_COUNT_CYCLES_THRESHOLD     4
#endif

/* PUBLIC TYPE DEFINITIONS ***************************************************/

/*! Pointer to Rotary Encoder class instance.
 *
 *  The rotary encoder struct definition is private, encapsulated in
 *  mqosRotaryEncoder.c.
 */
typedef struct MqosRotaryEncoder* tMqosRotaryEncoder_Encoder;


/*! Rotary encoder count type */
typedef uint16_t tMqosRotaryEncoder_Count;


/* PUBLIC FUNCTION DECLARATIONS **********************************************/


/*!
 * Rotary encoder constructor.
 *
 * This function creates an instance of the ROTARY ENCODER class, which is an
 * interrupt driven device driver for rotary encoders. It was designed for use
 * with Bourns PEC12R series incremental encoders, but should work for any
 * encoder that has two quadrature optical or mechanical switch outputs, which
 * are connected to two GPIO input pins A and B.
 * 
 * The rotary encoder's output increments for clockwise rotation and decrements
 * for anticlockwise rotation. The number of counts per full cycle of the A/B
 * quadrature waveform is configurable to 1, 2 or 4 by setting the preprocessor
 * symbol MQOS_ROTARY_ENCODER_COUNTS_PER_CYCLE. Choose 1 if your rotary encoder
 * has one mechanical detent per full cycle of the quadrature waveform. Choose
 * 2 or 4 to obtain more counts per revolution if your encoder has no detents or
 * if it has 2 or 4 detents per cycle.
 *
 * To enable fast counting, set MQOS_ROTARY_ENCODER_FAST_COUNT_ENABLE to 1.
 * When enabled, if you turn the rotary encoder knob rapidly, so that
 * MQOS_ROTARY_ENCODER_FAST_COUNT_CYCLES_THRESHOLD or more cycles of the
 * quadrature waveform occur within a sliding window of duration
 * fastCountTimeThresholdTicks system ticks, then fast counting is triggered.
 * For fast counting, the count is incremented or decremented in steps of size
 * fastCountStepSize instead of 1. When you turn the rotary encoder knob slowly,
 * fast counting is disabled and the step size returns to 1. The recommended
 * fast count parameter values are:
 * 
 *     fastCountTimeThresholdTicks = 100
 *     fastCountStepSize = 10
 *     
 * Before calling this function, the application code must have configured the
 * two selected I/O pins as a digital inputs. Both pins must be configured for
 * interrupt-on-change (i.e. an interrupt is generated on both positive and
 * negative edges). The interrupt service routine must invoke the function
 * rotaryEncoder_StateChangedHandler(), with the appropriate encoder parameter.
 *
 * @param  portPinA                     The port pin to which encoder input A is connected.
 * @param  portPinB                     The port pin to which encoder input B is connected.
 * @param  minCount                     Minimum output count value.
 * @param  maxCount                     Minimum output count value.
 * @param  initialCount                 Initial value of output count.
 * @param  fastCountTimeThresholdTicks  Time window within which MQOS_ROTARY_ENCODER_FAST_COUNT_CYCLES_THRESHOLD cycles must occur to trigger fast count mode, in MQOS system clock ticks.
 * @param  fastCountStepSize            Count increment/decrement value when fast count mode has been triggered.
 *
 * @return                  The rotary encoder handle, if successful, or NULL on
 *                          failure. Rotary encoder creation will fail if
 *                          MQOS_ROTARY_ENCODER_MAX_INSTANCES instances have already
 *                          been created. 
 * 
 */
tMqosRotaryEncoder_Encoder mqosRotaryEncoder_Create(const tMqosGpio_PortPin portPinA,
                                                    const tMqosGpio_PortPin portPinB,
                                                    const tMqosRotaryEncoder_Count minCount,
                                                    const tMqosRotaryEncoder_Count maxCount,
                                                    tMqosRotaryEncoder_Count initialCount
#if MQOS_ROTARY_ENCODER_FAST_COUNT_ENABLE
                                                  , tMqosConfig_TimerCount fastCountTimeThresholdTicks,
                                                    tMqosRotaryEncoder_Count fastCountStepSize
#endif  /* #if MQOS_ROTARY_ENCODER_FAST_COUNT_ENABLE */
                                                   );


/*!
 * Get the current rotary encoder count.
 *
 * @param encoder   Rotary encoder instance.
 */
tMqosRotaryEncoder_Count mqosRotaryEncoder_GetCount(tMqosRotaryEncoder_Encoder encoder);


/*!
 * Set the current rotary encoder count.
 *
 * @param encoder   Rotary encoder instance.
 * @param count     Rotary encoder count value.
 */
void mqosRotaryEncoder_SetCount(tMqosRotaryEncoder_Encoder encoder, tMqosRotaryEncoder_Count count);


/*!
 * State changed handler for rotary encoder.
 *
 * @param encoder   Rotary encoder instance.
 */
void mqosRotaryEncoder_StateChangedHandler(tMqosRotaryEncoder_Encoder encoder);


/*!
 * Set the rotary encoder maximum count.
 *
 * @param encoder   Rotary encoder instance.
 * @param count     Rotary encoder maximum count value.
 */
void mqosRotaryEncoder_SetMaxCount(tMqosRotaryEncoder_Encoder encoder, tMqosRotaryEncoder_Count maxCount);


/*!
 * Set the rotary encoder minimum count.
 *
 * @param encoder   Rotary encoder instance.
 * @param count     Rotary encoder minimum count value.
 */
void mqosRotaryEncoder_SetMinCount(tMqosRotaryEncoder_Encoder encoder, tMqosRotaryEncoder_Count minCount);


#ifdef TEST
    /*!
     * Reset the allocated button count.
     *
     * There is no need to call rotaryEncoder_Init() in embedded applications,
     * because the rotary encoder instance count is initialised to zero at
     * boot-time. However it is useful to restore boot-time state in unit tests.
     */
void mqosRotaryEncoder_Init(void);
#endif


#ifdef __cplusplus
}
#endif

#endif  /* MQOS_ROTARY_ENCODER_H */
