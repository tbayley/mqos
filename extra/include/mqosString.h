/*
 * mqosCliString.h - mqosCliString module header file.
 *
 * This module implements a small selection of the standard library <string.h>
 * module functions that are used by the MQOS CLI. The aim is to achieve a
 * smaller memory footprint than would be achieved using the full <string.h>
 * library, and to enable MQOS CLI to be used on systems that do not implement
 * the <string.h> standard library.
 * 
 * Copyright (C) 2021 Antony Bayley.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef MQOSCLISTRING_H
#define MQOSCLISTRING_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdlib.h>
#include "mqosConfig.h"

#if(MQOS_CONFIG_STRING_LIBRARY == MQOS_CONFIG_MQOS_STRING_LIBRARY)


/* PUBLIC MACRO DEFINITIONS **************************************************/

#ifdef TEST
/* The Catch2 unit test framework uses functions from the standard <string.h> 
 * libary in its test macros. To test the functions in the mqosString module
 * they must be renamed differently to their counterparts in <string.h>
 */
#define memcmp test_memcmp
#define memcpy test_memcpy
#define memset test_memset
#define strlen test_strlen
#define strncmp test_strncmp
#define strncpy test_strncpy
#endif  /* #ifdef TEST */


/* PUBLIC FUNCTION DECLARATIONS **********************************************/

/*!
 *  Compare the contents of two memory blocks.
 *
 *  Compare the first n bytes of the block of memory pointed by ptr1 with the
 *  first n bytes pointed by ptr2. Return 0 if all n bytes match, or a non-zero
 *  integer if one or more bytes are different.
 *
 *  @param  ptr1    Pointer to the first memory block.
 *  @param  ptr2    Pointer to the second memory block.
 *  @param  n       The number of bytes to be compared.
 * 
 *  @return         Result of the comparison:
 *                  < 0: The first byte that does not match has a lower value in
 *                       ptr1 than in ptr2 (if evaluated as unsigned char).
 *                    0: The contents of both memory blocks are equal.
 *                  > 0: The first byte that does not match has a higher value
 *                       in ptr1 than in ptr2 (if evaluated as unsigned char).
 */
int memcmp(const void* ptr1, const void* ptr2, size_t n);


/*!
 *  Copy the first n bytes from the src memory block to the dest memory block.
 * 
 *  The function does not check for any terminating null character in src. It
 *  always copies exactly n bytes. To avoid overflows, the size of the memory
 *  blocks pointed to by both the dest and src parameters must be at least n
 *  bytes and must not overlap.
 *
 *  @param  dest    Pointer to the destination memory block, to which bytes are copied.
 *  @param  src     Pointer to the source memory block, from which bytes are copied.
 *  @param  n       The maximum number of characters to be copied from src.
 * 
 *  @return         Pointer to the destination memory block.
 */
void* memcpy(void* dest, const void* src, size_t n);


/*!
 *  Set the first n bytes of the block of memory dest to the specified value.
 * 
 *  The function fills memory using an unsigned char conversion of the int value
 *  that is passed in.
 *
 *  @param  dest    Pointer to the destination block of memory.
 *  @param  value   Value to be set.
 *  @param  n       The maximum number of bytes to be set.
 * 
 *  @return         Pointer to the destination block of memory.
 */
void* memset(void* dest, int value, size_t n);


/*!
 *  Return the length of the C string str.
 * 
 *  The length of a C string is the number of characters between the start of
 *  the string and the terminating null character '\0', not including the null
 *  character itself.
 *
 *  @param  str     Pointer to the C string whose length is to be measured.
 * 
 *  @return         Length of the C string.
 */
size_t strlen(const char* str);


/*!
 *  Compare up to n characters of C strings str1 and str2.
 *
 *  This function starts by comparing the first characters of each string. If
 *  they match, it continues with the next characters of each string and repeats
 *  until the characters differ, a terminating null-character is reached, or
 *  n characters match in both strings, whichever happens first.
 *
 *  @param  str1    Pointer to the first C string.
 *  @param  str2    Pointer to the second C string.
 *  @param  n       The number of characters to be compared.
 * 
 *  @return         Result of the comparison:
 *                  < 0: The first character that does not match has a lower
 *                       value in str1 than in str2.
 *                    0: The contents of both strings are equal.
 *                  > 0: The first character that does not match has a higher
 *                       value in str1 than in str2.
 */
int strncmp(const char* str1, const char* str2, size_t n);


/*!
 *  Copy the first n characters from the src C string to the dest C string.
 * 
 *  If src contains less than n characters, dest is padded with the null
 *  character '\0' to a length of n.  If src contains more than n characters,
 *  dest is truncated to n characters and is not guaranteed to be terminated
 *  with a null character.
 *
 *  @param  dest    Pointer to the destination C string, to which characters are copied.
 *  @param  src     Pointer to the source C string, from which characters are copied.
 *  @param  n       The maximum number of characters to be copied from src.
 * 
 *  @return         Pointer to the destination char array.
 */
char* strncpy(char* dest, const char* src, size_t n);


#endif  /* #if(MQOS_CONFIG_STRING_LIBRARY == MQOS_CONFIG_MQOS_STRING_LIBRARY) */

#ifdef __cplusplus
}
#endif

#endif  /* MQOSCLISTRING_H */
