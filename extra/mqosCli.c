/*
 * mqosCli.c - mqosCli module source file.
 *
 * Copyright (C) 2021 Antony Bayley.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include "mqosCli.h"
#include "mqosCliCommands.h"

#if(MQOS_CONFIG_STRING_LIBRARY == MQOS_CONFIG_STANDARD_STRING_LIBRARY)
#include <string.h>
#elif(MQOS_CONFIG_STRING_LIBRARY == MQOS_CONFIG_MQOS_STRING_LIBRARY)
#include "mqosString.h"
#endif

/* PRIVATE MACRO DEFINITIONS *************************************************/

#define INT32_NEGATIVE_MAX_DIGITS   11 // length of largest negative 32-bit integer, including minus sign


/* PRIVATE TYPE DEFINITIONS **************************************************/


/* PRIVATE VARIABLE DECLARATIONS *********************************************/

/*! Decimal digit values table used for integer to string conversion */
static const int32_t dv[10] =
{
    1000000000,     // +0
     100000000,     // +1
      10000000,     // +2
       1000000,     // +3
        100000,     // +4
         10000,     // +5
          1000,     // +6
           100,     // +7
            10,     // +8
             1      // +9
};


/* PRIVATE FUNCTION DECLARATIONS *********************************************/

static void mqosCli_DeleteToEndOfLine(tMqosCbuffer* cbuf);
static void mqosCli_DeleteDelimiters(tMqosCbuffer* cbuf);


/* PRIVATE FUNCTION DEFINITIONS **********************************************/

/*! Delete characters from the read index up to the first end-of-line.
 *
 *  If the Cbuffer contains no end-of-line ('\n') characters, the entire
 *  contents of the Cbuffer will be deleted.
 * 
 *  @param[in]  cbuf    Pointer to the input Cbuffer.
 */
static void mqosCli_DeleteToEndOfLine(tMqosCbuffer* cbuf)
{
    uint8_t data;
    while((MQOS_CBUFFER_OK == mqosCbuffer_Get(cbuf, &data)) && ('\n' != (char) data))
    {
        /* discard characters until end-of-line or until Cbuffer is empty */
    }
}


/*!
 *  Delete token delimiters (whitespace characters) from start of Cbuffer.
 *
 *  When this function returns, the Cbuffer's read index either points to the
 *  start of the first token or the Cbuffer is empty.
 * 
 *  @param[in]  cbuf    Pointer to the input Cbuffer.
 */
static void mqosCli_DeleteDelimiters(tMqosCbuffer* cbuf)
{
    const uint8_t* data;
    while(NULL != (data = mqosCbuffer_Peek(cbuf, 0)))
    {
        if(mqosCli_CharIsTokenDelimiter((char) (*data), NULL))
        {
            mqosCbuffer_Get(cbuf, NULL);
        }
        else
        {
            /* character at start of Cbuffer is not a delimiter (whitespace) */
            break;
        }
    }
}

/* PUBLIC VARIABLE DEFINITIONS ***********************************************/

/* Default token parsing delimiters: whitespace characters and null-character. */
const char* defaultTokenDelimiters = " \n\r\t\f\v\0";

/* PUBLIC FUNCTION DEFINITIONS ***********************************************/

tMqosConfig_BaseType mqosCli_InvokeCommand(tMqosCbuffer* inCbuffer, tMqosCbuffer* outCbuffer)
{
    tMqosConfig_BaseType result = MQOS_CLI_EOL_NOT_FOUND;

    /* delete delimiters (whitespace) from the start of the Cbuffer */
    mqosCli_DeleteDelimiters(inCbuffer);

    tMqosCbufferIndex eolIndex = mqosCbuffer_IndexOf(inCbuffer, 0, '\n');
    if(MQOS_CBUFFER_NOT_FOUND != eolIndex)
    {
        /* inCbuffer contains an input command line - parse it */
        result = MQOS_CLI_CMD_NOT_FOUND;
        tMqosConfig_UBaseType numberOfCommands = mqosCliCommands_NumberOfCommands();
        for(tMqosConfig_UBaseType i = 0; i < numberOfCommands ; i++)
        {
            const tMqosCli_CmdDefinition* cmd = mqosCliCommands_GetCmdDefinition(i);
            tMqosCbufferIndex cmdStringLen = (tMqosCbufferIndex) strlen(cmd->cmdString);
            if(0 == mqosCbuffer_Strncmp(inCbuffer, 0, cmd->cmdString, cmdStringLen))
            {
                /* command keyword matches - call the command function */
                result = cmd->cmdFunction(inCbuffer, cmdStringLen, eolIndex, outCbuffer);
                break;
            }
        }
        if(MQOS_CLI_CMD_NOT_FINISHED != result)
        {
            /* If the command has finished, discard the command line from inCbuffer */
            mqosCli_DeleteToEndOfLine(inCbuffer);
        }
    }
    return result;
}


/*! Find the next token or block of delimiter characters.
 *
 *  The delimiter characters are specified in the C string pointed to by delim.
 *  If delim is an empty string (""), or NULL, the default delimiter characters
 *  are used. The default delimiter characters are all the whitespace characters
 *  and the null character (\0).
 *
 *  A token is defined as a block of contiguous characters, none of which is a
 *  delimiter character.
 *
 *  @param[in]  cbuf    Pointer to the input Cbuffer.
 *  @param      index   Index at which to start searching, relative to the read
 *                      pointer.
 *  @param      token   If true search for a token, else search for a delimiter.
 *  @param      delim   Pointer to a string containing delimiter characters. If
 *                      an empty string ("") or NULL is passed, then the default
 *                      delimiters " \n\r\t\f\v\0" are used.
 *
 *  @return             Index and length of the first block of contiguous token
 *                      or delimiter characters, relative to the read pointer.
 *                      The value returned is {MQOS_CBUFFER_NOT_FOUND, 0} if no
 *                      token or delimiter was not found.
 */
tMqosCliToken mqosCli_NextTokenOrDelimiter(tMqosCbuffer* cbuf, tMqosCbufferIndex index, bool token, const char* delim)
{
    /* run-time assert: check that Cbuffer pointer is not NULL */
    MQOS_CONFIG_ASSERT(NULL != cbuf);

    tMqosCliToken result = {MQOS_CBUFFER_NOT_FOUND, 0};

    /* find the start of the token or delimiter block */
    bool finished = false;
    while(!finished)
    {
        const uint8_t* nextChar = mqosCbuffer_Peek(cbuf, index);
        if(NULL == nextChar)
        {
            /* end of Cbuffer - start of token or delimiter was not found */
            finished = true;
        }
        else if((!token) != (!mqosCli_CharIsTokenDelimiter(*nextChar, delim)))
        {
            /* start of token or delimiter was found */
            finished = true;
            result.index = index;
        }
        else
        {
            /* continue searching */
        }
        index += 1;
    }

    if(MQOS_CBUFFER_NOT_FOUND != result.index)
    {
        /* find the end of the token or delimiter block */
        finished = false;
        while(!finished)
        {
            const uint8_t* nextChar = mqosCbuffer_Peek(cbuf, index);
            if((NULL == nextChar) || ((!token) == (!mqosCli_CharIsTokenDelimiter(*nextChar, delim))))
            {
                finished = true;
                result.len = index - result.index;
            }
            index += 1;
        }
    }
    return result;
}


bool mqosCli_CharIsTokenDelimiter(char c, const char* delim)
{
    bool isDelimiter = false;

    if((delim == NULL) || (*delim == '\0'))
    {
        /* if delim is NULL or an empty string, use the default delimiters */
        delim = defaultTokenDelimiters;
    }

    /* deliberately including the \0 string-termination character in delimiters */
    for(size_t i = 0; i < 1 + strlen(delim); i++)
    {
        if(delim[i] == c)
        {
            isDelimiter = true;
        }
    }
    return isDelimiter;
}


int32_t mqosCli_TokenToInt(tMqosCbuffer* inCbuffer, tMqosCliToken token)
{
    int32_t result = 0;
    if(INT32_NEGATIVE_MAX_DIGITS < token.len)  // length includes minus sign
    {
        token.len = INT32_NEGATIVE_MAX_DIGITS;
    }
    char tokenStr[INT32_NEGATIVE_MAX_DIGITS + 1] = {0, };  // + 1 for NULL character string termination
    if(token.len == mqosCbuffer_MemcpyFromCbuffer((uint8_t*) (&tokenStr), inCbuffer, token.index, token.len))
    {
        /* check int length on the target system to find the correct library function */
        if(sizeof(int) == sizeof(int32_t))
        {
            result = atoi(tokenStr);
        }
        else
        {
            result = atol(tokenStr);
        }
    }
    return result;
}


tMqosConfig_BaseType mqosCli_WriteIntToCbuffer(tMqosCbuffer* outCbuffer, int32_t val)
{
    tMqosConfig_BaseType result = MQOS_CLI_OK;

    if(-2147483648 == val)
    {
        /* Maximum negative integer is a special case: -val (2's complement
         * negation) does not yield the corresponding positive integer. Instead
         * it returns the unchanged maximum negative integer!
         */
        if(INT32_NEGATIVE_MAX_DIGITS != mqosCbuffer_StrcpyToCbuffer(outCbuffer, "-2147483648"))
        {
            result = MQOS_CLI_INSUFFICIENT_SPACE;
        }
    }
    else if(0 == val)
    {
        /* handle zero value */
        result = mqosCbuffer_Put(outCbuffer, '0');
    }
    else
    {
        if(0 > val)
        {
            /* handle negative values */
            val = -val;
            result = mqosCbuffer_Put(outCbuffer, '-');
        }

        if(MQOS_CLI_OK == result)
        {
            uint8_t c;
            int32_t divisor;
            uint8_t digitIndex = 0;
            while(val < dv[digitIndex])
            {
                digitIndex += 1;
            }
            do 
            {
                divisor = dv[digitIndex++];
                c = '0';
                while(val >= divisor)
                {
                    c += 1;
                    val -= divisor;
                }
                result = mqosCbuffer_Put(outCbuffer, c);
            } while((divisor != 1) && (MQOS_CLI_OK == result));
        }
    }
    return result;
}


tMqosConfig_BaseType mqosCli_Help(tMqosCbuffer* inCbuffer, tMqosCbufferIndex startIndex, tMqosCbufferIndex eolIndex, tMqosCbuffer* outCbuffer)
{
    /* inCbuffer, startIndex and eolIndex parameters are not used */
    (void) inCbuffer;
    (void) startIndex;
    (void) eolIndex;

    /* index to command definitions array */
    static size_t index = 0;

    tMqosConfig_UBaseType numCommands = mqosCliCommands_NumberOfCommands();
    tMqosConfig_BaseType result = MQOS_CLI_CMD_NOT_FINISHED;
    size_t maxCmdStringLen = 0;

    /* calculate command string field length, based on the maximum command string length */
    for(tMqosConfig_UBaseType i = 0; i < numCommands; i++)
    {
        size_t len = strlen(mqosCliCommands_GetCmdDefinition(i)->cmdString);
        if(len > maxCmdStringLen)
        {
            maxCmdStringLen = len;
        }
    }
    size_t cmdStringFieldLen = maxCmdStringLen + MQOS_CLI_HELP_SPACING;

    /* check if there is sufficient space in the output Cbuffer */
    size_t helpLineLen = cmdStringFieldLen + strlen(mqosCliCommands_GetCmdDefinition(index)->helpString + 2);
    if(helpLineLen > mqosCbuffer_Space(outCbuffer))
    {
        result = MQOS_CLI_CMD_NOT_FINISHED;
    }
    else
    {
        /* write help for the current command to the output buffer */
        (void) mqosCbuffer_StrcpyToCbuffer(outCbuffer, mqosCliCommands_GetCmdDefinition(index)->cmdString);
        size_t paddingLength = cmdStringFieldLen - strlen(mqosCliCommands_GetCmdDefinition(index)->cmdString);
        for(size_t i = 0; i < paddingLength; i++)
        {
            (void) mqosCbuffer_Put(outCbuffer, ' ');
        }
        (void) mqosCbuffer_StrcpyToCbuffer(outCbuffer, mqosCliCommands_GetCmdDefinition(index)->helpString);
        (void) mqosCbuffer_StrcpyToCbuffer(outCbuffer, "\r\n");

        /* check if this is the last line of help output */
        index += 1;
        if(index == (size_t) numCommands)
        {
            result = MQOS_CLI_OK;
            index = 0;
        }
    }
    return result;
}
