/*
 * mqosRotaryEncoder.c - mqosRotaryEncoder module source file.
 *
 * The rotary encoder module implements an interrupt-driven rotary encoder user
 * interface for a Bourns PEC12R series incremental encoder. It should be easy
 * to adapt for use with any other rotary encoder that has two mechanical or
 * optical switch outputs that generate quadrature square-wave waveforms.
 * 
 * The processing is based on the following web article by Ling Dian Miao:
 * https://tutorial.cytron.io/2012/01/17/quadrature-encoder/
 *
 * Copyright (C) 2021 Antony Bayley.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <stddef.h>
#include "mqosRotaryEncoder.h"
#include "mqos.h"


/* PRIVATE MACRO DEFINITIONS *************************************************/


/* PRIVATE TYPE DEFINITIONS **************************************************/

/*! Rotary encoder object type. */
struct MqosRotaryEncoder
{
    tMqosGpio_PortPin portPinA;                         //!< IO pin connected to encoder input A.
    tMqosGpio_PortPin portPinB;                         //!< IO pin connected to encoder input B.
    volatile tMqosRotaryEncoder_Count count;            //!< Current output count.
    tMqosRotaryEncoder_Count minCount;                  //!< Minimum count value.
    tMqosRotaryEncoder_Count maxCount;                  //!< Maximum count value.
    volatile uint8_t state;                             //!< Rotary encoder state.
    tMqosRotaryEncoder_Count stepSize;                  //!< Rotary encoder's current count step size.
#if MQOS_ROTARY_ENCODER_FAST_COUNT_ENABLE
    tMqosConfig_TimerCount fastCountTimeThresholdTicks; //!< Fast count time threshold in MQOS system ticks.
    tMqosRotaryEncoder_Count fastCountStepSize;         //!< Fast counting step size.
    tMqosConfig_TimerCount timeStamp[MQOS_ROTARY_ENCODER_FAST_COUNT_CYCLES_THRESHOLD]; //!< time stamps for previous cycles
    uint8_t index;                                      //!< Index into timeStamp array.
#endif  /* #if MQOS_ROTARY_ENCODER_FAST_COUNT_ENABLE */
};
 

/* PRIVATE VARIABLE DEFINITIONS **********************************************/

/*! Rotary encoder instances array */
static struct MqosRotaryEncoder instances[MQOS_ROTARY_ENCODER_MAX_INSTANCES];

/*! The number of allocated rotary encoder instances */
static tMqosConfig_UBaseType allocatedEncoderInstances = 0;


/* PRIVATE FUNCTION DECLARATIONS *********************************************/

static tMqosRotaryEncoder_Encoder getInstance(void);
static void incrementCount(tMqosRotaryEncoder_Encoder encoder);
static void decrementCount(tMqosRotaryEncoder_Encoder encoder);

#if MQOS_ROTARY_ENCODER_FAST_COUNT_ENABLE
static void setStepSize(tMqosRotaryEncoder_Encoder encoder);
#else
#define setStepSize(encoder)    ((void) (encoder))
#endif  /* #if MQOS_ROTARY_ENCODER_FAST_COUNT_ENABLE */


/* PUBLIC FUNCTION DEFINITIONS ***********************************************/

tMqosRotaryEncoder_Encoder mqosRotaryEncoder_Create(const tMqosGpio_PortPin portPinA,
                                                    const tMqosGpio_PortPin portPinB,
                                                    const tMqosRotaryEncoder_Count minCount,
                                                    const tMqosRotaryEncoder_Count maxCount,
                                                    tMqosRotaryEncoder_Count initialCount
#if MQOS_ROTARY_ENCODER_FAST_COUNT_ENABLE
                                                  , tMqosConfig_TimerCount fastCountTimeThresholdTicks,
                                                    tMqosRotaryEncoder_Count fastCountStepSize
#endif  /* #if MQOS_ROTARY_ENCODER_FAST_COUNT_ENABLE */
                                                   )
{
    tMqosRotaryEncoder_Encoder encoder = getInstance();
    if(NULL != encoder)
    {
        encoder->portPinA = portPinA;
        encoder->portPinB = portPinB;
        encoder->minCount = minCount;
        encoder->maxCount = (maxCount > minCount) ? maxCount : minCount;
        mqosRotaryEncoder_SetCount(encoder, initialCount);
        encoder->count = initialCount;
        encoder->state = 3;  // PEC12R encoder has mechanical detents at positions with state A=1, B=1
        encoder->stepSize = 1;

#if MQOS_ROTARY_ENCODER_FAST_COUNT_ENABLE
        encoder->fastCountTimeThresholdTicks = fastCountTimeThresholdTicks;
        encoder->fastCountStepSize = fastCountStepSize;
        encoder->index = 0;
        tMqosConfig_TimerCount timeNow = mqos_getSystemTickCount();
        for(uint8_t i = 0; i < MQOS_ROTARY_ENCODER_FAST_COUNT_CYCLES_THRESHOLD; i++)
        {
            encoder->timeStamp[i] = timeNow;
        }
#endif  /* #if MQOS_ROTARY_ENCODER_FAST_COUNT_ENABLE */

    }
    return encoder;
}


tMqosRotaryEncoder_Count mqosRotaryEncoder_GetCount(tMqosRotaryEncoder_Encoder encoder)
{
    mqosPort_ENTER_CRITICAL();
    tMqosRotaryEncoder_Count count = encoder->count;
    mqosPort_EXIT_CRITICAL();
    return count;
}


void mqosRotaryEncoder_SetCount(tMqosRotaryEncoder_Encoder encoder, tMqosRotaryEncoder_Count count)
{
    mqosPort_ENTER_CRITICAL();

    /* range checks */
    if(count > encoder->maxCount)
    {
        count = encoder->maxCount;
    }
    else if(count < encoder->minCount)
    {
        count = encoder->minCount;
    }

    encoder->count = count;
    mqosPort_EXIT_CRITICAL();
}


#if (MQOS_ROTARY_ENCODER_COUNTS_PER_CYCLE == 4)

void mqosRotaryEncoder_StateChangedHandler(tMqosRotaryEncoder_Encoder encoder)
{
    uint8_t state = (mqosGpio_Read(encoder->portPinA) << 1) + mqosGpio_Read(encoder->portPinB);
    switch(state)
    {
        case 0:
        {
            setStepSize(encoder);
            if(1 == encoder->state)
            {
                incrementCount(encoder);
            }
            else if(2 == encoder->state)
            {
                decrementCount(encoder);
            }
        }
        break;

        case 1:
        {
            if(3 == encoder->state)
            {
                incrementCount(encoder);
            }
            else if(0 == encoder->state)
            {
                decrementCount(encoder);
            }
        }
        break;

        case 2:
        {
            if(0 == encoder->state)
            {
                incrementCount(encoder);
            }
            else if(3 == encoder->state)
            {
                decrementCount(encoder);
            }
        }
        break;

        case 3:
        {
            if(2 == encoder->state)
            {
                incrementCount(encoder);
            }
            else if(1 == encoder->state)
            {
                decrementCount(encoder);
            }
        }
        break;

        default:
        {
            /* do nothing */
        }
        break;
    }
    encoder->state = state;
}

#elif (MQOS_ROTARY_ENCODER_COUNTS_PER_CYCLE == 2 )

void mqosRotaryEncoder_StateChangedHandler(tMqosRotaryEncoder_Encoder encoder)
{
    uint8_t state = (mqosGpio_Read(encoder->portPinA) << 1) + mqosGpio_Read(encoder->portPinB);
    switch(state)
    {
        case 0:
        {
            setStepSize(encoder);
            if(1 == encoder->state)
            {
                incrementCount(encoder);
            }
            else if(2 == encoder->state)
            {
                decrementCount(encoder);
            }
        }
        break;

        case 3:
        {
            if(2 == encoder->state)
            {
                incrementCount(encoder);
            }
            else if(1 == encoder->state)
            {
                decrementCount(encoder);
            }
        }
        break;

        default:
        {
            /* do nothing */
        }
        break;
    }
    encoder->state = state;
}

#else

void mqosRotaryEncoder_StateChangedHandler(tMqosRotaryEncoder_Encoder encoder)
{
    uint8_t state = (mqosGpio_Read(encoder->portPinA) << 1) + mqosGpio_Read(encoder->portPinB);
    switch(state)
    {
        case 0:
        {
            setStepSize(encoder);
            if(1 == encoder->state)
            {
                incrementCount(encoder);
            }
            else if(2 == encoder->state)
            {
                decrementCount(encoder);
            }
        }
        break;

        default:
        {
            /* do nothing */
        }
        break;
    }
    encoder->state = state;
}

#endif  /* #if (MQOS_ROTARY_ENCODER_COUNTS_PER_CYCLE == 4) */


void mqosRotaryEncoder_SetMaxCount(tMqosRotaryEncoder_Encoder encoder, tMqosRotaryEncoder_Count maxCount)
{
    mqosPort_ENTER_CRITICAL();
    encoder->maxCount = maxCount;

    /* ensure that count is not out of range */
    if(encoder->count > encoder->maxCount)
    {
        encoder->count = encoder->maxCount;
    }
    mqosPort_EXIT_CRITICAL();
}


void mqosRotaryEncoder_SetMinCount(tMqosRotaryEncoder_Encoder encoder, tMqosRotaryEncoder_Count minCount)
{
    mqosPort_ENTER_CRITICAL();
    encoder->minCount = minCount;

    /* ensure that count is not out of range */
    if(encoder->count < encoder->minCount)
    {
        encoder->count = encoder->minCount;
    }
    mqosPort_EXIT_CRITICAL();
}


#ifdef TEST
void mqosRotaryEncoder_Init(void)
{
    /* Reset the allocated rotary encoder instance count */
    allocatedEncoderInstances = 0;
}
#endif


/* PRIVATE FUNCTION DEFINITIONS **********************************************/

/*!
 * Get an instance of the rotary encoder class.
 *
 * @return The rotary encoder class instance.
 */
static tMqosRotaryEncoder_Encoder getInstance(void)
{
    tMqosRotaryEncoder_Encoder encoder = NULL;
    if(MQOS_ROTARY_ENCODER_MAX_INSTANCES > allocatedEncoderInstances)
    {
        encoder = &(instances[allocatedEncoderInstances++]);
    }
    return encoder;
}


/*!
 * Increment the rotary encoder count if less than the maximum count value.
 * 
 * @param encoder   Rotary encoder instance.
 */
static void incrementCount(tMqosRotaryEncoder_Encoder encoder)
{
    mqosPort_ENTER_CRITICAL();
    if((encoder->maxCount - encoder->count) < encoder->stepSize)
    {
        encoder->count = encoder->maxCount;
    }
    else
    {
        encoder->count += encoder->stepSize;
    }
    mqosPort_EXIT_CRITICAL();
}


/*!
 * Decrement the rotary encoder count if greater than the minimum count value.
 * 
 * @param encoder   Rotary encoder instance.
 */
static void decrementCount(tMqosRotaryEncoder_Encoder encoder)
{
    mqosPort_ENTER_CRITICAL();
    if((encoder->count - encoder->minCount) < encoder->stepSize)
    {
        encoder->count = encoder->minCount;
    }
    else
    {
        encoder->count -= encoder->stepSize;
    }
    mqosPort_EXIT_CRITICAL();
}


#if MQOS_ROTARY_ENCODER_FAST_COUNT_ENABLE
static void setStepSize(tMqosRotaryEncoder_Encoder encoder)
{
    /* Set the rotary encoder's count step size - fast or slow counting */
    tMqosConfig_TimerCount timeNow = mqos_getSystemTickCount();
    if((timeNow - encoder->timeStamp[encoder->index]) < encoder->fastCountTimeThresholdTicks)
    {
        encoder->stepSize = encoder->fastCountStepSize;
    }
    else
    {
        encoder->stepSize = 1;
    }
    encoder->timeStamp[encoder->index] = timeNow;
    encoder->index = (encoder->index + 1) % MQOS_ROTARY_ENCODER_FAST_COUNT_CYCLES_THRESHOLD;
}
#endif  /* #if MQOS_ROTARY_ENCODER_FAST_COUNT_ENABLE */
