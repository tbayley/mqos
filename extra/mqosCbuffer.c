/*
 * mqosCbuffer.c - mqosCbuffer module source file.
 * 
 * A circular buffer containing uint8_t data.
 *
 * Copyright (C) 2021 Antony Bayley.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <stdlib.h>
#include "mqosCbuffer.h"

#if(MQOS_CONFIG_STRING_LIBRARY == MQOS_CONFIG_STANDARD_STRING_LIBRARY)
#include <string.h>
#elif(MQOS_CONFIG_STRING_LIBRARY == MQOS_CONFIG_MQOS_STRING_LIBRARY)
#include "mqosString.h"
#endif


/* PUBLIC FUNCTION DEFINITIONS ***********************************************/

tMqosConfig_BaseType mqosCbuffer_Put(tMqosCbuffer* cbuf, uint8_t data)
{
    /* run-time assert: check that pointer is not NULL */
    MQOS_CONFIG_ASSERT(NULL != cbuf);

    return mqosCbuffer_Put_NoAssert(cbuf, data);
}


tMqosConfig_BaseType mqosCbuffer_Put_NoAssert(tMqosCbuffer* cbuf, uint8_t data)
{
    tMqosConfig_BaseType status =   MQOS_CBUFFER_OK;
    if((cbuf->writeIndex + 1) % cbuf->dataArrayLen == cbuf->readIndex)
    {
        status = MQOS_CBUFFER_FULL;
    }
    else
    {
        cbuf->data[cbuf->writeIndex++] = data;
        if(cbuf->writeIndex == cbuf->dataArrayLen)
        {
            cbuf->writeIndex = 0;
        }
    }
    return status;
}


tMqosConfig_BaseType mqosCbuffer_Get(tMqosCbuffer* cbuf, uint8_t* data)
{
    /* run-time assert: check that cbuf pointer is not NULL.
     * data pointer can legitimately be NULL to discard a byte from the Cbuffer.
     */
    MQOS_CONFIG_ASSERT(NULL != cbuf);

    tMqosConfig_BaseType status = MQOS_CBUFFER_OK;
    if(mqosCbuffer_IsEmpty(cbuf))
    {
        status = MQOS_CBUFFER_EMPTY;
    }
    else
    {
        if(data == NULL)
        {
            /* if data pointer is NULL, discard one byte of data */
            status = MQOS_CBUFFER_DATA_NULL;
            cbuf->readIndex++;
        }
        else
        {
            /* if data pointer is not NULL, read one byte of data */
            *data = cbuf->data[cbuf->readIndex++];
        }
        if(cbuf->readIndex == cbuf->dataArrayLen)
        {
            cbuf->readIndex = 0;
        }
    }
    return status;
}


const uint8_t* mqosCbuffer_Peek(tMqosCbuffer* cbuf, tMqosCbufferIndex index)
{
    /* run-time assert: check that pointer is not NULL */
    MQOS_CONFIG_ASSERT(NULL != cbuf);

    uint8_t* ptr = NULL;
    if(index < mqosCbuffer_Length(cbuf))
    {
        tMqosCbufferIndex i = (cbuf->readIndex + index) % cbuf->dataArrayLen;
        ptr = &(cbuf->data[i]);
    }
    return ptr;
}


void mqosCbuffer_Reset(tMqosCbuffer* cbuf)
{
    /* run-time assert: check that pointer is not NULL */
    MQOS_CONFIG_ASSERT(NULL != cbuf);

    cbuf->readIndex = 0;
    cbuf->writeIndex = 0;
}


bool mqosCbuffer_IsFull(tMqosCbuffer* cbuf)
{
    /* run-time assert: check that pointer is not NULL */
    MQOS_CONFIG_ASSERT(NULL != cbuf);

    return ((cbuf->writeIndex + 1) % cbuf->dataArrayLen == cbuf->readIndex);
}


bool mqosCbuffer_IsEmpty(tMqosCbuffer* cbuf)
{
    /* run-time assert: check that pointer is not NULL */
    MQOS_CONFIG_ASSERT(NULL != cbuf);

    return (cbuf->readIndex == cbuf->writeIndex);
}


tMqosCbufferIndex mqosCbuffer_Length(tMqosCbuffer* cbuf)
{ 
    /* run-time assert: check that pointer is not NULL */
    MQOS_CONFIG_ASSERT(NULL != cbuf);

    /* need to add cbuf->dataArrayLen before subtraction, to handle modulo wrap-around */
    return ((cbuf->writeIndex + cbuf->dataArrayLen - cbuf->readIndex) % cbuf->dataArrayLen);
}


tMqosCbufferIndex mqosCbuffer_Space(tMqosCbuffer* cbuf)
{ 
    /* run-time assert: check that pointer is not NULL */
    MQOS_CONFIG_ASSERT(NULL != cbuf);

    /* need to add cbuf->dataArrayLen before subtraction, to handle modulo wrap-around */
    return ((cbuf->readIndex + cbuf->dataArrayLen - cbuf->writeIndex - 1) % cbuf->dataArrayLen);
}


tMqosCbufferIndex mqosCbuffer_IndexOf(tMqosCbuffer* cbuf, tMqosCbufferIndex index, const uint8_t data)
{
    /* run-time assert: check that pointer is not NULL */
    MQOS_CONFIG_ASSERT(NULL != cbuf);

    bool finished = false;
    while(!finished)
    {
        const uint8_t* cbufData = mqosCbuffer_Peek(cbuf, index);
        if((NULL != cbufData) && (data == *cbufData))
        {
            /* the specified data value has been found */
            finished = true;
        }
        else if(NULL == cbufData)
        {
            /* Cbuffer does not contain the specified data value */
            finished = true;
            index = MQOS_CBUFFER_NOT_FOUND;
        }
        else
        {
            /* read the next byte */
           index += 1;
        }
    }

    return index;
}


tMqosConfig_BaseType mqosCbuffer_Strncmp(tMqosCbuffer* cbuf, tMqosCbufferIndex index, const char* str, tMqosCbufferIndex n)
{
    /* run-time asserts: check that pointers are not NULL */
    MQOS_CONFIG_ASSERT(NULL != cbuf);
    MQOS_CONFIG_ASSERT(NULL != str);

    tMqosConfig_BaseType result = 0;

    size_t stringLen = strlen(str);
    if(stringLen < (size_t) n)
    {
        /* if string contains less than n characters, reduce n to match the string length */
        n = (tMqosCbufferIndex) stringLen;
    }

    /* do the string comparison */
    for(tMqosCbufferIndex i = 0; (i < n) && (result == 0); i++)
    {
        const uint8_t* cbufferChar = mqosCbuffer_Peek(cbuf, index + i);
        if(NULL == cbufferChar)
        {
            /* The circular buffer does not contain enough characters */
            result = -1;
            break;
        }
        else
        {
            result = ((tMqosConfig_BaseType) *cbufferChar) - ((tMqosConfig_BaseType) str[i]);
        }
    }

    return result;
}


tMqosCbufferIndex mqosCbuffer_StrncpyToCbuffer(tMqosCbuffer* cbuf, const char* src, tMqosCbufferIndex n)
{
    /* run-time asserts: check that pointers are not NULL */
    MQOS_CONFIG_ASSERT(NULL != cbuf);
    MQOS_CONFIG_ASSERT(NULL != src);

    /* length of source string */
    tMqosCbufferIndex stringLen = (tMqosCbufferIndex) strlen(src);

    /* number of characters to copy */
    tMqosCbufferIndex copyCount = mqosCbuffer_Space(cbuf);
    if(n < copyCount)
    {
        copyCount = n;
    }
    if(stringLen < copyCount)
    {
        copyCount = stringLen;
    }

    for(tMqosCbufferIndex i = 0; i < copyCount; i++)
    {
        (void) mqosCbuffer_Put(cbuf, src[i]);
    }
    return copyCount;
}


tMqosCbufferIndex mqosCbuffer_MemcpyFromCbuffer(uint8_t* dest, tMqosCbuffer* cbuf, tMqosCbufferIndex index, tMqosCbufferIndex n)
{
    /* run-time asserts: check that pointers are not NULL */
    MQOS_CONFIG_ASSERT(NULL != dest);
    MQOS_CONFIG_ASSERT(NULL != cbuf);

    /* reduce copy length if the source circular buffer contains insufficient data */
    tMqosCbufferIndex cbufLen = mqosCbuffer_Length(cbuf);
    tMqosCbufferIndex cbufBytesCopied = cbufLen - index;
    if(cbufBytesCopied < n)
    {
        n = cbufBytesCopied;
    }

    for(tMqosCbufferIndex i = 0; i < n; i++)
    {
        const uint8_t* data = mqosCbuffer_Peek(cbuf, index + i);
        if(NULL != data)
        {
            dest[i] = *data;
        }
    }
    return n;
}
