/*
 * test_mqosButton.c - mqosButton module unit test source file.
 *
 * Copyright (C) 2021 Antony Bayley.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include "mqos.h"
#include "mqosButton.h"
#include "catch.hpp"

// Want to keep the fake function argument history for 15 calls
#define FFF_ARG_HISTORY_LEN 15
#include "fff.h"


/* MACROS ********************************************************************/

/* Mock functions are created using "fake function framework" (fff.h) */
DEFINE_FFF_GLOBALS;

FAKE_VALUE_FUNC(tMqosMessageContainer*, mqos_MessageSendLater, tMqosConfig_MessageId, tMqosConfig_MessagePayload, tMqosConfig_TimerCount);
FAKE_VALUE_FUNC(tMqosConfig_TimerCount, mqos_getSystemTickCount);
FAKE_VALUE_FUNC(tMqosConfig_UBaseType, mqosGpio_Read, tMqosGpio_PortPin);

#define EXPECTED_POLL_PERIOD_TICKS  5  //!< Button polling period in MQOS system ticks.

/* PRIVATE VARIABLE DEFINITIONS **********************************************/

/* Define a dummy IO port for unit tests */
static volatile tMqosConfig_UBaseType PORTA = 0;


/* PUBLIC FUNCTION DEFINITIONS ***********************************************/


SCENARIO("Tests for mqosButton_Create().", "[mqosButton-Create]")
{
    tMqosButton_Button buttons[MQOS_BUTTON_MAX_BUTTONS + 1];
    char msg[100];

    for(tMqosConfig_UBaseType buttonIndex = 0; buttonIndex <= MQOS_BUTTON_MAX_BUTTONS; buttonIndex++)
    {
        snprintf(msg, 100, "mqosButton_Create() is called to create an active low button: Iteration %u", 1 + buttonIndex);
        WHEN(msg)
        {
            if(0 == buttonIndex)
            {
                /* reset the allocated button count to zero, at start of test sequence only */
                mqosButton_Init();
            }
            tMqosGpio_PortPin buttonPortPin = {&PORTA, buttonIndex};
            const bool activeLow = true;

            // UUT - Create a button instance
            buttons[buttonIndex] = mqosButton_Create(buttonPortPin, activeLow);

            if(buttonIndex < MQOS_BUTTON_MAX_BUTTONS)
            {
                THEN("mqosButton_Create() shall return a valid button handle")
                {
                    REQUIRE(NULL != buttons[buttonIndex]);
                }
            }
            else
            {
                THEN("mqosButton_Create() shall return NULL")
                {
                    REQUIRE(NULL == buttons[buttonIndex]);
                }
            }
        }
    }

    for(tMqosConfig_UBaseType buttonIndex = 0; buttonIndex <= MQOS_BUTTON_MAX_BUTTONS; buttonIndex++)
    {
        snprintf(msg, 100, "mqosButton_Create() is called to create an active high button: Iteration %u", 1 + buttonIndex);
        WHEN(msg)
        {
            if(0 == buttonIndex)
            {
                /* reset the allocated button count to zero, at start of test sequence only */
                mqosButton_Init();
            }
            tMqosGpio_PortPin buttonPortPin = {&PORTA, buttonIndex};
            const bool activeHigh = false;

            // UUT - Create a button instance
            buttons[buttonIndex] = mqosButton_Create(buttonPortPin, activeHigh);

            if(buttonIndex < MQOS_BUTTON_MAX_BUTTONS)
            {
                THEN("mqosButton_Create() shall return a valid button handle")
                {
                    REQUIRE(NULL != buttons[buttonIndex]);
                }
            }
            else
            {
                THEN("mqosButton_Create() shall return NULL")
                {
                    REQUIRE(NULL == buttons[buttonIndex]);
                }
            }
        }
    }
}


//----------------------------------------------------------------------------


SCENARIO("Tests for a single button instance, active low polarity", "[mqosButton-ActiveLow]")
{
    GIVEN("A single active-low button is created on PORTA bit 2, but not enabled")
    {
        /* reset the allocated button count to zero */
        mqosButton_Init();

        /* reset the mock functions */
        RESET_FAKE(mqos_getSystemTickCount);
        RESET_FAKE(mqos_MessageSendLater);

        /* allocate a single button */
        tMqosGpio_PortPin button1PortPin = {&PORTA, 2};
        tMqosButton_Button button1 = mqosButton_Create(button1PortPin, true);
        REQUIRE(NULL != button1);

        WHEN("The function mqosButton_IsPressed() is called")
        {
            bool isPressed = mqosButton_IsPressed(button1);
            THEN("It shall return false")
            {
                REQUIRE(false == isPressed);
            }
        }

        WHEN("The function mqosButton_Poll() is called")
        {
            mqosButton_Poll();
            THEN("Nothing shall happen because the button is disabled by default")
            {
                REQUIRE(0 == mqos_getSystemTickCount_fake.call_count);
                REQUIRE(0 == mqos_MessageSendLater_fake.call_count);
            }
        }
    }


    //-------------------------------------------------------------------------

    GIVEN("A single active-low button is created on PORTA bit 2, and enabled while not pressed")
    {
        /* reset the allocated button count to zero */
        mqosButton_Init();

        /* reset the mock functions */
        RESET_FAKE(mqos_getSystemTickCount);
        RESET_FAKE(mqos_MessageSendLater);
        RESET_FAKE(mqosGpio_Read);

        /* allocate a single button */
        tMqosGpio_PortPin button1PortPin = {&PORTA, 2};
        tMqosButton_Button button1 = mqosButton_Create(button1PortPin, true);
        REQUIRE(NULL != button1);

        /* enable the button: it is not currently being pressed */
        mqosGpio_Read_fake.return_val = 1;
        mqosButton_Enable(button1);
        /* expect button Gpio pin to be read */
        REQUIRE(1 == mqosGpio_Read_fake.call_count);
        /* expect button polling to be scheduled by sending an MQOS message */
        REQUIRE(1 == mqos_MessageSendLater_fake.call_count);
        REQUIRE(mqosConfig_MSG_POLL_BUTTONS == mqos_MessageSendLater_fake.arg0_val);
        REQUIRE(0 == mqos_MessageSendLater_fake.arg1_val.data);
        REQUIRE(EXPECTED_POLL_PERIOD_TICKS == mqos_MessageSendLater_fake.arg2_val);

        RESET_FAKE(mqosGpio_Read);
        RESET_FAKE(mqos_MessageSendLater);

        WHEN("The function mqosButton_IsPressed() is called")
        {
            bool isPressed = mqosButton_IsPressed(button1);
            THEN("It shall return false")
            {
                REQUIRE_FALSE(isPressed);
            }
        }

        WHEN("The function mqosButton_Poll() is called repeatedly and a falling edge occurs on the Button Gpio pin at sample [2]")
        {
            tMqosConfig_TimerCount expectedSystemTickSequence[7] = 
            {
                100,                    // tickCount when BUTTON_PRESSED event occurs: 100 ticks
                100, 105, 110, 1099,    // tickCount while button pressed for less than long press period
                1100,                   // tick count for first poll where button pressed for >= long press period
                5000                    // tick count is not read again after BUTTON_LONG_PRESS event has occurred
            };
            SET_RETURN_SEQ(mqos_getSystemTickCount, expectedSystemTickSequence, 7);

            tMqosConfig_UBaseType gpioReadSequenceFallingEdge[10] =
            {
                1, 1, 0, 0,     // mqosButton_IsPressed() is false
                0,              // BUTTON_PRESSED event occurs after three consecutive '0' values
                0, 0, 0, 0, 0   // mqosButton_IsPressed() is true
            };
            SET_RETURN_SEQ(mqosGpio_Read, gpioReadSequenceFallingEdge, 10);

            for(size_t i = 0; i < 10; i++)
            {
                /* call the UUT */
                mqosButton_Poll();

                /* BUTTON_PRESSED event occurs at sample [4] and the button remains pressed thereafter */ 
                if(4 > i)
                {
                    REQUIRE_FALSE(mqosButton_IsPressed(button1));
                }
                else
                {
                    REQUIRE(mqosButton_IsPressed(button1));
                }
            }

            THEN("A BUTTON_PRESSED event shall occur at poll [4] and a BUTTON_LONG_PRESS at poll [8]")
            {
                /* Check the mock function call history */
                REQUIRE(10 == mqosGpio_Read_fake.call_count);
                REQUIRE(6 == mqos_getSystemTickCount_fake.call_count);
                REQUIRE(12 == mqos_MessageSendLater_fake.call_count);

                /* check the sequence of MQOS messages that were sent */
                for(size_t i = 0; i < 12; i++)
                {
                    if(4 == i)
                    {
                        /* message [4] is a BUTTON_PRESSED event message */
                        REQUIRE(mqosConfig_MSG_BUTTON_PRESSED == mqos_MessageSendLater_fake.arg0_history[i]);
                        REQUIRE((tMqosConfig_MessagePayloadData) button1 == mqos_MessageSendLater_fake.arg1_history[i].data);
                        REQUIRE(0 == mqos_MessageSendLater_fake.arg2_history[i]);
                    }
                    else if(9 == i)
                    {
                        /* message [9] is a BUTTON_LONG_PRESS event message */
                        REQUIRE(mqosConfig_MSG_BUTTON_LONG_PRESS == mqos_MessageSendLater_fake.arg0_history[i]);
                        REQUIRE((tMqosConfig_MessagePayloadData) button1 == mqos_MessageSendLater_fake.arg1_history[i].data);
                        REQUIRE(0 == mqos_MessageSendLater_fake.arg2_history[i]);
                    }
                    else
                    {
                        /* all other messages are POLL_BUTTONS request messages */
                        REQUIRE(mqosConfig_MSG_POLL_BUTTONS == mqos_MessageSendLater_fake.arg0_history[i]);
                        REQUIRE(0 == mqos_MessageSendLater_fake.arg1_history[i].data);
                        REQUIRE(EXPECTED_POLL_PERIOD_TICKS == mqos_MessageSendLater_fake.arg2_history[i]);
                    }
                }
            }
        }

        WHEN("mqosButton_Disable() is called")
        {
            mqosButton_Disable(button1);

            AND_WHEN("The function mqosButton_Poll() is called")
            {
                mqosButton_Poll();
                THEN("Nothing shall happen because button1 has been disabled")
                {
                    REQUIRE(0 == mqos_getSystemTickCount_fake.call_count);
                    REQUIRE(0 == mqos_MessageSendLater_fake.call_count);
                }
            }
        }

        WHEN("mqosButton_DisableAll() is called")
        {
            mqosButton_DisableAll();

            AND_WHEN("The function mqosButton_Poll() is called")
            {
                mqosButton_Poll();
                THEN("Nothing shall happen because button1 has been disabled")
                {
                    REQUIRE(0 == mqos_getSystemTickCount_fake.call_count);
                    REQUIRE(0 == mqos_MessageSendLater_fake.call_count);
                }
            }
        }
    }


    //-------------------------------------------------------------------------

    GIVEN("A single active-low button is created on PORTA bit 2, and enabled while being pressed")
    {
        /* reset the allocated button count to zero */
        mqosButton_Init();

        /* reset the mock functions */
        RESET_FAKE(mqos_getSystemTickCount);
        RESET_FAKE(mqos_MessageSendLater);
        RESET_FAKE(mqosGpio_Read);

        /* allocate a single button */
        tMqosGpio_PortPin button1PortPin = {&PORTA, 2};
        tMqosButton_Button button1 = mqosButton_Create(button1PortPin, true);
        REQUIRE(NULL != button1);

        /* enable the button: while it is being pressed */
        mqosGpio_Read_fake.return_val = 0;
        mqosButton_EnableAll();
        /* expect button Gpio pin to be read */
        REQUIRE(1 == mqosGpio_Read_fake.call_count);
        /* expect button polling to be scheduled by sending an MQOS message */
        REQUIRE(1 == mqos_MessageSendLater_fake.call_count);
        REQUIRE(mqosConfig_MSG_POLL_BUTTONS == mqos_MessageSendLater_fake.arg0_val);
        REQUIRE(0 == mqos_MessageSendLater_fake.arg1_val.data);
        REQUIRE(EXPECTED_POLL_PERIOD_TICKS == mqos_MessageSendLater_fake.arg2_val);

        RESET_FAKE(mqosGpio_Read);
        RESET_FAKE(mqos_MessageSendLater);

        WHEN("The function mqosButton_IsPressed() is called")
        {
            bool isPressed = mqosButton_IsPressed(button1);
            THEN("It shall return true")
            {
                REQUIRE(isPressed);
            }
        }

        WHEN("The function mqosButton_Poll() is called repeatedly and a rising edge occurs on the Button Gpio pin at sample [2]")
        {
            tMqosConfig_UBaseType gpioReadSequenceRisingEdge[10] =
            {
                0, 0, 1, 1,     // mqosButton_IsPressed() is true
                1,              // BUTTON_RELEASED event occurs after three consecutive '1' values
                1, 1, 1, 1, 1   // mqosButton_IsPressed() is false
            };
            SET_RETURN_SEQ(mqosGpio_Read, gpioReadSequenceRisingEdge, 10);

            for(size_t i = 0; i < 10; i++)
            {
                /* call the UUT */
                mqosButton_Poll();

                /* BUTTON_RELEASED event occurs at sample [4] and the button remains released thereafter */ 
                if(4 > i)
                {
                    REQUIRE(mqosButton_IsPressed(button1));
                }
                else
                {
                    REQUIRE_FALSE(mqosButton_IsPressed(button1));
                }
            }

            THEN("A BUTTON_RELEASED event shall occur at poll [4]")
            {
                /* Check the mock function call history */
                REQUIRE(10 == mqosGpio_Read_fake.call_count);
                REQUIRE(0 == mqos_getSystemTickCount_fake.call_count);
                REQUIRE(11 == mqos_MessageSendLater_fake.call_count);

                /* check the sequence of MQOS messages that were sent */
                for(size_t i = 0; i < 11; i++)
                {
                    if(4 == i)
                    {
                        /* message [4] is a BUTTON_RELEASED event message */
                        REQUIRE(mqosConfig_MSG_BUTTON_RELEASED == mqos_MessageSendLater_fake.arg0_history[i]);
                        REQUIRE((tMqosConfig_MessagePayloadData) button1 == mqos_MessageSendLater_fake.arg1_history[i].data);
                        REQUIRE(0 == mqos_MessageSendLater_fake.arg2_history[i]);
                    }
                    else
                    {
                        /* all other messages are POLL_BUTTONS request messages */
                        REQUIRE(mqosConfig_MSG_POLL_BUTTONS == mqos_MessageSendLater_fake.arg0_history[i]);
                        REQUIRE(0 == mqos_MessageSendLater_fake.arg1_history[i].data);
                        REQUIRE(EXPECTED_POLL_PERIOD_TICKS == mqos_MessageSendLater_fake.arg2_history[i]);
                    }
                }
            }
        }
    }
}


//----------------------------------------------------------------------------


SCENARIO("Tests for a single button instance, active high polarity", "[mqosButton-ActiveHigh]")
{
    GIVEN("A single active-high button is created on PORTA bit 3, but not enabled")
    {
        /* reset the allocated button count to zero */
        mqosButton_Init();

        /* reset the mock functions */
        RESET_FAKE(mqos_getSystemTickCount);
        RESET_FAKE(mqos_MessageSendLater);

        /* allocate a single button */
        tMqosGpio_PortPin button1PortPin = {&PORTA, 3};
        tMqosButton_Button button1 = mqosButton_Create(button1PortPin, false);
        REQUIRE(NULL != button1);

        WHEN("The function mqosButton_IsPressed() is called")
        {
            bool isPressed = mqosButton_IsPressed(button1);
            THEN("It shall return false")
            {
                REQUIRE(false == isPressed);
            }
        }

        WHEN("The function mqosButton_Poll() is called")
        {
            mqosButton_Poll();
            THEN("Nothing shall happen because the button is disabled by default")
            {
                REQUIRE(0 == mqos_getSystemTickCount_fake.call_count);
                REQUIRE(0 == mqos_MessageSendLater_fake.call_count);
            }
        }
    }


    //-------------------------------------------------------------------------

    GIVEN("A single active-high button is created on PORTA bit 3, and enabled while not pressed")
    {
        /* reset the allocated button count to zero */
        mqosButton_Init();

        /* reset the mock functions */
        RESET_FAKE(mqos_getSystemTickCount);
        RESET_FAKE(mqos_MessageSendLater);
        RESET_FAKE(mqosGpio_Read);

        /* allocate a single button */
        tMqosGpio_PortPin button1PortPin = {&PORTA, 3};
        tMqosButton_Button button1 = mqosButton_Create(button1PortPin, false);
        REQUIRE(NULL != button1);

        /* enable the button: it is not currently being pressed */
        mqosGpio_Read_fake.return_val = 0;
        mqosButton_Enable(button1);
        /* expect button Gpio pin to be read */
        REQUIRE(1 == mqosGpio_Read_fake.call_count);
        /* expect button polling to be scheduled by sending an MQOS message */
        REQUIRE(1 == mqos_MessageSendLater_fake.call_count);
        REQUIRE(mqosConfig_MSG_POLL_BUTTONS == mqos_MessageSendLater_fake.arg0_val);
        REQUIRE(0 == mqos_MessageSendLater_fake.arg1_val.data);
        REQUIRE(EXPECTED_POLL_PERIOD_TICKS == mqos_MessageSendLater_fake.arg2_val);

        RESET_FAKE(mqosGpio_Read);
        RESET_FAKE(mqos_MessageSendLater);

        WHEN("The function mqosButton_IsPressed() is called")
        {
            bool isPressed = mqosButton_IsPressed(button1);
            THEN("It shall return false")
            {
                REQUIRE_FALSE(isPressed);
            }
        }

        WHEN("The function mqosButton_Poll() is called repeatedly and a rising edge occurs on the Button Gpio pin at sample [2]")
        {
            tMqosConfig_TimerCount expectedSystemTickSequence[7] = 
            {
                100,                    // tickCount when BUTTON_PRESSED event occurs: 100 ticks
                100, 105, 110, 1099,    // tickCount while button pressed for less than long press period
                1100,                   // tick count for first poll where button pressed for >= long press period
                5000                    // tick count is not read again after BUTTON_LONG_PRESS event has occurred
            };
            SET_RETURN_SEQ(mqos_getSystemTickCount, expectedSystemTickSequence, 7);

            tMqosConfig_UBaseType gpioReadSequenceRisingEdge[10] =
            {
                0, 0, 1, 1,     // mqosButton_IsPressed() is false
                1,              // BUTTON_PRESSED event occurs after three consecutive '1' values
                1, 1, 1, 1, 1   // mqosButton_IsPressed() is true
            };
            SET_RETURN_SEQ(mqosGpio_Read, gpioReadSequenceRisingEdge, 10);

            for(size_t i = 0; i < 10; i++)
            {
                /* call the UUT */
                mqosButton_Poll();

                /* BUTTON_PRESSED event occurs at sample [4] and the button remains pressed thereafter */ 
                if(4 > i)
                {
                    REQUIRE_FALSE(mqosButton_IsPressed(button1));
                }
                else
                {
                    REQUIRE(mqosButton_IsPressed(button1));
                }
            }

            THEN("A BUTTON_PRESSED event shall occur at poll [4] and a BUTTON_LONG_PRESS at poll [8]")
            {
                /* Check the mock function call history */
                REQUIRE(10 == mqosGpio_Read_fake.call_count);
                REQUIRE(6 == mqos_getSystemTickCount_fake.call_count);
                REQUIRE(12 == mqos_MessageSendLater_fake.call_count);

                /* check the sequence of MQOS messages that were sent */
                for(size_t i = 0; i < 12; i++)
                {
                    if(4 == i)
                    {
                        /* message [4] is a BUTTON_PRESSED event message */
                        REQUIRE(mqosConfig_MSG_BUTTON_PRESSED == mqos_MessageSendLater_fake.arg0_history[i]);
                        REQUIRE((tMqosConfig_MessagePayloadData) button1 == mqos_MessageSendLater_fake.arg1_history[i].data);
                        REQUIRE(0 == mqos_MessageSendLater_fake.arg2_history[i]);
                    }
                    else if(9 == i)
                    {
                        /* message [9] is a BUTTON_LONG_PRESS event message */
                        REQUIRE(mqosConfig_MSG_BUTTON_LONG_PRESS == mqos_MessageSendLater_fake.arg0_history[i]);
                        REQUIRE((tMqosConfig_MessagePayloadData) button1 == mqos_MessageSendLater_fake.arg1_history[i].data);
                        REQUIRE(0 == mqos_MessageSendLater_fake.arg2_history[i]);
                    }
                    else
                    {
                        /* all other messages are POLL_BUTTONS request messages */
                        REQUIRE(mqosConfig_MSG_POLL_BUTTONS == mqos_MessageSendLater_fake.arg0_history[i]);
                        REQUIRE(0 == mqos_MessageSendLater_fake.arg1_history[i].data);
                        REQUIRE(EXPECTED_POLL_PERIOD_TICKS == mqos_MessageSendLater_fake.arg2_history[i]);
                    }
                }
            }
        }
    }


    //-------------------------------------------------------------------------

    GIVEN("A single active-high button is created on PORTA bit 3, and enabled while being pressed")
    {
        /* reset the allocated button count to zero */
        mqosButton_Init();

        /* reset the mock functions */
        RESET_FAKE(mqos_getSystemTickCount);
        RESET_FAKE(mqos_MessageSendLater);
        RESET_FAKE(mqosGpio_Read);

        /* allocate a single button */
        tMqosGpio_PortPin button1PortPin = {&PORTA, 3};
        tMqosButton_Button button1 = mqosButton_Create(button1PortPin, false);
        REQUIRE(NULL != button1);

        /* enable the button: while it is being pressed */
        mqosGpio_Read_fake.return_val = 1;
        mqosButton_EnableAll();
        /* expect button Gpio pin to be read */
        REQUIRE(1 == mqosGpio_Read_fake.call_count);
        /* expect button polling to be scheduled by sending an MQOS message */
        REQUIRE(1 == mqos_MessageSendLater_fake.call_count);
        REQUIRE(mqosConfig_MSG_POLL_BUTTONS == mqos_MessageSendLater_fake.arg0_val);
        REQUIRE(0 == mqos_MessageSendLater_fake.arg1_val.data);
        REQUIRE(EXPECTED_POLL_PERIOD_TICKS == mqos_MessageSendLater_fake.arg2_val);

        RESET_FAKE(mqosGpio_Read);
        RESET_FAKE(mqos_MessageSendLater);

        WHEN("The function mqosButton_IsPressed() is called")
        {
            bool isPressed = mqosButton_IsPressed(button1);
            THEN("It shall return true")
            {
                REQUIRE(isPressed);
            }
        }

        WHEN("The function mqosButton_Poll() is called repeatedly and a falling edge occurs on the Button Gpio pin at sample [2]")
        {
            tMqosConfig_UBaseType gpioReadSequenceFallingEdge[10] =
            {
                1, 1, 0, 0,     // mqosButton_IsPressed() is true
                0,              // BUTTON_RELEASED event occurs after three consecutive '0' values
                0, 0, 0, 0, 0   // mqosButton_IsPressed() is false
            };
            SET_RETURN_SEQ(mqosGpio_Read, gpioReadSequenceFallingEdge, 10);

            for(size_t i = 0; i < 10; i++)
            {
                /* call the UUT */
                mqosButton_Poll();

                /* BUTTON_RELEASED event occurs at sample [4] and the button remains released thereafter */ 
                if(4 > i)
                {
                    REQUIRE(mqosButton_IsPressed(button1));
                }
                else
                {
                    REQUIRE_FALSE(mqosButton_IsPressed(button1));
                }
            }

            THEN("A BUTTON_RELEASED event shall occur at poll [4]")
            {
                /* Check the mock function call history */
                REQUIRE(10 == mqosGpio_Read_fake.call_count);
                REQUIRE(0 == mqos_getSystemTickCount_fake.call_count);
                REQUIRE(11 == mqos_MessageSendLater_fake.call_count);

                /* check the sequence of MQOS messages that were sent */
                for(size_t i = 0; i < 11; i++)
                {
                    if(4 == i)
                    {
                        /* message [4] is a BUTTON_RELEASED event message */
                        REQUIRE(mqosConfig_MSG_BUTTON_RELEASED == mqos_MessageSendLater_fake.arg0_history[i]);
                        REQUIRE((tMqosConfig_MessagePayloadData) button1 == mqos_MessageSendLater_fake.arg1_history[i].data);
                        REQUIRE(0 == mqos_MessageSendLater_fake.arg2_history[i]);
                    }
                    else
                    {
                        /* all other messages are POLL_BUTTONS request messages */
                        REQUIRE(mqosConfig_MSG_POLL_BUTTONS == mqos_MessageSendLater_fake.arg0_history[i]);
                        REQUIRE(0 == mqos_MessageSendLater_fake.arg1_history[i].data);
                        REQUIRE(EXPECTED_POLL_PERIOD_TICKS == mqos_MessageSendLater_fake.arg2_history[i]);
                    }
                }
            }
        }
    }
}
