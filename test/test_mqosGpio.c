/*
 * test_mqosGpio.c - mqosGpio module unit test source file.
 *
 * Copyright (C) 2021 Antony Bayley.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */


#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include "mqosGpio.h"
#include "catch.hpp"


/* Define a dummy IO port for unit tests */
static volatile tMqosConfig_UBaseType PORTA = 0;


SCENARIO("Tests for MQOS GPIO module's mqosGpio_Read function", "[mqosGpio-Read]")
{
    GIVEN("PORTA contains the value 0xAA: bit pattern 1, 0, 1, 0, 1, 0, 1, 0")
    {
        PORTA = 0xAA;

        WHEN("Port pin A0 is read")
        {
            tMqosGpio_PortPin A0 = {&PORTA, 0};
            tMqosConfig_UBaseType value = mqosGpio_Read(A0);
            THEN("The value 0 shall be returned.")
            {
                REQUIRE(0 == value);
            }
        }

        WHEN("Port pin A1 is read")
        {
            tMqosGpio_PortPin A1 = {&PORTA, 1};
            tMqosConfig_UBaseType value = mqosGpio_Read(A1);
            THEN("The value 1 shall be returned.")
            {
                REQUIRE(1 == value);
            }
        }

        WHEN("Port pin A2 is read")
        {
            tMqosGpio_PortPin A2 = {&PORTA, 2};
            tMqosConfig_UBaseType value = mqosGpio_Read(A2);
            THEN("The value 0 shall be returned.")
            {
                REQUIRE(0 == value);
            }
        }

        WHEN("Port pin A3 is read")
        {
            tMqosGpio_PortPin A3 = {&PORTA, 3};
            tMqosConfig_UBaseType value = mqosGpio_Read(A3);
            THEN("The value 1 shall be returned.")
            {
                REQUIRE(1 == value);
            }
        }
 
        WHEN("Port pin A4 is read")
        {
            tMqosGpio_PortPin A4 = {&PORTA, 4};
            tMqosConfig_UBaseType value = mqosGpio_Read(A4);
            THEN("The value 0 shall be returned.")
            {
                REQUIRE(0 == value);
            }
        }

        WHEN("Port pin A5 is read")
        {
            tMqosGpio_PortPin A5 = {&PORTA, 5};
            tMqosConfig_UBaseType value = mqosGpio_Read(A5);
            THEN("The value 1 shall be returned.")
            {
                REQUIRE(1 == value);
            }
        }

        WHEN("Port pin A6 is read")
        {
            tMqosGpio_PortPin A6 = {&PORTA, 6};
            tMqosConfig_UBaseType value = mqosGpio_Read(A6);
            THEN("The value 0 shall be returned.")
            {
                REQUIRE(0 == value);
            }
        }

        WHEN("Port pin A7 is read")
        {
            tMqosGpio_PortPin A7 = {&PORTA, 7};
            tMqosConfig_UBaseType value = mqosGpio_Read(A7);
            THEN("The value 1 shall be returned.")
            {
                REQUIRE(1 == value);
            }
        }
    }
}


//----------------------------------------------------------------------------


SCENARIO("Tests for MQOS GPIO module's mqosGpio_Write function", "[mqosGpio-Write]")
{
    GIVEN("PORTA contains the value 0x00")
    {
        PORTA = 0x00;

        WHEN("The value 1 is written to Port pin A0")
        {
            tMqosGpio_PortPin A0 = {&PORTA, 0};
            tMqosConfig_UBaseType value = 1;
            mqosGpio_Write(A0, value);
            THEN("Bit 0 of PORTA shall be set.")
            {
                REQUIRE((0x01U << 0) == PORTA);
            }
        }

        WHEN("The value 0xFF is written to Port pin A1")
        {
            tMqosGpio_PortPin A1 = {&PORTA, 1};
            tMqosConfig_UBaseType value = 0xFF;
            mqosGpio_Write(A1, value);
            THEN("Bit 1 of PORTA shall be set.")
            {
                REQUIRE((0x01U << 1) == PORTA);
            }
        }

        WHEN("The value 100 is written to Port pin A2")
        {
            tMqosGpio_PortPin A2 = {&PORTA, 2};
            tMqosConfig_UBaseType value = 100;
            mqosGpio_Write(A2, value);
            THEN("Bit 2 of PORTA shall be set.")
            {
                REQUIRE((0x01U << 2) == PORTA);
            }
        }

        WHEN("The value 1 is written to Port pin A3")
        {
            tMqosGpio_PortPin A3 = {&PORTA, 3};
            tMqosConfig_UBaseType value = 1;
            mqosGpio_Write(A3, value);
            THEN("Bit 3 of PORTA shall be set.")
            {
                REQUIRE((0x01U << 3) == PORTA);
            }
        }

        WHEN("The value 1 is written to Port pin A4")
        {
            tMqosGpio_PortPin A4 = {&PORTA, 4};
            tMqosConfig_UBaseType value = 1;
            mqosGpio_Write(A4, value);
            THEN("Bit 4 of PORTA shall be set.")
            {
                REQUIRE((0x01U << 4) == PORTA);
            }
        }

        WHEN("The value 1 is written to Port pin A5")
        {
            tMqosGpio_PortPin A5 = {&PORTA, 5};
            tMqosConfig_UBaseType value = 1;
            mqosGpio_Write(A5, value);
            THEN("Bit 5 of PORTA shall be set.")
            {
                REQUIRE((0x01U << 5) == PORTA);
            }
        }

        WHEN("The value 1 is written to Port pin A6")
        {
            tMqosGpio_PortPin A6 = {&PORTA, 6};
            tMqosConfig_UBaseType value = 1;
            mqosGpio_Write(A6, value);
            THEN("Bit 6 of PORTA shall be set.")
            {
                REQUIRE((0x01U << 6) == PORTA);
            }
        }

        WHEN("The value 1 is written to Port pin A7")
        {
            tMqosGpio_PortPin A7 = {&PORTA, 7};
            tMqosConfig_UBaseType value = 1;
            mqosGpio_Write(A7, value);
            THEN("Bit 7 of PORTA shall be set.")
            {
                REQUIRE((0x01U << 7) == PORTA);
            }
        }
    }

    GIVEN("PORTA contains the value 0xFF")
    {
        PORTA = 0xFF;

        WHEN("The value 0 is written to Port pin A0")
        {
            tMqosGpio_PortPin A0 = {&PORTA, 0};
            tMqosConfig_UBaseType value = 0;
            mqosGpio_Write(A0, value);
            THEN("Bit 0 of PORTA shall be cleared.")
            {
                tMqosConfig_UBaseType expectedValue = (tMqosConfig_UBaseType) ~(0x01U << 0);
                REQUIRE(expectedValue == PORTA);
            }
        }

        WHEN("The value 0 is written to Port pin A1")
        {
            tMqosGpio_PortPin A1 = {&PORTA, 1};
            tMqosConfig_UBaseType value = 0;
            mqosGpio_Write(A1, value);
            THEN("Bit 1 of PORTA shall be cleared.")
            {
                tMqosConfig_UBaseType expectedValue = (tMqosConfig_UBaseType) ~(0x01U << 1);
                REQUIRE(expectedValue == PORTA);
            }
        }

        WHEN("The value 0 is written to Port pin A2")
        {
            tMqosGpio_PortPin A2 = {&PORTA, 2};
            tMqosConfig_UBaseType value = 0;
            mqosGpio_Write(A2, value);
            THEN("Bit 2 of PORTA shall be cleared.")
            {
                tMqosConfig_UBaseType expectedValue = (tMqosConfig_UBaseType) ~(0x01U << 2);
                REQUIRE(expectedValue == PORTA);
            }
        }

        WHEN("The value 0 is written to Port pin A3")
        {
            tMqosGpio_PortPin A3 = {&PORTA, 3};
            tMqosConfig_UBaseType value = 0;
            mqosGpio_Write(A3, value);
            THEN("Bit 3 of PORTA shall be cleared.")
            {
                tMqosConfig_UBaseType expectedValue = (tMqosConfig_UBaseType) ~(0x01U << 3);
                REQUIRE(expectedValue == PORTA);
            }
        }

        WHEN("The value 0 is written to Port pin A4")
        {
            tMqosGpio_PortPin A4 = {&PORTA, 4};
            tMqosConfig_UBaseType value = 0;
            mqosGpio_Write(A4, value);
            THEN("Bit 4 of PORTA shall be cleared.")
            {
                tMqosConfig_UBaseType expectedValue = (tMqosConfig_UBaseType) ~(0x01U << 4);
                REQUIRE(expectedValue == PORTA);
            }
        }

        WHEN("The value 0 is written to Port pin A5")
        {
            tMqosGpio_PortPin A5 = {&PORTA, 5};
            tMqosConfig_UBaseType value = 0;
            mqosGpio_Write(A5, value);
            THEN("Bit 5 of PORTA shall be cleared.")
            {
                tMqosConfig_UBaseType expectedValue = (tMqosConfig_UBaseType) ~(0x01U << 5);
                REQUIRE(expectedValue == PORTA);
            }
        }

        WHEN("The value 0 is written to Port pin A6")
        {
            tMqosGpio_PortPin A6 = {&PORTA, 6};
            tMqosConfig_UBaseType value = 0;
            mqosGpio_Write(A6, value);
            THEN("Bit 6 of PORTA shall be cleared.")
            {
                tMqosConfig_UBaseType expectedValue = (tMqosConfig_UBaseType) ~(0x01U << 6);
                REQUIRE(expectedValue == PORTA);
            }
        }

        WHEN("The value 0 is written to Port pin A7")
        {
            tMqosGpio_PortPin A7 = {&PORTA, 7};
            tMqosConfig_UBaseType value = 0;
            mqosGpio_Write(A7, value);
            THEN("Bit 7 of PORTA shall be cleared.")
            {
                tMqosConfig_UBaseType expectedValue = (tMqosConfig_UBaseType) ~(0x01U << 7);
                REQUIRE(expectedValue == PORTA);
            }
        }
    }
}
