/*
 * test_mqosMessage.c - mqosMessage module unit test source file.
 *
 * Copyright (C) 2021 Antony Bayley.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <stdlib.h>
#include <string.h>
#include "mqosMessage.h"
#include "catch.hpp"


SCENARIO("MQOS message tests", "[mqosMessage]")
{
    GIVEN("No messages have been allocated from the heap")
    {
        /* reset the mqosMessage heap and message allocation statistics */
        mqosMessage_Reset();

        WHEN("mqosMessage_Malloc is called MQOS_CONFIG_HEAP_SIZE times")
        {
            for(tMqosConfig_MessageIndex i = 0; i < MQOS_CONFIG_HEAP_SIZE; i++)
            {
                /* UUT: call mqosMessage_Malloc() */
                tMqosMessageContainer* msg = mqosMessage_Malloc();

                char str[80];
                sprintf(str, "Message allocation shall succeed for message number %d", i + 1);
                THEN(str)
                {
                    REQUIRE(NULL != msg);
                    REQUIRE(0 == mqosMessage_Mallinfo().allocFaults);
                }

                THEN("The message allocation counts shall increment")
                {
                    REQUIRE((i + 1) == mqosMessage_Mallinfo().allocCount);
                    REQUIRE((i + 1) == mqosMessage_Mallinfo().maxAllocCount);
                }

                THEN("The message shall be located within the message heap")
                {
                    tMqosMessageContainer* heap = mqosMessage_GetHeapPointer();
                    /* allocated message address is above the bottom of the heap */
                    REQUIRE(&heap[0] <= msg);
                    /* allocated message address is below the top of the heap */
                    REQUIRE(&heap[MQOS_CONFIG_HEAP_SIZE] >= msg);
                }
            }
        }
    }


    //-------------------------------------------------------------------------

    GIVEN("All messages on the heap have been allocated")
    {
        /* allocate all messages on the heap */
        mqosMessage_Reset();
        tMqosMessageContainer* allocatedMsg[MQOS_CONFIG_HEAP_SIZE];
        for(tMqosConfig_MessageIndex i = 0; i < MQOS_CONFIG_HEAP_SIZE; i++)
        {
            allocatedMsg[i] = mqosMessage_Malloc();
        }

        REQUIRE(MQOS_CONFIG_HEAP_SIZE == mqosMessage_Mallinfo().allocCount);
        REQUIRE(MQOS_CONFIG_HEAP_SIZE == mqosMessage_Mallinfo().maxAllocCount);
        REQUIRE(0 == mqosMessage_Mallinfo().allocFaults);


        WHEN("mqosMessage_Free is called MQOS_CONFIG_HEAP_SIZE times")
        {
            for(tMqosConfig_MessageIndex i = 0; i < MQOS_CONFIG_HEAP_SIZE; i++)
            {
                /* UUT: call mqosMessage_Free() */
                mqosMessage_Free(allocatedMsg[i]);

                char str[80];
                sprintf(str, "After %d calls the message allocation count shall decrease to %d", (i + 1), MQOS_CONFIG_HEAP_SIZE - (i + 1));
                THEN(str)
                {
                    REQUIRE(MQOS_CONFIG_HEAP_SIZE - (i + 1) == mqosMessage_Mallinfo().allocCount);
                }

                THEN("The message allocation maximum counts counts shall remain unchanged")
                {
                    REQUIRE(MQOS_CONFIG_HEAP_SIZE == mqosMessage_Mallinfo().maxAllocCount);
                }
            }
        }


        WHEN("An attempt is made to allocate a new message")
        {
            /* UUT: call mqosMessage_Malloc() */
            tMqosMessageContainer* msg = mqosMessage_Malloc();

            THEN("mqosMessage_Malloc shall return NULL")
            {
                REQUIRE(NULL == msg);
            }

            THEN("The allocation faults count shall increment")
            {
                REQUIRE(1 == mqosMessage_Mallinfo().allocFaults);
            }

            THEN("The message allocation counts shall remain unchanged")
            {
                REQUIRE(MQOS_CONFIG_HEAP_SIZE == mqosMessage_Mallinfo().allocCount);
                REQUIRE(MQOS_CONFIG_HEAP_SIZE == mqosMessage_Mallinfo().maxAllocCount);
            }

            AND_WHEN("One message is freed")
            {
                /* UUT: call mqosMessage_Free() */
                mqosMessage_Free(allocatedMsg[0]);

                THEN("The message allocation count shall decrease by one")
                {
                    REQUIRE((MQOS_CONFIG_HEAP_SIZE - 1) == mqosMessage_Mallinfo().allocCount);
                }

                THEN("The allocation faults count and the maximum allocation count shall remain unchanged")
                {
                    REQUIRE(1 == mqosMessage_Mallinfo().allocFaults);
                    REQUIRE(MQOS_CONFIG_HEAP_SIZE == mqosMessage_Mallinfo().maxAllocCount);
                }
            }
        }
    }


    //-------------------------------------------------------------------------

    GIVEN("Two messages in the heap are unallocated")
    {
        /* allocate all messages on the heap, then free two of them */
        mqosMessage_Reset();
        tMqosMessageContainer* allocatedMsg[MQOS_CONFIG_HEAP_SIZE];
        for(tMqosConfig_MessageIndex i = 0; i < MQOS_CONFIG_HEAP_SIZE; i++)
        {
            allocatedMsg[i] = mqosMessage_Malloc();
        }
        mqosMessage_Free(allocatedMsg[0]);
        mqosMessage_Free(allocatedMsg[2]);

        REQUIRE((MQOS_CONFIG_HEAP_SIZE - 2) == mqosMessage_Mallinfo().allocCount);
        REQUIRE(MQOS_CONFIG_HEAP_SIZE == mqosMessage_Mallinfo().maxAllocCount);
        REQUIRE(0 == mqosMessage_Mallinfo().allocFaults);


        WHEN("mqosMessage_Malloc is called")
        {
            /* UUT: call mqosMessage_Malloc() */
            tMqosMessageContainer* msg = mqosMessage_Malloc();
            THEN("The first unallocated message pointer shall be returned")
            {
                REQUIRE(allocatedMsg[0] == msg);
                REQUIRE((MQOS_CONFIG_HEAP_SIZE - 1) == mqosMessage_Mallinfo().allocCount);
            }

           AND_WHEN("mqosMessage_Malloc is called a second time")
           {
                /* UUT: call mqosMessage_Malloc() */
                tMqosMessageContainer* msg = mqosMessage_Malloc();
                THEN("The second unallocated message pointer shall be returned")
                {
                    REQUIRE(allocatedMsg[2] == msg);
                    REQUIRE(MQOS_CONFIG_HEAP_SIZE == mqosMessage_Mallinfo().allocCount);
                }
           }
        }


        WHEN("An attempt is made to free a NULL pointer")
        {
            /* UUT: call mqosMessage_Free() with NULL pointer */
            mqosMessage_Free(NULL);

            THEN("No message shall be freed")
            {
                REQUIRE((MQOS_CONFIG_HEAP_SIZE - 2) == mqosMessage_Mallinfo().allocCount);
                REQUIRE(MQOS_CONFIG_HEAP_SIZE == mqosMessage_Mallinfo().maxAllocCount);
                REQUIRE(0 == mqosMessage_Mallinfo().allocFaults);
            }
        }


        WHEN("An attempt is made to free a message that has already been freed")
        {
            /* UUT: call mqosMessage_Free() with a pointer to a message that has already been freed */
            mqosMessage_Free(allocatedMsg[0]);

            THEN("No message shall be freed")
            {
                REQUIRE((MQOS_CONFIG_HEAP_SIZE - 2) == mqosMessage_Mallinfo().allocCount);
                REQUIRE(MQOS_CONFIG_HEAP_SIZE == mqosMessage_Mallinfo().maxAllocCount);
                REQUIRE(0 == mqosMessage_Mallinfo().allocFaults);
            }
        }
    }


    //-------------------------------------------------------------------------

    GIVEN("One message has been allocated and has a valid message ID and non-zero delay")
    {
        /* allocate one messages and assign message ID and delay values */
        mqosMessage_Reset();
        tMqosMessageContainer* allocatedMsg = mqosMessage_Malloc();
        allocatedMsg->msg.msgId = mqosConfig_MSG_TEST1;
        allocatedMsg->delay = 100;

        REQUIRE(mqosConfig_MSG_NONE != allocatedMsg->msg.msgId);
        REQUIRE(0 != allocatedMsg->delay);
        REQUIRE(1 == mqosMessage_Mallinfo().allocCount);
        REQUIRE(1 == mqosMessage_Mallinfo().maxAllocCount);
        REQUIRE(0 == mqosMessage_Mallinfo().allocFaults);


        WHEN("mqosMessage_Free is called")
        {
            /* UUT: call mqosMessage_Free() */
            mqosMessage_Free(allocatedMsg);
            THEN("The message ID shall be set to mqosConfig_MSG_NONE to mark the message container as free")
            {
                REQUIRE(mqosConfig_MSG_NONE == allocatedMsg->msg.msgId);
            }

            THEN("The delay shall be set to zero, to prevent the scheduler wasting time decrementing inactive messae delays")
            {
                REQUIRE(0 == allocatedMsg->delay);
            }

            THEN("The message allocation statistics shall show that the message has been freed")
            {
                REQUIRE(0 == mqosMessage_Mallinfo().allocCount);
                REQUIRE(1 == mqosMessage_Mallinfo().maxAllocCount);
                REQUIRE(0 == mqosMessage_Mallinfo().allocFaults);
            }
        }
    }
}
