/*
 * test_mqosCli.c - mqosCli module unit test source file.
 *
 * Copyright (C) 2021 Antony Bayley.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */


#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include "mqosCli.h"
#include "mqosCliCommands.h"
#include "catch.hpp"


/* MACROS ********************************************************************/

#define TEST_CBUFFER_SIZE           10  //!< Circular buffer size for simple tests.
#define COMMAND_TEST_CBUFFER_SIZE   70  //!< Circular buffer size for command tests.


/* PRIVATE VARIABLE DEFINITIONS **********************************************/

/* define the circular buffers to be be used for mqosCli unit tests */
MQOS_CBUFFER_DEFINE(cbuf, TEST_CBUFFER_SIZE);
MQOS_CBUFFER_DEFINE(inCbuf, COMMAND_TEST_CBUFFER_SIZE);
MQOS_CBUFFER_DEFINE(outCbuf, COMMAND_TEST_CBUFFER_SIZE);


/* PRIVATE FUNCTION DEFINITIONS **********************************************/


/* PUBLIC FUNCTION DEFINITIONS ***********************************************/

SCENARIO("mqosCliCommands_NumberOfCommands() function tests", "[mqosCliCommands_NumberOfCommands]")
{
    /* The number of CLI commands defined in mqosCliCommand_cmdDefinition[] */
    REQUIRE(4 == mqosCliCommands_NumberOfCommands());
}


//-----------------------------------------------------------------------------


SCENARIO("mqosCliCommands_GetCmdDefinition() function tests", "[mqosCliCommands_GetCmdDefinition]")
{
    /* The number of CLI commands defined in mqosCliCommand_cmdDefinition[] */
    tMqosConfig_UBaseType numCommands = mqosCliCommands_NumberOfCommands();

    WHEN("mqosCliCommands_GetCmdDefinition() is called with index < numCommands")
    {
        tMqosConfig_UBaseType index = numCommands - 1;
        const tMqosCli_CmdDefinition* cmd = mqosCliCommands_GetCmdDefinition(index);
        THEN("The function shall return a valid command definition pointer")
        {
            REQUIRE(NULL != cmd);
        }
    }

    WHEN("mqosCliCommands_GetCmdDefinition() is called with index out of range (index == numCommands)")
    {
        tMqosConfig_UBaseType index = numCommands;
        const tMqosCli_CmdDefinition* cmd = mqosCliCommands_GetCmdDefinition(index);
        THEN("The function shall return NULL")
        {
            REQUIRE(NULL == cmd);
        }
    }
}


//-----------------------------------------------------------------------------


SCENARIO("mqosCli_CharIsTokenDelimiter() function tests", "[mqosCli_CharIsTokenDelimiter]")
{
    GIVEN("A specified set of delimiter characters are used")
    {
        const char* delimiters = " \n";

        WHEN("mqosCli_CharIsTokenDelimiter() is called with delimiter character ' '")
        {
            REQUIRE(mqosCli_CharIsTokenDelimiter(' ', delimiters));
        }

        WHEN("mqosCli_CharIsTokenDelimiter() is called with delimiter character '\n'")
        {
            REQUIRE(mqosCli_CharIsTokenDelimiter('\n', delimiters));
        }

        WHEN("mqosCli_CharIsTokenDelimiter() is called with non-delimiter character '\r'")
        {
            REQUIRE_FALSE(mqosCli_CharIsTokenDelimiter('\r', delimiters));
        }

        WHEN("mqosCli_CharIsTokenDelimiter() is called with printable non-delimiter character 'z'")
        {
            REQUIRE_FALSE(mqosCli_CharIsTokenDelimiter('z', delimiters));
        }
    }


    GIVEN("The delimiter parameter is an empty string")
    {
        const char* delimiters = "";    /* use default token delimiters */

        WHEN("mqosCli_CharIsTokenDelimiter() is called with default delimiter character ' '")
        {
            REQUIRE(mqosCli_CharIsTokenDelimiter(' ', delimiters));
        }

        WHEN("mqosCli_CharIsTokenDelimiter() is called with default delimiter character '\n'")
        {
            REQUIRE(mqosCli_CharIsTokenDelimiter('\n', delimiters));
        }

        WHEN("mqosCli_CharIsTokenDelimiter() is called with default delimiter character '\r'")
        {
            REQUIRE(mqosCli_CharIsTokenDelimiter('\r', delimiters));
        }

        WHEN("mqosCli_CharIsTokenDelimiter() is called with default delimiter character '\t'")
        {
            REQUIRE(mqosCli_CharIsTokenDelimiter('\t', delimiters));
        }

        WHEN("mqosCli_CharIsTokenDelimiter() is called with default delimiter character '\f'")
        {
            REQUIRE(mqosCli_CharIsTokenDelimiter('\f', delimiters));
        }

        WHEN("mqosCli_CharIsTokenDelimiter() is called with default delimiter character '\v'")
        {
            REQUIRE(mqosCli_CharIsTokenDelimiter('\v', delimiters));
        }

        WHEN("mqosCli_CharIsTokenDelimiter() is called with null character '\0'")
        {
            REQUIRE(mqosCli_CharIsTokenDelimiter('\0', delimiters));
        }

        WHEN("mqosCli_CharIsTokenDelimiter() is called with printable character 'A'")
        {
            REQUIRE_FALSE(mqosCli_CharIsTokenDelimiter('A', delimiters));
        }

        WHEN("mqosCli_CharIsTokenDelimiter() is called with non-printing character 7 (BEL)")
        {
            REQUIRE_FALSE(mqosCli_CharIsTokenDelimiter(7, delimiters));
        }
    }


    GIVEN("The delimiter parameter is NULL")
    {
        const char* delimiters = NULL;  /* use default token delimiters */

        WHEN("mqosCli_CharIsTokenDelimiter() is called with default delimiter character ' '")
        {
            REQUIRE(mqosCli_CharIsTokenDelimiter(' ', delimiters));
        }

        WHEN("mqosCli_CharIsTokenDelimiter() is called with default delimiter character '\n'")
        {
            REQUIRE(mqosCli_CharIsTokenDelimiter('\n', delimiters));
        }

        WHEN("mqosCli_CharIsTokenDelimiter() is called with default delimiter character '\r'")
        {
            REQUIRE(mqosCli_CharIsTokenDelimiter('\r', delimiters));
        }

        WHEN("mqosCli_CharIsTokenDelimiter() is called with default delimiter character '\t'")
        {
            REQUIRE(mqosCli_CharIsTokenDelimiter('\t', delimiters));
        }

        WHEN("mqosCli_CharIsTokenDelimiter() is called with default delimiter character '\f'")
        {
            REQUIRE(mqosCli_CharIsTokenDelimiter('\f', delimiters));
        }

        WHEN("mqosCli_CharIsTokenDelimiter() is called with default delimiter character '\v'")
        {
            REQUIRE(mqosCli_CharIsTokenDelimiter('\v', delimiters));
        }

        WHEN("mqosCli_CharIsTokenDelimiter() is called with null character '\0'")
        {
            REQUIRE(mqosCli_CharIsTokenDelimiter('\0', delimiters));
        }

        WHEN("mqosCli_CharIsTokenDelimiter() is called with printable character 'A'")
        {
            REQUIRE_FALSE(mqosCli_CharIsTokenDelimiter('A', delimiters));
        }

        WHEN("mqosCli_CharIsTokenDelimiter() is called with non-printing character 7 (BEL)")
        {
            REQUIRE_FALSE(mqosCli_CharIsTokenDelimiter(7, delimiters));
        }
    }
}


//-----------------------------------------------------------------------------


SCENARIO("mqosCli_NextTokenOrDelimiter() function tests", "[mqosCli_NextTokenOrDelimiter]")
{
    GIVEN("The Cbuffer contains three tokens and three delimiters")
    {
        mqosCbuffer_Reset(&cbuf);

        /* The 10 character test string "T1\nX\r\n\0 Y\n" must be written in
         * three separate operations, because the mqosCbuffer_StrcpyToCbuffer()
         * function cannot write the null character (\0).
         */
        mqosCbuffer_StrcpyToCbuffer(&cbuf, "T1\nX\r\n");  // first 6 characters
        mqosCbuffer_Put(&cbuf, 0);                        // null character '\0'
        mqosCbuffer_StrcpyToCbuffer(&cbuf, " Y\n");       // final 3 characters

        /* parser index is used iteratively during each test */
        tMqosCbufferIndex parserIndex = 0;

        //---------------------------------------------------------------------

        WHEN("The first token is read using default delimiters")
        {
            tMqosCliToken token = mqosCli_NextTokenOrDelimiter(&cbuf, parserIndex, true, NULL);
            THEN("The returned token shall have index = 0 and length = 2")
            {
                REQUIRE(0 == token.index);
                REQUIRE(2 == token.len);
            }

            THEN("The returned token string shall be \"T1\"")
            {
                REQUIRE(0 == mqosCbuffer_Strncmp(&cbuf, token.index, "T1", token.len));
            }

            /* resume parsing at the character after the current token */
            parserIndex = token.index + token.len;

            AND_WHEN("The second token is read using default delimiters")
            {
                tMqosCliToken token = mqosCli_NextTokenOrDelimiter(&cbuf, parserIndex, true, "");  // alternative delimiter specifier: "" instead of NULL
                THEN("The returned token shall have index = 3 and length = 1")
                {
                    REQUIRE(3 == token.index);
                    REQUIRE(1 == token.len);
                }

                THEN("The returned token string shall be \"X\"")
                {
                    REQUIRE(0 == mqosCbuffer_Strncmp(&cbuf, token.index, "X", token.len));
                }

                /* resume parsing at the character after the current token */
                parserIndex = token.index + token.len;

                AND_WHEN("The third token is read using default delimiters")
                {
                    tMqosCliToken token = mqosCli_NextTokenOrDelimiter(&cbuf, parserIndex, true, NULL);
                    THEN("The returned token shall have index = 8 and length = 1")
                    {
                        REQUIRE(8 == token.index);
                        REQUIRE(1 == token.len);
                    }

                    THEN("The returned token string shall be \"Y\"")
                    {
                        REQUIRE(0 == mqosCbuffer_Strncmp(&cbuf, token.index, "Y", token.len));
                    }

                    /* resume parsing at the character after the current token */
                    parserIndex = token.index + token.len;

                    AND_WHEN("The fourth token is read using default delimiters")
                    {
                        tMqosCliToken token = mqosCli_NextTokenOrDelimiter(&cbuf, parserIndex, true, NULL);
                        THEN("The returned token shall have index = MQOS_CBUFFER_NOT_FOUND and length = 0")
                        {
                            REQUIRE(MQOS_CBUFFER_NOT_FOUND == token.index);
                            REQUIRE(0 == token.len);
                        }
                    }
                }
            }
        }

        //---------------------------------------------------------------------

        WHEN("The first delimiter block is read using default delimiters")
        {
            tMqosCliToken token = mqosCli_NextTokenOrDelimiter(&cbuf, parserIndex, false, NULL);
            THEN("The returned delimiter block shall have index = 2 and length = 1")
            {
                REQUIRE(2 == token.index);
                REQUIRE(1 == token.len);
            }

            /* resume parsing at the character after the current delimiter block */
            parserIndex = token.index + token.len;

            AND_WHEN("The second delimiter block is read using default delimiters")
            {
                tMqosCliToken token = mqosCli_NextTokenOrDelimiter(&cbuf, parserIndex, false, "");  // alternative delimiter specifier: "" instead of NULL
                THEN("The returned delimiter block shall have index = 4 and length = 4")
                {
                    REQUIRE(4 == token.index);
                    REQUIRE(4 == token.len);
                }

                /* resume parsing at the character after the current delimiter block */
                parserIndex = token.index + token.len;

                AND_WHEN("The third delimiter block is read using default delimiters")
                {
                    tMqosCliToken token = mqosCli_NextTokenOrDelimiter(&cbuf, parserIndex, false, NULL);
                    THEN("The returned delimiter block shall have index = 9 and length = 1")
                    {
                        REQUIRE(9 == token.index);
                        REQUIRE(1 == token.len);
                    }

                    /* resume parsing at the character after the current delimiter block */
                    parserIndex = token.index + token.len;

                    AND_WHEN("The fourth delimiter block is read using default delimiters")
                    {
                        tMqosCliToken token = mqosCli_NextTokenOrDelimiter(&cbuf, parserIndex, false, NULL);
                        THEN("The returned token shall have index = MQOS_CBUFFER_NOT_FOUND and length = 0")
                        {
                            REQUIRE(MQOS_CBUFFER_NOT_FOUND == token.index);
                            REQUIRE(0 == token.len);
                        }
                    }
                }
            }
        }

        //---------------------------------------------------------------------

        WHEN("The first token is read with user-specified delimiters")
        {
            tMqosCliToken token = mqosCli_NextTokenOrDelimiter(&cbuf, parserIndex, true, "\n");
            THEN("The returned token shall have index = 0 and length = 2")
            {
                REQUIRE(0 == token.index);
                REQUIRE(2 == token.len);
            }

            THEN("The returned token string shall be \"T1\"")
            {
                REQUIRE(0 == mqosCbuffer_Strncmp(&cbuf, token.index, "T1", token.len));
            }

            /* resume parsing at the character after the current token */
            parserIndex = token.index + token.len;

            AND_WHEN("The second token is read with user-specified delimiters")
            {
                tMqosCliToken token = mqosCli_NextTokenOrDelimiter(&cbuf, parserIndex, true, "\n");
                THEN("The returned token shall have index = 3 and length = 2")
                {
                    REQUIRE(3 == token.index);
                    REQUIRE(2 == token.len);
                }

                THEN("The returned token string shall be \"X\r\"")
                {
                    REQUIRE(0 == mqosCbuffer_Strncmp(&cbuf, token.index, "X\r", token.len));
                }

                /* resume parsing at the character after the current token */
                parserIndex = token.index + token.len;

                AND_WHEN("The third token is read with user-specified delimiters")
                {
                    tMqosCliToken token = mqosCli_NextTokenOrDelimiter(&cbuf, parserIndex, true, "\n");
                    THEN("The returned token shall have index = 7 and length = 2")
                    {
                        REQUIRE(7 == token.index);
                        REQUIRE(2 == token.len);
                    }

                    THEN("The returned token string shall be \" Y\"")
                    {
                        REQUIRE(0 == mqosCbuffer_Strncmp(&cbuf, token.index, " Y", token.len));
                    }

                    /* resume parsing at the character after the current token */
                    parserIndex = token.index + token.len;

                    AND_WHEN("The fourth token is read with user-specified delimiters")
                    {
                        tMqosCliToken token = mqosCli_NextTokenOrDelimiter(&cbuf, parserIndex, true, "\n");
                        THEN("The returned token shall have index = MQOS_CBUFFER_NOT_FOUND and length = 0")
                        {
                            REQUIRE(MQOS_CBUFFER_NOT_FOUND == token.index);
                            REQUIRE(0 == token.len);
                        }
                    }
                }
            }
        }
    }

    //-------------------------------------------------------------------------

#if 0   /* Disable tests for command strings containing backspace character */

    GIVEN("The token in the Cbuffer contains a backspace character in the middle")
    {
        mqosCbuffer_Reset(&cbuf);
        mqosCbuffer_StrcpyToCbuffer(&cbuf, "HellX\bo\n");
        tMqosCliToken token = mqosCli_NextTokenOrDelimiter(&cbuf, 0, true, NULL);

        WHEN("A string is compared with the token using mqosCbuffer_Strncmp()")
        {
            THEN("The backspace in the token shall erase the preceding character in the token")
            {
                REQUIRE(0 == mqosCbuffer_Strncmp(&cbuf, token.index, "Hello", token.len));
            }
        }
    }

    //-------------------------------------------------------------------------

    GIVEN("The token in the Cbuffer contains two consecutive backspace characters in the middle")
    {
        mqosCbuffer_Reset(&cbuf);
        mqosCbuffer_StrcpyToCbuffer(&cbuf, "HellXY\b\bo");
        tMqosCliToken token = mqosCli_NextTokenOrDelimiter(&cbuf, 0, true, NULL);

        WHEN("A string is compared with the token using mqosCbuffer_Strncmp()")
        {
            THEN("The two backspaces in the token shall erase the preceding two characters in the token")
            {
                REQUIRE(0 == mqosCbuffer_Strncmp(&cbuf, token.index, "Hello", token.len));
            }
        }
    }

    //-------------------------------------------------------------------------

    GIVEN("The token in the Cbuffer contains two consecutive backspace characters at the start")
    {
        mqosCbuffer_Reset(&cbuf);
        mqosCbuffer_StrcpyToCbuffer(&cbuf, "\b\bHello\n");
        tMqosCliToken token = mqosCli_NextTokenOrDelimiter(&cbuf, 0, true, NULL);

        WHEN("A string is compared with the token using mqosCbuffer_Strncmp()")
        {
            THEN("The two backspaces in the token shall be ignored")
            {
                REQUIRE(0 == mqosCbuffer_Strncmp(&cbuf, token.index, "Hello", token.len));
            }
        }
    }

    //-------------------------------------------------------------------------

    GIVEN("The token in the Cbuffer contains two consecutive backspace characters at the end")
    {
        mqosCbuffer_Reset(&cbuf);
        mqosCbuffer_StrcpyToCbuffer(&cbuf, "HelloXY\b\b");
        tMqosCliToken token = mqosCli_NextTokenOrDelimiter(&cbuf, 0, true, NULL);

        WHEN("A string is compared with the token using mqosCbuffer_Strncmp()")
        {
            THEN("The two backspaces in the token shall erase the preceding two characters in the token")
            {
                REQUIRE(0 == mqosCbuffer_Strncmp(&cbuf, token.index, "Hello", token.len));
                REQUIRE(0 != mqosCbuffer_Strncmp(&cbuf, token.index, "HelloXY", token.len));
            }
        }
    }

#endif  /* Disable tests for command strings containing backspace character */

}


//-----------------------------------------------------------------------------


SCENARIO("mqosCli_WriteIntToCbuffer() function tests", "[mqosCli_WriteIntToCbuffer]")
{
    GIVEN("The circular buffer outCbuf is empty")
    {
        mqosCbuffer_Reset(&outCbuf);

        WHEN("mqosCli_WriteIntToCbuffer() is called with integer value 123")
        {
            int32_t value = 123;
            tMqosConfig_BaseType result = mqosCli_WriteIntToCbuffer(&outCbuf, value);

            THEN("mqosCli_InvokeCommand() shall return MQOS_CLI_OK")
            {
                REQUIRE(MQOS_CLI_OK == result);
            }

            THEN("The circular buffer shall contain the numeric string \"123\".")
            {
                const char* expectedOutput = "123";
                tMqosCbufferIndex expectedLength = (tMqosCbufferIndex) strlen(expectedOutput);
                REQUIRE(expectedLength == mqosCbuffer_Length(&outCbuf));
                REQUIRE(0 == mqosCbuffer_Strncmp(&outCbuf, 0, expectedOutput, expectedLength));
            }
        }

        //---------------------------------------------------------------------

        WHEN("mqosCli_WriteIntToCbuffer() is called with integer value -45678")
        {
            int32_t value = -45678;
            tMqosConfig_BaseType result = mqosCli_WriteIntToCbuffer(&outCbuf, value);

            THEN("mqosCli_InvokeCommand() shall return MQOS_CLI_OK")
            {
                REQUIRE(MQOS_CLI_OK == result);
            }

            THEN("The circular buffer shall contain the numeric string \"-45678\".")
            {
                const char* expectedOutput = "-45678";
                tMqosCbufferIndex expectedLength = (tMqosCbufferIndex) strlen(expectedOutput);
                REQUIRE(expectedLength == mqosCbuffer_Length(&outCbuf));
                REQUIRE(0 == mqosCbuffer_Strncmp(&outCbuf, 0, expectedOutput, expectedLength));
            }
        }

        //---------------------------------------------------------------------

        WHEN("mqosCli_WriteIntToCbuffer() is called with integer value 0")
        {
            int32_t value = 0;
            tMqosConfig_BaseType result = mqosCli_WriteIntToCbuffer(&outCbuf, value);

            THEN("mqosCli_InvokeCommand() shall return MQOS_CLI_OK")
            {
                REQUIRE(MQOS_CLI_OK == result);
            }

            THEN("The circular buffer shall contain the numeric string \"0\".")
            {
                const char* expectedOutput = "0";
                tMqosCbufferIndex expectedLength = (tMqosCbufferIndex) strlen(expectedOutput);
                REQUIRE(expectedLength == mqosCbuffer_Length(&outCbuf));
                REQUIRE(0 == mqosCbuffer_Strncmp(&outCbuf, 0, expectedOutput, expectedLength));
            }
        }

        //---------------------------------------------------------------------

        WHEN("mqosCli_WriteIntToCbuffer() is called with the largest positive integer value")
        {
            int32_t value = 0x7FFFFFFF;
            tMqosConfig_BaseType result = mqosCli_WriteIntToCbuffer(&outCbuf, value);

            THEN("mqosCli_InvokeCommand() shall return MQOS_CLI_OK")
            {
                REQUIRE(MQOS_CLI_OK == result);
            }

            THEN("The circular buffer shall contain the numeric string \"2147483647\".")
            {
                const char* expectedOutput = "2147483647";
                tMqosCbufferIndex expectedLength = (tMqosCbufferIndex) strlen(expectedOutput);
                REQUIRE(expectedLength == mqosCbuffer_Length(&outCbuf));
                REQUIRE(0 == mqosCbuffer_Strncmp(&outCbuf, 0, expectedOutput, expectedLength));
            }
        }

        //---------------------------------------------------------------------

        WHEN("mqosCli_WriteIntToCbuffer() is called with the largest negative integer value")
        {
            int32_t value = 0x80000000;
            tMqosConfig_BaseType result = mqosCli_WriteIntToCbuffer(&outCbuf, value);

            THEN("mqosCli_InvokeCommand() shall return MQOS_CLI_OK")
            {
                REQUIRE(MQOS_CLI_OK == result);
            }

            THEN("The circular buffer shall contain the numeric string \"-2147483648\".")
            {
                const char* expectedOutput = "-2147483648";
                tMqosCbufferIndex expectedLength = (tMqosCbufferIndex) strlen(expectedOutput);
                REQUIRE(expectedLength == mqosCbuffer_Length(&outCbuf));
                REQUIRE(0 == mqosCbuffer_Strncmp(&outCbuf, 0, expectedOutput, expectedLength));
            }
        }
    }


    //-------------------------------------------------------------------------


    GIVEN("The circular buffer cbuf is is nearly full and has only 5 bytes of space left")
    {
        mqosCbuffer_Reset(&cbuf);
        tMqosCbufferIndex index = 0;
        while(5 < mqosCbuffer_Space(&cbuf))
        {
            mqosCbuffer_Put(&cbuf, 'X');
            index += 1;
        }

        WHEN("mqosCli_WriteIntToCbuffer() is called with integer value 123456")
        {
            int32_t value = 123456;
            tMqosConfig_BaseType result = mqosCli_WriteIntToCbuffer(&cbuf, value);

            THEN("mqosCli_InvokeCommand() shall return MQOS_CLI_INSUFFICIENT_SPACE")
            {
                REQUIRE(MQOS_CLI_INSUFFICIENT_SPACE == result);
            }

            THEN("The circular buffer shall be full")
            {
                REQUIRE(mqosCbuffer_IsFull(&cbuf));
            }

            THEN("The circular buffer shall contain the truncated numeric string \"12345\"")
            {
                const char* expectedOutput = "12345";
                tMqosCbufferIndex expectedLength = TEST_CBUFFER_SIZE;
                REQUIRE(expectedLength == mqosCbuffer_Length(&cbuf));
                REQUIRE(0 == mqosCbuffer_Strncmp(&cbuf, index, expectedOutput, expectedLength - index));
            }
        }

        //---------------------------------------------------------------------

        WHEN("mqosCli_WriteIntToCbuffer() is called with integer value -123456")
        {
            int32_t value = -123456;
            tMqosConfig_BaseType result = mqosCli_WriteIntToCbuffer(&cbuf, value);

            THEN("mqosCli_InvokeCommand() shall return MQOS_CLI_INSUFFICIENT_SPACE")
            {
                REQUIRE(MQOS_CLI_INSUFFICIENT_SPACE == result);
            }

            THEN("The circular buffer shall be full")
            {
                REQUIRE(mqosCbuffer_IsFull(&cbuf));
            }

            THEN("The circular buffer shall contain the truncated numeric string \"1234\"")
            {
                const char* expectedOutput = "-1234";
                tMqosCbufferIndex expectedLength = TEST_CBUFFER_SIZE;
                REQUIRE(expectedLength == mqosCbuffer_Length(&cbuf));
                REQUIRE(0 == mqosCbuffer_Strncmp(&cbuf, index, expectedOutput, expectedLength - index));
            }
        }

        //---------------------------------------------------------------------

        WHEN("mqosCli_WriteIntToCbuffer() is called with the largest negative integer value")
        {
            int32_t value = 0x80000000;
            tMqosConfig_BaseType result = mqosCli_WriteIntToCbuffer(&cbuf, value);

            THEN("mqosCli_InvokeCommand() shall return MQOS_CLI_INSUFFICIENT_SPACE")
            {
                REQUIRE(MQOS_CLI_INSUFFICIENT_SPACE == result);
            }

            THEN("The circular buffer shall be full")
            {
                REQUIRE(mqosCbuffer_IsFull(&cbuf));
            }

            THEN("The circular buffer shall contain the truncated numeric string \"-2147\"")
            {
                const char* expectedOutput = "-2147";
                tMqosCbufferIndex expectedLength = TEST_CBUFFER_SIZE;
                REQUIRE(expectedLength == mqosCbuffer_Length(&cbuf));
                REQUIRE(0 == mqosCbuffer_Strncmp(&cbuf, index, expectedOutput, expectedLength - index));
            }
        }
    }


    //-------------------------------------------------------------------------


    GIVEN("The circular buffer cbuf is full")
    {
        mqosCbuffer_Reset(&cbuf);
        while(!mqosCbuffer_IsFull(&cbuf))
        {
            mqosCbuffer_Put(&cbuf, 'X');
        }

        WHEN("mqosCli_WriteIntToCbuffer() is called with integer value 0")
        {
            int32_t value = 0;
            tMqosConfig_BaseType result = mqosCli_WriteIntToCbuffer(&cbuf, value);

            THEN("mqosCli_InvokeCommand() shall return MQOS_CLI_INSUFFICIENT_SPACE")
            {
                REQUIRE(MQOS_CLI_INSUFFICIENT_SPACE == result);
            }

            THEN("The circular buffer shall be full")
            {
                REQUIRE(mqosCbuffer_IsFull(&cbuf));
            }
        }
    }
}


//-----------------------------------------------------------------------------


SCENARIO("mqosCli_InvokeCommand() function tests", "[mqosCli_InvokeCommand]")
{
    GIVEN("inCbuf contains an echo command and outCbuf is empty")
    {
        mqosCbuffer_Reset(&inCbuf);
        mqosCbuffer_Reset(&outCbuf);
        mqosCbuffer_StrcpyToCbuffer(&inCbuf, "echo string to be echoed.\n");

        WHEN("mqosCli_InvokeCommand() is called")
        {
            tMqosConfig_BaseType result = mqosCli_InvokeCommand(&inCbuf, &outCbuf);

            THEN("mqosCli_InvokeCommand() shall return MQOS_CLI_OK")
            {
                REQUIRE(MQOS_CLI_OK == result);
            }

            THEN("The echo command's parameters shall be written to outCbuf")
            {
                const char* expectedOutput = "string to be echoed.\n";
                tMqosCbufferIndex expectedLength = (tMqosCbufferIndex) strlen(expectedOutput);
                REQUIRE(expectedLength == mqosCbuffer_Length(&outCbuf));
                REQUIRE(0 == mqosCbuffer_Strncmp(&outCbuf, 0, expectedOutput, expectedLength));
            }

            THEN("The echo command line shall be deleted from inCbuf after it has been processed")
            {
                REQUIRE(mqosCbuffer_IsEmpty(&inCbuf));
            }
        }
    }


    GIVEN("inCbuf contains an echo command and outCbuf has insufficient space for echo")
    {
        mqosCbuffer_Reset(&inCbuf);
        mqosCbuffer_Reset(&outCbuf);
        mqosCbuffer_StrcpyToCbuffer(&inCbuf, "echo string to be echoed.\n");

        const char* expectedOutput = "string to b";
        tMqosCbufferIndex expectedLength = (tMqosCbufferIndex) strlen(expectedOutput);
        tMqosCbufferIndex paddingBytesCount = mqosCbuffer_Space(&outCbuf) - expectedLength;
        for(tMqosCbufferIndex i = 0; i < paddingBytesCount; i++)
        {
            REQUIRE(MQOS_CBUFFER_OK == mqosCbuffer_Put(&outCbuf, 'X'));
        }

        /* verify that there is insufficient free space in outCbuf */
        const char* expectedEchoStr = "string to be echoed.\n";
        REQUIRE(mqosCbuffer_Space(&outCbuf) < strlen(expectedEchoStr));

        WHEN("mqosCli_InvokeCommand() is called")
        {
            tMqosConfig_BaseType result = mqosCli_InvokeCommand(&inCbuf, &outCbuf);

            THEN("mqosCli_InvokeCommand() shall return MQOS_CLI_INSUFFICIENT_SPACE")
            {
                REQUIRE(MQOS_CLI_INSUFFICIENT_SPACE == result);
            }

            THEN("Characters shall have been copied from inCbuf to outCbuf until outCbuf is full")
            {
                REQUIRE(mqosCbuffer_IsFull(&outCbuf));
                REQUIRE(0 == mqosCbuffer_Strncmp(&outCbuf, paddingBytesCount, expectedOutput, expectedLength));
            }
        }
    }


    GIVEN("inCbuf contains an echo command with no parameters")
    {
        mqosCbuffer_Reset(&inCbuf);
        mqosCbuffer_Reset(&outCbuf);
        mqosCbuffer_StrcpyToCbuffer(&inCbuf, "echo \r\n");

        WHEN("mqosCli_InvokeCommand() is called")
        {
            tMqosConfig_BaseType result = mqosCli_InvokeCommand(&inCbuf, &outCbuf);

            THEN("mqosCli_InvokeCommand() shall return MQOS_CLI_TOKEN_NOT_FOUND")
            {
                REQUIRE(MQOS_CLI_TOKEN_NOT_FOUND == result);
            }

            THEN("outCbuf shall remain empty")
            {
                REQUIRE(mqosCbuffer_IsEmpty(&outCbuf));
            }

            THEN("The echo command line shall be deleted from inCbuf after it has been processed")
            {
                REQUIRE(mqosCbuffer_IsEmpty(&inCbuf));
            }
        }
    }


    GIVEN("inCbuf contains an echo command with a linefeed character between the command and the parameters")
    {
        mqosCbuffer_Reset(&inCbuf);
        mqosCbuffer_Reset(&outCbuf);
        mqosCbuffer_StrcpyToCbuffer(&inCbuf, "echo \r\n string to be echoed.\n");

        WHEN("mqosCli_InvokeCommand() is called")
        {
            tMqosConfig_BaseType result = mqosCli_InvokeCommand(&inCbuf, &outCbuf);

            THEN("mqosCli_InvokeCommand() shall return MQOS_CLI_TOKEN_NOT_FOUND")
            {
                REQUIRE(MQOS_CLI_TOKEN_NOT_FOUND == result);
            }

            THEN("outCbuf shall remain empty")
            {
                REQUIRE(mqosCbuffer_IsEmpty(&outCbuf));
            }

            THEN("The echo command line shall be deleted from inCbuf after it has been processed, but the next line shall remain")
            {
                const char* expectedInput = " string to be echoed.\n";
                tMqosCbufferIndex expectedLength = (tMqosCbufferIndex) strlen(expectedInput);
                REQUIRE(expectedLength == mqosCbuffer_Length(&inCbuf));
                REQUIRE(0 == mqosCbuffer_Strncmp(&inCbuf, 0, expectedInput, expectedLength));
            }
        }
    }


    //-------------------------------------------------------------------------


    GIVEN("inCbuf contains a test1 command, preceded by whitespace, and outCbuf is empty")
    {
        mqosCbuffer_Reset(&inCbuf);
        mqosCbuffer_Reset(&outCbuf);
        mqosCbuffer_StrcpyToCbuffer(&inCbuf, "\t  test1  \r\n");

        WHEN("mqosCli_InvokeCommand() is called")
        {
            tMqosConfig_BaseType result = mqosCli_InvokeCommand(&inCbuf, &outCbuf);

            THEN("mqosCli_InvokeCommand() shall return MQOS_CLI_OK")
            {
                REQUIRE(MQOS_CLI_OK == result);
            }

            THEN("The test1 command's output string shall be written to outCbuf.")
            {
                const char* expectedOutput = "This is test1\r\n";
                tMqosCbufferIndex expectedLength = (tMqosCbufferIndex) strlen(expectedOutput);
                REQUIRE(expectedLength == mqosCbuffer_Length(&outCbuf));
                REQUIRE(0 == mqosCbuffer_Strncmp(&outCbuf, 0, expectedOutput, expectedLength));
            }
        }
    }


    GIVEN("inCbuf contains a test1 command and outCbuf has insufficient space for the output string")
    {
        mqosCbuffer_Reset(&inCbuf);
        mqosCbuffer_Reset(&outCbuf);
        mqosCbuffer_StrcpyToCbuffer(&inCbuf, "test1  \r\n");
        const char* expectedOutput = "This is test1\r\n";

        tMqosCbufferIndex paddingBytesCount = mqosCbuffer_Space(&outCbuf) - 10;
        for(tMqosCbufferIndex i = 0; i < paddingBytesCount; i++)
        {
            REQUIRE(MQOS_CBUFFER_OK == mqosCbuffer_Put(&outCbuf, 'X'));
        }
        tMqosCbufferIndex initialOutCbufSpace = mqosCbuffer_Space(&outCbuf);
        REQUIRE(initialOutCbufSpace < strlen(expectedOutput));

        WHEN("mqosCli_InvokeCommand() is called")
        {
            tMqosConfig_BaseType result = mqosCli_InvokeCommand(&inCbuf, &outCbuf);

            THEN("mqosCli_InvokeCommand() shall return MQOS_CLI_INSUFFICIENT_SPACE")
            {
                REQUIRE(MQOS_CLI_INSUFFICIENT_SPACE == result);
            }

            THEN("No data shall be written to outCbuf")
            {
                REQUIRE(initialOutCbufSpace == mqosCbuffer_Space(&outCbuf));
            }
        }
    }


    //-------------------------------------------------------------------------


    GIVEN("inCbuf contains an add command and outCbuf is empty")
    {
        mqosCbuffer_Reset(&inCbuf);
        mqosCbuffer_Reset(&outCbuf);
        mqosCbuffer_StrcpyToCbuffer(&inCbuf, "add 10000 2345 \r\n");

        WHEN("mqosCli_InvokeCommand() is called")
        {
            tMqosConfig_BaseType result = mqosCli_InvokeCommand(&inCbuf, &outCbuf);

            THEN("mqosCli_InvokeCommand() shall return MQOS_CLI_OK")
            {
                REQUIRE(MQOS_CLI_OK == result);
            }

            THEN("The sum of the two parameters shall be written to outCbuf")
            {
                const char* expectedOutput = "12345\r\n";
                tMqosCbufferIndex expectedLength = (tMqosCbufferIndex) strlen(expectedOutput);
                REQUIRE(expectedLength == mqosCbuffer_Length(&outCbuf));
                REQUIRE(0 == mqosCbuffer_Strncmp(&outCbuf, 0, expectedOutput, expectedLength));
            }

            THEN("The add command line shall be deleted from inCbuf after it has been processed")
            {
                REQUIRE(mqosCbuffer_IsEmpty(&inCbuf));
            }
        }
    }


    GIVEN("inCbuf contains an add command with one negative parameter and outCbuf is empty")
    {
        mqosCbuffer_Reset(&inCbuf);
        mqosCbuffer_Reset(&outCbuf);
        mqosCbuffer_StrcpyToCbuffer(&inCbuf, "add -100000 2\n");

        WHEN("mqosCli_InvokeCommand() is called")
        {
            tMqosConfig_BaseType result = mqosCli_InvokeCommand(&inCbuf, &outCbuf);

            THEN("mqosCli_InvokeCommand() shall return MQOS_CLI_OK")
            {
                REQUIRE(MQOS_CLI_OK == result);
            }

            THEN("The sum of the two parameters shall be written to outCbuf")
            {
                const char* expectedOutput = "-99998\r\n";
                tMqosCbufferIndex expectedLength = (tMqosCbufferIndex) strlen(expectedOutput);
                REQUIRE(expectedLength == mqosCbuffer_Length(&outCbuf));
                REQUIRE(0 == mqosCbuffer_Strncmp(&outCbuf, 0, expectedOutput, expectedLength));
            }

            THEN("The add command line shall be deleted from inCbuf after it has been processed")
            {
                REQUIRE(mqosCbuffer_IsEmpty(&inCbuf));
            }
        }
    }


    GIVEN("inCbuf contains an add command and outCbuf has insufficient space for the result")
    {
        mqosCbuffer_Reset(&inCbuf);
        mqosCbuffer_Reset(&outCbuf);

        // fill outCbuf until just 5 bytes of space remain
        tMqosCbufferIndex index = 0;
        while(5 < mqosCbuffer_Space(&outCbuf))
        {
            mqosCbuffer_Put(&outCbuf, 'X');
            index += 1;
        }

        // result is 6 digits, so will not fit
        mqosCbuffer_StrcpyToCbuffer(&inCbuf, "add 120000 3456\r\n");

        WHEN("mqosCli_InvokeCommand() is called")
        {
            tMqosConfig_BaseType result = mqosCli_InvokeCommand(&inCbuf, &outCbuf);

            THEN("mqosCli_InvokeCommand() shall return MQOS_CLI_INSUFFICIENT_SPACE")
            {
                REQUIRE(MQOS_CLI_INSUFFICIENT_SPACE == result);
            }

            THEN("The sum of the two parameters shall be written to outCbuf, truncated to 5 digits")
            {
                const char* expectedOutput = "12345";
                tMqosCbufferIndex expectedLength = COMMAND_TEST_CBUFFER_SIZE;
                REQUIRE(expectedLength == mqosCbuffer_Length(&outCbuf));
                REQUIRE(0 == mqosCbuffer_Strncmp(&outCbuf, index, expectedOutput, expectedLength - index));
            }

            THEN("The add command line shall be deleted from inCbuf after it has been processed")
            {
                REQUIRE(mqosCbuffer_IsEmpty(&inCbuf));
            }
        }
    }


    GIVEN("inCbuf contains an add command with no parameters")
    {
        mqosCbuffer_Reset(&inCbuf);
        mqosCbuffer_Reset(&outCbuf);
        mqosCbuffer_StrcpyToCbuffer(&inCbuf, "add\r\n");

        WHEN("mqosCli_InvokeCommand() is called")
        {
            tMqosConfig_BaseType result = mqosCli_InvokeCommand(&inCbuf, &outCbuf);

            THEN("mqosCli_InvokeCommand() shall return MQOS_CLI_TOKEN_NOT_FOUND")
            {
                REQUIRE(MQOS_CLI_TOKEN_NOT_FOUND == result);
            }

            THEN("outCbuf shall remain empty")
            {
                REQUIRE(mqosCbuffer_IsEmpty(&outCbuf));
            }

            THEN("The add command line shall be deleted from inCbuf after it has been processed")
            {
                REQUIRE(mqosCbuffer_IsEmpty(&inCbuf));
            }
        }
    }


    GIVEN("inCbuf contains an add command with a linefeed character between the two parameters")
    {
        mqosCbuffer_Reset(&inCbuf);
        mqosCbuffer_Reset(&outCbuf);
        mqosCbuffer_StrcpyToCbuffer(&inCbuf, "add 10000 \r\n 2345");

        WHEN("mqosCli_InvokeCommand() is called")
        {
            tMqosConfig_BaseType result = mqosCli_InvokeCommand(&inCbuf, &outCbuf);

            THEN("mqosCli_InvokeCommand() shall return MQOS_CLI_TOKEN_NOT_FOUND")
            {
                REQUIRE(MQOS_CLI_TOKEN_NOT_FOUND == result);
            }

            THEN("outCbuf shall remain empty")
            {
                REQUIRE(mqosCbuffer_IsEmpty(&outCbuf));
            }

            THEN("The add command line shall be deleted from inCbuf after it has been processed, but the next line shall remain")
            {
                const char* expectedInput = " 2345";
                tMqosCbufferIndex expectedLength = (tMqosCbufferIndex) strlen(expectedInput);
                REQUIRE(expectedLength == mqosCbuffer_Length(&inCbuf));
                REQUIRE(0 == mqosCbuffer_Strncmp(&inCbuf, 0, expectedInput, expectedLength));
            }
        }
    }


    //-------------------------------------------------------------------------


    GIVEN("outCbuf is empty")
    {
        mqosCbuffer_Reset(&inCbuf);
        mqosCbuffer_Reset(&outCbuf);

        const char* expectedHelpString[4] = {
            "test1    Write a test message to the output buffer.\r\n",
            "echo     Echo command line parameters back to sender.\r\n",
            "add      Add two integer parameters.\r\n",
            "help     Print a list of all available commands.\r\n"
        };

        tMqosConfig_BaseType expectedInvokeResult[4] = {
            MQOS_CLI_CMD_NOT_FINISHED,
            MQOS_CLI_CMD_NOT_FINISHED,
            MQOS_CLI_CMD_NOT_FINISHED,
            MQOS_CLI_OK
        };

        const char* expectedInvokeResultStr[4] = {
            "MQOS_CLI_CMD_NOT_FINISHED",
            "MQOS_CLI_CMD_NOT_FINISHED",
            "MQOS_CLI_CMD_NOT_FINISHED",
            "MQOS_CLI_OK"
        };

        WHEN("The help command is written to inCbuf and mqosCli_InvokeCommand() is called 4 times")
        {
            /* "help" command is written to inCbuf just once */
            const char* cmd = "help\r\n";
            tMqosCbufferIndex writeResult = mqosCbuffer_StrcpyToCbuffer(&inCbuf, cmd);
            REQUIRE(strlen(cmd) == writeResult);

            for(uint8_t i = 0; i < 4; i++)
            {
                /* "help" command is parsed repeatedly, until it returns MQOS_CLI_OK */
                char str[80];
                tMqosConfig_BaseType invokeResult = mqosCli_InvokeCommand(&inCbuf, &outCbuf);

                sprintf(str, "Iteration %u: mqosCli_InvokeCommand() shall return %s", i + 1, expectedInvokeResultStr[i]);
                THEN(str)
                {
                    REQUIRE(expectedInvokeResult[i] == invokeResult);
                }

                sprintf(str, "Iteration %u: The expected help string shall be written to outCbuf.", i + 1);
                THEN(str)
                {
                    size_t expectedLength = strlen(expectedHelpString[i]);
                    REQUIRE(expectedLength == (size_t) mqosCbuffer_Length(&outCbuf));
                    REQUIRE(0 == memcmp(expectedHelpString[i], outCbuf.data, expectedLength));
                }

                /* clear outCbuf, ready for the next iteration */
                (void) mqosCbuffer_Reset(&outCbuf);
            }
        }
    }


    //-------------------------------------------------------------------------


    GIVEN("outCbuf contains the first line of help command output")
    {
        mqosCbuffer_Reset(&inCbuf);
        mqosCbuffer_Reset(&outCbuf);

        const char* expectedHelpString[4] = {
            "test1    Write a test message to the output buffer.\r\n",
            "echo     Echo command line parameters back to sender.\r\n"
        };

        /* "help" command is written to inCbuf just once */
        const char* cmd = "help\r\n";
        tMqosCbufferIndex writeResult = mqosCbuffer_StrcpyToCbuffer(&inCbuf, cmd);
        REQUIRE(strlen(cmd) == writeResult);

        /* mqosCli_InvokeCommand() is called to write the first line of help output */
        tMqosConfig_BaseType invokeResult = mqosCli_InvokeCommand(&inCbuf, &outCbuf);

        size_t expectedLength = strlen(expectedHelpString[0]);
        REQUIRE(expectedLength == (size_t) mqosCbuffer_Length(&outCbuf));
        REQUIRE(0 == memcmp(expectedHelpString[0], outCbuf.data, expectedLength));
        REQUIRE(MQOS_CLI_CMD_NOT_FINISHED == invokeResult);

        WHEN("mqosCli_InvokeCommand() is invoked again with insufficient space in outCbuf for the next line of help output")
        {
            invokeResult = mqosCli_InvokeCommand(&inCbuf, &outCbuf);

            THEN("mqosCli_InvokeCommand() shall return MQOS_CLI_CMD_NOT_FINISHED and no additional data shall be written to outCbuf")
            {
                REQUIRE(MQOS_CLI_CMD_NOT_FINISHED == invokeResult);

                size_t expectedLength = strlen(expectedHelpString[0]);
                REQUIRE(expectedLength == (size_t) mqosCbuffer_Length(&outCbuf));
                REQUIRE(0 == memcmp(expectedHelpString[0], outCbuf.data, expectedLength));

                AND_WHEN("the first help output line is read from outCbuf then mqosCli_InvokeCommand() is invoked again")
                {
                    /* clear outCbuf, ready for the next iteration */
                    (void) mqosCbuffer_Reset(&outCbuf);
                    invokeResult = mqosCli_InvokeCommand(&inCbuf, &outCbuf);

                    THEN("mqosCli_InvokeCommand() shall return MQOS_CLI_CMD_NOT_FINISHED and the second line of help output shall be written to outCbuf")
                    {
                        REQUIRE(MQOS_CLI_CMD_NOT_FINISHED == invokeResult);

                        size_t expectedLength = strlen(expectedHelpString[1]);
                        REQUIRE(expectedLength == (size_t) mqosCbuffer_Length(&outCbuf));
                        REQUIRE(0 == memcmp(expectedHelpString[1], outCbuf.data, expectedLength));
                    }
                }
            }
        }
    }
}
