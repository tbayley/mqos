/*
 * mqosPort.h - MQOS port for unit tests built with GCC
 *
 * Copyright (C) 2021 Antony Bayley.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef MQOSPORT_H
#define	MQOSPORT_H

#ifdef	__cplusplus
extern "C" {
#endif

/* MACROS ********************************************************************/

/* Critical section management. */
#define mqosPort_GlobalInterruptDisable()   /* not used for unit tests */
#define mqosPort_GlobalInterruptEnable()    /* not used for unit tests */


/*! Enter critical section macro.
 * 
 *  Critical section macros are not required for unit tests because all MQOS
 *  functions, including mqos_Tick(), are called from the main application
 *  thread.
 */
#define mqosPort_ENTER_CRITICAL()   test_ENTER_CRITICAL()


/*! Exit critical section macro.
 * 
 *  Critical section macros are not required for unit tests because all MQOS
 *  functions, including mqos_Tick(), are called from the main application
 *  thread.
 */
#define mqosPort_EXIT_CRITICAL()    test_EXIT_CRITICAL()


/** PUBLIC FUNCTION DECLARATIONS FOR UNIT TEST *******************************/

/*!
 * Function to be called by mqosPort_ENTER_CRITICAL() macro.
 */
void test_ENTER_CRITICAL(void);


/*!
 * Function to be called by mqosPort_EXIT_CRITICAL() macro.
 */
void test_EXIT_CRITICAL(void);


#ifdef	__cplusplus
}
#endif

#endif	/* MQOSPORT_H */
