# MQOS: Unit Tests #


## Introduction ##

This directory contains unit tests for MQOS (Message Queueing Operating System)
kernel source code, which is in the _mqos/source_ subdirectory and for the
optional add-on modules that are defined in the _mqos/extra_ subdirectory. The
MQOS ports for specific processors, in subdirectories under
_mqos/source/portable_ are not tested.

The unit tests use the [Catch2](https://github.com/catchorg/Catch2) unit test
framework and [Fake Function Framework](https://github.com/meekrosoft/fff)
mocking framework.  These are both C++ header-only frameworks, with no
dependencies on other software packages.


## Installing the prerequisites ##

Although _MQOS_ is written in C, the unit test and mocking frameworks are C++.
Therefore all source files must be built using a C++ compiler. The unit tests
are intended to be built and run using the GNU g++ compiler on a Linux machine.
Tests are built using the [SCons](https://scons.org/) build system. Test
coverage reports are generated using the Python package [GCOVR](https://gcovr.com/).

The GNU toolchain is pre-installed on many Linux distributions. The unit tests
use GCC [Sanitizer](https://gcc.gnu.org/onlinedocs/gcc-9.3.0/gcc/Instrumentation-Options.html#Instrumentation-Options)
options to check for addressing faults and undefined-behaviour at run-time. The
sanitizers require Linux libraries _libasan_ and _libubsan_ to be installed. If
you need to install any of these packages, use your system's package manager
utility. On Ubuntu Linux the relevant command would be:

```bash
sudo apt install build-essential libasan5 libubsan0 libubsan1
```

[SCons](https://scons.org/) and [GCOVR](https://gcovr.com/) are Python packages
that are installed from the Python Package Index (PyPi). First install the
latest version of [Python](https://www.python.org/downloads/). Python 3.5 or
later is recommended. Then use _pip_ to install SCons and GCOVR:

```bash
pip install scons
pip install gcovr
```


## Building and running unit tests ##

To build and run all unit tests, open a terminal session in the _mqos_ root
directory and enter the following command:

```bash
scons coverage
```

This command builds and runs the unit tests, writing pass/fail results to the
command line terminal. HTML test coverage reports are generated for each file
and a summary report is created at _mqos/build/coverage-reports/coverage.html_.

If the command line option _--xmlreport_ is used, the test runner writes
Junit-style XML test reports to subdirectory _mqos/build/test-reports_ instead
of writing human-readable text pass/fail reports to the console.

```bash
scons coverage --xmlreport
```

If SCons is executed with no build target specified, it defaults to building all
targets - including _coverage_. 

To delete the build artefacts, enter the command:

```bash
scons --clean
```

You can also build and run a single test_XXX.c file (for example test_mqos.c or
test_mqosMessageQueue.c) by executing the two commands:

```bash
scons test_XXX
build/test/test_XXX
```

The [Visual Studio Code](https://code.visualstudio.com/) workspace
_mqos.code-workspace_, in the project's root directory, enables unit tests to be
built, run and debugged via _Visual Studio Code_ menus.  For example, to build
and run the unit tests defined in the source file _test\_mqosMessageQueue.c_
from within _Visual Studio Code_, open the file _test/test\_mqosMessageQueue.c_
in the editor, then select the menu item
_Terminal > Run Task... > run selected unit test file_.
