/*
 * test_mqosRotaryEncoder.c - mqosRotaryEncoder module unit test source file.
 *
 * Copyright (C) 2021 Antony Bayley.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include "mqos.h"
#include "mqosRotaryEncoder.h"
#include "catch.hpp"

// Want to keep the fake function argument history for 15 calls
#define FFF_ARG_HISTORY_LEN 15
#include "fff.h"


/* MACROS ********************************************************************/

/* Mock functions are created using "fake function framework" (fff.h) */
DEFINE_FFF_GLOBALS;

FAKE_VALUE_FUNC(tMqosConfig_TimerCount, mqos_getSystemTickCount);
FAKE_VALUE_FUNC(tMqosConfig_UBaseType, mqosGpio_Read, tMqosGpio_PortPin);
FAKE_VOID_FUNC(test_ENTER_CRITICAL);
FAKE_VOID_FUNC(test_EXIT_CRITICAL);


/* PRIVATE VARIABLE DEFINITIONS **********************************************/

/* Define a dummy IO port for unit tests */
static volatile tMqosConfig_UBaseType PORTA = 0;


/* PUBLIC FUNCTION DEFINITIONS ***********************************************/


SCENARIO("Tests for mqosRotaryEncoder_Create().", "[mqosRotaryEncoder-Create]")
{
    tMqosRotaryEncoder_Encoder encoders[MQOS_ROTARY_ENCODER_MAX_INSTANCES + 1U];
    char msg[100];

    for(uint8_t encoderIndex = 0; encoderIndex <= MQOS_ROTARY_ENCODER_MAX_INSTANCES; encoderIndex++)
    {
        snprintf(msg, 100, "mqosRotaryEncoder_Create() is called to create a rotary encoder: Iteration %u", 1 + encoderIndex);
        WHEN(msg)
        {
            if(0 == encoderIndex)
            {
                /* reset the allocated encoder count to zero, at start of test sequence only */
                mqosRotaryEncoder_Init();
            }

            tMqosGpio_PortPin expectedEncoderPortPinA = {&PORTA, (uint8_t) (2U * encoderIndex)};
            tMqosGpio_PortPin expectedEncoderPortPinB = {&PORTA, (uint8_t) ((2U * encoderIndex) + 1U)};
            const tMqosRotaryEncoder_Count expectedMinCount = 100U;
            const tMqosRotaryEncoder_Count expectedMaxCount = 1000U;
            tMqosRotaryEncoder_Count expectedInitialCount = 500U;
#if MQOS_ROTARY_ENCODER_FAST_COUNT_ENABLE
            tMqosConfig_TimerCount expectedFastCountTimeThresholdTicks = 100U;
            tMqosRotaryEncoder_Count expectedFastCountStepSize = 10U;
#endif  /* #if MQOS_ROTARY_ENCODER_FAST_COUNT_ENABLE */

            // UUT - Create a rotary encoder instance
            encoders[encoderIndex] = mqosRotaryEncoder_Create(expectedEncoderPortPinA,
                                                            expectedEncoderPortPinB,
                                                            expectedMinCount,
                                                            expectedMaxCount,
                                                            expectedInitialCount
#if MQOS_ROTARY_ENCODER_FAST_COUNT_ENABLE
                                                            , expectedFastCountTimeThresholdTicks,
                                                            expectedFastCountStepSize
#endif  /* #if MQOS_ROTARY_ENCODER_FAST_COUNT_ENABLE */
                                                );

            if(encoderIndex < MQOS_ROTARY_ENCODER_MAX_INSTANCES)
            {
                THEN("mqosRotaryEncoder_Create() shall return a valid encoder handle")
                {
                    REQUIRE(NULL != encoders[encoderIndex]);
                }
            }
            else
            {
                THEN("mqosRotaryEncoder_Create() shall return NULL")
                {
                    REQUIRE(NULL == encoders[encoderIndex]);
                }
            }
        }
    }
}


//----------------------------------------------------------------------------


