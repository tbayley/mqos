/*
 * test_mqosString.c - mqosString module unit test source file.
 *
 * Copyright (C) 2021 Antony Bayley.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */


#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include "mqosString.h"
#include "catch.hpp"


/* The Catch2 unit test framework uses functions from the standard <string.h> 
 * libary in its test macros. To test the functions in the mqosString module
 * they must be renamed differently to their counterparts in <string.h>.
 * 
 * When the TEST preprocessor symbol is defined, all function names declared in
 * the header file mqosString.h have the prefix test_ added.
 */

SCENARIO("Tests for MQOS string module's memcmp function", "[mqosString-memcmp]")
{
    GIVEN("Two identical uint8_t arrays")
    {
        uint8_t array1[5] = {0, 1, 2, 3, 4};
        uint8_t array2[5] = {0, 1, 2, 3, 4};

        THEN("memcmp shall return 0")
        {
            REQUIRE(0 == test_memcmp(array1, array2, 5));
        }
    }


    GIVEN("Two different uint8_t arrays")
    {
        uint8_t array1[5] = {0, 1, 2, 0, 0};
        uint8_t array2[5] = {0, 1, 2, 3, 4};

        WHEN("The first different byte is lower in the array pointed to by ptr1")
        {
            THEN("memcmp shall return a negative value")
            {
                REQUIRE(0 > test_memcmp(array1, array2, 5));
            }
        }

        WHEN("The first different byte is higher in the array pointed to by ptr1")
        {
            THEN("memcmp shall return a positive value")
            {
                REQUIRE(0 < test_memcmp(array2, array1, 5));
            }
        }
    }
}


//----------------------------------------------------------------------------

SCENARIO("Tests for MQOS string module's memcpy function", "[mqosString-memcpy]")
{
    GIVEN("Two different uint8_t arrays")
    {
        uint8_t array1[5] = {5, 6, 7, 8, 9};
        uint8_t array2[5] = {0, 0, 0, 0, 0};

        WHEN("Bytes 2 and 3 are copied from array1 to array2")
        {
            void* ptr = test_memcpy(&array2[2], &array1[2], 2);

            THEN("array2 shall contain the expected values")
            {
                uint8_t expectedArray2[5] = {0, 0, 7, 8, 0};
                REQUIRE(0 == test_memcmp(expectedArray2, array2, 5));
            }

            THEN("memcpy shall return a pointer to the start address of the copied values")
            {
                REQUIRE(&array2[2] == ptr);
            }
        }
    }
}


//----------------------------------------------------------------------------

SCENARIO("Tests for MQOS string module's memset function", "[mqosString-memset]")
{
    GIVEN("A uint8_t array of length 10 bytes, filled with value 0")
    {
        uint8_t buffer[10] = {0, };

        WHEN("memset sets 5 bytes at array index 2 to value 255")
        {
            void* ptr = test_memset(&buffer[2], 255, 5);

            THEN("The array shall contain the expected values")
            {
                uint8_t expectedBuffer[10] = {0, 0, 255, 255, 255, 255, 255, 0, 0, 0};
                REQUIRE(0 == test_memcmp(expectedBuffer, buffer, 10));
            }

            THEN("memset shall return a pointer to the array")
            {
                REQUIRE(&buffer[2] == ptr);
            }
        }

        WHEN("memset is called with a value parameter greater than 255")
        {
            (void) test_memset(&buffer[5], 258, 1);

            THEN("value shall be cast to an unsigned char")
            {
                uint8_t expectedBuffer[10] = {0, 0, 0, 0, 0, 2, 0, 0, 0, 0};
                REQUIRE(0 == test_memcmp(expectedBuffer, buffer, 10));
            }
        }

        WHEN("memset is called with a negative value parameter")
        {
            (void) memset(&buffer[4], -5, 1);

            THEN("value shall be cast to an unsigned char")
            {
                uint8_t expectedBuffer[10] = {0, 0, 0, 0, 251, 0, 0, 0, 0, 0};
                REQUIRE(0 == test_memcmp(expectedBuffer, buffer, 10));
            }
        }
    }
}


//----------------------------------------------------------------------------

SCENARIO("Tests for MQOS string module's strlen function", "[mqosString-strlen]")
{
    GIVEN("A string length 42 bytes")
    {
        const char* str = "Once upon a time in a galaxy far, far away";

        WHEN("strlen is called")
        {
            size_t len = test_strlen(str);

            THEN("strlen shall return 42")
            {
                REQUIRE(42 == len);
            }
        }
    }
}


//----------------------------------------------------------------------------

SCENARIO("Tests for MQOS string module's strncmp function", "[mqosString-strncmp]")
{
    GIVEN("Two identical C stings")
    {
        const char* str1 = "Test string";
        const char* str2 = "Test string";

        THEN("strncmp shall return 0, when n is equal to the string length")
        {
            REQUIRE(0 == test_strncmp(str1, str2, strlen(str1)));
        }

        THEN("strncmp shall return 0, when n is greater than the string length")
        {
            REQUIRE(0 == test_strncmp(str1, str2, 5 + strlen(str1)));
        }

        THEN("strncmp shall return 0, when n is less than the string length")
        {
            REQUIRE(0 == test_strncmp(str1, str2, strlen(str1) - 5));
        }
    }


    GIVEN("Two different C strings of equal length")
    {
        const char* a = "Test string 111";
        const char* b = "Test string 222";

        WHEN("The first different character is lower in str1")
        {
            THEN("strncmp shall return a negative value")
            {
                REQUIRE(0 > test_strncmp(a, b, strlen(a)));
            }
        }

        WHEN("The first different byte is higher in str1")
        {
            THEN("strncmp shall return a positive value")
            {
                REQUIRE(0 < test_strncmp(b, a, 5 + strlen(a)));
            }
        }

        WHEN("The first n characters are the same in different C strings str1 and str2")
        {
            THEN("strncmp shall return 0")
            {
                REQUIRE(0 == test_strncmp(a, b, 12));
            }
        }
    }


    GIVEN("Two C strings 'a' and 'b' of different length, where 'b' is equal to 'a' with extra characters appended")
    {
        const char* a = "Test string";
        const char* b = "Test string 222";

        WHEN("str1 is the shorter string")
        {
            THEN("strncmp shall return a negative value")
            {
                REQUIRE(0 > test_strncmp(a, b, strlen(b)));
            }
        }

        WHEN("str2 is the shorter string")
        {
            THEN("strncmp shall return a positive value")
            {
                REQUIRE(0 < test_strncmp(b, a, 5 + strlen(a)));
            }
        }
    }
}


//----------------------------------------------------------------------------

SCENARIO("Tests for MQOS string module's strncpy function", "[mqosString-strncpy]")
{
    GIVEN("Two different strings")
    {
        const char* src = "abcdef";
        char dest[] = "ghijkl";

        WHEN("characters 2 and 3 are copied from src to dest")
        {
            char* ptr = test_strncpy(&dest[2], &src[2], 2);

            THEN("str2 shall contain the expected values")
            {
                const char* expectedDest = "ghcdkl";
                REQUIRE(0 == test_memcmp(expectedDest, dest, test_strlen(dest)));
            }

            THEN("strncpy shall return a pointer to the start address of the copied characters")
            {
                REQUIRE(&dest[2] == ptr);
            }
        }

        WHEN("The src string is shorter than the number of characters to be copied")
        {
            char* ptr = test_strncpy(&dest[2], &src[4], test_strlen(dest) - 2);

            THEN("The remaining characters shall be filled with the null character '\0'")
            {
                const char* expectedDest = "ghef\0\0";
                REQUIRE(0 == test_memcmp(expectedDest, dest, test_strlen(dest)));
            }

            THEN("strncpy shall return a pointer to the start address of the copied characters")
            {
                REQUIRE(&dest[2] == ptr);
            }
        }

        WHEN("The src string is longer than the number of characters to be copied")
        {
            char* ptr = test_strncpy(&dest[2], src, test_strlen(dest) - 2);

            THEN("The src string shall be truncated to fit in dest")
            {
                const char* expectedDest = "ghabcd";
                REQUIRE(0 == test_memcmp(expectedDest, dest, test_strlen(dest)));
            }

            THEN("strncpy shall return a pointer to the start address of the copied characters")
            {
                REQUIRE(&dest[2] == ptr);
            }
        }
    }
}
