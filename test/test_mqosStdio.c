/*
 * test_mqosPrintf.c - mqosPrintf module unit test source file.
 *
 * Copyright (C) 2021 Antony Bayley.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */


#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <limits.h>
#include "mqosStdio.h"
#include "catch.hpp"


/** PRIVATE MACRO DEFINITIONS ************************************************/

#if INT_MIN == -128
#define INT_MIN_STRING  "-128"
#elif INT_MIN == -32768
#define INT_MIN_STRING  "-32768"
#elif INT_MIN == -2147483648
#define INT_MIN_STRING  "-2147483648"
#else
#define INT_MIN_STRING  "-9223372036854775808"
#endif

#if INT_MIN == -128
#define INT_MIN_HEX_STRING  "80"
#elif INT_MIN == -32768
#define INT_MIN_HEX_STRING  "8000"
#elif INT_MIN == -2147483648
#define INT_MIN_HEX_STRING  "80000000"
#else
#define INT_MIN_HEX_STRING  "8000000000000000"
#endif

#if INT_MAX == 127
#define INT_MAX_STRING  "127"
#elif INT_MAX == 32767
#define INT_MAX_STRING  "32767"
#elif INT_MAX == 2147483647
#define INT_MAX_STRING  "2147483647"
#else
#define INT_MAX_STRING  "9223372036854775807"
#endif

#if INT_MAX == 127
#define INT_MAX_HEX_STRING_LOWERCASE  "7f"
#elif INT_MAX == 32767
#define INT_MAX_HEX_STRING_LOWERCASE  "7fff"
#elif INT_MAX == 2147483647
#define INT_MAX_HEX_STRING_LOWERCASE  "7fffffff"
#else
#define INT_MAX_HEX_STRING_LOWERCASE  "7fffffffffffffff"
#endif

#if INT_MAX == 127
#define INT_MAX_HEX_STRING_UPPERCASE  "7F"
#elif INT_MAX == 32767
#define INT_MAX_HEX_STRING_UPPERCASE  "7FFF"
#elif INT_MAX == 2147483647
#define INT_MAX_HEX_STRING_UPPERCASE  "7FFFFFFF"
#else
#define INT_MAX_HEX_STRING_UPPERCASE  "7FFFFFFFFFFFFFFF"
#endif

#if LONG_MIN == -32768L
#define LONG_MIN_STRING  "-32768"
#elif LONG_MIN == -2147483648L
#define LONG_MIN_STRING  "-2147483648"
#else
#define LONG_MIN_STRING  "-9223372036854775808"
#endif

#if LONG_MIN == -32768L
#define LONG_MIN_HEX_STRING_LOWERCASE  "8000"
#elif LONG_MIN == -2147483648L
#define LONG_MIN_HEX_STRING_LOWERCASE  "80000000"
#else
#define LONG_MIN_HEX_STRING_LOWERCASE  "8000000000000000"
#endif

#if LONG_MAX == 32767L
#define LONG_MAX_STRING  "32767"
#elif LONG_MAX == 2147483647L
#define LONG_MAX_STRING  "2147483647"
#else
#define LONG_MAX_STRING  "9223372036854775807"
#endif

#if UINT_MAX == 255
#define UINT_MAX_STRING  "255"
#elif UINT_MAX == 65535
#define UINT_MAX_STRING  "65535"
#elif UINT_MAX == 4294967295
#define UINT_MAX_STRING  "4294967295"
#else
#define UINT_MAX_STRING  "18446744073709551615"
#endif

#if UINT_MAX == 255
#define UINT_MAX_HEX_STRING_LOWERCASE  "ff"
#elif UINT_MAX == 65535
#define UINT_MAX_HEX_STRING_LOWERCASE  "ffff"
#elif UINT_MAX == 4294967295
#define UINT_MAX_HEX_STRING_LOWERCASE  "ffffffff"
#else
#define UINT_MAX_HEX_STRING_LOWERCASE  "ffffffffffffffff"
#endif

#if UINT_MAX == 255
#define UINT_MAX_HEX_STRING_UPPERCASE  "FF"
#elif UINT_MAX == 65535
#define UINT_MAX_HEX_STRING_UPPERCASE  "FFFF"
#elif UINT_MAX == 4294967295
#define UINT_MAX_HEX_STRING_UPPERCASE  "FFFFFFFF"
#else
#define UINT_MAX_HEX_STRING_UPPERCASE  "FFFFFFFFFFFFFFFF"
#endif

#if ULONG_MAX == 65535UL
#define ULONG_MAX_STRING  "65535"
#elif ULONG_MAX == 4294967295UL
#define ULONG_MAX_STRING  "4294967295"
#else
#define ULONG_MAX_STRING  "18446744073709551615"
#endif

#if ULONG_MAX == 65535UL
#define ULONG_MAX_HEX_STRING_UPPERCASE  "FFFF"
#elif ULONG_MAX == 4294967295UL
#define ULONG_MAX_HEX_STRING_UPPERCASE  "FFFFFFFF"
#else
#define ULONG_MAX_HEX_STRING_UPPERCASE  "FFFFFFFFFFFFFFFF"
#endif


/* PRIVATE FUNCTION DECLARATIONS *********************************************/

void putch(char c);
static void resetTestBuffer(void);


/* buffer to contain printf() output */
char testBuffer[64] = {0, };

/* test buffer write pointer */
char* writePointer = testBuffer;

/* test buffer read pointer */
char* readPointer = testBuffer;


/* Fill the test buffer with character '~' and reset the read and write pointers.
 * The '~' characters make it easy to verify that the right number of characters
 * have been written, without an additional NULL string termination character.
 */
static void resetTestBuffer(void)
{
    memset(testBuffer, '~', sizeof(testBuffer));
    writePointer = testBuffer;
    readPointer = testBuffer;
}

/* PUBLIC FUNCTION DEFINITIONS ***********************************************/


/* The MQOS printf() function relies on the system implementing a mqosPutch()
 * function that writes one character to the standard output (stdout). For unit
 * testing mqosPutch() diverts the printf() functions ouptut to a buffer so that
 * the printed output can be compared to the expected output.
 */
int mqosPutch(char c)
{
    int n = 0;
    if(writePointer != (testBuffer + sizeof(testBuffer)))
    {
        /* there is space to write to the buffer */
        *writePointer++ = (char) c;
         n = 1;
    }
    return n;
}


SCENARIO("mqosStdio module tests for printf() function with string data", "[mqosStdio_printf_string]")
{
    GIVEN("The test buffer is empty")
    {
        resetTestBuffer();

        /* ----------------------------------------------------------------- */
        
        WHEN("printf() writes a string to stdout")
        {
            int n = test_printf("Hello world\r\n");

            THEN("The test buffer shall contain the expected string")
            {
                const char* expectedString = "Hello world\r\n~";
                int expectedStringLen = 14;
                REQUIRE((expectedStringLen - 1) == n);
                REQUIRE(0 == strncmp(expectedString, readPointer, expectedStringLen));
            }
        }

        /* ----------------------------------------------------------------- */

        WHEN("printf() uses the string print specifier")
        {
            int n = test_printf("Hello %s world\r\n", "cruel");

            THEN("The test buffer shall contain the expected string")
            {
                const char* expectedString = "Hello cruel world\r\n~";
                int expectedStringLen = strlen(expectedString);
                REQUIRE((expectedStringLen - 1) == n);
                REQUIRE(0 == strncmp(expectedString, readPointer, expectedStringLen));
            }
        }

        /* ----------------------------------------------------------------- */
        
        WHEN("printf() uses unsupported left justify and precision specifiers")
        {
            /* MQOS printf does not support left justify or precision specifiers */
            int n = test_printf("Value is %-8.4d\n", 25);

            THEN("The expected string shall be truncated at the first unsupported specifier")
            {
                const char* expectedString = "Value is ~";
                int expectedStringLen = strlen(expectedString);
                REQUIRE((expectedStringLen - 1) == n);
                REQUIRE(0 == strncmp(expectedString, readPointer, expectedStringLen));
            }
        }

        /* ----------------------------------------------------------------- */
        
        WHEN("printf() uses unsupported floating point specifier")
        {
            /* MQOS printf does not support left justify or precision specifiers */
            int n = test_printf("Value is %0.4f\n", 37.63);

            THEN("The expected string shall be truncated at the first unsupported specifier")
            {
                const char* expectedString = "Value is ~";
                int expectedStringLen = strlen(expectedString);
                REQUIRE((expectedStringLen - 1) == n);
                REQUIRE(0 == strncmp(expectedString, readPointer, expectedStringLen));
            }
        }
    }

    /* ********************************************************************* */

    GIVEN("The test buffer is nearly full, leaving just 5 characters of free space")
    {
        resetTestBuffer();
        for(size_t i = 0; i < sizeof(testBuffer) - 5; i++)
        {
            test_printf("X");
            readPointer += 1;
        }

        /* ----------------------------------------------------------------- */
        
        WHEN("printf() writes a string to stdout")
        {
            int n = test_printf("Hello world\r\n");

            THEN("Only the first 5 characters shall be written to the test buffer")
            {
                const char* expectedString = "Hello";
                int expectedStringLen = 5;
                REQUIRE(expectedStringLen == n);
                REQUIRE(0 == strncmp(expectedString, readPointer, expectedStringLen));
            }
        }

        /* ----------------------------------------------------------------- */

        WHEN("printf() uses the string print specifier")
        {
            int n = test_printf("A %s world\r\n", "better");

            THEN("Only the first 5 characters shall be written to the test buffer")
            {
                const char* expectedString = "A bet";
                int expectedStringLen = 5;
                REQUIRE(expectedStringLen == n);
                REQUIRE(0 == strncmp(expectedString, readPointer, expectedStringLen));
            }
        }
    }
}


SCENARIO("mqosStdio module tests for printf() function with char data", "[mqosStdio_printf_char]")
{
    GIVEN("The test buffer is empty")
    {
        resetTestBuffer();

        /* ----------------------------------------------------------------- */
        
        WHEN("printf() uses the %c character specifier")
        {
            int n = test_printf("The %c character is inserted\r\n", 'X');

            THEN("The test buffer shall contain the expected string")
            {
                const char* expectedString = "The X character is inserted\r\n~";
                int expectedStringLen = strlen(expectedString);
                REQUIRE((expectedStringLen - 1) == n);
                REQUIRE(0 == strncmp(expectedString, readPointer, expectedStringLen));
            }
        }
    }

    /* ********************************************************************* */

    GIVEN("The test buffer is nearly full, leaving just 1 character of free space")
    {
        resetTestBuffer();
        for(size_t i = 0; i < sizeof(testBuffer) - 1; i++)
        {
            test_printf("X");
            readPointer += 1;
        }

        /* ----------------------------------------------------------------- */
        
        WHEN("printf() writes 2 characters to stdout")
        {
            int n = test_printf("%c%c", 'A', 'B');

            THEN("Only the first character shall be written to the test buffer")
            {
                const char* expectedString = "A";
                int expectedStringLen = 1;
                REQUIRE(expectedStringLen == n);
                REQUIRE(0 == strncmp(expectedString, readPointer, expectedStringLen));
            }
        }
    }
}


SCENARIO("mqosStdio module tests for printf() function with signed int data", "[mqosStdio_printf_int]")
{
    GIVEN("The test buffer is empty")
    {
        resetTestBuffer();

        /* ----------------------------------------------------------------- */
        
        WHEN("printf() uses the %d signed integer specifier and maximum positive integer value")
        {
            int n = test_printf("The answer is %d\r\n", INT_MAX);
        
            THEN("The test buffer shall contain the expected string")
            {
                const char* expectedString = "The answer is " INT_MAX_STRING "\r\n~";
                int expectedStringLen = strlen(expectedString);
                REQUIRE((expectedStringLen - 1) == n);
                REQUIRE(0 == strncmp(expectedString, readPointer, expectedStringLen));
            }
        }

        /* ----------------------------------------------------------------- */
        
        WHEN("printf() uses the %d signed integer specifier and a positive integer value")
        {
            int n = test_printf("The answer is %d\r\n", 12345);

            THEN("The test buffer shall contain the expected string")
            {
                const char* expectedString = "The answer is 12345\r\n~";
                int expectedStringLen = strlen(expectedString);
                REQUIRE((expectedStringLen - 1) == n);
                REQUIRE(0 == strncmp(expectedString, readPointer, expectedStringLen));
            }
        }

        /* ----------------------------------------------------------------- */
        
        WHEN("printf() uses the %d signed integer specifier and a negative integer value")
        {
            int n = test_printf("The answer is %d\r\n", -6789);

            THEN("The test buffer shall contain the expected string")
            {
                const char* expectedString = "The answer is -6789\r\n~";
                int expectedStringLen = strlen(expectedString);
                REQUIRE((expectedStringLen - 1) == n);
                REQUIRE(0 == strncmp(expectedString, readPointer, expectedStringLen));
            }
        }

        /* ----------------------------------------------------------------- */
        
        WHEN("printf() uses the %i signed integer specifier and maximum negative integer value")
        {
            int n = test_printf("The answer is %i\r\n", INT_MIN);

            THEN("The test buffer shall contain the expected string")
            {
                const char* expectedString = "The answer is " INT_MIN_STRING "\r\n~";
                int expectedStringLen = strlen(expectedString);
                REQUIRE((expectedStringLen - 1) == n);
                REQUIRE(0 == strncmp(expectedString, readPointer, expectedStringLen));
            }
        }

        /* ----------------------------------------------------------------- */
        
        WHEN("printf() uses the %d signed integer specifier and zero value")
        {
            int n = test_printf("The answer is %d\r\n", -0);

            THEN("The test buffer shall contain the expected string")
            {
                const char* expectedString = "The answer is 0\r\n~";
                int expectedStringLen = strlen(expectedString);
                REQUIRE((expectedStringLen - 1) == n);
                REQUIRE(0 == strncmp(expectedString, readPointer, expectedStringLen));
            }
        }

        /* ----------------------------------------------------------------- */
        
        WHEN("printf() uses the %8d specifier to write signed integer data to a field of width of 8 characters")
        {
            int n = test_printf("Data:%8d%8d%8d\r\n", 1234, 0, -56789);

            THEN("The test buffer shall contain the expected string")
            {
                const char* expectedString = "Data:    1234       0  -56789\r\n~";
                int expectedStringLen = strlen(expectedString);
                REQUIRE((expectedStringLen - 1) == n);
                REQUIRE(0 == strncmp(expectedString, readPointer, expectedStringLen));
            }
        }

        /* ----------------------------------------------------------------- */
        
        WHEN("printf() uses the %06d specifier to write signed integer data to a field of width of 6 characters with zero padding")
        {
            int n = test_printf("Data: %06d %06d %06d\r\n", 1234, 0, -5678);

            THEN("The test buffer shall contain the expected string")
            {
                const char* expectedString = "Data: 001234 000000 -05678\r\n~";
                int expectedStringLen = strlen(expectedString);
                REQUIRE((expectedStringLen - 1) == n);
                REQUIRE(0 == strncmp(expectedString, readPointer, expectedStringLen));
            }
        }

        /* ----------------------------------------------------------------- */
        
        WHEN("printf() uses the %04i specifier to write a positive integer data of length 5 characters to a field of width of 4 characters")
        {
            int n = test_printf("Timestamp: %04i milliseconds\r\n", 12345);

            THEN("The field width shall be increased to fit the number")
            {
                const char* expectedString = "Timestamp: 12345 milliseconds\r\n~";
                int expectedStringLen = strlen(expectedString);
                REQUIRE((expectedStringLen - 1) == n);
                REQUIRE(0 == strncmp(expectedString, readPointer, expectedStringLen));
            }
        }

        /* ----------------------------------------------------------------- */
        
        WHEN("printf() uses the %04d specifier to write a negative integer data of length 6 characters to a field of width of 4 characters with zero padding")
        {
            int n = test_printf("Score: %04d points\r\n", -12345);

            THEN("The field width shall be increased to fit the number")
            {
                const char* expectedString = "Score: -12345 points\r\n~";
                int expectedStringLen = strlen(expectedString);
                REQUIRE((expectedStringLen - 1) == n);
                REQUIRE(0 == strncmp(expectedString, readPointer, expectedStringLen));
            }
        }
    }

    /* ********************************************************************* */

    GIVEN("The test buffer is nearly full, leaving just 5 characters of free space")
    {
        resetTestBuffer();
        for(size_t i = 0; i < sizeof(testBuffer) - 5; i++)
        {
            test_printf("X");
            readPointer += 1;
        }

        /* ----------------------------------------------------------------- */
        
        WHEN("printf() uses the %d signed integer specifier and a positive integer value")
        {
            int n = test_printf("x = %d\r\n", 1234);

            THEN("The expected string shall be truncated after 5 characters have been printed")
            {
                const char* expectedString = "x = 1";
                int expectedStringLen = 5;
                REQUIRE(expectedStringLen == n);
                REQUIRE(0 == strncmp(expectedString, readPointer, expectedStringLen));
            }
        }

        /* ----------------------------------------------------------------- */
        
        WHEN("printf() uses the %d signed integer specifier and a negative integer value")
        {
            int n = test_printf("x: %d\r\n", -1234);

            THEN("The expected string shall be truncated after 5 characters have been printed")
            {
                const char* expectedString = "x: -1";
                int expectedStringLen = 5;
                REQUIRE(expectedStringLen== n);
                REQUIRE(0 == strncmp(expectedString, readPointer, expectedStringLen));
            }
        }

        /* ----------------------------------------------------------------- */
        
        WHEN("printf() uses the %d signed integer specifier and a negative integer value for which the minus sign fails to print")
        {
            int n = test_printf("x2 = %d\r\n", -1234);

            THEN("The expected string shall be truncated after 5 characters have been printed")
            {
                const char* expectedString = "x2 = ";
                int expectedStringLen = 5;
                REQUIRE(expectedStringLen== n);
                REQUIRE(0 == strncmp(expectedString, readPointer, expectedStringLen));
            }
        }

        /* ----------------------------------------------------------------- */
        
        WHEN("printf() uses the %8d specifier to write positive integer data to a field of width of 8 characters")
        {
            int n = test_printf("%8d%8d%8d\r\n", 123456, 0, -56789);

            THEN("The test buffer shall contain the first 5 characters of the expected string")
            {
                const char* expectedString = "  123";
                int expectedStringLen = 5;
                REQUIRE(expectedStringLen == n);
                REQUIRE(0 == strncmp(expectedString, readPointer, expectedStringLen));
            }
        }

        /* ----------------------------------------------------------------- */
        
        WHEN("printf() uses the %8d specifier to write negative integer data to a field of width of 8 characters")
        {
            int n = test_printf("%8d%8d%8d\r\n", -56789, 0, 12345);

            THEN("The test buffer shall contain the first 5 characters of the expected string")
            {
                const char* expectedString = "  -56";
                int expectedStringLen = 5;
                REQUIRE(expectedStringLen == n);
                REQUIRE(0 == strncmp(expectedString, readPointer, expectedStringLen));
            }
        }
    }
}


SCENARIO("mqosStdio module tests for printf() function with unsigned int data", "[mqosStdio_printf_uint]")
{
    GIVEN("The test buffer is empty")
    {
        resetTestBuffer();

        /* ----------------------------------------------------------------- */
        
        WHEN("printf() uses the %u unsigned integer specifier and maximum positive integer value")
        {
            int n = test_printf("The answer is %u\r\n", UINT_MAX);

            THEN("The test buffer shall contain the expected string")
            {
                const char* expectedString = "The answer is " UINT_MAX_STRING "\r\n~";
                int expectedStringLen = strlen(expectedString);
                REQUIRE((expectedStringLen - 1) == n);
                REQUIRE(0 == strncmp(expectedString, readPointer, expectedStringLen));
            }
        }

        /* ----------------------------------------------------------------- */
        
        WHEN("printf() uses the %u unsigned integer specifier and a positive integer value")
        {
            int n = test_printf("The answer is %u\r\n", 54321);

            THEN("The test buffer shall contain the expected string")
            {
                const char* expectedString = "The answer is 54321\r\n~";
                int expectedStringLen = strlen(expectedString);
                REQUIRE((expectedStringLen - 1) == n);
                REQUIRE(0 == strncmp(expectedString, readPointer, expectedStringLen));
            }
        }

        /* ----------------------------------------------------------------- */
        
        WHEN("printf() uses the %u unsigned integer specifier and zero value")
        {
            int n = test_printf("The answer is %u\r\n", 0);

            THEN("The test buffer shall contain the expected string")
            {
                const char* expectedString = "The answer is 0\r\n~";
                int expectedStringLen = strlen(expectedString);
                REQUIRE((expectedStringLen - 1) == n);
                REQUIRE(0 == strncmp(expectedString, readPointer, expectedStringLen));
            }
        }

        /* ----------------------------------------------------------------- */
        
        WHEN("printf() uses the %8u specifier to write unsigned integer data to a field of width of 8 characters")
        {
            int n = test_printf("Data:%8u%8u\r\n", 12345U, 0U);

            THEN("The test buffer shall contain the expected string")
            {
                const char* expectedString = "Data:   12345       0\r\n~";
                int expectedStringLen = strlen(expectedString);
                REQUIRE((expectedStringLen - 1) == n);
                REQUIRE(0 == strncmp(expectedString, readPointer, expectedStringLen));
            }
        }

        /* ----------------------------------------------------------------- */
        
        WHEN("printf() uses the %06u specifier to write unsigned integer data to a field of width of 6 characters with zero padding")
        {
            int n = test_printf("Data: %06u %06u %06u\r\n", 1234, 0, 456789);

            THEN("The test buffer shall contain the expected string")
            {
                const char* expectedString = "Data: 001234 000000 456789\r\n~";
                int expectedStringLen = strlen(expectedString);
                REQUIRE((expectedStringLen - 1) == n);
                REQUIRE(0 == strncmp(expectedString, readPointer, expectedStringLen));
            }
        }

        /* ----------------------------------------------------------------- */
        
        WHEN("printf() uses the %04u specifier to write unsigned integer data of length 5 characters to a field of width of 4 characters")
        {
            int n = test_printf("Timestamp: %04u milliseconds\r\n", 12345);

            THEN("The field width shall be increased to fit the number")
            {
                const char* expectedString = "Timestamp: 12345 milliseconds\r\n~";
                int expectedStringLen = strlen(expectedString);
                REQUIRE((expectedStringLen - 1) == n);
                REQUIRE(0 == strncmp(expectedString, readPointer, expectedStringLen));
            }
        }
    }

    /* ********************************************************************* */

    GIVEN("The test buffer is nearly full, leaving just 5 characters of free space")
    {
        resetTestBuffer();
        for(size_t i = 0; i < sizeof(testBuffer) - 5; i++)
        {
            test_printf("X");
            readPointer += 1;
        }

        /* ----------------------------------------------------------------- */

        WHEN("printf() uses the %u unsigned integer specifier and a positive integer value")
        {
            int n = test_printf("x = %u\r\n", 1234);

            THEN("The expected string shall be truncated after 5 characters have been printed")
            {
                const char* expectedString = "x = 1";
                int expectedStringLen = 5;
                REQUIRE(expectedStringLen == n);
                REQUIRE(0 == strncmp(expectedString, readPointer, expectedStringLen));
            }
        }
    }
}


SCENARIO("mqosStdio module tests for printf() function with hexadecimal formatted unsigned int data", "[mqosStdio_printf_hex]")
{
    GIVEN("The test buffer is empty")
    {
        resetTestBuffer();

        /* ----------------------------------------------------------------- */
        
        WHEN("printf() uses the %X hexadecimal specifier with values from 0 to 255")
        {
            int testVal[10] = { 0x00, 0xFF , 0x01, 0x23, 0x45,
                                0x67, 0x89, 0xAB, 0xCD, 0xEF };
            int n = 0;
            for(size_t i = 0; i < sizeof(testVal) / sizeof(testVal[0]); i++)
            {
                n += test_printf("0x%X ", testVal[i]);
            }

            THEN("The test buffer shall contain the expected string")
            {
                const char* expectedString = { "0x0 0xFF 0x1 0x23 0x45 "
                                               "0x67 0x89 0xAB 0xCD 0xEF ~" };
                int expectedStringLen = strlen(expectedString);
                REQUIRE((expectedStringLen - 1) == n);
                REQUIRE(0 == strncmp(expectedString, readPointer, expectedStringLen));
            }
        }

        /* ----------------------------------------------------------------- */
        
        WHEN("printf() uses the %x hexadecimal specifier with values from 0 to 255")
        {
            int testVal[10] = { 0x00, 0xFF , 0x01, 0x23, 0x45,
                                0x67, 0x89, 0xAB, 0xCD, 0xEF };
            int n = 0;
            for(size_t i = 0; i < sizeof(testVal) / sizeof(testVal[0]); i++)
            {
                n += test_printf("0x%x ", testVal[i]);
            }

            THEN("The test buffer shall contain the expected string")
            {
                const char* expectedString = { "0x0 0xff 0x1 0x23 0x45 "
                                               "0x67 0x89 0xab 0xcd 0xef ~" };
                int expectedStringLen = strlen(expectedString);
                REQUIRE((expectedStringLen - 1) == n);
                REQUIRE(0 == strncmp(expectedString, readPointer, expectedStringLen));
            }
        }

        /* ----------------------------------------------------------------- */
        
        WHEN("printf() uses the %X hexadecimal specifier with maximum positive unsigned integer value")
        {
            int n = test_printf("0x%X\n", UINT_MAX);

            THEN("The test buffer shall contain the expected string")
            {
                const char* expectedString = "0x" UINT_MAX_HEX_STRING_UPPERCASE "\n~";
                int expectedStringLen = strlen(expectedString);
                REQUIRE((expectedStringLen - 1) == n);
                REQUIRE(0 == strncmp(expectedString, readPointer, expectedStringLen));
            }
        }

        /* ----------------------------------------------------------------- */
        
        WHEN("printf() uses the %X hexadecimal specifier with an unsigned integer value")
        {
            int n = test_printf("0x%X\n", 0xbeef);

            THEN("The test buffer shall contain the expected string")
            {
                const char* expectedString = "0xBEEF\n~";
                int expectedStringLen = strlen(expectedString);
                REQUIRE((expectedStringLen - 1) == n);
                REQUIRE(0 == strncmp(expectedString, readPointer, expectedStringLen));
            }
        }

        /* ----------------------------------------------------------------- */
        
        WHEN("printf() uses the %x hexadecimal specifier with an unsigned integer value")
        {
            int n = test_printf("0x%x\n", 0xC0DE);

            THEN("The test buffer shall contain the expected string")
            {
                const char* expectedString = "0xc0de\n~";
                int expectedStringLen = strlen(expectedString);
                REQUIRE((expectedStringLen - 1) == n);
                REQUIRE(0 == strncmp(expectedString, readPointer, expectedStringLen));
            }
        }

        /* ----------------------------------------------------------------- */
        
        WHEN("printf() uses the %x hexadecimal specifier with maximum negative signed integer value")
        {
            int n = test_printf("0x%x\n", INT_MIN);

            THEN("The test buffer shall contain the expected string")
            {
                const char* expectedString = "0x" INT_MIN_HEX_STRING "\n~";
                int expectedStringLen = strlen(expectedString);
                REQUIRE((expectedStringLen - 1) == n);
                REQUIRE(0 == strncmp(expectedString, readPointer, expectedStringLen));
            }
        }

        /* ----------------------------------------------------------------- */
        
        WHEN("printf() uses the %8X hexadecimal specifier to write data to a field of width of 8 characters")
        {
            int n = test_printf("Data:%8X%8X\r\n", 0x5678, 0x9ab);

            THEN("The test buffer shall contain the expected string")
            {
                const char* expectedString = "Data:    5678     9AB\r\n~";
                int expectedStringLen = strlen(expectedString);
                REQUIRE((expectedStringLen - 1) == n);
                REQUIRE(0 == strncmp(expectedString, readPointer, expectedStringLen));
            }
        }

        /* ----------------------------------------------------------------- */
        
        WHEN("printf() uses the %04x hexadecimal specifier to write unsigned integer data to a field of width of 4 characters with zero padding")
        {
            int n = test_printf("Data: 0x%04x 0x%04x 0x%04x\r\n", 0x10, 0, 0x479B);

            THEN("The test buffer shall contain the expected string")
            {
                const char* expectedString = "Data: 0x0010 0x0000 0x479b\r\n~";
                int expectedStringLen = strlen(expectedString);
                REQUIRE((expectedStringLen - 1) == n);
                REQUIRE(0 == strncmp(expectedString, readPointer, expectedStringLen));
            }
        }

        /* ----------------------------------------------------------------- */
        
        WHEN("printf() uses the %02X hexadecimal specifier to write unsigned integer data of length 4 characters to a field of width of 2 characters")
        {
            int n = test_printf("Timestamp: 0x%02X milliseconds\r\n", 0x12ab);

            THEN("The field width shall be increased to fit the number")
            {
                const char* expectedString = "Timestamp: 0x12AB milliseconds\r\n~";
                int expectedStringLen = strlen(expectedString);
                REQUIRE((expectedStringLen - 1) == n);
                REQUIRE(0 == strncmp(expectedString, readPointer, expectedStringLen));
            }
        }
    }


    /* ********************************************************************* */

    GIVEN("The test buffer is nearly full, leaving just 5 characters of free space")
    {
        resetTestBuffer();
        for(size_t i = 0; i < sizeof(testBuffer) - 5; i++)
        {
            test_printf("X");
            readPointer += 1;
        }

        /* ----------------------------------------------------------------- */
        
        WHEN("printf() uses the %X hexadecimal integer specifier and a positive integer value")
        {
            int n = test_printf("0x%X\r\n", 0xabcd);

            THEN("The expected string shall be truncated after 5 characters have been printed")
            {
                const char* expectedString = "0xABC";
                int expectedStringLen = 5;
                REQUIRE(expectedStringLen == n);
                REQUIRE(0 == strncmp(expectedString, readPointer, expectedStringLen));
            }
        }

        /* ----------------------------------------------------------------- */
        
        WHEN("printf() uses the %x hexadecimal integer specifier and a negative integer value")
        {
            int n = test_printf("0x%x\r\n", -1);

            THEN("The expected string shall be truncated after 5 characters have been printed")
            {
                const char* expectedString = "0xfff";
                int expectedStringLen = 5;
                REQUIRE(expectedStringLen == n);
                REQUIRE(0 == strncmp(expectedString, readPointer, expectedStringLen));
            }
        }
    }
}


#if MQOS_CONFIG_STDIO_LIBRARY_LONG_PRINTF_ENABLED

SCENARIO("mqosStdio module tests for printf() function with signed long data", "[mqosPrintf_long]")
{
    GIVEN("The test buffer is empty")
    {
        resetTestBuffer();

        /* ----------------------------------------------------------------- */

        WHEN("printf() uses the %ld signed long specifier and the maximum positive integer value")
        {
            int n = test_printf("The answer is %ld\r\n", LONG_MAX);

            THEN("The test buffer shall contain the expected string")
            {
                const char* expectedString = "The answer is " LONG_MAX_STRING "\r\n~";
                int expectedStringLen = strlen(expectedString);
                REQUIRE((expectedStringLen - 1) == n);
                REQUIRE(0 == strncmp(expectedString, readPointer, expectedStringLen));
            }
        }

        /* ----------------------------------------------------------------- */
        
        WHEN("printf() uses the %li signed integer specifier and a positive integer value")
        {
            int n = test_printf("The answer is %li\r\n", 789012L);

            THEN("The test buffer shall contain the expected string")
            {
                const char* expectedString = "The answer is 789012\r\n~";
                int expectedStringLen = strlen(expectedString);
                REQUIRE((expectedStringLen - 1) == n);
                REQUIRE(0 == strncmp(expectedString, readPointer, expectedStringLen));
            }
        }

        /* ----------------------------------------------------------------- */
        
        WHEN("printf() uses the %li signed integer specifier and a negative integer value")
        {
            int n = test_printf("The answer is %li\r\n", -12345L);

            THEN("The test buffer shall contain the expected string")
            {
                const char* expectedString = "The answer is -12345\r\n~";
                int expectedStringLen = strlen(expectedString);
                REQUIRE((expectedStringLen - 1) == n);
                REQUIRE(0 == strncmp(expectedString, readPointer, expectedStringLen));
            }
        }

        /* ----------------------------------------------------------------- */
        
        WHEN("printf() uses the %li signed long specifier and maximum negative integer value")
        {
            int n = test_printf("The answer is %li\r\n", LONG_MIN);

            THEN("The test buffer shall contain the expected string")
            {
                const char* expectedString = "The answer is " LONG_MIN_STRING "\r\n~";
                int expectedStringLen = strlen(expectedString);
                REQUIRE((expectedStringLen - 1) == n);
                REQUIRE(0 == strncmp(expectedString, readPointer, expectedStringLen));
            }
        }

        /* ----------------------------------------------------------------- */
        
        WHEN("printf() uses the %ld signed integer specifier and zero value")
        {
            int n = test_printf("The answer is %ld\r\n", 0L);

            THEN("The test buffer shall contain the expected string")
            {
                const char* expectedString = "The answer is 0\r\n~";
                int expectedStringLen = strlen(expectedString);
                REQUIRE((expectedStringLen - 1) == n);
                REQUIRE(0 == strncmp(expectedString, readPointer, expectedStringLen));
            }
        }

        /* ----------------------------------------------------------------- */
        
        WHEN("printf() uses the %8ld specifier to write signed long data to a field of width of 8 characters")
        {
            int n = test_printf("Data:%8ld%8ld%8ld\r\n", 1234L, 0L, -567890L);

            THEN("The test buffer shall contain the expected string")
            {
                const char* expectedString = "Data:    1234       0 -567890\r\n~";
                int expectedStringLen = strlen(expectedString);
                REQUIRE((expectedStringLen - 1) == n);
                REQUIRE(0 == strncmp(expectedString, readPointer, expectedStringLen));
            }
        }

        /* ----------------------------------------------------------------- */
        
        WHEN("printf() uses the %010d specifier to write signed LONG data to a field of width of 10 characters with zero padding")
        {
            int n = test_printf("Data: %010d %010d %010d\r\n", 123456L, 0L, -567890L);

            THEN("The test buffer shall contain the expected string")
            {
                const char* expectedString = "Data: 0000123456 0000000000 -000567890\r\n~";
                int expectedStringLen = strlen(expectedString);
                REQUIRE((expectedStringLen - 1) == n);
                REQUIRE(0 == strncmp(expectedString, readPointer, expectedStringLen));
            }
        }

        /* ----------------------------------------------------------------- */
        
        WHEN("printf() uses the %04li specifier to write a positive long data of length 5 characters to a field of width of 4 characters")
        {
            int n = test_printf("Timestamp: %04li milliseconds\r\n", 12345L);

            THEN("The field width shall be increased to fit the number")
            {
                const char* expectedString = "Timestamp: 12345 milliseconds\r\n~";
                int expectedStringLen = strlen(expectedString);
                REQUIRE((expectedStringLen - 1) == n);
                REQUIRE(0 == strncmp(expectedString, readPointer, expectedStringLen));
            }
        }

        /* ----------------------------------------------------------------- */
        
        WHEN("printf() uses the %04ld specifier to write negative long data of length 6 characters to a field of width of 4 characters with zero padding")
        {
            int n = test_printf("Score: %04ld points\r\n", -12345L);

            THEN("The field width shall be increased to fit the number")
            {
                const char* expectedString = "Score: -12345 points\r\n~";
                int expectedStringLen = strlen(expectedString);
                REQUIRE((expectedStringLen - 1) == n);
                REQUIRE(0 == strncmp(expectedString, readPointer, expectedStringLen));
            }
        }
    }

    /* ********************************************************************* */

    GIVEN("The test buffer is nearly full, leaving just 5 characters of free space")
    {
        resetTestBuffer();
        for(size_t i = 0; i < sizeof(testBuffer) - 5; i++)
        {
            test_printf("X");
            readPointer += 1;
        }

        /* ----------------------------------------------------------------- */

        WHEN("printf() uses the %ld signed long specifier and a positive integer value")
        {
            int n = test_printf("x = %ld\r\n", 12345678L);

            THEN("The expected string shall be truncated after 5 characters have been printed")
            {
                const char* expectedString = "x = 1";
                int expectedStringLen = 5;
                REQUIRE(expectedStringLen == n);
                REQUIRE(0 == strncmp(expectedString, readPointer, expectedStringLen));
            }
        }

        /* ----------------------------------------------------------------- */
        
        WHEN("printf() uses the %ld signed long specifier and a negative integer value")
        {
            int n = test_printf("x: %ld\r\n", -12345678L);

            THEN("The expected string shall be truncated after 5 characters have been printed")
            {
                const char* expectedString = "x: -1";
                int expectedStringLen = 5;
                REQUIRE(expectedStringLen== n);
                REQUIRE(0 == strncmp(expectedString, readPointer, expectedStringLen));
            }
        }

        /* ----------------------------------------------------------------- */
        
        WHEN("printf() uses the %ld signed long specifier and a negative integer value for which the minus sign fails to print")
        {
            int n = test_printf("x2 = %ld\r\n", -12345678L);

            THEN("The expected string shall be truncated after 5 characters have been printed")
            {
                const char* expectedString = "x2 = ";
                int expectedStringLen = 5;
                REQUIRE(expectedStringLen== n);
                REQUIRE(0 == strncmp(expectedString, readPointer, expectedStringLen));
            }
        }
    }
}


SCENARIO("mqosStdio module tests for printf() function with unsigned long data", "[mqosStdio_printf_ulong]")
{
    GIVEN("The test buffer is empty")
    {
        resetTestBuffer();

        /* ----------------------------------------------------------------- */
        
        WHEN("printf() uses the %lu unsigned integer specifier and the maximum positive integer value")
        {
            int n = test_printf("The answer is %lu\r\n", ULONG_MAX);

            THEN("The test buffer shall contain the expected string")
            {
                const char* expectedString = "The answer is " ULONG_MAX_STRING "\r\n~";
                int expectedStringLen = strlen(expectedString);
                REQUIRE((expectedStringLen - 1) == n);
                REQUIRE(0 == strncmp(expectedString, readPointer, expectedStringLen));
            }
        }

        /* ----------------------------------------------------------------- */
        
        WHEN("printf() uses the %lu unsigned integer specifier and a positive integer value")
        {
            int n = test_printf("The answer is %lu\r\n", 6497341UL);

            THEN("The test buffer shall contain the expected string")
            {
                const char* expectedString = "The answer is 6497341\r\n~";
                int expectedStringLen = strlen(expectedString);
                REQUIRE((expectedStringLen - 1) == n);
                REQUIRE(0 == strncmp(expectedString, readPointer, expectedStringLen));
            }
        }

        /* ----------------------------------------------------------------- */
        
        WHEN("printf() uses the %lu unsigned integer specifier and zero value")
        {
            int n = test_printf("The answer is %lu\r\n", 0UL);

            THEN("The test buffer shall contain the expected string")
            {
                const char* expectedString = "The answer is 0\r\n~";
                int expectedStringLen = strlen(expectedString);
                REQUIRE((expectedStringLen - 1) == n);
                REQUIRE(0 == strncmp(expectedString, readPointer, expectedStringLen));
            }
        }

        /* ----------------------------------------------------------------- */
        
        WHEN("printf() uses the %010lu specifier to write unsigned long data to a field of width of 10 characters with zero padding")
        {
            int n = test_printf("Data: %010lu %010lu %010lu\r\n", 123456UL, 0UL, 1234567890UL);

            THEN("The test buffer shall contain the expected string")
            {
                const char* expectedString = "Data: 0000123456 0000000000 1234567890\r\n~";
                int expectedStringLen = strlen(expectedString);
                REQUIRE((expectedStringLen - 1) == n);
                REQUIRE(0 == strncmp(expectedString, readPointer, expectedStringLen));
            }
        }

        /* ----------------------------------------------------------------- */
        
        WHEN("printf() uses the %04lu specifier to write positive unsigned long data of length 5 characters to a field of width of 4 characters")
        {
            int n = test_printf("Timestamp: %04lu milliseconds\r\n", 12345UL);

            THEN("The field width shall be increased to fit the number")
            {
                const char* expectedString = "Timestamp: 12345 milliseconds\r\n~";
                int expectedStringLen = strlen(expectedString);
                REQUIRE((expectedStringLen - 1) == n);
                REQUIRE(0 == strncmp(expectedString, readPointer, expectedStringLen));
            }
        }
    }

    /* ********************************************************************* */

    GIVEN("The test buffer is nearly full, leaving just 5 characters of free space")
    {
        resetTestBuffer();
        for(size_t i = 0; i < sizeof(testBuffer) - 5; i++)
        {
            test_printf("X");
            readPointer += 1;
        }

        /* ----------------------------------------------------------------- */

        WHEN("printf() uses the %lu long unsigned signed integer specifier and a positive integer value")
        {
            int n = test_printf("x = %lu\r\n", 1234UL);

            THEN("The expected string shall be truncated after 5 characters have been printed")
            {
                const char* expectedString = "x = 1";
                int expectedStringLen = 5;
                REQUIRE(expectedStringLen == n);
                REQUIRE(0 == strncmp(expectedString, readPointer, expectedStringLen));
            }
        }
    }
}


SCENARIO("mqosStdio module tests for printf() function with hexadecimal formatted long int data", "[mqosStdio_printf_hexlong]")
{
    GIVEN("The test buffer is empty")
    {
        resetTestBuffer();

        /* ----------------------------------------------------------------- */
        
        WHEN("printf() uses the %lX hexadecimal specifier with values from 0 to 255")
        {
            long testVal[10] = { 0x00L, 0xFFL, 0x01L, 0x23L, 0x45L,
                                 0x67L, 0x89L, 0xABL, 0xCDL, 0xEFL };
            int n = 0;
            for(size_t i = 0; i < sizeof(testVal) / sizeof(testVal[0]); i++)
            {
                n += test_printf("0x%lX ", testVal[i]);
            }

            THEN("The test buffer shall contain the expected string")
            {
                const char* expectedString = { "0x0 0xFF 0x1 0x23 0x45 "
                                               "0x67 0x89 0xAB 0xCD 0xEF ~" };
                int expectedStringLen = strlen(expectedString);
                REQUIRE((expectedStringLen - 1) == n);
                REQUIRE(0 == strncmp(expectedString, readPointer, expectedStringLen));
            }
        }

        /* ----------------------------------------------------------------- */
        
        WHEN("printf() uses the %lx hexadecimal specifier with values from 0 to 255")
        {
            long testVal[10] = { 0x00L, 0xFFL, 0x01L, 0x23L, 0x45L,
                                 0x67L, 0x89L, 0xABL, 0xCDL, 0xEFL };
            int n = 0;
            for(size_t i = 0; i < sizeof(testVal) / sizeof(testVal[0]); i++)
            {
                n += test_printf("0x%lx ", testVal[i]);
            }

            THEN("The test buffer shall contain the expected string")
            {
                const char* expectedString = { "0x0 0xff 0x1 0x23 0x45 "
                                               "0x67 0x89 0xab 0xcd 0xef ~" };
                int expectedStringLen = strlen(expectedString);
                REQUIRE((expectedStringLen - 1) == n);
                REQUIRE(0 == strncmp(expectedString, readPointer, expectedStringLen));
            }
        }

        /* ----------------------------------------------------------------- */
        
        WHEN("printf() uses the %lX hexadecimal specifier with maximum positive unsigned long value")
        {
            int n = test_printf("0x%lX\n", ULONG_MAX);

            THEN("The test buffer shall contain the expected string")
            {
                const char* expectedString = "0x" ULONG_MAX_HEX_STRING_UPPERCASE "\n~";
                int expectedStringLen = strlen(expectedString);
                REQUIRE((expectedStringLen - 1) == n);
                REQUIRE(0 == strncmp(expectedString, readPointer, expectedStringLen));
            }
        }

        /* ----------------------------------------------------------------- */
        
        WHEN("printf() uses the %lX hexadecimal specifier with an unsigned long value")
        {
            int n = test_printf("0x%lX\n", 0x1001beefUL);

            THEN("The test buffer shall contain the expected string")
            {
                const char* expectedString = "0x1001BEEF\n~";
                int expectedStringLen = strlen(expectedString);
                REQUIRE((expectedStringLen - 1) == n);
                REQUIRE(0 == strncmp(expectedString, readPointer, expectedStringLen));
            }
        }

        /* ----------------------------------------------------------------- */
        
        WHEN("printf() uses the %lx hexadecimal specifier with an unsigned long value")
        {
            int n = test_printf("0x%x\n", 0xDEADC0DEUL);

            THEN("The test buffer shall contain the expected string")
            {
                const char* expectedString = "0xdeadc0de\n~";
                int expectedStringLen = strlen(expectedString);
                REQUIRE((expectedStringLen - 1) == n);
                REQUIRE(0 == strncmp(expectedString, readPointer, expectedStringLen));
            }
        }

        /* ----------------------------------------------------------------- */
        
        WHEN("printf() uses the %lx hexadecimal specifier with maximum negative signed long value")
        {
            int n = test_printf("0x%lx\n", LONG_MIN);

            THEN("The test buffer shall contain the expected string")
            {
                const char* expectedString = "0x" LONG_MIN_HEX_STRING_LOWERCASE "\n~";
                int expectedStringLen = strlen(expectedString);
                REQUIRE((expectedStringLen - 1) == n);
                REQUIRE(0 == strncmp(expectedString, readPointer, expectedStringLen));
            }
        }

        /* ----------------------------------------------------------------- */
        
        WHEN("printf() uses the %10lX hexadecimal specifier to write data to a field of width of 10 characters")
        {
            int n = test_printf("Data:%10lX%10lX\r\n", 0x56789abcL, 0x9abL);

            THEN("The test buffer shall contain the expected string")
            {
                const char* expectedString = "Data:  56789ABC       9AB\r\n~";
                int expectedStringLen = strlen(expectedString);
                REQUIRE((expectedStringLen - 1) == n);
                REQUIRE(0 == strncmp(expectedString, readPointer, expectedStringLen));
            }
        }

        /* ----------------------------------------------------------------- */
        
        WHEN("printf() uses the %012x hexadecimal specifier to write unsigned long data to a field of width of 12 characters with zero padding")
        {
            int n = test_printf("Data: 0x%012lx 0x%012lx 0x%012lx\r\n", 0x10UL, 0UL, 0x479B0102UL);

            THEN("The test buffer shall contain the expected string")
            {
                const char* expectedString = "Data: 0x000000000010 0x000000000000 0x0000479b0102\r\n~";
                int expectedStringLen = strlen(expectedString);
                REQUIRE((expectedStringLen - 1) == n);
                REQUIRE(0 == strncmp(expectedString, readPointer, expectedStringLen));
            }
        }

        /* ----------------------------------------------------------------- */
        
        WHEN("printf() uses the %02X hexadecimal specifier to write unsigned long data of length 4 characters to a field of width of 2 characters")
        {
            int n = test_printf("Timestamp: 0x%02X milliseconds\r\n", 0x12abUL);

            THEN("The field width shall be increased to fit the number")
            {
                const char* expectedString = "Timestamp: 0x12AB milliseconds\r\n~";
                int expectedStringLen = strlen(expectedString);
                REQUIRE((expectedStringLen - 1) == n);
                REQUIRE(0 == strncmp(expectedString, readPointer, expectedStringLen));
            }
        }
    }

    /* ********************************************************************* */

    GIVEN("The test buffer is nearly full, leaving just 5 characters of free space")
    {
        resetTestBuffer();
        for(size_t i = 0; i < sizeof(testBuffer) - 5; i++)
        {
            test_printf("X");
            readPointer += 1;
        }

        /* ----------------------------------------------------------------- */
        
        WHEN("printf() uses the %lX hexadecimal integer specifier and an unsigned long value")
        {
            int n = test_printf("0x%lX\r\n", 0xabcdUL);

            THEN("The expected string shall be truncated after 5 characters have been printed")
            {
                const char* expectedString = "0xABC";
                int expectedStringLen = 5;
                REQUIRE(expectedStringLen == n);
                REQUIRE(0 == strncmp(expectedString, readPointer, expectedStringLen));
            }
        }

        /* ----------------------------------------------------------------- */
        
        WHEN("printf() uses the %lx hexadecimal integer specifier and a negative integer value")
        {
            int n = test_printf("0x%x\r\n", -1L);

            THEN("The expected string shall be truncated after 5 characters have been printed")
            {
                const char* expectedString = "0xfff";
                int expectedStringLen = 5;
                REQUIRE(expectedStringLen == n);
                REQUIRE(0 == strncmp(expectedString, readPointer, expectedStringLen));
            }
        }
    }
}


#endif  /* #if MQOS_CONFIG_STDIO_LIBRARY_LONG_PRINTF_ENABLED */


SCENARIO("mqosStdio module tests for sprintf() function", "[mqosStdio_sprintf]")
{
    GIVEN("The destination C string is filled with ~ characters")
    {
        char sprintTestBuffer[80];
        memset(sprintTestBuffer, '~', sizeof(sprintTestBuffer));

        /* ----------------------------------------------------------------- */
        
        WHEN("sprintf() writes a string to dest")
        {
            int n = test_sprintf(sprintTestBuffer, "Hello world\r\n");

            THEN("The destination shall contain the expected string, with a terminating null character")
            {
                const char* expectedString = "Hello world\r\n\0~";
                int expectedStringLen = strlen(expectedString);
                REQUIRE(expectedStringLen == n);
                REQUIRE(0 == memcmp(expectedString, sprintTestBuffer, expectedStringLen + 2));
            }
        }

        /* ----------------------------------------------------------------- */

        WHEN("sprintf() uses the string print specifier")
        {
            int n = test_sprintf(sprintTestBuffer, "Hello %s world\r\n", "cruel");

            THEN("The test buffer shall contain the expected string, with a terminating null character")
            {
                const char* expectedString = "Hello cruel world\r\n\0~";
                int expectedStringLen = strlen(expectedString);
                REQUIRE(expectedStringLen == n);
                REQUIRE(0 == memcmp(expectedString, sprintTestBuffer, expectedStringLen + 2));
            }
        }

        /* ----------------------------------------------------------------- */
        
        WHEN("sprintf() uses unsupported left justify and precision specifiers")
        {
            /* MQOS printf does not support left justify or precision specifiers */
            int n = test_sprintf(sprintTestBuffer, "Value is %-8.4d\n", 25);

            THEN("The expected string shall be truncated at the first unsupported specifier")
            {
                const char* expectedString = "Value is \0~";
                int expectedStringLen = strlen(expectedString);
                REQUIRE(expectedStringLen == n);
                REQUIRE(0 == memcmp(expectedString, sprintTestBuffer, expectedStringLen + 2));
            }
        }

        /* ----------------------------------------------------------------- */
        
        WHEN("sprintf() uses unsupported floating point specifier")
        {
            /* MQOS printf does not support left justify or precision specifiers */
            int n = test_sprintf(sprintTestBuffer, "Value is %0.4f\n", 37.63);

            THEN("The expected string shall be truncated at the first unsupported specifier")
            {
                const char* expectedString = "Value is \0~";
                int expectedStringLen = strlen(expectedString);
                REQUIRE(expectedStringLen == n);
                REQUIRE(0 == memcmp(expectedString, sprintTestBuffer, expectedStringLen + 2));
            }
        }

        /* ----------------------------------------------------------------- */
        
        WHEN("sprintf() uses the %c character specifier")
        {
            int n = test_sprintf(sprintTestBuffer, "The %c character is inserted\r\n", 'X');

            THEN("The test buffer shall contain the expected string")
            {
                const char* expectedString = "The X character is inserted\r\n\0~";
                int expectedStringLen = strlen(expectedString);
                REQUIRE(expectedStringLen == n);
                REQUIRE(0 == memcmp(expectedString, sprintTestBuffer, expectedStringLen + 2));
            }
        }

        /* ----------------------------------------------------------------- */
        
        WHEN("sprintf() uses the %d signed integer specifier and maximum positive integer value")
        {
            int n = test_sprintf(sprintTestBuffer, "The answer is %d\r\n", INT_MAX);
        
            THEN("The test buffer shall contain the expected string")
            {
                const char* expectedString = "The answer is " INT_MAX_STRING "\r\n\0~";
                int expectedStringLen = strlen(expectedString);
                REQUIRE(expectedStringLen == n);
                REQUIRE(0 == memcmp(expectedString, sprintTestBuffer, expectedStringLen + 2));
            }
        }

        /* ----------------------------------------------------------------- */
        
        WHEN("sprintf() uses the %d signed integer specifier and a positive integer value")
        {
            int n = test_sprintf(sprintTestBuffer, "The answer is %d\r\n", 12345);

            THEN("The test buffer shall contain the expected string")
            {
                const char* expectedString = "The answer is 12345\r\n\0~";
                int expectedStringLen = strlen(expectedString);
                REQUIRE(expectedStringLen == n);
                REQUIRE(0 == memcmp(expectedString, sprintTestBuffer, expectedStringLen + 2));
            }
        }

        /* ----------------------------------------------------------------- */
        
        WHEN("sprintf() uses the %d signed integer specifier and a negative integer value")
        {
            int n = test_sprintf(sprintTestBuffer, "The answer is %d\r\n", -6789);

            THEN("The test buffer shall contain the expected string")
            {
                const char* expectedString = "The answer is -6789\r\n\0~";
                int expectedStringLen = strlen(expectedString);
                REQUIRE(expectedStringLen == n);
                REQUIRE(0 == memcmp(expectedString, sprintTestBuffer, expectedStringLen + 2));
            }
        }

        /* ----------------------------------------------------------------- */
        
        WHEN("sprintf() uses the %i signed integer specifier and maximum negative integer value")
        {
            int n = test_sprintf(sprintTestBuffer, "The answer is %i\r\n", INT_MIN);

            THEN("The test buffer shall contain the expected string")
            {
                const char* expectedString = "The answer is " INT_MIN_STRING "\r\n\0~";
                int expectedStringLen = strlen(expectedString);
                REQUIRE(expectedStringLen == n);
                REQUIRE(0 == memcmp(expectedString, sprintTestBuffer, expectedStringLen + 2));
            }
        }

        /* ----------------------------------------------------------------- */
        
        WHEN("sprintf() uses the %d signed integer specifier and zero value")
        {
            int n = test_sprintf(sprintTestBuffer, "The answer is %d\r\n", -0);

            THEN("The test buffer shall contain the expected string")
            {
                const char* expectedString = "The answer is 0\r\n\0~";
                int expectedStringLen = strlen(expectedString);
                REQUIRE(expectedStringLen == n);
                REQUIRE(0 == memcmp(expectedString, sprintTestBuffer, expectedStringLen + 2));
            }
        }

        /* ----------------------------------------------------------------- */
        
        WHEN("sprintf() uses the %8d specifier to write signed integer data to a field of width of 8 characters")
        {
            int n = test_sprintf(sprintTestBuffer, "Data:%8d%8d%8d\r\n", 1234, 0, -56789);

            THEN("The test buffer shall contain the expected string")
            {
                const char* expectedString = "Data:    1234       0  -56789\r\n\0~";
                int expectedStringLen = strlen(expectedString);
                REQUIRE(expectedStringLen == n);
                REQUIRE(0 == memcmp(expectedString, sprintTestBuffer, expectedStringLen + 2));
            }
        }

        /* ----------------------------------------------------------------- */
        
        WHEN("sprintf() uses the %06d specifier to write signed integer data to a field of width of 6 characters with zero padding")
        {
            int n = test_sprintf(sprintTestBuffer, "Data: %06d %06d %06d\r\n", 1234, 0, -5678);

            THEN("The test buffer shall contain the expected string")
            {
                const char* expectedString = "Data: 001234 000000 -05678\r\n\0~";
                int expectedStringLen = strlen(expectedString);
                REQUIRE(expectedStringLen == n);
                REQUIRE(0 == memcmp(expectedString, sprintTestBuffer, expectedStringLen + 2));
            }
        }

        /* ----------------------------------------------------------------- */
        
        WHEN("sprintf() uses the %04i specifier to write a positive integer data of length 5 characters to a field of width of 4 characters")
        {
            int n = test_sprintf(sprintTestBuffer, "Timestamp: %04i milliseconds\r\n", 12345);

            THEN("The field width shall be increased to fit the number")
            {
                const char* expectedString = "Timestamp: 12345 milliseconds\r\n\0~";
                int expectedStringLen = strlen(expectedString);
                REQUIRE(expectedStringLen == n);
                REQUIRE(0 == memcmp(expectedString, sprintTestBuffer, expectedStringLen + 2));
            }
        }

        /* ----------------------------------------------------------------- */
        
        WHEN("sprintf() uses the %04d specifier to write a negative integer data of length 6 characters to a field of width of 4 characters with zero padding")
        {
            int n = test_sprintf(sprintTestBuffer, "Score: %04d points\r\n", -12345);

            THEN("The field width shall be increased to fit the number")
            {
                const char* expectedString = "Score: -12345 points\r\n\0~";
                int expectedStringLen = strlen(expectedString);
                REQUIRE(expectedStringLen == n);
                REQUIRE(0 == memcmp(expectedString, sprintTestBuffer, expectedStringLen + 2));
            }
        }

        /* ----------------------------------------------------------------- */
        
        WHEN("sprintf() uses the %u unsigned integer specifier and maximum positive integer value")
        {
            int n = test_sprintf(sprintTestBuffer, "The answer is %u\r\n", UINT_MAX);

            THEN("The test buffer shall contain the expected string")
            {
                const char* expectedString = "The answer is " UINT_MAX_STRING "\r\n\0~";
                int expectedStringLen = strlen(expectedString);
                REQUIRE(expectedStringLen == n);
                REQUIRE(0 == memcmp(expectedString, sprintTestBuffer, expectedStringLen + 2));
            }
        }

        /* ----------------------------------------------------------------- */
        
        WHEN("sprintf() uses the %u unsigned integer specifier and a positive integer value")
        {
            int n = test_sprintf(sprintTestBuffer, "The answer is %u\r\n", 54321);

            THEN("The test buffer shall contain the expected string")
            {
                const char* expectedString = "The answer is 54321\r\n\0~";
                int expectedStringLen = strlen(expectedString);
                REQUIRE(expectedStringLen == n);
                REQUIRE(0 == memcmp(expectedString, sprintTestBuffer, expectedStringLen + 2));
            }
        }

        /* ----------------------------------------------------------------- */
        
        WHEN("sprintf() uses the %u unsigned integer specifier and zero value")
        {
            int n = test_sprintf(sprintTestBuffer, "The answer is %u\r\n", 0);

            THEN("The test buffer shall contain the expected string")
            {
                const char* expectedString = "The answer is 0\r\n\0~";
                int expectedStringLen = strlen(expectedString);
                REQUIRE(expectedStringLen == n);
                REQUIRE(0 == memcmp(expectedString, sprintTestBuffer, expectedStringLen + 2));
            }
        }

        /* ----------------------------------------------------------------- */
        
        WHEN("sprintf() uses the %8u specifier to write unsigned integer data to a field of width of 8 characters")
        {
            int n = test_sprintf(sprintTestBuffer, "Data:%8u%8u\r\n", 12345U, 0U);

            THEN("The test buffer shall contain the expected string")
            {
                const char* expectedString = "Data:   12345       0\r\n\0~";
                int expectedStringLen = strlen(expectedString);
                REQUIRE(expectedStringLen == n);
                REQUIRE(0 == memcmp(expectedString, sprintTestBuffer, expectedStringLen + 2));
            }
        }

        /* ----------------------------------------------------------------- */
        
        WHEN("sprintf() uses the %06u specifier to write unsigned integer data to a field of width of 6 characters with zero padding")
        {
            int n = test_sprintf(sprintTestBuffer, "Data: %06u %06u %06u\r\n", 1234, 0, 456789);

            THEN("The test buffer shall contain the expected string")
            {
                const char* expectedString = "Data: 001234 000000 456789\r\n\0~";
                int expectedStringLen = strlen(expectedString);
                REQUIRE(expectedStringLen == n);
                REQUIRE(0 == memcmp(expectedString, sprintTestBuffer, expectedStringLen + 2));
            }
        }

        /* ----------------------------------------------------------------- */
        
        WHEN("sprintf() uses the %04u specifier to write unsigned integer data of length 5 characters to a field of width of 4 characters")
        {
            int n = test_sprintf(sprintTestBuffer, "Timestamp: %04u milliseconds\r\n", 12345);

            THEN("The field width shall be increased to fit the number")
            {
                const char* expectedString = "Timestamp: 12345 milliseconds\r\n\0~";
                int expectedStringLen = strlen(expectedString);
                REQUIRE(expectedStringLen == n);
                REQUIRE(0 == memcmp(expectedString, sprintTestBuffer, expectedStringLen + 2));
            }
        }

        /* ----------------------------------------------------------------- */
        
        WHEN("sprintf() uses the %X hexadecimal specifier with values from 0 to 255")
        {
            int testVal[10] = { 0x00, 0xFF , 0x01, 0x23, 0x45,
                                0x67, 0x89, 0xAB, 0xCD, 0xEF };
            int n = 0;
            for(size_t i = 0; i < sizeof(testVal) / sizeof(testVal[0]); i++)
            {
                n += test_sprintf(&sprintTestBuffer[n], "0x%X ", testVal[i]);
            }

            THEN("The test buffer shall contain the expected string")
            {
                const char* expectedString = { "0x0 0xFF 0x1 0x23 0x45 "
                                               "0x67 0x89 0xAB 0xCD 0xEF \0~" };
                int expectedStringLen = strlen(expectedString);
                REQUIRE(expectedStringLen == n);
                REQUIRE(0 == memcmp(expectedString, sprintTestBuffer, expectedStringLen + 2));
            }
        }

        /* ----------------------------------------------------------------- */
        
        WHEN("sprintf() uses the %x hexadecimal specifier with values from 0 to 255")
        {
            int testVal[10] = { 0x00, 0xFF , 0x01, 0x23, 0x45,
                                0x67, 0x89, 0xAB, 0xCD, 0xEF };
            int n = 0;
            for(size_t i = 0; i < sizeof(testVal) / sizeof(testVal[0]); i++)
            {
                n += test_sprintf(&sprintTestBuffer[n], "0x%x ", testVal[i]);
            }

            THEN("The test buffer shall contain the expected string")
            {
                const char* expectedString = { "0x0 0xff 0x1 0x23 0x45 "
                                               "0x67 0x89 0xab 0xcd 0xef \0~" };
                int expectedStringLen = strlen(expectedString);
                REQUIRE(expectedStringLen == n);
                REQUIRE(0 == memcmp(expectedString, sprintTestBuffer, expectedStringLen + 2));
            }
        }

        /* ----------------------------------------------------------------- */
        
        WHEN("sprintf() uses the %X hexadecimal specifier with maximum positive unsigned integer value")
        {
            int n = test_sprintf(sprintTestBuffer, "0x%X\n", UINT_MAX);

            THEN("The test buffer shall contain the expected string")
            {
                const char* expectedString = "0x" UINT_MAX_HEX_STRING_UPPERCASE "\n\0~";
                int expectedStringLen = strlen(expectedString);
                REQUIRE(expectedStringLen == n);
                REQUIRE(0 == memcmp(expectedString, sprintTestBuffer, expectedStringLen + 2));
            }
        }

        /* ----------------------------------------------------------------- */
        
        WHEN("sprintf() uses the %X hexadecimal specifier with an unsigned integer value")
        {
            int n = test_sprintf(sprintTestBuffer, "0x%X\n", 0xbeef);

            THEN("The test buffer shall contain the expected string")
            {
                const char* expectedString = "0xBEEF\n\0~";
                int expectedStringLen = strlen(expectedString);
                REQUIRE(expectedStringLen == n);
                REQUIRE(0 == memcmp(expectedString, sprintTestBuffer, expectedStringLen + 2));
            }
        }

        /* ----------------------------------------------------------------- */
        
        WHEN("sprintf() uses the %x hexadecimal specifier with an unsigned integer value")
        {
            int n = test_sprintf(sprintTestBuffer,"0x%x\n", 0xC0DE);

            THEN("The test buffer shall contain the expected string")
            {
                const char* expectedString = "0xc0de\n\0~";
                int expectedStringLen = strlen(expectedString);
                REQUIRE(expectedStringLen == n);
                REQUIRE(0 == memcmp(expectedString, sprintTestBuffer, expectedStringLen + 2));
            }
        }

        /* ----------------------------------------------------------------- */
        
        WHEN("sprintf() uses the %x hexadecimal specifier with maximum negative signed integer value")
        {
            int n = test_sprintf(sprintTestBuffer, "0x%x\n", INT_MIN);

            THEN("The test buffer shall contain the expected string")
            {
                const char* expectedString = "0x" INT_MIN_HEX_STRING "\n\0~";
                int expectedStringLen = strlen(expectedString);
                REQUIRE(expectedStringLen == n);
                REQUIRE(0 == memcmp(expectedString, sprintTestBuffer, expectedStringLen + 2));
            }
        }

        /* ----------------------------------------------------------------- */
        
        WHEN("sprintf() uses the %8X hexadecimal specifier to write data to a field of width of 8 characters")
        {
            int n = test_sprintf(sprintTestBuffer, "Data:%8X%8X\r\n", 0x5678, 0x9ab);

            THEN("The test buffer shall contain the expected string")
            {
                const char* expectedString = "Data:    5678     9AB\r\n\0~";
                int expectedStringLen = strlen(expectedString);
                REQUIRE(expectedStringLen == n);
                REQUIRE(0 == memcmp(expectedString, sprintTestBuffer, expectedStringLen + 2));
            }
        }

        /* ----------------------------------------------------------------- */
        
        WHEN("sprintf() uses the %04x hexadecimal specifier to write unsigned integer data to a field of width of 4 characters with zero padding")
        {
            int n = test_sprintf(sprintTestBuffer, "Data: 0x%04x 0x%04x 0x%04x\r\n", 0x10, 0, 0x479B);

            THEN("The test buffer shall contain the expected string")
            {
                const char* expectedString = "Data: 0x0010 0x0000 0x479b\r\n\0~";
                int expectedStringLen = strlen(expectedString);
                REQUIRE(expectedStringLen == n);
                REQUIRE(0 == memcmp(expectedString, sprintTestBuffer, expectedStringLen + 2));
            }
        }

        /* ----------------------------------------------------------------- */
        
        WHEN("sprintf() uses the %02X hexadecimal specifier to write unsigned integer data of length 4 characters to a field of width of 2 characters")
        {
            int n = test_sprintf(sprintTestBuffer, "Timestamp: 0x%02X milliseconds\r\n", 0x12ab);

            THEN("The field width shall be increased to fit the number")
            {
                const char* expectedString = "Timestamp: 0x12AB milliseconds\r\n\0~";
                int expectedStringLen = strlen(expectedString);
                REQUIRE(expectedStringLen == n);
                REQUIRE(0 == memcmp(expectedString, sprintTestBuffer, expectedStringLen));
            }
        }

        /* ----------------------------------------------------------------- */

#if MQOS_CONFIG_STDIO_LIBRARY_LONG_PRINTF_ENABLED

        WHEN("sprintf() uses the %ld signed long specifier and the maximum positive integer value")
        {
            int n = test_sprintf(sprintTestBuffer, "The answer is %ld\r\n", LONG_MAX);

            THEN("The test buffer shall contain the expected string")
            {
                const char* expectedString = "The answer is " LONG_MAX_STRING "\r\n\0~";
                int expectedStringLen = strlen(expectedString);
                REQUIRE(expectedStringLen == n);
                REQUIRE(0 == memcmp(expectedString, sprintTestBuffer, expectedStringLen + 2));
            }
        }

        /* ----------------------------------------------------------------- */
        
        WHEN("sprintf() uses the %li signed integer specifier and a positive integer value")
        {
            int n = test_sprintf(sprintTestBuffer, "The answer is %li\r\n", 789012);

            THEN("The test buffer shall contain the expected string")
            {
                const char* expectedString = "The answer is 789012\r\n\0~";
                int expectedStringLen = strlen(expectedString);
                REQUIRE(expectedStringLen == n);
                REQUIRE(0 == memcmp(expectedString, sprintTestBuffer, expectedStringLen + 2));
            }
        }

        /* ----------------------------------------------------------------- */
        
        WHEN("sprintf() uses the %li signed integer specifier and a negative integer value")
        {
            int n = test_sprintf(sprintTestBuffer, "The answer is %li\r\n", -12345L);

            THEN("The test buffer shall contain the expected string")
            {
                const char* expectedString = "The answer is -12345\r\n\0~";
                int expectedStringLen = strlen(expectedString);
                REQUIRE(expectedStringLen == n);
                REQUIRE(0 == memcmp(expectedString, sprintTestBuffer, expectedStringLen + 2));
            }
        }

        /* ----------------------------------------------------------------- */
        
        WHEN("sprintf() uses the %li signed long specifier and maximum negative integer value")
        {
            int n = test_sprintf(sprintTestBuffer, "The answer is %li\r\n", LONG_MIN);

            THEN("The test buffer shall contain the expected string")
            {
                const char* expectedString = "The answer is " LONG_MIN_STRING "\r\n\0~";
                int expectedStringLen = strlen(expectedString);
                REQUIRE(expectedStringLen == n);
                REQUIRE(0 == memcmp(expectedString, sprintTestBuffer, expectedStringLen + 2));
            }
        }

        /* ----------------------------------------------------------------- */
        
        WHEN("sprintf() uses the %ld signed integer specifier and zero value")
        {
            int n = test_sprintf(sprintTestBuffer, "The answer is %ld\r\n", 0);

            THEN("The test buffer shall contain the expected string")
            {
                const char* expectedString = "The answer is 0\r\n\0~";
                int expectedStringLen = strlen(expectedString);
                REQUIRE(expectedStringLen == n);
                REQUIRE(0 == memcmp(expectedString, sprintTestBuffer, expectedStringLen + 2));
            }
        }

        /* ----------------------------------------------------------------- */
        
        WHEN("sprintf() uses the %8ld specifier to write signed long data to a field of width of 8 characters")
        {
            int n = test_sprintf(sprintTestBuffer, "Data:%8ld%8ld%8ld\r\n", 1234L, 0L, -567890L);

            THEN("The test buffer shall contain the expected string")
            {
                const char* expectedString = "Data:    1234       0 -567890\r\n\0~";
                int expectedStringLen = strlen(expectedString);
                REQUIRE(expectedStringLen == n);
                REQUIRE(0 == memcmp(expectedString, sprintTestBuffer, expectedStringLen + 2));
            }
        }

        /* ----------------------------------------------------------------- */
        
        WHEN("sprintf() uses the %010d specifier to write signed LONG data to a field of width of 10 characters with zero padding")
        {
            int n = test_sprintf(sprintTestBuffer, "Data: %010d %010d %010d\r\n", 123456L, 0L, -567890L);

            THEN("The test buffer shall contain the expected string")
            {
                const char* expectedString = "Data: 0000123456 0000000000 -000567890\r\n\0~";
                int expectedStringLen = strlen(expectedString);
                REQUIRE(expectedStringLen == n);
                REQUIRE(0 == memcmp(expectedString, sprintTestBuffer, expectedStringLen + 2));
            }
        }

        /* ----------------------------------------------------------------- */
        
        WHEN("sprintf() uses the %04li specifier to write a positive long data of length 5 characters to a field of width of 4 characters")
        {
            int n = test_sprintf(sprintTestBuffer, "Timestamp: %04li milliseconds\r\n", 12345L);

            THEN("The field width shall be increased to fit the number")
            {
                const char* expectedString = "Timestamp: 12345 milliseconds\r\n\0~";
                int expectedStringLen = strlen(expectedString);
                REQUIRE(expectedStringLen == n);
                REQUIRE(0 == memcmp(expectedString, sprintTestBuffer, expectedStringLen + 2));
            }
        }

        /* ----------------------------------------------------------------- */
        
        WHEN("sprintf() uses the %04ld specifier to write negative long data of length 6 characters to a field of width of 4 characters with zero padding")
        {
            int n = test_sprintf(sprintTestBuffer, "Score: %04ld points\r\n", -12345L);

            THEN("The field width shall be increased to fit the number")
            {
                const char* expectedString = "Score: -12345 points\r\n\0~";
                int expectedStringLen = strlen(expectedString);
                REQUIRE(expectedStringLen == n);
                REQUIRE(0 == memcmp(expectedString, sprintTestBuffer, expectedStringLen + 2));
            }
        }

        /* ----------------------------------------------------------------- */

        WHEN("sprintf() uses the %lu unsigned integer specifier and the maximum positive integer value")
        {
            int n = test_sprintf(sprintTestBuffer, "The answer is %lu\r\n", ULONG_MAX);

            THEN("The test buffer shall contain the expected string")
            {
                const char* expectedString = "The answer is " ULONG_MAX_STRING "\r\n\0~";
                int expectedStringLen = strlen(expectedString);
                REQUIRE(expectedStringLen == n);
                REQUIRE(0 == memcmp(expectedString, sprintTestBuffer, expectedStringLen + 2));
            }
        }

        /* ----------------------------------------------------------------- */
        
        WHEN("sprintf() uses the %lu unsigned integer specifier and a positive integer value")
        {
            int n = test_sprintf(sprintTestBuffer, "The answer is %lu\r\n", 6497341);

            THEN("The test buffer shall contain the expected string")
            {
                const char* expectedString = "The answer is 6497341\r\n\0~";
                int expectedStringLen = strlen(expectedString);
                REQUIRE(expectedStringLen == n);
                REQUIRE(0 == memcmp(expectedString, sprintTestBuffer, expectedStringLen + 1));
            }
        }

        /* ----------------------------------------------------------------- */
        
        WHEN("sprintf() uses the %lu unsigned integer specifier and zero value")
        {
            int n = test_sprintf(sprintTestBuffer, "The answer is %lu\r\n", 0);

            THEN("The test buffer shall contain the expected string")
            {
                const char* expectedString = "The answer is 0\r\n\0~";
                int expectedStringLen = strlen(expectedString);
                REQUIRE(expectedStringLen == n);
                REQUIRE(0 == memcmp(expectedString, sprintTestBuffer, expectedStringLen + 2));
            }
        }

        /* ----------------------------------------------------------------- */
        
        WHEN("sprintf() uses the %010lu specifier to write unsigned long data to a field of width of 10 characters with zero padding")
        {
            int n = test_sprintf(sprintTestBuffer, "Data: %010lu %010lu %010lu\r\n", 123456UL, 0UL, 1234567890UL);

            THEN("The test buffer shall contain the expected string")
            {
                const char* expectedString = "Data: 0000123456 0000000000 1234567890\r\n\0~";
                int expectedStringLen = strlen(expectedString);
                REQUIRE(expectedStringLen == n);
                REQUIRE(0 == memcmp(expectedString, sprintTestBuffer, expectedStringLen + 2));
            }
        }

        /* ----------------------------------------------------------------- */
        
        WHEN("sprintf() uses the %04lu specifier to write positive unsigned long data of length 5 characters to a field of width of 4 characters")
        {
            int n = test_sprintf(sprintTestBuffer, "Timestamp: %04lu milliseconds\r\n", 12345UL);

            THEN("The field width shall be increased to fit the number")
            {
                const char* expectedString = "Timestamp: 12345 milliseconds\r\n\0~";
                int expectedStringLen = strlen(expectedString);
                REQUIRE(expectedStringLen == n);
                REQUIRE(0 == memcmp(expectedString, sprintTestBuffer, expectedStringLen + 2));
            }
        }

        /* ----------------------------------------------------------------- */
        
        WHEN("sprintf() uses the %lX hexadecimal specifier with values from 0 to 255")
        {
            int testVal[10] = { 0x00, 0xFF , 0x01, 0x23, 0x45,
                                0x67, 0x89, 0xAB, 0xCD, 0xEF };
            int n = 0;
            for(size_t i = 0; i < sizeof(testVal) / sizeof(testVal[0]); i++)
            {
                n += test_sprintf(&sprintTestBuffer[n], "0x%lX ", testVal[i]);
            }

            THEN("The test buffer shall contain the expected string")
            {
                const char* expectedString = { "0x0 0xFF 0x1 0x23 0x45 "
                                               "0x67 0x89 0xAB 0xCD 0xEF \0~" };
                int expectedStringLen = strlen(expectedString);
                REQUIRE(expectedStringLen == n);
                REQUIRE(0 == memcmp(expectedString, sprintTestBuffer, expectedStringLen + 2));
            }
        }

        /* ----------------------------------------------------------------- */
        
        WHEN("sprintf() uses the %lx hexadecimal specifier with values from 0 to 255")
        {
            int testVal[10] = { 0x00, 0xFF , 0x01, 0x23, 0x45,
                                0x67, 0x89, 0xAB, 0xCD, 0xEF };
            int n = 0;
            for(size_t i = 0; i < sizeof(testVal) / sizeof(testVal[0]); i++)
            {
                n += test_sprintf(&sprintTestBuffer[n], "0x%lx ", testVal[i]);
            }

            THEN("The test buffer shall contain the expected string")
            {
                const char* expectedString = { "0x0 0xff 0x1 0x23 0x45 "
                                               "0x67 0x89 0xab 0xcd 0xef \0~" };
                int expectedStringLen = strlen(expectedString);
                REQUIRE(expectedStringLen == n);
                REQUIRE(0 == memcmp(expectedString, sprintTestBuffer, expectedStringLen + 2));
            }
        }

        /* ----------------------------------------------------------------- */
        
        WHEN("sprintf() uses the %lX hexadecimal specifier with maximum positive unsigned long value")
        {
            int n = test_sprintf(sprintTestBuffer, "0x%lX\n", ULONG_MAX);

            THEN("The test buffer shall contain the expected string")
            {
                const char* expectedString = "0x" ULONG_MAX_HEX_STRING_UPPERCASE "\n\0~";
                int expectedStringLen = strlen(expectedString);
                REQUIRE(expectedStringLen == n);
                REQUIRE(0 == memcmp(expectedString, sprintTestBuffer, expectedStringLen + 2));
            }
        }

        /* ----------------------------------------------------------------- */
        
        WHEN("sprintf() uses the %lX hexadecimal specifier with an unsigned long value")
        {
            int n = test_sprintf(sprintTestBuffer, "0x%lX\n", 0x1001beefUL);

            THEN("The test buffer shall contain the expected string")
            {
                const char* expectedString = "0x1001BEEF\n\0~";
                int expectedStringLen = strlen(expectedString);
                REQUIRE(expectedStringLen == n);
                REQUIRE(0 == memcmp(expectedString, sprintTestBuffer, expectedStringLen + 2));
            }
        }

        /* ----------------------------------------------------------------- */
        
        WHEN("sprintf() uses the %lx hexadecimal specifier with an unsigned long value")
        {
            int n = test_sprintf(sprintTestBuffer, "0x%x\n", 0xDEADC0DEUL);

            THEN("The test buffer shall contain the expected string")
            {
                const char* expectedString = "0xdeadc0de\n\0~";
                int expectedStringLen = strlen(expectedString);
                REQUIRE(expectedStringLen == n);
                REQUIRE(0 == memcmp(expectedString, sprintTestBuffer, expectedStringLen + 2));
            }
        }

        /* ----------------------------------------------------------------- */
        
        WHEN("sprintf() uses the %lx hexadecimal specifier with maximum negative signed long value")
        {
            int n = test_sprintf(sprintTestBuffer, "0x%lx\n", LONG_MIN);

            THEN("The test buffer shall contain the expected string")
            {
                const char* expectedString = "0x" LONG_MIN_HEX_STRING_LOWERCASE "\n\0~";
                int expectedStringLen = strlen(expectedString);
                REQUIRE(expectedStringLen == n);
                REQUIRE(0 == memcmp(expectedString, sprintTestBuffer, expectedStringLen + 2));
            }
        }

        /* ----------------------------------------------------------------- */
        
        WHEN("sprintf() uses the %10lX hexadecimal specifier to write data to a field of width of 10 characters")
        {
            int n = test_sprintf(sprintTestBuffer, "Data:%10lX%10lX\r\n", 0x56789abcUL, 0x9abUL);

            THEN("The test buffer shall contain the expected string")
            {
                const char* expectedString = "Data:  56789ABC       9AB\r\n\0~";
                int expectedStringLen = strlen(expectedString);
                REQUIRE(expectedStringLen == n);
                REQUIRE(0 == memcmp(expectedString, sprintTestBuffer, expectedStringLen + 2));
            }
        }

        /* ----------------------------------------------------------------- */
        
        WHEN("sprintf() uses the %012x hexadecimal specifier to write unsigned long data to a field of width of 12 characters with zero padding")
        {
            int n = test_sprintf(sprintTestBuffer, "Data: 0x%012lx 0x%012lx 0x%012lx\r\n", 0x10UL, 0UL, 0x479B0102UL);

            THEN("The test buffer shall contain the expected string")
            {
                const char* expectedString = "Data: 0x000000000010 0x000000000000 0x0000479b0102\r\n\0~";
                int expectedStringLen = strlen(expectedString);
                REQUIRE(expectedStringLen == n);
                REQUIRE(0 == memcmp(expectedString, sprintTestBuffer, expectedStringLen + 2));
            }
        }

        /* ----------------------------------------------------------------- */
        
        WHEN("sprintf() uses the %02X hexadecimal specifier to write unsigned long data of length 4 characters to a field of width of 2 characters")
        {
            int n = test_sprintf(sprintTestBuffer, "Timestamp: 0x%02X milliseconds\r\n", 0x12abUL);

            THEN("The field width shall be increased to fit the number")
            {
                const char* expectedString = "Timestamp: 0x12AB milliseconds\r\n\0~";
                int expectedStringLen = strlen(expectedString);
                REQUIRE(expectedStringLen == n);
                REQUIRE(0 == memcmp(expectedString, sprintTestBuffer, expectedStringLen + 2));
            }
        }

#endif  /* #if MQOS_CONFIG_STDIO_LIBRARY_LONG_PRINTF_ENABLED */

    }
}


#if MQOS_CONFIG_STDIO_LIBRARY_LONG_PRINTF_ENABLED

SCENARIO("Tests to debug a fault observed on 8-bit PIC, where sprintf() using ld format string prints non-numeric character in the least significant digit", "[mqosStdio_sprintf_debug_ld_fault]")
{
    GIVEN("The destination C string is filled with ~ characters")
    {
        char sprintTestBuffer[80];
        memset(sprintTestBuffer, '~', sizeof(sprintTestBuffer));

        /* ----------------------------------------------------------------- */

        WHEN("sprintf() writes a series of frequency values to dest using ld print format")
        {
            int32_t rawFreq = 0;
            int32_t increment = 50;
            int16_t freqScaleFactor = 0x3E80;   // 1000.0 in signed 12.4 fixed-point format
            int16_t freqOffset = 0;
            const char* expectedString[20] =
            {
                "0\n\0~",
                "50000\n\0~",
                "100000\n\0~",
                "150000\n\0~",
                "200000\n\0~",
                "250000\n\0~",
                "300000\n\0~",
                "350000\n\0~",
                "400000\n\0~",
                "450000\n\0~",
                "500000\n\0~",
                "550000\n\0~",
                "600000\n\0~",
                "650000\n\0~",
                "700000\n\0~",
                "750000\n\0~",
                "800000\n\0~",
                "850000\n\0~",
                "900000\n\0~",
                "950000\n\0~",
            };

            for(size_t i = 0; i < 20; i++)
            {
                /* do the fixed-point frequency calibration calculation */
                int32_t calFreq = ((((int32_t) freqScaleFactor) * rawFreq) >> 4) + freqOffset;

                int n = test_sprintf(sprintTestBuffer, "%ld\n", calFreq);

                THEN("The destination shall contain the expected string, with a terminating null character")
                {
                    int expectedStringLen = strlen(expectedString[i]);
                    REQUIRE(expectedStringLen == n);
                    REQUIRE(0 == memcmp(expectedString[i], sprintTestBuffer, expectedStringLen + 2));
                }
                rawFreq += increment;   // increment the raw temperature value
            }
        }
    }
}

#endif  /* #if MQOS_CONFIG_STDIO_LIBRARY_LONG_PRINTF_ENABLED */


SCENARIO("Tests to debug a fault observed on 8-bit PIC, where %u outputs hexadecimal digits if it appears after %04X in the format string")
{
    GIVEN("The destination C string is filled with ~ characters")
    {
        char sprintTestBuffer[80];
        memset(sprintTestBuffer, '~', sizeof(sprintTestBuffer));

        /* ----------------------------------------------------------------- */

        WHEN("sprintf() writes mixed hexadecimal and decimal values")
        {
            int16_t address = 0x1FE2;
            int16_t delay = 100;
            int8_t range = 0;
            const char* expectedString = "Read settings: A=0x1FE2, D=100, R=0\n\0~";

            int n = test_sprintf(sprintTestBuffer, "Read settings: A=0x%04X, D=%u, R=%u\n", address, delay, range);

            THEN("The destination shall contain the expected string, with a terminating null character")
            {
                int expectedStringLen = strlen(expectedString);
                REQUIRE(expectedStringLen == n);
                REQUIRE(0 == memcmp(expectedString, sprintTestBuffer, expectedStringLen + 2));
            }
        }
    }
}
