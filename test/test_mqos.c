/*
 * test_mqos.c - mqos module unit test source file.
 *
 * Copyright (C) 2021 Antony Bayley.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */


#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include "mqos.h"
#include "mqosMessage.h"
#include "catch.hpp"
#include "fff.h"


/* MACROS ********************************************************************/

/* Mock functions are created using "fake function framework" (fff.h) */
DEFINE_FFF_GLOBALS;
FAKE_VOID_FUNC(test_ENTER_CRITICAL);
FAKE_VOID_FUNC(test_EXIT_CRITICAL);


/* PRIVATE FUNCTION DEFINITIONS **********************************************/

/*!
 *  Test equality of two MQOS messages.
 * 
 *  @param  msg1    First MQOS message.
 *  @param  msg2    Second MQOS message.
 * 
 * @return          true: if all fields of the two messages are identical.
 *                  false: if the messages are different.
 */
static bool test_mqos_MessagesAreEqual(const tMqosMessage* msg1, const tMqosMessage* msg2)
{
    bool result = (msg1->msgId == msg2->msgId);
#if MQOS_CONFIG_MSG_CONTAINS_PAYLOAD
    result &= (msg1->payload.data == msg2->payload.data);
#endif  /* #if MQOS_CONFIG_MSG_CONTAINS_PAYLOAD */
    return result;
}


/* PUBLIC FUNCTION DEFINITIONS ***********************************************/

SCENARIO("MQOS tests", "[mqos]")
{
    for(tMqosConfig_TimerCount numberOfTicks = 0; numberOfTicks < 3; numberOfTicks++)
    {
        char str[80];
        sprintf(str, "%d system ticks have occurred since boot-up", numberOfTicks);
        GIVEN(str)
        {
            /* Reset system to power-up state */
            mqos_Reset();

            /* Generate the required number of system ticks */
            for(tMqosConfig_TimerCount i = 0; i < numberOfTicks; i++)
            {
                mqos_Tick();
            }

            /* Reset the mock critical section management macros */
            RESET_FAKE(test_ENTER_CRITICAL);
            RESET_FAKE(test_EXIT_CRITICAL);

            WHEN("mqos_getSystemTickCount() is called")
            {
                // UUT: call mqos_getSystemTickCount()
                tMqosConfig_TimerCount count = mqos_getSystemTickCount();

                sprintf(str, "The tick count shall be %d", numberOfTicks);
                THEN(str)
                {
                    REQUIRE(numberOfTicks == count);
                }

                THEN("mqosPort_ENTER_CRITICAL() and mqosPort_EXIT_CRITICAL() shall have been called once")
                {
                    REQUIRE(1 == test_ENTER_CRITICAL_fake.call_count);
                    REQUIRE(1 == test_EXIT_CRITICAL_fake.call_count);
                }
            }
        }
    }


    GIVEN("No messages have been sent")
    {
        /* Reset system to power-up state */
        mqos_Reset();

        /* Reset the mock critical section management macros */
        RESET_FAKE(test_ENTER_CRITICAL);
        RESET_FAKE(test_EXIT_CRITICAL);

        WHEN("An attempt is made to receive a message from the ready queue")
        {
            /* UUT: Call mqos_MessageReceive() */
            tMqosMessage msg = mqos_MessageReceive();

            THEN("A null message shall be returned with message ID set to mqosConfig_MSG_NONE")
            {
                REQUIRE(mqosConfig_MSG_NONE == msg.msgId);
#if MQOS_CONFIG_MSG_CONTAINS_PAYLOAD
                REQUIRE(0 == msg.payload.data);
#endif  /* #if MQOS_CONFIG_MSG_CONTAINS_PAYLOAD */
            }

            THEN("mqosPort_ENTER_CRITICAL() and mqosPort_EXIT_CRITICAL() shall have been called once")
            {
                REQUIRE(1 == test_ENTER_CRITICAL_fake.call_count);
                REQUIRE(1 == test_EXIT_CRITICAL_fake.call_count);
            }
        }


        WHEN("A message is sent immediately")
        {
            /* UUT: Call mqos_MessageSend() */
#if MQOS_CONFIG_MSG_CONTAINS_PAYLOAD
            tMqosConfig_MessagePayload payload = {1};
            (void) mqos_MessageSend(mqosConfig_MSG_TEST1, payload);
#else   /* #if MQOS_CONFIG_MSG_CONTAINS_PAYLOAD */
            (void) mqos_MessageSend(mqosConfig_MSG_TEST1);
#endif  /* #if MQOS_CONFIG_MSG_CONTAINS_PAYLOAD */

            THEN("mqosPort_ENTER_CRITICAL() and mqosPort_EXIT_CRITICAL() shall have been called once")
            {
                REQUIRE(1 == test_ENTER_CRITICAL_fake.call_count);
                REQUIRE(1 == test_EXIT_CRITICAL_fake.call_count);
            }

            THEN("One message shall have been allocated from the heap")
            {
                REQUIRE(1 == mqosMessage_Mallinfo().allocCount);
            }
        }


        WHEN("A message is sent for delivery later")
        {
            /* UUT: Call mqos_MessageSendLater() */
#if MQOS_CONFIG_MSG_CONTAINS_PAYLOAD
            tMqosConfig_MessagePayload payload = {1};
            (void) mqos_MessageSendLater(mqosConfig_MSG_TEST1, payload, 5);
#else   /* #if MQOS_CONFIG_MSG_CONTAINS_PAYLOAD */
            (void) mqos_MessageSendLater(mqosConfig_MSG_TEST1, 5);
#endif  /* #if MQOS_CONFIG_MSG_CONTAINS_PAYLOAD */

            THEN("mqosPort_ENTER_CRITICAL() and mqosPort_EXIT_CRITICAL() shall have been called once")
            {
                REQUIRE(1 == test_ENTER_CRITICAL_fake.call_count);
                REQUIRE(1 == test_EXIT_CRITICAL_fake.call_count);
            }

            THEN("One message shall have been allocated from the heap")
            {
                REQUIRE(1 == mqosMessage_Mallinfo().allocCount);
            }
        }
    }


    //-------------------------------------------------------------------------

    GIVEN("One message has been sent immediately (zero delay)")
    {
        mqos_Reset();
#if MQOS_CONFIG_MSG_CONTAINS_PAYLOAD
        tMqosConfig_MessagePayload payload = {1};
        tMqosMessageContainer* sentMsg = mqos_MessageSend(mqosConfig_MSG_TEST1, payload);
#else   /* #if MQOS_CONFIG_MSG_CONTAINS_PAYLOAD */
        tMqosMessageContainer* sentMsg = mqos_MessageSend(mqosConfig_MSG_TEST1);
#endif  /* #if MQOS_CONFIG_MSG_CONTAINS_PAYLOAD */
        tMqosMessage expectedReceivedMsg = sentMsg->msg;

        REQUIRE(1 == mqosMessage_Mallinfo().allocCount);

        /* Reset the mock critical section management macros */
        RESET_FAKE(test_ENTER_CRITICAL);
        RESET_FAKE(test_EXIT_CRITICAL);


        WHEN("mqos_MessageReceive() is called")
        {
            /* UUT: Call mqos_MessageReceive() */
            tMqosMessage msg = mqos_MessageReceive();

            THEN("The sent message shall be returned")
            {
                REQUIRE(test_mqos_MessagesAreEqual(&expectedReceivedMsg, &msg));
                REQUIRE(mqosConfig_MSG_TEST1 == msg.msgId);
#if MQOS_CONFIG_MSG_CONTAINS_PAYLOAD
                REQUIRE(1 == msg.payload.data);
#endif  /* #if MQOS_CONFIG_MSG_CONTAINS_PAYLOAD */
            }

            THEN("The received message shall have been returned to the message heap")
            {
                REQUIRE(0 == mqosMessage_Mallinfo().allocCount);
            }

            THEN("mqosPort_ENTER_CRITICAL() and mqosPort_EXIT_CRITICAL() shall both have been called once")
            {
                REQUIRE(1 == test_ENTER_CRITICAL_fake.call_count);
                REQUIRE(1 == test_EXIT_CRITICAL_fake.call_count);
            }
        }


        WHEN("mqos_MessageCancel() is called to cancel the sent message")
        {
            /* UUT: Call mqos_MessageCancel() */
            mqos_MessageCancel(sentMsg);

            THEN("The sent message shall remain allocated from the message heap")
            {
                REQUIRE(1 == mqosMessage_Mallinfo().allocCount);
            }

            AND_WHEN("mqos_MessageReceive() is called")
            {
                /* UUT: Call mqos_MessageReceive() */
                tMqosMessage msg = mqos_MessageReceive();

                THEN("The sent message shall be returned")
                {
                    REQUIRE(test_mqos_MessagesAreEqual(&expectedReceivedMsg, &msg));
                    REQUIRE(mqosConfig_MSG_TEST1 == msg.msgId);
#if MQOS_CONFIG_MSG_CONTAINS_PAYLOAD
                REQUIRE(1 == msg.payload.data);
#endif  /* #if MQOS_CONFIG_MSG_CONTAINS_PAYLOAD */
                }

                THEN("The received message shall have been returned to the message heap")
                {
                    REQUIRE(0 == mqosMessage_Mallinfo().allocCount);
                }
            }
        }


        WHEN("mqos_MessageCancelAll(mqosConfig_MSG_TEST1) is called")
        {
            mqos_MessageCancelAll(mqosConfig_MSG_TEST1);

            THEN("The sent message shall remain allocated from the message heap")
            {
                REQUIRE(1 == mqosMessage_Mallinfo().allocCount);
            }

            AND_WHEN("mqos_MessageReceive() is called")
            {
                /* UUT: Call mqos_MessageReceive() */
                tMqosMessage msg = mqos_MessageReceive();

                THEN("The sent message shall be returned")
                {
                    REQUIRE(test_mqos_MessagesAreEqual(&expectedReceivedMsg, &msg));
                    REQUIRE(mqosConfig_MSG_TEST1 == msg.msgId);
#if MQOS_CONFIG_MSG_CONTAINS_PAYLOAD
                REQUIRE(1 == msg.payload.data);
#endif  /* #if MQOS_CONFIG_MSG_CONTAINS_PAYLOAD */
                }

                THEN("The received message shall have been returned to the message heap")
                {
                    REQUIRE(0 == mqosMessage_Mallinfo().allocCount);
                }
            }
        }
    }


    //-------------------------------------------------------------------------

    GIVEN("One message has been sent with a delay of 5 MQOS system clock ticks")
    {
        /* Send a message with a delay of 5 MQOS system clock ticks */
        mqos_Reset();
#if MQOS_CONFIG_MSG_CONTAINS_PAYLOAD
        tMqosConfig_MessagePayload payload = {2};
        (void) mqos_MessageSendLater(mqosConfig_MSG_TEST2, payload, 5);
#else   /* #if MQOS_CONFIG_MSG_CONTAINS_PAYLOAD */
        (void) mqos_MessageSendLater(mqosConfig_MSG_TEST2, 5);
#endif  /* #if MQOS_CONFIG_MSG_CONTAINS_PAYLOAD */

        REQUIRE(1 == mqosMessage_Mallinfo().allocCount);

        /* Reset the mock critical section management macros */
        RESET_FAKE(test_ENTER_CRITICAL);
        RESET_FAKE(test_EXIT_CRITICAL);


        WHEN("mqos_MessageReceive() is called before 5 clock ticks have elapsed")
        {
            /* Number critical sections entered: mqos_MessageReceive() and mqos_Tick() */
            uint8_t criticalSectionCount = 0;

            for(tMqosConfig_TimerCount i=0; i < 5; i++)
            {
                /* UUT: Call mqos_MessageReceive() */
                tMqosMessage msg = mqos_MessageReceive();
                criticalSectionCount++;

                THEN("A null message shall be returned with message ID set to mqosConfig_MSG_NONE")
                {
                    REQUIRE(mqosConfig_MSG_NONE == msg.msgId);
#if MQOS_CONFIG_MSG_CONTAINS_PAYLOAD
                    REQUIRE(0 == msg.payload.data);
#endif  /* #if MQOS_CONFIG_MSG_CONTAINS_PAYLOAD */
                }

                char str[90];
                sprintf(str, "mqosPort_ENTER_CRITICAL() and mqosPort_EXIT_CRITICAL() shall have been called %d times", criticalSectionCount);
                THEN(str)
                {
                    REQUIRE(criticalSectionCount == test_ENTER_CRITICAL_fake.call_count);
                    REQUIRE(criticalSectionCount == test_EXIT_CRITICAL_fake.call_count);
                }

                THEN("One message shall have been allocated from the heap")
                {
                    REQUIRE(1 == mqosMessage_Mallinfo().allocCount);
                }

                /* MQOS system clock tick */
                mqos_Tick();
                criticalSectionCount++;
                REQUIRE(criticalSectionCount == test_ENTER_CRITICAL_fake.call_count);
                REQUIRE(criticalSectionCount == test_EXIT_CRITICAL_fake.call_count);
            }
        }


        WHEN("mqos_MessageReceive() is called after 5 clock ticks have elapsed")
        {
            /* 5 MQOS system clock ticks */
            for(tMqosConfig_TimerCount i=0; i < 5; i++)
            {
                mqos_Tick();
            }
            /* UUT: Call mqos_MessageReceive() */
            tMqosMessage msg = mqos_MessageReceive();

            THEN("The sent message shall be returned")
            {
                /* UUT: Call mqos_MessageReceive() */
                REQUIRE(mqosConfig_MSG_TEST2 == msg.msgId);
#if MQOS_CONFIG_MSG_CONTAINS_PAYLOAD
                REQUIRE(2 == msg.payload.data);
#endif  /* #if MQOS_CONFIG_MSG_CONTAINS_PAYLOAD */
            }
        }
    }


    //-------------------------------------------------------------------------

    GIVEN("4 delayed messages have been sent")
    {
        /* Send 4 delayed messages, 2 of which have msgId set to mqosConfig_MSG_TEST2 */
        mqos_Reset();
#if MQOS_CONFIG_MSG_CONTAINS_PAYLOAD
        tMqosConfig_MessagePayload payload = {1};
        (void) mqos_MessageSendLater(mqosConfig_MSG_TEST1, payload, 1);
        payload = {2};
        tMqosMessageContainer* msgToCancel = mqos_MessageSendLater(mqosConfig_MSG_TEST2, payload, 2);
        payload = {3};
        (void) mqos_MessageSendLater(mqosConfig_MSG_TEST3, payload, 3);
        payload = {4};
        (void) mqos_MessageSendLater(mqosConfig_MSG_TEST2, payload, 4);
#else   /* #if MQOS_CONFIG_MSG_CONTAINS_PAYLOAD */
        (void) mqos_MessageSendLater(mqosConfig_MSG_TEST1, 1);
        tMqosMessageContainer* msgToCancel = mqos_MessageSendLater(mqosConfig_MSG_TEST2, 2);
        (void) mqos_MessageSendLater(mqosConfig_MSG_TEST3, 3);
        (void) mqos_MessageSendLater(mqosConfig_MSG_TEST2, 4);
#endif  /* #if MQOS_CONFIG_MSG_CONTAINS_PAYLOAD */

        REQUIRE(4 == mqosMessage_Mallinfo().allocCount);


        WHEN("mqos_MessageCancel() is called to cancel the second message sent")
        {
            mqos_MessageCancel(msgToCancel);

            THEN("Only 3 messages shall remain allocated from the message heap")
            {
                REQUIRE(3 == mqosMessage_Mallinfo().allocCount);
            }

            AND_WHEN("Sufficient ticks have elapsed to place all messages on the ready queue")
            {
                /* 4 MQOS system clock ticks */
                for(tMqosConfig_TimerCount i=0; i < 4; i++)
                {
                    mqos_Tick();
                }

                THEN("The 1st, 3rd and 4th messages that were sent shall be received")
                {
                    /* UUT: Call mqos_MessageReceive() */
                    tMqosMessage msg = mqos_MessageReceive();
                    REQUIRE(mqosConfig_MSG_TEST1 == msg.msgId);
#if MQOS_CONFIG_MSG_CONTAINS_PAYLOAD
                    REQUIRE(1 == msg.payload.data);
#endif  /* #if MQOS_CONFIG_MSG_CONTAINS_PAYLOAD */
                    msg = mqos_MessageReceive();
                    REQUIRE(mqosConfig_MSG_TEST3 == msg.msgId);
#if MQOS_CONFIG_MSG_CONTAINS_PAYLOAD
                    REQUIRE(3 == msg.payload.data);
#endif  /* #if MQOS_CONFIG_MSG_CONTAINS_PAYLOAD */
                    msg = mqos_MessageReceive();
                    REQUIRE(mqosConfig_MSG_TEST2 == msg.msgId);
#if MQOS_CONFIG_MSG_CONTAINS_PAYLOAD
                    REQUIRE(4 == msg.payload.data);
#endif  /* #if MQOS_CONFIG_MSG_CONTAINS_PAYLOAD */
                }
            }
        }


        WHEN("mqos_MessageCancelAll(mqosConfig_MSG_TEST2) is called")
        {
            mqos_MessageCancelAll(mqosConfig_MSG_TEST2);

            THEN("Only 2 messages shall remain allocated from the message heap")
            {
                REQUIRE(2 == mqosMessage_Mallinfo().allocCount);
            }

            AND_WHEN("Sufficient ticks have elapsed to place all messages on the ready queue")
            {
                /* 4 MQOS system clock ticks */
                for(tMqosConfig_TimerCount i=0; i < 4; i++)
                {
                    mqos_Tick();
                }

                THEN("2 messages shall be received, neither of which has msgId set to mqosConfig_MSG_TEST2")
                {
                    /* UUT: Call mqos_MessageReceive() */
                    tMqosMessage msg = mqos_MessageReceive();
                    REQUIRE(mqosConfig_MSG_TEST1 == msg.msgId);
#if MQOS_CONFIG_MSG_CONTAINS_PAYLOAD
                    REQUIRE(1 == msg.payload.data);
#endif  /* #if MQOS_CONFIG_MSG_CONTAINS_PAYLOAD */
                    msg = mqos_MessageReceive();
                    REQUIRE(mqosConfig_MSG_TEST3 == msg.msgId);
#if MQOS_CONFIG_MSG_CONTAINS_PAYLOAD
                    REQUIRE(3 == msg.payload.data);
#endif  /* #if MQOS_CONFIG_MSG_CONTAINS_PAYLOAD */
                }
            }
        }
    }


    //-------------------------------------------------------------------------

    GIVEN("All messages on the message heap have been allocated")
    {
        /* Send MQOS_CONFIG_HEAP_SIZE messages */
        mqos_Reset();
        for(tMqosConfig_MessageIndex i = 0; i < MQOS_CONFIG_HEAP_SIZE; i++)
        {
#if MQOS_CONFIG_MSG_CONTAINS_PAYLOAD
            tMqosConfig_MessagePayload payload = {i};
            (void) mqos_MessageSendLater(mqosConfig_MSG_TEST1, payload, (tMqosConfig_TimerCount)i);
#else   /* #if MQOS_CONFIG_MSG_CONTAINS_PAYLOAD */
            (void) mqos_MessageSendLater(mqosConfig_MSG_TEST1, (tMqosConfig_TimerCount)i);
#endif  /* #if MQOS_CONFIG_MSG_CONTAINS_PAYLOAD */
        }

        REQUIRE(MQOS_CONFIG_HEAP_SIZE == mqosMessage_Mallinfo().allocCount);


        WHEN("mqos_MessageSendLater() is called")
        {
#if MQOS_CONFIG_MSG_CONTAINS_PAYLOAD
            tMqosConfig_MessagePayload payload = {0};
            /* UUT: Attempt to send a message */
            tMqosMessageContainer* sentMsg = mqos_MessageSendLater(mqosConfig_MSG_TEST2, payload, 0);
#else   /* #if MQOS_CONFIG_MSG_CONTAINS_PAYLOAD */
            tMqosMessageContainer* sentMsg = mqos_MessageSendLater(mqosConfig_MSG_TEST2, 0);
#endif  /* #if MQOS_CONFIG_MSG_CONTAINS_PAYLOAD */

            THEN("No message is sent and NULL is returned")
            {
                REQUIRE(NULL == sentMsg);
            }
        }
    }
}
