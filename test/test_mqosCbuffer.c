/*
 * test_mqosCbuffer.c - mqosCbuffer module unit test source file.
 *
 * Copyright (C) 2021 Antony Bayley.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */


#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include "mqosCbuffer.h"
#include "catch.hpp"


/* MACROS ********************************************************************/

#define TEST_CBUFFER_SIZE 15    //!< Size of circular buffer for unit tests.


/* PRIVATE VARIABLE DEFINITIONS **********************************************/

/* define the circular buffer that will be used for mqosCbuffer unit tests */
MQOS_CBUFFER_DEFINE(cbuf, TEST_CBUFFER_SIZE);

/* number of run-time assert failures */
static uint32_t runtimeAssertFailureCount = 0;


/* PRIVATE FUNCTION DEFINITIONS **********************************************/


/* PUBLIC FUNCTION DEFINITIONS ***********************************************/

/* helper function to test run-time assert macro */
void mqosConfig_HandleAssertFailure(void)
{
    runtimeAssertFailureCount += 1;
};


SCENARIO("MQOS circular buffer tests", "[mqosCbuffer]")
{
    GIVEN("The Cbuffer is empty")
    {
        /* reset the Cbuffer struct to initial state */
        mqosCbuffer_Reset(&cbuf);

        THEN("mqosCbuffer_IsEmpty() shall return true")
        {
            REQUIRE(mqosCbuffer_IsEmpty(&cbuf));
        }

        THEN("mqosCbuffer_IsFull() shall return false")
        {
            REQUIRE(!mqosCbuffer_IsFull(&cbuf));
        }

        THEN("mqosCbuffer_Length() shall return 0")
        {
            REQUIRE(0 == mqosCbuffer_Length(&cbuf));
        }

        THEN("mqosCbuffer_Space() shall return TEST_CBUFFER_SIZE")
        {
            REQUIRE(TEST_CBUFFER_SIZE == mqosCbuffer_Space(&cbuf));
        }

        THEN("mqosCbuffer_IndexOf() shall return MQOS_CBUFFER_NOT_FOUND for any byte value, with any index value")
        {
            for(uint32_t data = 0; data < 256; data++)
            {
                REQUIRE(MQOS_CBUFFER_NOT_FOUND == mqosCbuffer_IndexOf(&cbuf, 0, (uint8_t)data));
                REQUIRE(MQOS_CBUFFER_NOT_FOUND == mqosCbuffer_IndexOf(&cbuf, TEST_CBUFFER_SIZE - 1, (uint8_t)data));
                REQUIRE(MQOS_CBUFFER_NOT_FOUND == mqosCbuffer_IndexOf(&cbuf, TEST_CBUFFER_SIZE + 1, (uint8_t)data));
            }
        }

        THEN("mqosCbuffer_Contains() shall return false for any byte value")
        {
            for(uint32_t data = 0; data < 256; data++)
            {
                REQUIRE(!mqosCbuffer_Contains(&cbuf, 0, (uint8_t)data));
            }
        }


        WHEN("mqosCbuffer_Put() is called TEST_CBUFFER_SIZE times")
        {
            for(tMqosCbufferIndex i = 1; i <= TEST_CBUFFER_SIZE; i++)
            {
                REQUIRE(MQOS_CBUFFER_OK == mqosCbuffer_Put(&cbuf, (uint8_t) (i - 1)));

                char str[160];
                sprintf(str, "After call %d to mqosCbuffer_Put(), mqosCbuffer_IsEmpty() shall return false", i);
                THEN(str)
                {
                    REQUIRE(!mqosCbuffer_IsEmpty(&cbuf));
                }

                if(i < TEST_CBUFFER_SIZE)
                {
                    sprintf(str, "After call %d to mqosCbuffer_Put(), mqosCbuffer_IsFull() shall return false", i);
                    THEN(str)
                    {
                        REQUIRE(!mqosCbuffer_IsFull(&cbuf));
                    }
                }
                else
                {
                    THEN("After TEST_CBUFFER_SIZE calls to mqosCbuffer_Put(), mqosCbuffer_IsFull() shall return true")
                    {
                        REQUIRE(mqosCbuffer_IsFull(&cbuf));
                    }
                }

                sprintf(str, "After call %d to mqosCbuffer_Put(), mqosCbuffer_Length() shall return %d", i, i);
                THEN("mqosCbuffer_Length() shall return 0")
                {
                    REQUIRE(i == mqosCbuffer_Length(&cbuf));
                }

                sprintf(str, "After call %d to mqosCbuffer_Put(), mqosCbuffer_Space() shall return %d", i, TEST_CBUFFER_SIZE - i);
                THEN("mqosCbuffer_Length() shall return 0")
                {
                    REQUIRE(TEST_CBUFFER_SIZE - i == mqosCbuffer_Space(&cbuf));
                }

                sprintf(str, "After call %d to mqosCbuffer_Put(), mqosCbuffer_IndexOf(&cbuf, 0, x) shall return x for any byte value x < %d", i, i);
                THEN(str)
                {
                    for(tMqosCbufferIndex dataIndex = 0; dataIndex < i; dataIndex++)
                    {
                        REQUIRE(dataIndex == mqosCbuffer_IndexOf(&cbuf, 0, (uint8_t)dataIndex));
                    }
                }

                sprintf(str, "After call %d to mqosCbuffer_Put(), mqosCbuffer_IndexOf(&cbuf, 2, x) shall return MQOS_CBUFFER_NOT_FOUND for any byte value x < 2", i);
                THEN(str)
                {
                    tMqosCbufferIndex startIndex = 2;
                    for(tMqosCbufferIndex dataIndex = 0; dataIndex < startIndex; dataIndex++)
                    {
                        REQUIRE(MQOS_CBUFFER_NOT_FOUND == mqosCbuffer_IndexOf(&cbuf, startIndex, (uint8_t)dataIndex));
                    }
                }

                sprintf(str, "After call %d to mqosCbuffer_Put(), mqosCbuffer_IndexOf(&cbuf, 2, x) shall return x for any byte value 2 <= x < %d", i, i);
                THEN(str)
                {
                    tMqosCbufferIndex startIndex = 2;
                    for(tMqosCbufferIndex dataIndex = 2; dataIndex < i; dataIndex++)
                    {
                        REQUIRE(dataIndex == mqosCbuffer_IndexOf(&cbuf, startIndex, (uint8_t)dataIndex));
                    }
                }

                sprintf(str, "After call %d to mqosCbuffer_Put(), mqosCbuffer_IndexOf(&cbuf, 0, x) shall return MQOS_CBUFFER_NOT_FOUND for any byte value x >= %d", i, i);
                THEN(str)
                {
                    for(uint32_t data = (uint32_t)i; data < 256; data++)
                    {
                        REQUIRE(MQOS_CBUFFER_NOT_FOUND == mqosCbuffer_IndexOf(&cbuf, 0, (uint8_t)data));
                    }
                }

                sprintf(str, "After call %d to mqosCbuffer_Put(), mqosCbuffer_Contains() shall return true for any byte value < %d", i, i);
                THEN(str)
                {
                    for(uint8_t data = 0; data < (uint8_t)i; data++)
                    {
                        REQUIRE(mqosCbuffer_Contains(&cbuf, 0, data));
                    }
                }

                sprintf(str, "After call %d to mqosCbuffer_Put(), mqosCbuffer_Contains() shall return false for any byte value >= %d", i, i);
                THEN(str)
                {
                    for(uint32_t data = (uint32_t)i; data < 256; data++)
                    {
                        REQUIRE(!mqosCbuffer_Contains(&cbuf, 0, (uint8_t)data));
                    }
                }

            }
        }


        WHEN("mqosCbuffer_Get() is called")
        {
            uint8_t data = 0;
            tMqosConfig_BaseType status = mqosCbuffer_Get(&cbuf, &data);

            THEN("The fault code MQOS_CBUFFER_EMPTY shall be returned")
            {
                REQUIRE(MQOS_CBUFFER_EMPTY == status);
            }

            THEN("No data shall be retrieved")
            {
                REQUIRE(0 == data);
            }
        }


        WHEN("mqosCbuffer_Peek() is called with index value 0")
        {
            tMqosCbufferIndex index = 0;
            const uint8_t* data = mqosCbuffer_Peek(&cbuf, index);

            THEN("The NULL pointer shall be returned")
            {
                REQUIRE(NULL == data);
            }
        }


        WHEN("mqosCbuffer_Peek() is called with index value greater than the number of bytes in the Cbuffer")
        {
            tMqosCbufferIndex index = 1;
            REQUIRE(index > mqosCbuffer_Length(&cbuf));
            const uint8_t* data = mqosCbuffer_Peek(&cbuf, index);

            THEN("The NULL pointer shall be returned")
            {
                REQUIRE(NULL == data);
            }
        }


        WHEN("mqosCbuffer_StrncpyToCbuffer() is called with sufficient space in the Cbuffer and n is equal to the string length")
        {
            const char* str = "testing";
            tMqosCbufferIndex n = (tMqosCbufferIndex) strlen(str);
            tMqosCbufferIndex nCopied = mqosCbuffer_StrncpyToCbuffer(&cbuf, str, n);

            THEN("The number of characters copied shall be equal to the string length")
            {
                REQUIRE(n == nCopied);
                REQUIRE(n == mqosCbuffer_Length(&cbuf));
            }

            THEN("The circular buffer shall contain the string's characters")
            {
                uint8_t byteVal = 0;
                for(tMqosCbufferIndex i = 0; i < n; i++)
                {
                    mqosCbuffer_Get(&cbuf, &byteVal);
                    REQUIRE(str[i] == (char) byteVal);
                }
            }
        }


        WHEN("mqosCbuffer_StrncpyToCbuffer() is called with sufficient space in the Cbuffer and n is less than the string length")
        {
            const char* str = "testing";
            tMqosCbufferIndex n = 4;    // expect string to be truncated to "test", with no null character termination
            tMqosCbufferIndex nCopied = mqosCbuffer_StrncpyToCbuffer(&cbuf, str, n);

            THEN("The number of characters copied shall be equal to n")
            {
                REQUIRE(n == nCopied);
                REQUIRE(n == mqosCbuffer_Length(&cbuf));
            }

            THEN("The circular buffer shall contain the string's characters, truncated to length n")
            {
                uint8_t byteVal = 0;
                for(tMqosCbufferIndex i = 0; i < n; i++)
                {
                    mqosCbuffer_Get(&cbuf, &byteVal);
                    REQUIRE(str[i] == (char) byteVal);
                }
            }
        }


        WHEN("mqosCbuffer_StrncpyToCbuffer() is called with sufficient space in the Cbuffer and n is greater than the string length")
        {
            const char* str = "testing";
            size_t strLen = strlen(str);
            tMqosCbufferIndex n = 2 + strLen;   // requested copy length n is 2 characters more than the string length
            tMqosCbufferIndex nCopied = mqosCbuffer_StrncpyToCbuffer(&cbuf, str, n);

            THEN("The number of characters copied shall be equal to the string length")
            {
                REQUIRE(strLen == nCopied);
                REQUIRE(strLen == mqosCbuffer_Length(&cbuf));
            }

            THEN("The circular buffer shall contain the string's characters")
            {
                uint8_t byteVal = 0;
                tMqosConfig_BaseType result;
                for(tMqosCbufferIndex i = 0; i < strLen; i++)
                {
                    result = mqosCbuffer_Get(&cbuf, &byteVal);
                    REQUIRE(MQOS_CBUFFER_OK == result);
                    REQUIRE(str[i] == (char) byteVal);
                }
                /* Any further attempts to read from cbuf shall fail */
                result = mqosCbuffer_Get(&cbuf, &byteVal);
                REQUIRE(MQOS_CBUFFER_EMPTY == result);
            }
        }


        WHEN("mqosCbuffer_StrncpyToCbuffer() is called with insufficient space in the Cbuffer and n is equal to the string length")
        {
            tMqosCbufferIndex spaceAvailable = mqosCbuffer_Space(&cbuf);
            const char* str = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            tMqosCbufferIndex n = (tMqosCbufferIndex) strlen(str);
            REQUIRE(n > spaceAvailable);    // verify that there is insufficient space to copy the source string

            tMqosCbufferIndex nCopied = mqosCbuffer_StrncpyToCbuffer(&cbuf, str, n);

            THEN("The number of characters copied shall be truncated to fit the available space")
            {
                REQUIRE(spaceAvailable == nCopied);
                REQUIRE(spaceAvailable == mqosCbuffer_Length(&cbuf));
            }

            THEN("The circular buffer shall contain the string's characters, truncated to fit the available space")
            {
                uint8_t byteVal = 0;
                for(tMqosCbufferIndex i = 0; i < spaceAvailable; i++)
                {
                    mqosCbuffer_Get(&cbuf, &byteVal);
                    REQUIRE(str[i] == (char) byteVal);
                }
            }
        }


        WHEN("mqosCbuffer_StrcpyToCbuffer() is called with sufficient space in the Cbuffer")
        {
            const char* str = "testing";
            tMqosCbufferIndex n = (tMqosCbufferIndex) strlen(str);
            tMqosCbufferIndex nCopied = mqosCbuffer_StrcpyToCbuffer(&cbuf, str);

            THEN("The number of characters copied shall be equal to the string length")
            {
                REQUIRE(n == nCopied);
                REQUIRE(n == mqosCbuffer_Length(&cbuf));
            }

            THEN("The circular buffer shall contain the string's characters")
            {
                uint8_t byteVal = 0;
                for(tMqosCbufferIndex i = 0; i < n; i++)
                {
                    mqosCbuffer_Get(&cbuf, &byteVal);
                    REQUIRE(str[i] == (char) byteVal);
                }
            }
        }


        WHEN("mqosCbuffer_StrcpyToCbuffer() is called with insufficient space in the Cbuffer")
        {
            tMqosCbufferIndex spaceAvailable = mqosCbuffer_Space(&cbuf);
            const char* str = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            tMqosCbufferIndex n = (tMqosCbufferIndex) strlen(str);
            REQUIRE(n > spaceAvailable);    // verify that there is insufficient space to copy the source string

            tMqosCbufferIndex nCopied = mqosCbuffer_StrcpyToCbuffer(&cbuf, str);

            THEN("The number of characters copied shall be truncated to fit the available space")
            {
                REQUIRE(spaceAvailable == nCopied);
                REQUIRE(spaceAvailable == mqosCbuffer_Length(&cbuf));
            }

            THEN("The circular buffer shall contain the string's characters, truncated to fit the available space")
            {
                uint8_t byteVal = 0;
                for(tMqosCbufferIndex i = 0; i < spaceAvailable; i++)
                {
                    mqosCbuffer_Get(&cbuf, &byteVal);
                    REQUIRE(str[i] == (char) byteVal);
                }
            }
        }


        WHEN("mqosCbuffer_MemcpyFromCbuffer() is called")
        {
            uint8_t dest[TEST_CBUFFER_SIZE] = {0, };
            tMqosCbufferIndex copyLen = mqosCbuffer_MemcpyFromCbuffer(dest, &cbuf, 0, sizeof(dest));

            THEN("No data shall be copied")
            {
                REQUIRE(0 == copyLen);
                uint8_t expectedData[TEST_CBUFFER_SIZE] = {0, };
                for(size_t i = 0; i < sizeof(dest); i++)
                {
                    REQUIRE(expectedData[i] == dest[i]);
                }
            }
        }
    }


    //-------------------------------------------------------------------------

    GIVEN("The three bytes [1, 2, 3] have been written to the Cbuffer")
    {
        /* reset the Cbuffer struct to initial state, then write 3 bytes */
        mqosCbuffer_Reset(&cbuf);
        (void) mqosCbuffer_Put(&cbuf, 1);
        (void) mqosCbuffer_Put(&cbuf, 2);
        (void) mqosCbuffer_Put(&cbuf, 3);

        REQUIRE(3 == mqosCbuffer_Length(&cbuf));

        /* save initial state for later tests */
        tMqosCbufferIndex initialLen = mqosCbuffer_Length(&cbuf);

        WHEN("mqosCbuffer_Get() is called 3 times")
        {
            for(tMqosCbufferIndex i = 1; i < 4; i++)
            {
                uint8_t data = 0;
                REQUIRE(MQOS_CBUFFER_OK == mqosCbuffer_Get(&cbuf, &data));

                char str[160];
                sprintf(str, "On call %d to mqosCbuffer_Get(), the value %d shall be read", i, i);
                THEN(str)
                {
                    REQUIRE(data == (uint8_t)i);
                }

                if(i < 3)
                {
                    sprintf(str, "After call %d to mqosCbuffer_Get(), mqosCbuffer_IsEmpty() shall return false", i);
                    THEN(str)
                    {
                        REQUIRE(!mqosCbuffer_IsEmpty(&cbuf));
                    }
                }
                else
                {
                    THEN("After call 3 to mqosCbuffer_Get(), mqosCbuffer_IsEmpty() shall return true")
                    {
                        REQUIRE(mqosCbuffer_IsEmpty(&cbuf));
                    }
                }

                sprintf(str, "After call %d to mqosCbuffer_Get(), mqosCbuffer_IsFull() shall return false", i);
                THEN(str)
                {
                    REQUIRE(!mqosCbuffer_IsFull(&cbuf));
                }

                sprintf(str, "After call %d to mqosCbuffer_Get(), mqosCbuffer_Length() shall return %d", i, 3 - i);
                THEN(str)
                {
                    REQUIRE((3 - i) == mqosCbuffer_Length(&cbuf));
                }

                sprintf(str, "After call %d to mqosCbuffer_Get(), mqosCbuffer_Space() shall return %d", i, TEST_CBUFFER_SIZE - (3 - i));
                THEN(str)
                {
                    REQUIRE(TEST_CBUFFER_SIZE - (3 - i) == mqosCbuffer_Space(&cbuf));
                }

                sprintf(str, "After call %d to mqosCbuffer_Get(), mqosCbuffer_IndexOf() shall return MQOS_CBUFFER_NOT_FOUND for byte values <= %d", i, i);
                THEN(str)
                {
                    for(uint8_t data = 0; data <= (uint8_t)i; data++)
                    {
                        REQUIRE(MQOS_CBUFFER_NOT_FOUND == mqosCbuffer_IndexOf(&cbuf, 0, data));
                    }
                }

                sprintf(str, "After call %d to mqosCbuffer_Get(), mqosCbuffer_IndexOf() shall return an index for byte values > %d and <= 3", i, i);
                THEN(str)
                {
                    for(uint8_t data = (uint8_t)i + 1; data <= 3; data++)
                    {
                        REQUIRE(((tMqosCbufferIndex)data - (i + 1)) == mqosCbuffer_IndexOf(&cbuf, 0, data));
                    }
                }


                sprintf(str, "After call %d to mqosCbuffer_Get(), mqosCbuffer_Contains() shall return false for byte values <= %d", i, i);
                THEN(str)
                {
                    for(uint8_t data = 0; data <= (uint8_t)i; data++)
                    {
                        REQUIRE(!mqosCbuffer_Contains(&cbuf, 0, data));
                    }
                }

                sprintf(str, "After call %d to mqosCbuffer_Get(), mqosCbuffer_Contains() shall return true for byte values > %d and <= 3", i, i);
                THEN(str)
                {
                    for(uint8_t data = (uint8_t)i + 1; data <= 3; data++)
                    {
                        REQUIRE(mqosCbuffer_Contains(&cbuf, 0, data));
                    }
                }
            }
        }


        WHEN("mqosCbuffer_Peek() is called with index values from 0 to 2 inclusive")
        {
            for(tMqosCbufferIndex i = 0; i < 3; i++)
            {
                const uint8_t* data =  mqosCbuffer_Peek(&cbuf, i);

                char str[160];
                sprintf(str, "When the index is %d, mqosCbuffer_Peek() shall return %d", i, i + 1);
                THEN(str)
                {
                    REQUIRE(*data == (uint8_t) i + 1);  // data values are [1, 2, 3]
                }
            }
        }


        WHEN("mqosCbuffer_Peek() is called with index value greater than the number of bytes in the Cbuffer")
        {
            tMqosCbufferIndex index = 4;
            REQUIRE(index > mqosCbuffer_Length(&cbuf));
            const uint8_t* data = mqosCbuffer_Peek(&cbuf, index);

            THEN("The NULL pointer shall be returned")
            {
                REQUIRE(NULL == data);
            }
        }


        WHEN("mqosCbuffer_Reset() is called")
        {
            mqosCbuffer_Reset(&cbuf);

            THEN("mqosCbuffer_IsEmpty() shall return true")
            {
                REQUIRE(mqosCbuffer_IsEmpty(&cbuf));
            }

            THEN("mqosCbuffer_Length() shall return 0")
            {
                REQUIRE(0 == mqosCbuffer_Length(&cbuf));
            }

            THEN("mqosCbuffer_Space() shall return TEST_CBUFFER_SIZE")
            {
                REQUIRE(TEST_CBUFFER_SIZE == mqosCbuffer_Space(&cbuf));
            }

            THEN("mqosCbuffer_Get() shall retrieve no data and return MQOS_CBUFFER_EMPTY")
            {
                uint8_t data = 0;
                REQUIRE(MQOS_CBUFFER_EMPTY == mqosCbuffer_Get(&cbuf, &data));
                REQUIRE(0 == data);
            }

            THEN("mqosCbuffer_Peek() with index 0 shall return NULL")
            {
                tMqosCbufferIndex index = 0;
                const uint8_t* data = mqosCbuffer_Peek(&cbuf, index);
                REQUIRE(NULL == data);
            }
        }


        WHEN("mqosCbuffer_Get() is called with a NULL data pointer")
        {
            tMqosConfig_BaseType status = mqosCbuffer_Get(&cbuf, NULL);

            THEN("The fault code MQOS_CBUFFER_DATA_NULL shall be returned")
            {
                REQUIRE(MQOS_CBUFFER_DATA_NULL == status);
            }

            THEN("One byte shall be read from the Cbuffer and discarded")
            {
                /* Cbuffer length must be one less than its length at the start of the test */
                tMqosCbufferIndex expectedLen = initialLen - 1;
                REQUIRE(expectedLen == mqosCbuffer_Length(&cbuf));
            }
        }


        WHEN("mqosCbuffer_MemcpyFromCbuffer() is called with maximum copy length equal to destination buffer size")
        {
            uint8_t dest[TEST_CBUFFER_SIZE] = {0, };
            tMqosCbufferIndex copyLen = mqosCbuffer_MemcpyFromCbuffer(dest, &cbuf, 0, sizeof(dest));

            THEN("Three bytes shall be copied to the destination buffer")
            {
                REQUIRE(3 == copyLen);
                uint8_t expectedData[TEST_CBUFFER_SIZE] = {1, 2, 3, 0, };
                for(size_t i = 0; i < sizeof(dest); i++)
                {
                    REQUIRE(expectedData[i] == dest[i]);
                }
            }
        }


        WHEN("mqosCbuffer_MemcpyFromCbuffer() is called with maximum copy length equal to 3")
        {
            uint8_t dest[TEST_CBUFFER_SIZE] = {0, };
            tMqosCbufferIndex copyLen = mqosCbuffer_MemcpyFromCbuffer(dest, &cbuf, 0, 3);

            THEN("Three bytes shall be copied to the destination buffer")
            {
                REQUIRE(3 == copyLen);
                uint8_t expectedData[TEST_CBUFFER_SIZE] = {1, 2, 3, 0, };
                for(size_t i = 0; i < sizeof(dest); i++)
                {
                    REQUIRE(expectedData[i] == dest[i]);
                }
            }
        }


        WHEN("mqosCbuffer_MemcpyFromCbuffer() is called with maximum copy length equal to 2")
        {
            uint8_t dest[TEST_CBUFFER_SIZE] = {0, };
            tMqosCbufferIndex copyLen = mqosCbuffer_MemcpyFromCbuffer(dest, &cbuf, 0, 2);

            THEN("The first two bytes shall be copied to the destination buffer")
            {
                REQUIRE(2 == copyLen);
                uint8_t expectedData[TEST_CBUFFER_SIZE] = {1, 2, 0, };
                for(size_t i = 0; i < sizeof(dest); i++)
                {
                    REQUIRE(expectedData[i] == dest[i]);
                }
            }
        }


        WHEN("mqosCbuffer_MemcpyFromCbuffer() is called with the start index set to 1")
        {
            uint8_t dest[TEST_CBUFFER_SIZE] = {0, };
            tMqosCbufferIndex copyLen = mqosCbuffer_MemcpyFromCbuffer(dest, &cbuf, 1, sizeof(dest));

            THEN("The last two of the three bytes shall be copied to the destination buffer")
            {
                REQUIRE(2 == copyLen);
                uint8_t expectedData[TEST_CBUFFER_SIZE] = {2, 3, 0, };
                for(size_t i = 0; i < sizeof(dest); i++)
                {
                    REQUIRE(expectedData[i] == dest[i]);
                }
            }
        }
    }


    //-------------------------------------------------------------------------

    GIVEN("The Cbuffer is full")
    {
        /* reset the Cbuffer struct to initial state, then write TEST_CBUFFER_SIZE bytes */
        mqosCbuffer_Reset(&cbuf);
        for(tMqosCbufferIndex i = 0; i < TEST_CBUFFER_SIZE; i++)
        {
            (void) mqosCbuffer_Put(&cbuf, (uint8_t)i);
        }


        THEN("mqosCbuffer_IsFull() shall return true")
        {
            REQUIRE(mqosCbuffer_IsFull(&cbuf));
        }


        THEN("mqosCbuffer_Space() shall return 0")
        {
            REQUIRE(0 == mqosCbuffer_Space(&cbuf));
        }


        WHEN("mqosCbuffer_Put() is called")
        {
            tMqosConfig_BaseType status = mqosCbuffer_Put(&cbuf, 1);

            THEN("The fault code MQOS_CBUFFER_FULL shall be returned")
            {
                REQUIRE(MQOS_CBUFFER_FULL == status);
            }
        }
    }


    //-------------------------------------------------------------------------

    GIVEN("The write pointer has wrapped-around past the end of the data array, but the read pointer has not, and the cbuffer contains 3 bytes")
    {
        /* reset the Cbuffer struct to initial state */
        mqosCbuffer_Reset(&cbuf);
        /* read and write (TEST_CBUFFER_SIZE - 1) bytes, so that the Cbuffer is
         * empty, but readIndex and writeIndex are near the end of the data
         * array.
         */
        for(tMqosCbufferIndex i = 0; i < TEST_CBUFFER_SIZE - 1; i++)
        {
            (void) mqosCbuffer_Put(&cbuf, 0);
            (void) mqosCbuffer_Get(&cbuf, NULL);
        }
        /* write 3 bytes to the Cbuffer. writeIndex wraps-around past the end of
         * the data array and has a lower unsigned integer value than readIndex.
         */
        (void) mqosCbuffer_Put(&cbuf, 1);
        (void) mqosCbuffer_Put(&cbuf, 2);
        (void) mqosCbuffer_Put(&cbuf, 3);

        THEN("The reported Cbuffer length shall be 3")
        {
            REQUIRE(3 == mqosCbuffer_Length(&cbuf));
        }

        THEN("The indexes of byte values 1, 2 and 3 shall be 0, 1 and 2 respectively")
        {
            REQUIRE(0 == mqosCbuffer_IndexOf(&cbuf, 0, 1));
            REQUIRE(1 == mqosCbuffer_IndexOf(&cbuf, 0, 2));
            REQUIRE(2 == mqosCbuffer_IndexOf(&cbuf, 0, 3));
        }

        THEN("mqosCbuffer_IsEmpty shall return false")
        {
            REQUIRE(!mqosCbuffer_IsEmpty(&cbuf));
        }

        WHEN("mqosCbuffer_Get() is called 3 times, so that the readIndex wraps-around past the end of the data array")
        {
            for(uint32_t i = 1; i <= 3; i++)
            {
                uint8_t data = 0;
                REQUIRE(MQOS_CBUFFER_OK == mqosCbuffer_Get(&cbuf, &data));

                char str[160];
                sprintf(str, "On call %d to mqosCbuffer_Get(), the value %d shall be read", i, i);
                THEN(str)
                {
                    REQUIRE(data == (uint8_t)i);
                }

                if(i < 3)
                {
                    sprintf(str, "After call %d to mqosCbuffer_Get(), mqosCbuffer_IsEmpty() shall return false", i);
                    THEN(str)
                    {
                        REQUIRE(!mqosCbuffer_IsEmpty(&cbuf));
                    }
                }
                else
                {
                    THEN("After call 3 to mqosCbuffer_Get(), mqosCbuffer_IsEmpty() shall return true")
                    {
                        REQUIRE(mqosCbuffer_IsEmpty(&cbuf));
                    }
                }

                sprintf(str, "After call %d to mqosCbuffer_Get(), mqosCbuffer_IsFull() shall return false", i);
                THEN(str)
                {
                    REQUIRE(!mqosCbuffer_IsFull(&cbuf));
                }

                sprintf(str, "After call %d to mqosCbuffer_Get(), mqosCbuffer_Length() shall return %d", i, 3 - i);
                THEN(str)
                {
                    REQUIRE((3 - i) == mqosCbuffer_Length(&cbuf));
                }

                sprintf(str, "After call %d to mqosCbuffer_Get(), mqosCbuffer_Space() shall return %d", i, TEST_CBUFFER_SIZE - (3 - i));
                THEN(str)
                {
                    REQUIRE(TEST_CBUFFER_SIZE - (3 - i) == mqosCbuffer_Space(&cbuf));
                }

                sprintf(str, "After call %d to mqosCbuffer_Get(), mqosCbuffer_IndexOf() shall return MQOS_CBUFFER_NOT_FOUND for byte values <= %d", i, i);
                THEN(str)
                {
                    for(uint8_t data = 0; data <= (uint8_t)i; data++)
                    {
                        REQUIRE(MQOS_CBUFFER_NOT_FOUND == mqosCbuffer_IndexOf(&cbuf, 0, data));
                    }
                }

                sprintf(str, "After call %d to mqosCbuffer_Get(), mqosCbuffer_IndexOf() shall return an index for byte values > %d and <= 3", i, i);
                THEN(str)
                {
                    for(uint8_t data = (uint8_t)i + 1; data <= 3; data++)
                    {
                        REQUIRE(((tMqosCbufferIndex)data - (i + 1)) == mqosCbuffer_IndexOf(&cbuf, 0, data));
                    }
                }


                sprintf(str, "After call %d to mqosCbuffer_Get(), mqosCbuffer_Contains() shall return false for byte values <= %d", i, i);
                THEN(str)
                {
                    for(uint8_t data = 0; data <= (uint8_t)i; data++)
                    {
                        REQUIRE(!mqosCbuffer_Contains(&cbuf, 0, data));
                    }
                }

                sprintf(str, "After call %d to mqosCbuffer_Get(), mqosCbuffer_Contains() shall return true for byte values > %d and <= 3", i, i);
                THEN(str)
                {
                    for(uint8_t data = (uint8_t)i + 1; data <= 3; data++)
                    {
                        REQUIRE(mqosCbuffer_Contains(&cbuf, 0, data));
                    }
                }
            }
        }


        WHEN("mqosCbuffer_Peek() is called with index values from 0 to 2 inclusive")
        {
            for(tMqosCbufferIndex i = 0; i < 3; i++)
            {
                const uint8_t* data =  mqosCbuffer_Peek(&cbuf, i);

                char str[160];
                sprintf(str, "When the index is %d, mqosCbuffer_Peek() shall return %d", i, i + 1);
                THEN(str)
                {
                    REQUIRE(*data == (uint8_t) i + 1);  // data values are [1, 2, 3]
                }
            }
        }


        WHEN("mqosCbuffer_Peek() is called with index value greater than the number of bytes in the Cbuffer")
        {
            tMqosCbufferIndex index = 4;
            REQUIRE(index > mqosCbuffer_Length(&cbuf));
            const uint8_t* data = mqosCbuffer_Peek(&cbuf, index);

            THEN("The NULL pointer shall be returned")
            {
                REQUIRE(NULL == data);
            }
        }
    }


    //-------------------------------------------------------------------------

    GIVEN("The string \"Hello world\" has been written to the Cbuffer")
    {
        /* reset the Cbuffer struct to initial state, then write the string "Hello world" */
        mqosCbuffer_Reset(&cbuf);
        const char* expectedString = "Hello world";
        tMqosCbufferIndex expectedStringLen = strlen(expectedString); // not including the null character string termination
        REQUIRE(expectedStringLen == mqosCbuffer_StrncpyToCbuffer(&cbuf, expectedString, expectedStringLen));
        REQUIRE(expectedStringLen == mqosCbuffer_Length(&cbuf));

        WHEN("mqosCbuffer_Strncmp() is called with str = \"Hello world\", i = 0 and n = 11")
        {
            tMqosConfig_BaseType result = mqosCbuffer_Strncmp(&cbuf, 0, expectedString, expectedStringLen);

            THEN("The result shall be zero - i.e. the strings match")
            {
                REQUIRE(0 == result);
            }
        }


        WHEN("mqosCbuffer_Strncmp() is called with str = \"Hello\", i = 0 and n = 5")
        {
            const char* differentString = "Hello";
            tMqosCbufferIndex differentStringLen = strlen(differentString); // not including the null character string termination
            REQUIRE(5 == differentStringLen);

            tMqosConfig_BaseType result = mqosCbuffer_Strncmp(&cbuf, 0, differentString, differentStringLen);

            THEN("The result shall be zero - i.e. the strings match")
            {
                REQUIRE(0 == result);
            }
        }


        WHEN("mqosCbuffer_Strncmp() is called with str = \"Hello\", i = 0 and n = 25 (longer than the C string length)")
        {
            const char* differentString = "Hello";
            tMqosCbufferIndex differentStringLen = strlen(differentString); // not including the null character string termination
            tMqosCbufferIndex compareLen = 25;  // longer than the C string length
            REQUIRE(compareLen > differentStringLen);

            tMqosConfig_BaseType result = mqosCbuffer_Strncmp(&cbuf, 0, differentString, compareLen);

            THEN("The result shall be zero - i.e. the strings match, for all characters up to the C string length")
            {
                REQUIRE(0 == result);
            }
        }


        WHEN("mqosCbuffer_Strncmp() is called with str = \"Hello sailor\", i = 0 and n = 6 (less than string length)")
        {
            const char* differentString = "Hello sailor";
            tMqosCbufferIndex differentStringLen = strlen(differentString); // not including the null character string termination
            tMqosCbufferIndex compareLen = 6;
            REQUIRE(compareLen < differentStringLen);

            tMqosConfig_BaseType result = mqosCbuffer_Strncmp(&cbuf, 0, differentString, compareLen);

            THEN("The result shall be zero - i.e. the strings match")
            {
                REQUIRE(0 == result);
            }
        }


        WHEN("mqosCbuffer_Strncmp() is called with str = \"world\", i = 6 and n = 5")
        {
            const char* differentString = "world";
            tMqosCbufferIndex differentStringLen = strlen(differentString); // not including the null character string termination
            tMqosCbufferIndex differentIndex = 6;
            REQUIRE(5 == differentStringLen);

            tMqosConfig_BaseType result = mqosCbuffer_Strncmp(&cbuf, differentIndex, differentString, differentStringLen);

            THEN("The result shall be zero - i.e. the strings match")
            {
                REQUIRE(0 == result);
            }
        }


        WHEN("mqosCbuffer_Strncmp() is called with str = \"Goodbye\", i = 0 and n = 7")
        {
            const char* differentString = "Goodbye";
            tMqosCbufferIndex differentStringLen = strlen(differentString); // not including the null character string termination
            REQUIRE(7 == differentStringLen);

            tMqosConfig_BaseType result = mqosCbuffer_Strncmp(&cbuf, 0, differentString, differentStringLen);

            THEN("The result shall be greater than zero - i.e. the first character that does not match has a greater value in cbuf than in str")
            {
                REQUIRE(0 < result);
            }
        }


        WHEN("mqosCbuffer_Strncmp() is called with str = \"Hells world\", i = 0 and n = 8 (less than string length)")
        {
            const char* differentString = "Hells world";
            tMqosCbufferIndex differentStringLen = strlen(differentString); // not including the null character string termination
            tMqosCbufferIndex compareLen = 8;
            REQUIRE(compareLen < differentStringLen);

            tMqosConfig_BaseType result = mqosCbuffer_Strncmp(&cbuf, 0, differentString, compareLen);

            THEN("The result shall be less than zero - i.e. the first character that does not match has a lower value in cbuf than in str")
            {
                REQUIRE(0 > result);
            }
        }


        WHEN("mqosCbuffer_Strncmp() is called with str = \"Hello world. The C string is longer than the circular buffer length, but root parts match\", i = 0 and n = 5")
        {
            const char* differentString = "Hello world. The C string is longer than the circular buffer length, but root parts match";
            tMqosCbufferIndex differentStringLen = strlen(differentString); // not including the null character string termination
            REQUIRE(differentStringLen > mqosCbuffer_Length(&cbuf));

            tMqosConfig_BaseType result = mqosCbuffer_Strncmp(&cbuf, 0, differentString, differentStringLen);

            THEN("The result shall be less than zero - i.e. the first character that does not match has a lower value in cbuf than in str, or cbuf is shorter than str")
            {
                REQUIRE(0 > result);
            }
        }


        WHEN("mqosCbuffer_Strncmp() is called with str = \"A better world. The C string is longer than the circular buffer length, and root parts begins with lower character value\", i = 0 and n = 5")
        {
            const char* differentString = "A better world. The C string is longer than the circular buffer length, and root parts begins with lower character value";
            tMqosCbufferIndex differentStringLen = strlen(differentString); // not including the null character string termination
            REQUIRE(differentStringLen > mqosCbuffer_Length(&cbuf));

            tMqosConfig_BaseType result = mqosCbuffer_Strncmp(&cbuf, 0, differentString, differentStringLen);

            THEN("The result shall be greater than zero - i.e. the first character that does not match has a higher value in cbuf than in str")
            {
                REQUIRE(0 < result);
            }
         }
    }
}
