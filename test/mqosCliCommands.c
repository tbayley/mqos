/*
 * mqosCliCommands.c - mqosCliCommands module source file.
 *
 * Copyright (C) 2021 Antony Bayley.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include "mqosCli.h"
#include "mqosCliCommands.h"

#if MQOS_CONFIG_USE_STANDARD_STRING_LIBRARY
#include <string.h>
#else
#include "mqosString.h"
#endif


/* PRIVATE TYPE DEFINITIONS **************************************************/


/* PRIVATE FUNCTION DECLARATIONS *********************************************/

/*----- START OF APPLICATION-SPECIFIC COMMAND FUNCTION DECLARATIONS -----*/
static tMqosConfig_BaseType mqosCliCommands_Test1(tMqosCbuffer* inCbuffer, tMqosCbufferIndex startIndex, tMqosCbufferIndex eolIndex, tMqosCbuffer* outCbuffer);
static tMqosConfig_BaseType mqosCliCommands_Echo(tMqosCbuffer* inCbuffer, tMqosCbufferIndex startIndex, tMqosCbufferIndex eolIndex, tMqosCbuffer* outCbuffer);
static tMqosConfig_BaseType mqosCliCommands_Add(tMqosCbuffer* inCbuffer, tMqosCbufferIndex startIndex, tMqosCbufferIndex eolIndex, tMqosCbuffer* outCbuffer);
/*----- END OF APPLICATION-SPECIFIC COMMAND FUNCTION DECLARATIONS -----*/


/* PRIVATE VARIABLE DEFINITIONS **********************************************/

/*! MQOS CLI Command definition array.
 *
 *  This array defines all the application's CLI commands. The commands are
 *  application-specific, so each application implements a customised
 *  version of mqosCliCommands.c within its own source code directory.
 *
 *  MQOS CLI does not dynamically register CLI function handlers at run time.
 *  Instead the function handlers are statically defined at compile time. This
 *  architecture better suits the targeted microcontrollers, such as 8-bit
 *  Microchip PI devices, whose compilers do not support dynamic memory
 *  allocation (malloc).
 */
static tMqosCli_CmdDefinition mqosCliCommands_cmdDefinition[] =
{
    /*----- START OF APPLICATION-SPECIFIC COMMAND DEFINITIONS -----*/

    {
        "test1",                                        // The command keyword
        "Write a test message to the output buffer.",   // Help string: describes what the command does
        mqosCliCommands_Test1                           // Function called when the command is invoked
    },
    {
        "echo",                                         // The command keyword
        "Echo command line parameters back to sender.", // Help string: describes what the command does
        mqosCliCommands_Echo                            // Function called when the command is invoked
    },
    {
        "add",                                          // The command keyword
        "Add two integer parameters.",                  // Help string: describes what the command does
        mqosCliCommands_Add                             // Function called when the command is invoked
    },

    /*----- END OF APPLICATION-SPECIFIC COMMAND DEFINITIONS -----*/

    /* help is always the last command in the list - it is a default part of MQOS CLI */
    {
        "help",                                         // The command
        "Print a list of all available commands.",      // Help string: describes what the command does
        mqosCli_Help                                    // Function called when the command is invoked
    }
};


/* PRIVATE FUNCTION DEFINITIONS **********************************************/

/*--- START OF APPLICATION-SPECIFIC COMMAND HANDLER FUNCTION DEFINITIONS ---*/

/*! test1 command function.
 *
 *  This function is called when the user enters the test1 command. It writes
 *  the message "This is test1\r\n" to the output Cbuffer. If there is
 *  insufficient space in the output Cbuffer the function returns the fault
 *  code MQOS_CLI_INSUFFICIENT_SPACE.
 * 
 *  @param[in]  inCbuffer   Pointer to input Cbuffer containing the command
 *                          line.
 *
 *  @param      startIndex  Command parser starting index, relative to
 *                          inCbuffer's read index. On entry, index points
 *                          to the first character after the command keyword
 *                          in the command line.
 *
 *  @param      eolIndex    End-of-line index, relative to inCbuffer's
 *                          read index.
 *
 *  @param[out] outBuffer   Pointer to output Cbuffer.
 * 
 *  @return                 Result code:
 *                            MQOS_CLI_OK: Success.
 *                            MQOS_CLI_CMD_NOT_FINISHED: Success - the command
 *                              expects to be called again to finish its work.
 *                            Non-zero fault code on failure.
 */
static tMqosConfig_BaseType mqosCliCommands_Test1(tMqosCbuffer* inCbuffer, tMqosCbufferIndex startIndex, tMqosCbufferIndex eolIndex, tMqosCbuffer* outCbuffer)
{
    /* inCbuffer, startIndex and eolIndex parameters are not used */
    (void) inCbuffer;
    (void) startIndex;
    (void) eolIndex;

    const char* str = "This is test1\r\n";

    /* write command response if outCbuffer has space, else return fault code */
    tMqosConfig_BaseType result = MQOS_CBUFFER_FULL;
    if(mqosCbuffer_Space(outCbuffer) >= strlen(str))
    {
        (void) mqosCbuffer_StrcpyToCbuffer(outCbuffer, str);
        result = MQOS_CBUFFER_OK;
    }
    return result;
}


/*! Echo command handler function.
 *
 *  This function is called when the user enters the echo command. It echoes
 *  back all command line parameters to the sender, including the terminating
 *  newline character.
 * 
 *  If there is insufficient space in the output Cbuffer, the function writes
 *  data to the output Cbuffer until it is full and returns the fault code
 *  MQOS_CLI_INSUFFICIENT_SPACE. Any remaining echo command parameters are
 *  deleted from the input Cbuffer, up to the delimiting newline character.
 * 
 *  @param[in]  inCbuffer   Pointer to input Cbuffer containing the command
 *                          line.
 *
 *  @param      startIndex  Command parser starting index, relative to
 *                          inCbuffer's read index. On entry, index points
 *                          to the first character after the command keyword
 *                          in the command line.
 *
 *  @param      eolIndex    End-of-line index, relative to inCbuffer's
 *                          read index.
 *
 *  @param[out] outBuffer   Pointer to output Cbuffer.
 * 
 *  @return                 Result code:
 *                            MQOS_CLI_OK: Success.
 *                            MQOS_CLI_CMD_NOT_FINISHED: Success - the command
 *                              expects to be called again to finish its work.
 *                            Non-zero fault code on failure.
 */
static tMqosConfig_BaseType mqosCliCommands_Echo(tMqosCbuffer* inCbuffer, tMqosCbufferIndex startIndex, tMqosCbufferIndex eolIndex, tMqosCbuffer* outCbuffer)
{
    tMqosConfig_BaseType result = MQOS_CLI_OK;

    /* Find the first character in the echo command line that is not a delimiter (whitespace) */
    tMqosCliToken token = mqosCli_NextTokenOrDelimiter(inCbuffer, startIndex, true, NULL);
    if(token.index < eolIndex)
    {
        /* copy all characters from first token to end-of-line */
        const uint8_t* data;
        while((NULL != (data = mqosCbuffer_Peek(inCbuffer, token.index))))
        {
            if(MQOS_CLI_OK != mqosCbuffer_Put(outCbuffer, *data))
            {
                /* outCbuffer is full */
                result = MQOS_CLI_INSUFFICIENT_SPACE;
                break;
            }
            if('\n' == (char) *data)
            {
                break;
            }
            token.index += 1;
        }
    }
    else
    {
        result = MQOS_CLI_TOKEN_NOT_FOUND;
    }
    
    return result;
}


/*! Add command handler function.
 *
 *  This function is called when the user enters the add command. It parses the
 *  command line to read two integer parameters, adds them, and prints the
 *  result to outCbuf.
 * 
 *  If there is insufficient space in the output Cbuffer, the function writes
 *  data to the output Cbuffer until it is full and returns the fault code
 *  MQOS_CLI_INSUFFICIENT_SPACE.
 * 
 *  @param[in]  inCbuffer   Pointer to input Cbuffer containing the command
 *                          line.
 *
 *  @param      startIndex  Command parser starting index, relative to
 *                          inCbuffer's read index. On entry, index points
 *                          to the first character after the command keyword
 *                          in the command line.
 *
 *  @param      eolIndex    End-of-line index, relative to inCbuffer's
 *                          read index.
 *
 *  @param[out] outBuffer   Pointer to output Cbuffer.
 * 
 *  @return                 Result code:
 *                            MQOS_CLI_OK: Success.
 *                            MQOS_CLI_CMD_NOT_FINISHED: Success - the command
 *                              expects to be called again to finish its work.
 *                            Non-zero fault code on failure.
 */
static tMqosConfig_BaseType mqosCliCommands_Add(tMqosCbuffer* inCbuffer, tMqosCbufferIndex startIndex, tMqosCbufferIndex eolIndex, tMqosCbuffer* outCbuffer)
{
    tMqosConfig_BaseType result = MQOS_CLI_OK;
    int32_t input[2];

    /* Read two tokens from the command line and convert to integers */
    for(tMqosConfig_BaseType i = 0; i < 2; i++)
    {
        tMqosCliToken token = mqosCli_NextTokenOrDelimiter(inCbuffer, startIndex, true, NULL);
        if(token.index < eolIndex)
        {
            input[i] = mqosCli_TokenToInt(inCbuffer, token);
            startIndex = token.index + token.len;
        }
        else
        {
            result = MQOS_CLI_TOKEN_NOT_FOUND;
        }
    }
    if(MQOS_CLI_OK == result)
    {
        /* add the two integers and write the sum to outCbuffer */
        int32_t sum = input[0] + input[1];
        result = mqosCli_WriteIntToCbuffer(outCbuffer, sum);
        if(2 != mqosCbuffer_StrcpyToCbuffer(outCbuffer, "\r\n"))
        {
            result |= MQOS_CLI_INSUFFICIENT_SPACE;
        }
    }
    return result;
}


/*--- END OF APPLICATION-SPECIFIC COMMAND HANDLER FUNCTION DEFINITIONS ---*/


/* PUBLIC FUNCTION DEFINITIONS ***********************************************/

/* The function definitions for mqosCliCommands_NumberOfCommands() and
 * mqosCliCommands_GetCmdDefinition must be included in every project's
 * customised copy of mqosCliCommands.c.
 */


tMqosConfig_UBaseType mqosCliCommands_NumberOfCommands(void)
{
    /* To avoid the error: "incomplete type is not allowed", the number of
     * commands must be calculated in the same file in which the array
     * mqosCliCommands_cmdDefinition[] is defined. Therefore it cannot be
     * implemented as a macro or as a static inline function.
     */
    return (tMqosConfig_UBaseType) (sizeof(mqosCliCommands_cmdDefinition) / sizeof(mqosCliCommands_cmdDefinition[0]));
}


const tMqosCli_CmdDefinition* mqosCliCommands_GetCmdDefinition(tMqosConfig_UBaseType index)
{
    const tMqosCli_CmdDefinition* cmd = NULL;

    if( mqosCliCommands_NumberOfCommands() > index )
    {
        cmd = &mqosCliCommands_cmdDefinition[index];
    }
    return cmd;
}
