/*
 * test_mqosMessageQueue.c - mqosMessageQueue module unit test source file.
 *
 * Copyright (C) 2021 Antony Bayley.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <stdlib.h>
#include "mqosMessageQueue.h"
#include "catch.hpp"

#define UNINITIALISED_MSG_PTR   ((tMqosMessageContainer*)0xFF)

/* The mqosMessageQueue module is only included in the build when FIFO order
 * message scheduling is configured.  It is not used for unordered message
 * scheduling.
 */

#if MQOS_CONFIG_MSG_SCHEDULER_FIFO_ORDER

/*!
 *  Test helper function that counts the number of items in the message queue.
 * 
 *  @param  queue   Message queue pointer.
 *  @return         The number of items in the queue.
 */
tMqosConfig_MessageIndex getQueueLength(tMqosMessageQueue* queue)
{
    tMqosConfig_MessageIndex count = 0;
    tMqosMessageContainer* msg = queue->front;
    while(NULL != msg)
    {
        count++;
        msg = msg->nextMsg;
    }
    return count;
}


SCENARIO("MQOS message queue tests", "[mqosMessageQueue]")
{
    GIVEN("The queue is empty")
    {
        /* create an empty queue... */
        tMqosMessageQueue queue = {NULL, NULL};

        /* ...and a test message */
#if MQOS_CONFIG_MSG_CONTAINS_PAYLOAD
        tMqosMessageContainer msg1 = {{mqosConfig_MSG_TEST1, {1}}, 0, UNINITIALISED_MSG_PTR};
#else   /* #if MQOS_CONFIG_MSG_CONTAINS_PAYLOAD */
        tMqosMessageContainer msg1 = {{mqosConfig_MSG_TEST1}, 0, UNINITIALISED_MSG_PTR};
#endif  /* #if MQOS_CONFIG_MSG_CONTAINS_PAYLOAD */
        REQUIRE(msg1.nextMsg != NULL);

        WHEN("No messages have been sent to the queue")
        {
            THEN("The queue's front and back message pointers shall be NULL")
            {
                REQUIRE(queue.front == NULL);
                REQUIRE(queue.back == NULL);
            }

            THEN("The queue length shall be 0")
            {
                REQUIRE(getQueueLength(&queue) == 0);
            }
        }

        WHEN("An attempt is made to receive a message from the front of the queue")
        {
            /* attempt to receive one message */
            tMqosMessageContainer* msg = mqosMessageQueue_ReceiveFromFront(&queue);

            THEN("No message shall be received")
            {
                REQUIRE(msg == NULL);
            }

            THEN("The queue's front and back message pointers shall be NULL")
            {
                REQUIRE(queue.front == NULL);
                REQUIRE(queue.back == NULL);
            }

            THEN("The queue length shall be 0")
            {
                REQUIRE(getQueueLength(&queue) == 0);
            }
        }

        WHEN("One message is sent to the back of the queue")
        {
            /* add a message */
            mqosMessageQueue_SendToBack(&queue, &msg1);

            THEN("The queue's front and back message pointers shall both point to the sent message")
            {
                REQUIRE(queue.front == &msg1);
                REQUIRE(queue.back == &msg1);
            }

            THEN("The sent message's nextMsg pointer shall be NULL")
            {
                REQUIRE(msg1.nextMsg == NULL);
            }

            THEN("The queue length shall be 1")
            {
                REQUIRE(getQueueLength(&queue) == 1);
            }
        }
    }

    //-------------------------------------------------------------------------

    GIVEN("The queue contains one message")
    {
        /* create some test messages... */
#if MQOS_CONFIG_MSG_CONTAINS_PAYLOAD
        tMqosMessageContainer msg1 = {{mqosConfig_MSG_TEST1, {1}}, 0, UNINITIALISED_MSG_PTR};
        tMqosMessageContainer msg2 = {{mqosConfig_MSG_TEST2, {2}}, 0, UNINITIALISED_MSG_PTR};
#else   /* #if MQOS_CONFIG_MSG_CONTAINS_PAYLOAD */
        tMqosMessageContainer msg1 = {{mqosConfig_MSG_TEST1}, 0, UNINITIALISED_MSG_PTR};
        tMqosMessageContainer msg2 = {{mqosConfig_MSG_TEST2}, 0, UNINITIALISED_MSG_PTR};
#endif  /* #if MQOS_CONFIG_MSG_CONTAINS_PAYLOAD */
        REQUIRE(msg2.nextMsg != NULL);

        /* ...and a queue containing one message */
        tMqosMessageQueue queue = {NULL, NULL};
        mqosMessageQueue_SendToBack(&queue, &msg1);
        REQUIRE(getQueueLength(&queue) == 1);
        REQUIRE(queue.front == &msg1);
        REQUIRE(queue.back == &msg1);

        WHEN("An attempt is made to receive a message from the front of the queue")
        {
            /* attempt to receive one message */
            tMqosMessageContainer* msg = mqosMessageQueue_ReceiveFromFront(&queue);

            THEN("The message at the front of the queue shall be received")
            {
                REQUIRE(msg == &msg1);
            }

            THEN("The queue's front and back message pointers shall be NULL")
            {
                REQUIRE(queue.front == NULL);
                REQUIRE(queue.back == NULL);
            }

            THEN("The queue length shall be 0")
            {
                REQUIRE(getQueueLength(&queue) == 0);
            }
        }

        WHEN("One message is sent to the back of the queue")
        {
            /* add a message */
            mqosMessageQueue_SendToBack(&queue, &msg2);

            THEN("The queue's front pointer shall remain unchanged")
            {
                REQUIRE(queue.front == &msg1);
            }

            THEN("The queue's back pointer shall point to newly sent message")
            {
                REQUIRE(queue.back == &msg2);
            }

            THEN("The added message's nextMsg pointer shall be NULL")
            {
                REQUIRE(msg2.nextMsg == NULL);
            }

            THEN("The queue length shall be 2")
            {
                REQUIRE(getQueueLength(&queue) == 2);
            }
        }
    }

    //-------------------------------------------------------------------------

    GIVEN("The queue contains two messages")
    {
        /* create some test messages... */
#if MQOS_CONFIG_MSG_CONTAINS_PAYLOAD
        tMqosMessageContainer msg1 = {{mqosConfig_MSG_TEST1, {1}}, 0, UNINITIALISED_MSG_PTR};
        tMqosMessageContainer msg2 = {{mqosConfig_MSG_TEST2, {2}}, 0, UNINITIALISED_MSG_PTR};
        tMqosMessageContainer msg3 = {{mqosConfig_MSG_TEST3, {3}}, 0, UNINITIALISED_MSG_PTR};
#else   /* #if MQOS_CONFIG_MSG_CONTAINS_PAYLOAD */
        tMqosMessageContainer msg1 = {{mqosConfig_MSG_TEST1}, 0, UNINITIALISED_MSG_PTR};
        tMqosMessageContainer msg2 = {{mqosConfig_MSG_TEST2}, 0, UNINITIALISED_MSG_PTR};
        tMqosMessageContainer msg3 = {{mqosConfig_MSG_TEST3}, 0, UNINITIALISED_MSG_PTR};
#endif  /* #if MQOS_CONFIG_MSG_CONTAINS_PAYLOAD */
        REQUIRE(msg3.nextMsg != NULL);

        /* ...and a queue containing two messages */
        tMqosMessageQueue queue = {NULL, NULL};
        mqosMessageQueue_SendToBack(&queue, &msg1);
        mqosMessageQueue_SendToBack(&queue, &msg2);
        REQUIRE(getQueueLength(&queue) == 2);
        REQUIRE(queue.front == &msg1);
        REQUIRE(queue.back == &msg2);

        WHEN("An attempt is made to receive a message from the front of the queue")
        {
            /* attempt to receive one message */
            tMqosMessageContainer* msg = mqosMessageQueue_ReceiveFromFront(&queue);

            THEN("The message at the front of the queue shall be received")
            {
                REQUIRE(msg == &msg1);
            }

            THEN("The queue's front and back pointers shall point to the remaining message")
            {
                REQUIRE(queue.front == &msg2);
                REQUIRE(queue.back == &msg2);
            }

            THEN("The queue length shall be 1")
            {
                REQUIRE(getQueueLength(&queue) == 1);
            }
        }

        WHEN("One message is sent to the back of the queue")
        {
            /* add a message */
            mqosMessageQueue_SendToBack(&queue, &msg3);

            THEN("The queue's front pointer shall remain unchanged")
            {
                REQUIRE(queue.front == &msg1);
            }

            THEN("The queue's back pointer shall point to newly sent message")
            {
                REQUIRE(queue.back == &msg3);
            }

            THEN("The added message's nextMsg pointer shall be NULL")
            {
                REQUIRE(msg3.nextMsg == NULL);
            }

            THEN("The queue length shall be 3")
            {
                REQUIRE(getQueueLength(&queue) == 3);
            }
        }
    }

    //-------------------------------------------------------------------------

    GIVEN("The queue contains three messages")
    {
        /* create some test messages... */
#if MQOS_CONFIG_MSG_CONTAINS_PAYLOAD
        tMqosMessageContainer msg1 = {{mqosConfig_MSG_TEST1, {1}}, 0, UNINITIALISED_MSG_PTR};
        tMqosMessageContainer msg2 = {{mqosConfig_MSG_TEST2, {2}}, 0, UNINITIALISED_MSG_PTR};
        tMqosMessageContainer msg3 = {{mqosConfig_MSG_TEST3, {3}}, 0, UNINITIALISED_MSG_PTR};
#else   /* #if MQOS_CONFIG_MSG_CONTAINS_PAYLOAD */
        tMqosMessageContainer msg1 = {{mqosConfig_MSG_TEST1}, 0, UNINITIALISED_MSG_PTR};
        tMqosMessageContainer msg2 = {{mqosConfig_MSG_TEST2}, 0, UNINITIALISED_MSG_PTR};
        tMqosMessageContainer msg3 = {{mqosConfig_MSG_TEST3}, 0, UNINITIALISED_MSG_PTR};
#endif  /* #if MQOS_CONFIG_MSG_CONTAINS_PAYLOAD */

        /* ...and a queue containing three messages */
        tMqosMessageQueue queue = {NULL, NULL};
        mqosMessageQueue_SendToBack(&queue, &msg1);
        mqosMessageQueue_SendToBack(&queue, &msg2);
        mqosMessageQueue_SendToBack(&queue, &msg3);
        REQUIRE(getQueueLength(&queue) == 3);
        REQUIRE(queue.front == &msg1);
        REQUIRE(queue.back == &msg3);

        WHEN("An attempt is made to receive a message from the front of the queue")
        {
            /* attempt to receive one message */
            tMqosMessageContainer* msg = mqosMessageQueue_ReceiveFromFront(&queue);

            THEN("The message at the front of the queue shall be received")
            {
                REQUIRE(msg == &msg1);
            }

            THEN("The queue's front pointer shall point to msg2")
            {
                REQUIRE(queue.front == &msg2);
            }

            THEN("The queue's back pointer shall remain unchanged")
            {
                REQUIRE(queue.back == &msg3);
            }

            THEN("The queue length shall be 2")
            {
                REQUIRE(getQueueLength(&queue) == 2);
            }
        }
    }
}

#endif  /* #if MQOS_CONFIG_MSG_SCHEDULER_FIFO_ORDER */
