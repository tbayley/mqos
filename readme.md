# MQOS: Message Queueing Operating System #


## Introduction ##

MQOS (Message Queueing Operating System) is an operating system kernel for use
on embedded processors that have very little RAM, such as Microchip's mid-range
8-bit PIC processors (PIC16F, PIC18F). MQOS can be used with compilers that do
not support dynamic memory allocation (i.e. _malloc()_ and _free()_ functions).

MQOS is a cooperative, non-preemptive, event-driven operating system.  Event
messages are posted to the message queue by calling the function
_mqosMessageSend()_, either from a peripheral's interrupt handler (e.g. UART,
I2C port) or from application code.

The main body of the application includes an event message processing loop that
repeatedly calls the function _mqosMessageReceive()_ to read messages from the
message queue. Each message is typically passed to a message handler function,
which runs to completion before returning to the event message processing loop.

Arbitrary time delays and periodic jobs may be implemented by calling the
function _mqosMessageSendLater()_, which automatically posts an event message to
the message queue after the specified time period has elapsed.


## Directories ##

MQOS has the following directory structure:

```
mqos
  |
  +- demo               Contains a demo project for every current MQOS port.
  |
  +- source             Contains the MQOS kernel source code.
  |   |
  |   +- include        Contains the MQOS kernel header files.
  |   |
  |   +- portable       Contains processor/compiler specific MQOS kernel code.
  |
  +- extra              Contains source code for optional MQOS add-ons.
  |   |
  |   +- include        Contains header files for optional MQOS add-ons.
  |
  +- test               Contains unit tests for MQOS kernel source code.
```

Further readme files are contained in sub-directories as appropriate.


## Using MQOS ##

The easiest way to use MQOS is to start with one of the pre-configured demo 
application projects (found in the _mqos/demo_ directory). Although MQOS is
primarily intended for embedded applications, the demo applications also include
Windows and Linux desktop applications. That enables you to evaluate MQOS even
if your chosen embedded target processor is not yet supported.

Application specific configuration is performed within the header file
_mqosConfig.h_, which should be located in the directory containing the
application's source code.  Base your customised _mqosConfig.h_ on one of the
demo projects and add new message IDs to suit your own application. In _main.c_,
modify the event message processing loop's switch statement to implement
the desired functionality.


## Porting MQOS to a new compiler or processor ##

To port MQOS to a new compiler, processor or target board you normally need to
create three new files within the compiler / processor specific portions of the
directory tree:

- _mqos/demo/COMPILER/PROCESSOR/mqosConfig.h_  
  Target-specific and application-specific configuration.

- _mqos/source/portable/COMPILER/PROCESSOR/mqosPort.h_  
  Defines target-specific macros, variables and functions.

- _mqos/source/portable/COMPILER/PROCESSOR/mqosPort.c_  
  Target-specific implementation of the variables and functions defined in mqosPort.h.

Base your new _mqosConfig.h_, _mqosPort.h_ and _mqosPort.c_ files on one of the
existing ports, to see what each file should contain.

The target-specific code must define macros _mqosENTER_CRITICAL()_ and
_mqosEXIT_CRITICAL()_ that are used to disable interrupts within critical
sections of code. The target-specific code must also implement a system tick
timer that calls the function _mqosTick()_ every MQOS_CONFIG_TICK_PERIOD_US
microseconds.
