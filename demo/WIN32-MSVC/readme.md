# MQOS demo application for Windows (Microsoft Visual Studio) #

This is a MQOS demo application, written in C, that runs on Windows and builds
with [Microsoft Visual Studio](https://visualstudio.microsoft.com/vs/) 2017 or
later. The Visual Studio workspace file is _mqos/demo/WIN32-MSVC/MqosDemo.sln_.

MQOS is intended for use on resource-constrained embedded systems. The Windows
port enables developers that do not have access to an embedded microprocessor
development board to evaluate MQOS. It is not envisaged that MQOS will be used
to develop Windows applications!


## User instructions for the MQOS demo application ##

The application runs two periodic tasks that are triggered to run by MQOS
messages. Each task prints messages to the console to identify the task ID and
how many times the task has run. A keyboard polling task, triggered by another
MQOS message, periodically checks for keyboard input and responds as follows:

	<Esc> or 'Q' key pressed	Quit the application.
	'1' key pressed             Toggle task 1 between enabled/disabled state.
	'2' key pressed             Toggle task 2 between enabled/disabled state.
