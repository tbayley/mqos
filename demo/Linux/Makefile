# Makefile for MQOS demo application on Linux (GCC)
#
# Copyright (C) 2021 Antony Bayley.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.


# Unit test build output directory and executable file
BUILD_PATH := build
LINK_TARGET := $(BUILD_PATH)/mqos_demo

# VPATH is a space-separated list of source directories, relative to the makefile
APP_SRC_PATH := .
MQOS_SRC_PATH := ../../source
PORT_SRC_PATH := ../../source/portable/Linux
VPATH := $(APP_SRC_PATH) $(MQOS_SRC_PATH) $(PORT_SRC_PATH)

# Find all .c source files
SOURCES := $(foreach dir, $(VPATH), $(wildcard $(dir)/*.c))
SOURCES_NOTDIR := $(notdir $(SOURCES))

# Object files
OBJS := $(SOURCES_NOTDIR:%.c=$(BUILD_PATH)/%.o)

# Set the auto-generated dependency file names
DEPENDENCIES := $(SOURCES_NOTDIR:%.c=$(BUILD_PATH)/%.d)

# Space-separated list of include directories to be searched for header files
APP_INCLUDE_PATH := .
MQOS_INCLUDE_PATH := ../../source/include
PORT_INCLUDE_PATH := ../../source/portable/Linux
INCLUDE_PATHS := $(APP_INCLUDE_PATH) $(MQOS_INCLUDE_PATH) $(PORT_INCLUDE_PATH)

# Generate the compiler -I options to specify header file search paths
INCLUDES := $(foreach dir, $(INCLUDE_PATHS), $(dir:%=-I%))

# Compiler flags
#   -std=c99            Use C99 language standard
#   -Wall -Wextra       Implement all warnings
#   -MMD -MP            Auto generate makefile dependencies (non-system headers)
#   -O0                 Reduce compilation time and make debugging produce the expected results
#   -g                  Generate debugging information
#   -DDEBUG             Define DEBUG preprocesor symbol (equivalent to #define DEBUG)
#   -D_POSIX_C_SOURCE   Define _POSIX_C_SOURCE preprocesor symbol to use Posix libraries
COMPILE_FLAGS := -std=c99 -Wall -Wextra -MMD -MP -O0 -g -DDEBUG -D_POSIX_C_SOURCE=199309L
export CFLAGS := $(CFLAGS) $(COMPILE_FLAGS)

# Linker flags
#   -g                  Generate debugging information
LINK_FLAGS := -g
export LDFLAGS := $(LDFLAGS) $(LINK_FLAGS)

# Linker libraries
#   -lrt                Link the Posix realtime library (functions declared in <time.h>)
#   -lpthread           Link the Posix thread library (functions declared in <pthread.h>)
#   -lcurses            Link the ncurses library (functions declared in <curses.h>)

LINK_LIBS := -lrt -lpthread -lcurses

# Default target: display help
help:
	@echo "The available make targets are:"
	@echo "  make           -   List all make targets (equivalent to make help)"
	@echo "  make build     -   Build MQOS demo application: $(LINK_TARGET)"
	@echo "  make run       -   Run MQOS demo application"
	@echo "  make clean     -   Delete all build artefacts"
	@echo "  make help      -   List all make targets and the actions performed"

# Run the MQOS demo application
run: $(LINK_TARGET)
	@echo "Running MQOS demo application: $(LINK_TARGET)"
	./$(LINK_TARGET)

# Build the MQOS demo application's executable file
build: $(LINK_TARGET)
	@echo "Finished building MQOS demo application: $(LINK_TARGET)"

# Link each .o object file using a pattern rule
$(LINK_TARGET): $(OBJS)
	@echo "Linking: $@"
	gcc $(LDFLAGS) $^ -o $@ $(LINK_LIBS)

# Build each .c source file in VPATH
$(BUILD_PATH)/%.o: %.c
	@echo "Compiling: $<"
	mkdir -p $(BUILD_PATH)
	gcc $(CFLAGS) $(INCLUDES) -c $< -o $@ 

clean:
	rm -f $(OBJS) $(DEPENDENCIES) $(LINK_TARGET)
	rm -rf ../../.vscode/ipch	# VS Code intellisense pre-compiled headers

# Rule used to check MAKE variables, which is useful for debugging the Makefile.
# Use on the command line like this:
# make print-VARNAME
print-%: ; @echo $*=$($*)

# Include rules from automatically generated dependency files
-include $(DEPENDENCIES)
