/*
 * main.c - mqos demo for Linux source file.
 *
 * This application demonstrates how to use MQOS on Linux wit GCC compiler.
 *
 * MQOS is intended for use on resource-constrained embedded systems. The
 * Linux port enables developers that do not have access to an embedded
 * microprocessor development board to evaluate MQOS. It is not envisaged that
 * MQOS will be used to develop Linux applications!
 *
 * Copyright (C) 2021 Antony Bayley.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#include <stdio.h>
#include <stdbool.h>
#include "mqos.h"

#include <time.h>
#include <curses.h>


/* CONSTANTS *****************************************************************/

#define KBD_POLL_PERIOD     100     //!< Keyboard polling duration in milliseconds
#define TASK1_PERIOD        2000    //!< Task 1 duration in milliseconds
#define TASK2_PERIOD        8000    //!< Task 2 duration in milliseconds


/* PRIVATE VARIABLE DEFINITIONS **********************************************/

static bool enableTask1 = true;    /*! Task 1 enabled/disabled state */
static bool enableTask2 = true;    /*! Task 2 enabled/disabled state */

static tMqosConfig_MessagePayload zeroMsgPayload = { 0 };  /*! message payload with value 0 */


/* PRIVATE FUNCTION DECLARATIONS *********************************************/

static void runTask(uint32_t taskId, tMqosConfig_MessagePayload taskData);
static bool runKeyboardInputTask(void);


/* PRIVATE FUNCTION DEFINITIONS **********************************************/

/*!
 *  Run a periodic task that prints task ID and number of calls to the console.
 *
 *  @param  taskId      Task ID of the task to run.
 *  @param  taskData    Task data.
 */
static void runTask(uint32_t taskId, tMqosConfig_MessagePayload taskData)
{
    /* print the task ID and the number of times that the task has been called */
    float time = ((float) mqos_getSystemTickCount() * MQOS_CONFIG_TICK_PERIOD_US) / 1000000;
    printw("Time: %08.3f    Task: %d    Iteration: %d\n", time, taskId, taskData.data);
    fflush(stdout);

    /* increment the task data */
    taskData.data += 1;

    /* schedule the task to run again */
    if (1 == taskId)
    {
        (void)mqos_MessageSendLater(mqosConfig_MSG_TASK1, taskData, mqos_mSecToTicks(TASK1_PERIOD));
    }
    else
    {
        (void)mqos_MessageSendLater(mqosConfig_MSG_TASK2, taskData, mqos_mSecToTicks(TASK2_PERIOD));
    }
}


/*!
 *  Run the keyboard input polling task.
 *
 *  Poll for keyboard input and take action depending on the key pressed:
 *
 *    <Esc> key or <Q> key      Return true, to quit the application.
 *    <1> key                   Toggle task 1 between enabled and disabled state.
 *    <2> key                   Toggle task 2 between enabled and disabled state.
 *
 *  @return     true: quit the application, false: continue running.
 */
static bool runKeyboardInputTask(void)
{
    bool quit = false;

    char c = getch();
    if(ERR != c)
    {
        float time = ((float)mqos_getSystemTickCount() * MQOS_CONFIG_TICK_PERIOD_US) / 1000000;

        /* Exit if <Esc> key or <Q> key is pressed */
        if ((0x1B == c) || ('q' == c) || ('Q' == c))    // 0x1B is <Esc> character
        {
            quit = true;    // quit the application
        }

        /* Toggle task 1 between enabled/disabled state when <1> key is pressed */
        if ('1' == c)
        {
            /* toggle the task 1 enabled/disabled state */
            enableTask1 = !enableTask1;
            if (enableTask1)
            {
                /* enable task 1 - re-start task iterations at 0 */
                printw("Time: %08.3f    ***** TASK 1 ENABLED  *****\n", time);
                runTask(1, zeroMsgPayload);
            }
            else
            {
                /* disable task 1 */
                printw("Time: %08.3f    ***** TASK 1 DISABLED *****\n", time);
                mqos_MessageCancelAll(mqosConfig_MSG_TASK1);
            }
        }

        /* Toggle task 2 between enabled/disabled state when <2> key is pressed */
        if ('2' == c)
        {
            /* toggle the task 2 enabled/disabled state */
            enableTask2 = !enableTask2;
            if (enableTask2)
            {
                /* enable task 2 - re-start task iterations at 0 */
                printw("Time: %08.3f    ***** TASK 2 ENABLED  *****\n", time);
                runTask(2, zeroMsgPayload);
            }
            else
            {
                /* disable task 2 */
                printw("Time: %08.3f    ***** TASK 2 DISABLED *****\n", time);
                mqos_MessageCancelAll(mqosConfig_MSG_TASK2);
            }
        }
    }

    /* schedule the next run of the keyboard input task */
    (void)mqos_MessageSendLater(mqosConfig_MSG_POLL_KBD, zeroMsgPayload, mqos_mSecToTicks(KBD_POLL_PERIOD));

    return quit;
}


/* PUBLIC FUNCTION DEFINITIONS ***********************************************/

int main()
{
    /* Linux-specific initialisation: set system tick period (100ms) */
    mqosPort_Init();

    /* Linux-specific initialisation: set the main application thread sleep time */
    struct timespec sleepTimespec = {0, 1000000L};  /* 1ms */

    /* Linux-specific initialisation: Configure ncurses for raw keyboard input */
    initscr();
    cbreak();
    noecho();
    scrollok(stdscr, TRUE);
    nodelay(stdscr, TRUE);

    /* User instructions */
    printw("-------------------------------------------------------------------------\n");
    printw("Press <Esc> or 'Q' key       Quit the MQOS demo application\n");
    printw("Press '1' key                Toggle task 1 between enabled/disabled state\n");
    printw("Press '2' key                Toggle task 2 between enabled/disabled state\n");
    printw("-------------------------------------------------------------------------\n");

    /* Start the MQOS task that polls for keyboard input */
    (void)mqos_MessageSendLater(mqosConfig_MSG_POLL_KBD, zeroMsgPayload, mqos_mSecToTicks(KBD_POLL_PERIOD));

    /* Start two periodic MQOS tasks */
    runTask(1, zeroMsgPayload);  // task 1
    runTask(2, zeroMsgPayload);  // task 2

    bool quit = false;
    while(!quit)
    {
        tMqosMessage msg = mqos_MessageReceive();
        switch(msg.msgId)
        {
            case mqosConfig_MSG_TASK1:
            {
                if(enableTask1)
                {
                    /* run task 1 */
                    runTask(1, msg.payload);
                }
            }
            break;

            case mqosConfig_MSG_TASK2:
            {
                if(enableTask2)
                {
                    /* run task 2 */
                    runTask(2, msg.payload);
                }
            }
            break;

            case mqosConfig_MSG_POLL_KBD:
            {
                /* poll for keyboard input */
                quit = runKeyboardInputTask();
            }
            break;

            default:
            {
                /* do nothing */
            }
            break;
        }

        /* Suspends execution of the main application thread for 1 millisecond.
         * This significantly reduces the processor load.
         */
        nanosleep(&sleepTimespec, NULL);
    }

    /* Linux-specific teardown: free system resources before exiting */
    endwin();   /* exit ncurses mode */
    mqosPort_Teardown();

    return 0;
}
