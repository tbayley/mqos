# MQOS demo application for PIC16F886 #

This is a MQOS demo application for the PIC16F886 processor that runs on the
Microchip high pin-count 8-bit
[Curiosity HPC development board](http://www.microchip.com/developmenttools/ProductDetails/DM164136),
Part Number: DM164136. The board's built-in PKOB debugger (PIC-kit on board) is
not designed to work with older devices such as the PIC16F886, so the board was
modified to add a 6-pin ICSP (in-circuit serial programming) header. The project
is configured to use the MPLAB ICD 3 debugger, but you can change the settings
to connect any other Microchip debugger that supports 6-pin ICSP connectivity.

The development tool versions used to create the application are:
- [MPLAB X IDE](https://www.microchip.com/mplab/mplab-x-ide) v5.20
- [XC8 C compiler](https://www.microchip.com/mplab/compilers) v2.05

Microchip's [MPLAB Code Configurator](https://www.microchip.com/mplab/mplab-code-configurator)
does not support older devices such as the PIC16F886.  Therefore the demo
application uses a hand-coded board support package, in the _bsp_ subdirectory,
to implement drivers for the clock oscillator, GPIO and peripherals.


## Using an external debugger with the Curiosity board ##

It is possible to disconnect the Curiosity development board's built-in PKOB 
debugger (PIC-kit on board) and connect an external debugger that uses a
standard 6-pin ICSP (in-circuit serial programming) connector, such as MPLAB
ICD 3 or ICD 4.

Refer to the Curiosity HPC development board schematic in Appendix A.1 of the
[Curiosity Curiosity High Pin Count (HPC) Development Board User's Guide](http://ww1.microchip.com/downloads/en/DeviceDoc/Curiosity-High-Pin-Count-Dev-Board-User-Guide-40001856A.pdf).

You need to make up a custom cable to connect from the debugger's 6-pin ICSP
connector to the appropriate pins of the two PIC device dual-row expansion
headers J8 and J11.  The required cable connections are:

| ICSP: (Pin) Signal | J8: (Pin) Signal | J11: (Pin) Signal |
|--------------------|------------------|-------------------|
| (1) VPP/MCLR       | (2) RE3          |                   |
| (2) VDD            |                  | (17) TVDD         |
| (3) VSS            |                  | (19) GND          |
| (4) ICSPDAT        |                  | (1)  RB7          |
| (5) ICSPCLK        |                  | (3)  RB6          |
| (6) AUX            |                  |                   |

To avoid damaging the built-in PKOB debugger, you are advised to use the
Curiosity board in 3.3V self-powered mode and not to power it from the debugger.
Power the board via a USB cable plugged into the micro-B connector marked
_"USB"_. Connect the jumper link on connector J12 to set VDD to 3.3V. J12 is the
connector marked _"5V"_ on one side and _"3V3"_ on the other side. Use the
project properties dialog to set the debugger _"Power"_ option that does not
power the target device.

The PKOB circuit is powered up only when a USB cable is connected to the _"USB"_
connector.  If an external debugger or device programmer applies programming
waveforms to the ICSP pins while the PKOB circuit is unpowered, the PKOB
circuitry is likely to be permanently damaged.

The PKOB circuit runs from a 3.3V power rail. Voltage level shifters are fitted
between the PKOB circuit and the target processor, to enable the target
processor to run at either 3.3V or 5V. However the level shifters are intended
to drive programming waveforms onto the target device ICSP pins, not to
withstand programming waveforms applied by an external device. Although it is
probably OK to configure a 5V target processor power rail, it is safer to use
3.3V if that provides fast enough processor performance for your application.


## User instructions for the MQOS demo application ##

The demo application flashes LED D5 with a period of 1 second and flashes LED D4
with a period of 4 seconds.  When push-button switch S1 is pressed and held, the
flashing LEDs freeze in their current state for the duration of the button
press.  The LEDs resume flashing when the button is released.


## Profiler ##

LED D2 is driven by the MQOS kernel profiler, which sets GPIO pin RA4 high (LED
D2 on) on entry to the task scheduler and sets it low again (LED D4 off) when
the scheduler returns.  Connecting an oscilloscope to this GPIO pin provides a
simple way to check the system clock tick period, and to assess the amount of
processor time consumed by the MQOS kernel.


## Memory usage

When the demo application is built for PIC16F886 target device using Microchip
XC8 C compiler v2.05 in "free" mode, at optimization level 0 (no optimization),
the memory usage statistics using the FIFO order message scheduling algorithm
are:

```
Memory Summary:
    Program space        used   2D5h (   725) of  2000h words   (  8.9%)
    Data space           used    3Dh (    61) of   170h bytes   ( 16.6%)
    EEPROM space         used     0h (     0) of   100h bytes   (  0.0%)
    Data stack space     used     0h (     0) of    60h bytes   (  0.0%)
    Configuration bits   used     2h (     2) of     2h words   (100.0%)
    ID Location space    used     0h (     0) of     4h bytes   (  0.0%)
```

The memory usage statistics using the unordered message scheduling algorithm
are:

```
Memory Summary:
    Program space        used   276h (   630) of  2000h words   (  7.7%)
    Data space           used    36h (    54) of   170h bytes   ( 14.7%)
    EEPROM space         used     0h (     0) of   100h bytes   (  0.0%)
    Data stack space     used     0h (     0) of    60h bytes   (  0.0%)
    Configuration bits   used     2h (     2) of     2h words   (100.0%)
    ID Location space    used     0h (     0) of     4h bytes   (  0.0%)
```
