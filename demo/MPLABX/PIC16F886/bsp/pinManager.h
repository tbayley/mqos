/*
 * pinManager.h - Pin manager module header file for PIC16F886.
 *
 * This header file provides APIs for driver for GPIO pins. The LED and switch
 * names are for the Curiosity development board, Part Number: DM164137.
 * 
 * Copyright (C) 2021 Antony Bayley.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef PIN_MANAGER_H
#define PIN_MANAGER_H

#ifdef __cplusplus
extern "C" {
#endif

#include <xc.h>

/* PUBLIC MACROS *************************************************************/
    
#define INPUT   1
#define OUTPUT  0

#define HIGH    1
#define LOW     0

#define ANALOG      1
#define DIGITAL     0

#define PULL_UP_ENABLED      1
#define PULL_UP_DISABLED     0

/* LED_D2 aliases */
#define LED_D2_TRIS                 TRISAbits.TRISA4
#define LED_D2_PORT                 PORTAbits.RA4
#define LED_D2_WPU                  WPUAbits.WPUA4
#define LED_D2_OD                   ODCONAbits.ODA4
#define LED_D2_ANS                  ANSELAbits.ANSA4
#define LED_D2_SetHigh()            do{ PORTAbits.RA4 = 1; } while(0)
#define LED_D2_SetLow()             do{ PORTAbits.RA4 = 0; } while(0)
#define LED_D2_Toggle()             do{ PORTAbits.RA4 = ~PORTAbits.RA4; } while(0)
#define LED_D2_GetValue()           PORTAbits.RA4
#define LED_D2_SetDigitalInput()    do{ TRISAbits.TRISA4 = 1; } while(0)
#define LED_D2_SetDigitalOutput()   do{ TRISAbits.TRISA4 = 0; } while(0)
#define LED_D2_SetPullup()          do{ WPUAbits.WPUA4 = 1; } while(0)
#define LED_D2_ResetPullup()        do{ WPUAbits.WPUA4 = 0; } while(0)
#define LED_D2_SetPushPull()        do{ ODCONAbits.ODA4 = 0; } while(0)
#define LED_D2_SetOpenDrain()       do{ ODCONAbits.ODA4 = 1; } while(0)
#define LED_D2_SetAnalogMode()      do{ ANSELAbits.ANSA4 = 1; } while(0)
#define LED_D2_SetDigitalMode()     do{ ANSELAbits.ANSA4 = 0; } while(0)

/* LED_D3 aliases */
#define LED_D3_TRIS                 TRISAbits.TRISA5
#define LED_D3_PORT                 PORTAbits.RA5
#define LED_D3_WPU                  WPUAbits.WPUA5
#define LED_D3_OD                   ODCONAbits.ODA5
#define LED_D3_ANS                  ANSELAbits.ANSA5
#define LED_D3_SetHigh()            do{ PORTAbits.RA5 = 1; } while(0)
#define LED_D3_SetLow()             do{ PORTAbits.RA5 = 0; } while(0)
#define LED_D3_Toggle()             do{ PORTAbits.RA5 = ~PORTAbits.RA5; } while(0)
#define LED_D3_GetValue()           PORTAbits.RA5
#define LED_D3_SetDigitalInput()    do{ TRISAbits.TRISA5 = 1; } while(0)
#define LED_D3_SetDigitalOutput()   do{ TRISAbits.TRISA5 = 0; } while(0)
#define LED_D3_SetPullup()          do{ WPUAbits.WPUA5 = 1; } while(0)
#define LED_D3_ResetPullup()        do{ WPUAbits.WPUA5 = 0; } while(0)
#define LED_D3_SetPushPull()        do{ ODCONAbits.ODA5 = 0; } while(0)
#define LED_D3_SetOpenDrain()       do{ ODCONAbits.ODA5 = 1; } while(0)
#define LED_D3_SetAnalogMode()      do{ ANSELAbits.ANSA5 = 1; } while(0)
#define LED_D3_SetDigitalMode()     do{ ANSELAbits.ANSA5 = 0; } while(0)

/* LED_D4 aliases */
#define LED_D4_TRIS                 TRISAbits.TRISA6
#define LED_D4_PORT                 PORTAbits.RA6
#define LED_D4_WPU                  WPUAbits.WPUA6
#define LED_D4_OD                   ODCONAbits.ODA6
#define LED_D4_ANS                  ANSELAbits.ANSA6
#define LED_D4_SetHigh()            do{ PORTAbits.RA6 = 1; } while(0)
#define LED_D4_SetLow()             do{ PORTAbits.RA6 = 0; } while(0)
#define LED_D4_Toggle()             do{ PORTAbits.RA6 = ~PORTAbits.RA6; } while(0)
#define LED_D4_GetValue()           PORTAbits.RA6
#define LED_D4_SetDigitalInput()    do{ TRISAbits.TRISA6 = 1; } while(0)
#define LED_D4_SetDigitalOutput()   do{ TRISAbits.TRISA6 = 0; } while(0)
#define LED_D4_SetPullup()          do{ WPUAbits.WPUA6 = 1; } while(0)
#define LED_D4_ResetPullup()        do{ WPUAbits.WPUA6 = 0; } while(0)
#define LED_D4_SetPushPull()        do{ ODCONAbits.ODA6 = 0; } while(0)
#define LED_D4_SetOpenDrain()       do{ ODCONAbits.ODA6 = 1; } while(0)
#define LED_D4_SetAnalogMode()      do{ ANSELAbits.ANSA6 = 1; } while(0)
#define LED_D4_SetDigitalMode()     do{ ANSELAbits.ANSA6 = 0; } while(0)

/* LED_D5 aliases */
#define LED_D5_TRIS                 TRISAbits.TRISA7
#define LED_D5_PORT                 PORTAbits.RA7
#define LED_D5_WPU                  WPUAbits.WPUA7
#define LED_D5_OD                   ODCONAbits.ODA7
#define LED_D5_SetHigh()            do{ PORTAbits.RA7 = 1; } while(0)
#define LED_D5_SetLow()             do{ PORTAbits.RA7 = 0; } while(0)
#define LED_D5_Toggle()             do{ PORTAbits.RA7 = ~PORTAbits.RA7; } while(0)
#define LED_D5_GetValue()           PORTAbits.RA7
#define LED_D5_SetDigitalInput()    do{ TRISAbits.TRISA7 = 1; } while(0)
#define LED_D5_SetDigitalOutput()   do{ TRISAbits.TRISA7 = 0; } while(0)
#define LED_D5_SetPullup()          do{ WPUAbits.WPUA7 = 1; } while(0)
#define LED_D5_ResetPullup()        do{ WPUAbits.WPUA7 = 0; } while(0)
#define LED_D5_SetPushPull()        do{ ODCONAbits.ODA7 = 0; } while(0)
#define LED_D5_SetOpenDrain()       do{ ODCONAbits.ODA7 = 1; } while(0)

/* SWITCH_S1 aliases */
#define SWITCH_S1_TRIS                 TRISBbits.TRISB4
#define SWITCH_S1_PORT                 PORTBbits.RB4
#define SWITCH_S1_WPU                  WPUBbits.WPUB4
#define SWITCH_S1_OD                   ODCONBbits.ODB4
#define SWITCH_S1_SetHigh()            do{ PORTBbits.RB4 = 1; } while(0)
#define SWITCH_S1_SetLow()             do{ PORTBbits.RB4 = 0; } while(0)
#define SWITCH_S1_Toggle()             do{ PORTBbits.RB4 = ~PORTCbits.RB4; } while(0)
#define SWITCH_S1_GetValue()           PORTBbits.RB4
#define SWITCH_S1_SetDigitalInput()    do{ TRISBbits.TRISB4 = 1; } while(0)
#define SWITCH_S1_SetDigitalOutput()   do{ TRISBbits.TRISB4 = 0; } while(0)
#define SWITCH_S1_SetPullup()          do{ WPUBbits.WPUB4 = 1; } while(0)
#define SWITCH_S1_ResetPullup()        do{ WPUBbits.WPUB4 = 0; } while(0)
#define SWITCH_S1_SetPushPull()        do{ ODCONBbits.ODB4 = 0; } while(0)
#define SWITCH_S1_SetOpenDrain()       do{ ODCONBbits.ODB4 = 1; } while(0)


/* PUBLIC FUNCTION DECLARATIONS **********************************************/

/*!
 *  GPIO and peripheral I/O initialisation
 */
void pinManager_Initialise(void);


#ifdef __cplusplus
}
#endif

#endif // PIN_MANAGER_H
/**
 End of File
*/