/*
 * pinManager.c - Pin manager module source file for PIC16F886.
 *
 * This header file provides APIs for driver for GPIO pins. The LED and switch
 * names are for the Curiosity development board, Part Number: DM164137.
 * 
 * Copyright (C) 2021 Antony Bayley.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <xc.h>
#include "pinManager.h"


/* PUBLIC FUNCTION DEFINITIONS ***********************************************/

void pinManager_Initialise(void)
{
    /**
    TRISx registers
    */
    TRISA = 0x0F;   /* RA4 to RA7 are outputs, all other PORT A pins are inputs */
    TRISB = 0xFF;   /* all PORT B pins are inputs */
    TRISC = 0xFF;

    /**
    ANSEL registers
    */
    ANSEL = 0xEF;   // AN4 (RA5) is digital output, all other ANx pins are analog inputs
    ANSELH = 0xF7;  // ANS11 is digital input, all other pins are analog inputs

    /**
    WPUx registers
    */
    WPUB = 0x00;    // all weak pull-ups disabled
    OPTION_REGbits.nRBPU = 1;
    
    /* turn off all LEDs */
    PORTA = 0;
}
