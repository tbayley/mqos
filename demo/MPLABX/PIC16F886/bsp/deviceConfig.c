/*
 * deviceConfig.c - Device configuration module source file for PIC16F886.
 *
 * This source file sets the device configuration bits.
 * 
 * Copyright (C) 2021 Antony Bayley.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

/* Configuration bits */
#pragma config DEBUG = OFF      //!< In_Circuit Debugger disabled, RB6/ICSPCLK and RB7/ICSPDAT are GPIO
#pragma config LVP = OFF        //!< RB3 pin is digital I/O, HV on MCLR must be used for programming
#pragma config FCMEN = ON       //!< Fail-Safe Clock Monitor Enable->Fail-Safe Clock Monitor is enabled
#pragma config IESO = ON        //!< Internal/External Switch Over->Internal External Switch Over mode is enabled
#pragma config BOREN = ON       //!< Brown-out Reset Enable->Brown-out Reset enabled
#pragma config CPD = OFF        //!< EEPROM Data Memory Code Protection->Program memory code protection is disabled
#pragma config CP = OFF         //!< Flash Program Memory Code Protection->Program memory code protection is disabled
#pragma config MCLRE = ON       //!< MCLR Pin Function Select->MCLR/VPP pin function is MCLR
#pragma config PWRTE = OFF      //!< Power-up Timer Enable->PWRT disabled
#pragma config WDTE = OFF       //!< Watchdog Timer Enable->WDT disabled
#pragma config FOSC = INTRC_NOCLKOUT  //!< INTOSCIO oscillator: I/O function on RA6/OSC2/CLKOUT pin, I/O function on RA7/OSC1/CLKIN

#pragma config BOR4V = BOR21V   //!< Brown-out Reset set to 2.1V
#pragma config WRT = OFF        //!< Flash program memory self write protection off
