/*
 * interruptManager.c - Interrupt manager module source file for PIC16F690.
 *
 * This header file implements global interrupt handling functions.
 * For individual peripheral handlers please see the peripheral driver files.
 *
 * Copyright (C) 2021 Antony Bayley.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "interruptManager.h"
#include "bsp.h"


/* PUBLIC FUNCTION DEFINITIONS ***********************************************/

/*!
 *  Global interrupt handler function
 */
void __interrupt() interruptManager_InterruptManager (void)
{
    /* Interrupt handler - Interrupts remain disabled until ISR returns */
    if(INTCONbits.T0IE == 1 && INTCONbits.T0IF == 1)
    {
        tmr0_ISR();
    }
    else
    {
        //Unhandled Interrupt
    }
}
