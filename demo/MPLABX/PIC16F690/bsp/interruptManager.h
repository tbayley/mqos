/*
 * interruptManager.h - Interrupt manager module header file for PIC16F690.
 *
 * This header file defines global interrupt handling macros and functions.
 * For individual peripheral handlers please see the peripheral driver files.
 *
 * Copyright (C) 2021 Antony Bayley.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef INTERRUPT_MANAGER_H
#define INTERRUPT_MANAGER_H

#ifdef __cpluspl
extern "C" {
#endif

    
/* PUBLIC MACROS *************************************************************/

/*!
 *  This macro enables global interrupts.
 */
#define interruptManager_GlobalInterruptEnable() (INTCONbits.GIE = 1)


/*!
 *  This macro disables global interrupts.
 */
#define interruptManager_GlobalInterruptDisable() (INTCONbits.GIE = 0)


/*!
 *  This macro enables peripheral interrupts.
 */
#define interruptManager_PeripheralInterruptEnable() (INTCONbits.PEIE = 1)


/*!
 *  This macro disables peripheral interrupts.
 */
#define interruptManager_PeripheralInterruptDisable() (INTCONbits.PEIE = 0)


#ifdef __cplusplus
}
#endif

#endif  // INTERRUPT_MANAGER_H
