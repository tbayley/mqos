/*
 * main.c - MQOS demo application for PIC16F690
 *
 * This is a MQOS demo application that runs on the Microchip low pin-count
 * 8-bit "Curiosity" development board, Part Number:
 * [DM164137](https://www.microchip.com/DevelopmentTools/ProductDetails/PartNO/DM164137).
 * The board's default processor is replaced by a PIC16F690 processor.
 *
 * MQOS Kernel V1.0.0
 * Copyright (C) 2021 Antony Bayley.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */


#include <stdbool.h>
#include "bsp.h"
#include "mqos.h"

/* CONSTANTS *****************************************************************/

#define LED_D6_PERIOD   4000    // Time period of LED D6 flash in milliseconds
#define LED_D7_PERIOD   1000    // Time period of LED D7 flash in milliseconds
#define BTN_POLL_PERIOD 5       // Button S1 polling period in milliseconds


/* TYPE DEFINITIONS **********************************************************/

/*! Button pressed state */
typedef enum
{
    NO_CHANGE,
    PRESSED,
    RELEASED
} tButtonState;


/* PUBLIC VARIABLE DEFINITIONS ***********************************************/


/* PRIVATE VARIABLE DEFINITIONS **********************************************/

static bool enableLeds = true;                            //!< LED driver enable/disable
static tMqosConfig_MessagePayload nullMessageData = {0};  //!< Dummy MQOS message data


/* PRIVATE FUNCTION DECLARATIONS *********************************************/

tButtonState getButtonState(void);


/* PRIVATE FUNCTION DEFINITIONS **********************************************/

/*! 
 *  Return the debounced button-press value for switch S1
 * 
 *  This function is intended to be called repeatedly,  with a 5ms period.
 * 
 *  S1 GPIO input (RC4) is logic 1 when S1 is not pressed and is logic 0 when
 *  S1 is pressed. This function returns PRESSED when the state buffer contains
 *  a single 1 sample followed by 4 consecutive 0 samples. It returns RELEASED
 *  when the state buffer contains a single 0 sample followed by 4 consecutive
 *  1 samples.
 * 
 * @return  PRESSED   : Button press has occurred
 *          RELEASED  : Button release has occurred
 *          NO_CHANGE : Button state has not changed
 */
tButtonState getButtonState(void)
{
    static uint8_t rawStateBuffer = 0;   // previous five raw switch states
    tButtonState state = NO_CHANGE;
    
    rawStateBuffer = (uint8_t) ((rawStateBuffer << 1) | SWITCH_S1_GetValue() | 0xE0);
    if(rawStateBuffer == 0xF0)
    {
        state = PRESSED;
    }
    else if(rawStateBuffer == 0xEF)
    {
        state = RELEASED;
    }
    return state;
}


/* PUBLIC FUNCTION DEFINITIONS ***********************************************/

/**
 * Main application
 */
void main(void)
{
    /* Board support package - system initialisation */
    bsp_SystemInitialise();

    /* Initialise two LED flash timers and a timer for button press debouncing */
    (void) mqos_MessageSendLater(mqosConfig_MSG_POLL_BTN, nullMessageData, mqos_mSecToTicks(BTN_POLL_PERIOD));
    (void) mqos_MessageSendLater(mqosConfig_MSG_TOGGLE_D6, nullMessageData, mqos_mSecToTicks(LED_D6_PERIOD) / 2);
    (void) mqos_MessageSendLater(mqosConfig_MSG_TOGGLE_D7, nullMessageData, mqos_mSecToTicks(LED_D7_PERIOD) / 2);

    while(1)
    {
        tMqosMessage msg = mqos_MessageReceive();
        switch(msg.msgId)
        {
            case mqosConfig_MSG_TOGGLE_D6:
            {
                /* reset LED D6 flash timer */
                (void) mqos_MessageSendLater(mqosConfig_MSG_TOGGLE_D6, nullMessageData, mqos_mSecToTicks(LED_D6_PERIOD) / 2);
                // toggle LED D6, unless button S1 is pressed
                if(enableLeds)
                {
                    LED_D6_Toggle();
                }
            }
            break;

            case mqosConfig_MSG_TOGGLE_D7:
            {
                /* reset LED D7 flash timer */
                (void) mqos_MessageSendLater(mqosConfig_MSG_TOGGLE_D7, nullMessageData, mqos_mSecToTicks(LED_D7_PERIOD) / 2);
                // toggle LED D7, unless button S1 is pressed
                if(enableLeds)
                {
                    LED_D7_Toggle();
                }
            }
            break;

            case mqosConfig_MSG_POLL_BTN:
            {
                /* reset button press polling timer */
                (void) mqos_MessageSendLater(mqosConfig_MSG_POLL_BTN, nullMessageData, mqos_mSecToTicks(BTN_POLL_PERIOD));
                /* Poll the input switch */
                tButtonState btnState = getButtonState();
                if(btnState == PRESSED)
                {
                    /* disable LED flashing while the button is pressed */
                    enableLeds = false;
                }
                else if(btnState == RELEASED)
                {
                    /* enable LED flashing while the button is not pressed */
                    enableLeds = true;
                }
            }
            break;

            default:
            {
                /* do nothing */
            }
            break;
        }
    }
}
