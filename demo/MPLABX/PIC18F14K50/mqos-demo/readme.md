# MQOS demo application for PIC18F14K50 #

This is a MQOS demo application for the PIC18F14K50 processor that runs on the
Microchip [Low Pin Count USB Development Kit](https://www.microchip.com/DevelopmentTools/ProductDetails/PartNO/DM164127-2),
Part Number: DM164127.

![Low Pin Count USB Development Board](../../../.img/low-pin-count-USB-dev-kit.jpg "Low Pin Count USB Development Board")

Jumper _J12_ shorts the VUSB pin to Vdd rail. This jumper should always be left
open, unless an 'LF' device is used and an external 3.3V supply is connected to
J9, with the J14 jumper in the leftmost position).

Jumper _J14_ selects the power source for the board. Short pins 1 and 2 to use
an external power supply connected to J9. Short pins 2 and 3 to power the board
from the USB VBUS line.

The development tool versions used to create the application are:
- [MPLAB X IDE](https://www.microchip.com/mplab/mplab-x-ide) v5.20
- [XC8 C compiler](https://www.microchip.com/mplab/compilers) v2.05
- [Microchip Libraries for Applications (MLA)](https://www.microchip.com/mplab/microchip-libraries-for-applications) v2018_11_26

The chip peripherals were configured at design time by using Microchip's
[MPLAB Code Configurator](https://www.microchip.com/mplab/mplab-code-configurator),
which generates driver files for the clock oscillator, GPIO and peripherals in
the _mcc\_generated\_files_ subdirectory.


## Using an external debugger with the Low Pin Count USB Development Kit ##

The Low Pin Count USB Development Kit does not include a PICkit on board (PKOB)
debugger, so an external debugger must be used to program the target device and
debug the demo application. The project is configured to use the Microchip 
[ICD 3](https://www.microchip.com/Developmenttools/ProductDetails/DV164035)
debugger. However the project can be altered easily to specifiy a different
debugger such as [PICkit 4](https://www.microchip.com/developmenttools/ProductDetails/PG164140)
or [ICD 4](https://www.microchip.com/developmenttools/ProductDetails/dv164045).

The Low Pin Count USB Development Kit includes a PIC18F14K50 debug header, part
number AC244023, because the standard PIC18F14K50 device does not contain debug
circuitry on-chip.  To debug the application, fit the debug header to IC socket
U1 on the low pin count USB development kit and connect the debugger to the
6-pin ICSP connector on the debug header. You must set _Supported Debug Header_
to _AC244023_ in the project _Properties_ window to debug code using the debug
header. You cannot program a production build to a debug header - you can only
run/debug a debug build.

To program a production build to the PIC18F14K50 device and run the application
without debugging, fit a PIC18F14K50 in IC socket U1 and connect the
debugger/programmer to the 6-pin ICSP connector J6 (ICSP1) on the low pin count
USB development kit board. You must set _Supported Debug Header_ to _None_ in
the project _Properties_ window to run code on the standard PIC18F14K50 device.
You cannot program a debug build the standard device - you can only run a
production build.


## User instructions for the MQOS demo application ##

The demo application flashes LED D1 with a period of 1 second and flashes LED D2
with a period of 4 seconds.  When push-button switch S1 is pressed and held, the
flashing LEDs freeze in their current state for the duration of the button
press.  The LEDs resume flashing when the button is released.


## Profiler ##

LED D4 is driven by the MQOS kernel profiler, which sets GPIO pin RC3 high (LED
D4 on) on entry to the task scheduler and sets it low again (LED D4 off) when
the scheduler returns.  Connecting an oscilloscope to this GPIO pin provides a
simple way to check the system clock tick period, and to assess the amount of
processor time consumed by the MQOS kernel.


## Memory usage

When the demo application is built for PIC18F14K50 target device using Microchip
XC8 C compiler v2.05 in "free" mode, at optimization level 0 (no optimization),
the memory usage statistics using the FIFO order message scheduling algorithm
are:

```
Memory Summary:
    Program space        used   4F5h (  1269) of  4000h bytes   (  7.7%)
    Data space           used    44h (    68) of   300h bytes   (  8.9%)
    Configuration bits   used     7h (     7) of     7h words   (100.0%)
    EEPROM space         used     0h (     0) of   100h bytes   (  0.0%)
    ID Location space    used     8h (     8) of     8h bytes   (100.0%)
    Data stack space     used     0h (     0) of   2A0h bytes   (  0.0%)
```

The memory usage statistics using the unordered message scheduling algorithm
are:

```
Memory Summary:
    Program space        used   3DFh (   991) of  4000h bytes   (  6.0%)
    Data space           used    37h (    55) of   300h bytes   (  7.2%)
    Configuration bits   used     7h (     7) of     7h words   (100.0%)
    EEPROM space         used     0h (     0) of   100h bytes   (  0.0%)
    ID Location space    used     8h (     8) of     8h bytes   (100.0%)
    Data stack space     used     0h (     0) of   2A0h bytes   (  0.0%)
```
