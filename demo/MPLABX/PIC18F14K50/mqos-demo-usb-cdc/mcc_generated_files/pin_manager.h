/**
  @Generated Pin Manager Header File

  @Company:
    Microchip Technology Inc.

  @File Name:
    pin_manager.h

  @Summary:
    This is the Pin Manager file generated using PIC10 / PIC12 / PIC16 / PIC18 MCUs

  @Description
    This header file provides APIs for driver for .
    Generation Information :
        Product Revision  :  PIC10 / PIC12 / PIC16 / PIC18 MCUs - 1.76
        Device            :  PIC18F14K50
        Driver Version    :  2.11
    The generated drivers are tested against the following:
        Compiler          :  XC8 2.00
        MPLAB 	          :  MPLAB X 5.10	
*/

/*
    (c) 2018 Microchip Technology Inc. and its subsidiaries. 
    
    Subject to your compliance with these terms, you may use Microchip software and any 
    derivatives exclusively with Microchip products. It is your responsibility to comply with third party 
    license terms applicable to your use of third party software (including open source software) that 
    may accompany Microchip software.
    
    THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS". NO WARRANTIES, WHETHER 
    EXPRESS, IMPLIED OR STATUTORY, APPLY TO THIS SOFTWARE, INCLUDING ANY 
    IMPLIED WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY, AND FITNESS 
    FOR A PARTICULAR PURPOSE.
    
    IN NO EVENT WILL MICROCHIP BE LIABLE FOR ANY INDIRECT, SPECIAL, PUNITIVE, 
    INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, COST OR EXPENSE OF ANY KIND 
    WHATSOEVER RELATED TO THE SOFTWARE, HOWEVER CAUSED, EVEN IF MICROCHIP 
    HAS BEEN ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE FORESEEABLE. TO 
    THE FULLEST EXTENT ALLOWED BY LAW, MICROCHIP'S TOTAL LIABILITY ON ALL 
    CLAIMS IN ANY WAY RELATED TO THIS SOFTWARE WILL NOT EXCEED THE AMOUNT 
    OF FEES, IF ANY, THAT YOU HAVE PAID DIRECTLY TO MICROCHIP FOR THIS 
    SOFTWARE.
*/

#ifndef PIN_MANAGER_H
#define PIN_MANAGER_H

/**
  Section: Included Files
*/

#include <xc.h>

#define INPUT   1
#define OUTPUT  0

#define HIGH    1
#define LOW     0

#define ANALOG      1
#define DIGITAL     0

#define PULL_UP_ENABLED      1
#define PULL_UP_DISABLED     0

// get/set SWITCH_S1 aliases
#define SWITCH_S1_PORT                 PORTAbits.RA3
#define SWITCH_S1_WPU                  WPUAbits.WPUA3
#define SWITCH_S1_GetValue()           PORTAbits.RA3
#define SWITCH_S1_SetPullup()          do { WPUAbits.WPUA3 = 1; } while(0)
#define SWITCH_S1_ResetPullup()        do { WPUAbits.WPUA3 = 0; } while(0)

// get/set LED_D1 aliases
#define LED_D1_TRIS                 TRISCbits.TRISC0
#define LED_D1_LAT                  LATCbits.LATC0
#define LED_D1_PORT                 PORTCbits.RC0
#define LED_D1_ANS                  ANSELbits.ANS4
#define LED_D1_SetHigh()            do { LATCbits.LATC0 = 1; } while(0)
#define LED_D1_SetLow()             do { LATCbits.LATC0 = 0; } while(0)
#define LED_D1_Toggle()             do { LATCbits.LATC0 = ~LATCbits.LATC0; } while(0)
#define LED_D1_GetValue()           PORTCbits.RC0
#define LED_D1_SetDigitalInput()    do { TRISCbits.TRISC0 = 1; } while(0)
#define LED_D1_SetDigitalOutput()   do { TRISCbits.TRISC0 = 0; } while(0)
#define LED_D1_SetAnalogMode()      do { ANSELbits.ANS4 = 1; } while(0)
#define LED_D1_SetDigitalMode()     do { ANSELbits.ANS4 = 0; } while(0)

// get/set LED_D2 aliases
#define LED_D2_TRIS                 TRISCbits.TRISC1
#define LED_D2_LAT                  LATCbits.LATC1
#define LED_D2_PORT                 PORTCbits.RC1
#define LED_D2_ANS                  ANSELbits.ANS5
#define LED_D2_SetHigh()            do { LATCbits.LATC1 = 1; } while(0)
#define LED_D2_SetLow()             do { LATCbits.LATC1 = 0; } while(0)
#define LED_D2_Toggle()             do { LATCbits.LATC1 = ~LATCbits.LATC1; } while(0)
#define LED_D2_GetValue()           PORTCbits.RC1
#define LED_D2_SetDigitalInput()    do { TRISCbits.TRISC1 = 1; } while(0)
#define LED_D2_SetDigitalOutput()   do { TRISCbits.TRISC1 = 0; } while(0)
#define LED_D2_SetAnalogMode()      do { ANSELbits.ANS5 = 1; } while(0)
#define LED_D2_SetDigitalMode()     do { ANSELbits.ANS5 = 0; } while(0)

// get/set LED_D3 aliases
#define LED_D3_TRIS                 TRISCbits.TRISC2
#define LED_D3_LAT                  LATCbits.LATC2
#define LED_D3_PORT                 PORTCbits.RC2
#define LED_D3_ANS                  ANSELbits.ANS6
#define LED_D3_SetHigh()            do { LATCbits.LATC2 = 1; } while(0)
#define LED_D3_SetLow()             do { LATCbits.LATC2 = 0; } while(0)
#define LED_D3_Toggle()             do { LATCbits.LATC2 = ~LATCbits.LATC2; } while(0)
#define LED_D3_GetValue()           PORTCbits.RC2
#define LED_D3_SetDigitalInput()    do { TRISCbits.TRISC2 = 1; } while(0)
#define LED_D3_SetDigitalOutput()   do { TRISCbits.TRISC2 = 0; } while(0)
#define LED_D3_SetAnalogMode()      do { ANSELbits.ANS6 = 1; } while(0)
#define LED_D3_SetDigitalMode()     do { ANSELbits.ANS6 = 0; } while(0)

// get/set LED_D4 aliases
#define LED_D4_TRIS                 TRISCbits.TRISC3
#define LED_D4_LAT                  LATCbits.LATC3
#define LED_D4_PORT                 PORTCbits.RC3
#define LED_D4_ANS                  ANSELbits.ANS7
#define LED_D4_SetHigh()            do { LATCbits.LATC3 = 1; } while(0)
#define LED_D4_SetLow()             do { LATCbits.LATC3 = 0; } while(0)
#define LED_D4_Toggle()             do { LATCbits.LATC3 = ~LATCbits.LATC3; } while(0)
#define LED_D4_GetValue()           PORTCbits.RC3
#define LED_D4_SetDigitalInput()    do { TRISCbits.TRISC3 = 1; } while(0)
#define LED_D4_SetDigitalOutput()   do { TRISCbits.TRISC3 = 0; } while(0)
#define LED_D4_SetAnalogMode()      do { ANSELbits.ANS7 = 1; } while(0)
#define LED_D4_SetDigitalMode()     do { ANSELbits.ANS7 = 0; } while(0)

/**
   @Param
    none
   @Returns
    none
   @Description
    GPIO and peripheral I/O initialization
   @Example
    PIN_MANAGER_Initialize();
 */
void PIN_MANAGER_Initialize (void);

/**
 * @Param
    none
 * @Returns
    none
 * @Description
    Interrupt on Change Handling routine
 * @Example
    PIN_MANAGER_IOC();
 */
void PIN_MANAGER_IOC(void);



#endif // PIN_MANAGER_H
/**
 End of File
*/