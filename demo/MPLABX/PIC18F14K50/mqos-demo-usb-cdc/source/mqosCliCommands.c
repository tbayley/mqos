/*
 * mqosCliCommands.c - mqosCliCommands module source file.
 *
 * Copyright (C) 2021 Antony Bayley.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include "mqosCli.h"
#include "mqosCliCommands.h"
#include "main.h"

#if(MQOS_CONFIG_STRING_LIBRARY == MQOS_CONFIG_STANDARD_STRING_LIBRARY)
#include <string.h>
#elif(MQOS_CONFIG_STRING_LIBRARY == MQOS_CONFIG_MQOS_STRING_LIBRARY)
#include "mqosString.h"
#endif


/* PRIVATE FUNCTION DECLARATIONS *********************************************/

/*----- START OF APPLICATION-SPECIFIC COMMAND FUNCTION DECLARATIONS -----*/
static tMqosConfig_BaseType mqosCliCommand_Start(tMqosCbuffer* inCbuffer, tMqosCbufferIndex startIndex, tMqosCbufferIndex eolIndex, tMqosCbuffer* outCbuffer);
static tMqosConfig_BaseType mqosCliCommand_Stop(tMqosCbuffer* inCbuffer, tMqosCbufferIndex startIndex, tMqosCbufferIndex eolIndex, tMqosCbuffer* outCbuffer);
/*----- END OF APPLICATION-SPECIFIC COMMAND FUNCTION DECLARATIONS -----*/


/* PRIVATE VARIABLE DEFINITIONS **********************************************/

/*! MQOS CLI Command definition array.
 *
 *  This array defines all the application's CLI commands. The commands are
 *  application-specific, so each application implements a customised
 *  version of mqosCliCommands.c within its own source code directory.
 *
 *  MQOS CLI does not dynamically register CLI function handlers at run time.
 *  Instead the function handlers are statically defined at compile time. This
 *  architecture better suits the targeted microcontrollers, such as 8-bit
 *  Microchip PI devices, whose compilers do not support dynamic memory
 *  allocation (malloc).
 */
static tMqosCli_CmdDefinition mqosCliCommands_cmdDefinition[] =
{
    /*----- START OF APPLICATION-SPECIFIC COMMAND DEFINITIONS -----*/

    {
        "start",                                    // The command
        "Start writing LED events to CLI output.",  // Help string: describes what the command does
        mqosCliCommand_Start                        // Function called when the command is invoked
    },
    {
        "stop",                                     // The command
        "Stop writing LED events to CLI output.",   // Help string: describes what the command does
        mqosCliCommand_Stop                         // Function called when the command is invoked
    },
    
    /*----- END OF APPLICATION-SPECIFIC COMMAND DEFINITIONS -----*/

    /* help is always the last command in the list - it is a default part of MQOS CLI */
    {
        "help",                                     // The command
        "Print a list of all available commands.",  // Help string: describes what the command does
        mqosCli_Help                                // Function called when the command is invoked
    }
};


/* PRIVATE FUNCTION DEFINITIONS **********************************************/

/*--- START OF APPLICATION-SPECIFIC COMMAND HANDLER FUNCTION DEFINITIONS ---*/

/*! Start command function.
 *
 *  This function is called when the user enters the start command. It starts
 *  writing LED state change events to the CLI output.
 * 
 *  @param[in]  inCbuffer   Pointer to input Cbuffer containing the command
 *                          line.
 *
 *  @param      startIndex  Command parser starting index, relative to
 *                          inCbuffer's read index. On entry, index points
 *                          to the first character after the command keyword
 *                          in the command line.
 *
 *  @param      eolIndex    End-of-line index, relative to inCbuffer's
 *                          read index.
 *
 *  @param[out] outBuffer   Pointer to output Cbuffer.
 * 
 *  @return                 Result code:
 *                            MQOS_CLI_OK: Success.
 *                            MQOS_CLI_CMD_NOT_FINISHED: Success - the command
 *                              expects to be called again to finish its work.
 *                            Non-zero fault code on failure.
 */
static tMqosConfig_BaseType mqosCliCommand_Start(tMqosCbuffer* inCbuffer, tMqosCbufferIndex startIndex, tMqosCbufferIndex eolIndex, tMqosCbuffer* outCbuffer)
{
    /* Input parameters are not used */
    (void) inCbuffer;
    (void) startIndex;
    (void) eolIndex;
    (void) outCbuffer;
    
    main_WriteLedEventsToCli(true);
    return MQOS_CLI_OK;
}


/*! Stop command function.
 *
 *  This function is called when the user enters the stop command. It stops
 *  writing LED state change events to the CLI output.
 * 
 *  @param[in]  inCbuffer   Pointer to input Cbuffer containing the command
 *                          line.
 *
 *  @param      startIndex  Command parser starting index, relative to
 *                          inCbuffer's read index. On entry, index points
 *                          to the first character after the command keyword
 *                          in the command line.
 *
 *  @param      eolIndex    End-of-line index, relative to inCbuffer's
 *                          read index.
 *
 *  @param[out] outBuffer   Pointer to output Cbuffer.
 * 
 *  @return                 Result code:
 *                            MQOS_CLI_OK: Success.
 *                            MQOS_CLI_CMD_NOT_FINISHED: Success - the command
 *                              expects to be called again to finish its work.
 *                            Non-zero fault code on failure.
 */
static tMqosConfig_BaseType mqosCliCommand_Stop(tMqosCbuffer* inCbuffer, tMqosCbufferIndex startIndex, tMqosCbufferIndex eolIndex, tMqosCbuffer* outCbuffer)
{
    /* Input parameters are not used */
    (void) inCbuffer;
    (void) startIndex;
    (void) eolIndex;
    (void) outCbuffer;
    
    main_WriteLedEventsToCli(false);
    return MQOS_CLI_OK;
}


/*--- END OF APPLICATION-SPECIFIC COMMAND HANDLER FUNCTION DEFINITIONS ---*/


/* PUBLIC FUNCTION DEFINITIONS ***********************************************/

/* The function definitions for mqosCliCommands_NumberOfCommands() and
 * mqosCliCommands_GetCmdDefinition must be included in every project's
 * customised copy of mqosCliCommands.c.
 */


tMqosConfig_UBaseType mqosCliCommands_NumberOfCommands(void)
{
    /* To avoid the error: "incomplete type is not allowed", the number of
     * commands must be calculated in the same file in which the array
     * mqosCliCommands_cmdDefinition[] is defined. Therefore it cannot be
     * implemented as a macro or as a static inline function.
     */
    return (tMqosConfig_UBaseType) (sizeof(mqosCliCommands_cmdDefinition) / sizeof(mqosCliCommands_cmdDefinition[0]));
}


const tMqosCli_CmdDefinition* mqosCliCommands_GetCmdDefinition(tMqosConfig_UBaseType index)
{
    const tMqosCli_CmdDefinition* cmd = NULL;

    if( mqosCliCommands_NumberOfCommands() > index )
    {
        cmd = &mqosCliCommands_cmdDefinition[index];
    }
    return cmd;
}
