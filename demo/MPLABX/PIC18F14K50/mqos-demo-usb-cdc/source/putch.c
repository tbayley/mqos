/*
 * putch.c - putch module source file.
 * 
 * This module implements the putch() function to write a single byte to the USB
 * serial port. This enables either the printf() function from the <stdio.h>
 * standard library, or the limited-functionality printf() function from the
 * MQOS library "mqosStdio.h", to be used on Microchip PIC devices.
 *
 * Copyright (C) 2021 Antony Bayley.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "mqosConfig.h"
#include "mqosStdio.h"
#include "cdcDevice.h"


#if(MQOS_CONFIG_STDIO_LIBRARY == MQOS_CONFIG_STANDARD_STDIO_LIBRARY)

/* Microchip <stdio.h> library requires application to implement putch() function */
void putch(char c)
{
    (void) cdcDevice_putchar(c);
}

#endif  /* #if(MQOS_CONFIG_STDIO_LIBRARY == MQOS_CONFIG_STANDARD_STDIO_LIBRARY) */


#if(MQOS_CONFIG_STDIO_LIBRARY == MQOS_CONFIG_MQOS_STDIO_LIBRARY)

/* MQOS "mqosStdio.h" library requires application to implement mqosPutch() function */
int mqosPutch(char c)
{
    int result = 0;
    if(0 == cdcDevice_putchar(c))
    {
        /* success */
        result = 1;
    }
    return result;
}

#endif  /* #if(MQOS_CONFIG_STDIO_LIBRARY == MQOS_CONFIG_MQOS_STDIO_LIBRARY) */
