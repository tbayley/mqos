/*
 * cdcDevice.c - CDC Device source file.
 *
 * Copyright (C) 2021 Antony Bayley.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

/* INCLUDES ******************************************************************/
#include "mcc.h"

#include <stdint.h>

#include "usb.h"
#include "cdcDevice.h"
#include "usb_config.h"
#include "mqosCli.h"

// #define ECHO_TEST

/* VARIABLES *****************************************************************/

/*! application read Cbuffer */
MQOS_CBUFFER_DEFINE(inCbuffer, CDC_DATA_OUT_EP_SIZE);

/*! application write Cbuffer */
MQOS_CBUFFER_DEFINE(outCbuffer, CDC_DATA_IN_EP_SIZE);


void cdcDevice_ProcessReceivedData(void)
{
#ifdef ECHO_TEST
    if(getBytesUSBUSART(&inCbuffer))
    {
        // echo
        char c;
        while(MQOS_CBUFFER_OK == mqosCbuffer_Get(&inCbuffer, &c))
        {
            mqosCbuffer_Put(&outCbuffer, c);
        }
    }
    return;
#endif  /* ECHO_TEST */
    
    /* If bytes have been received by USB USART, copy them to input Cbuffer */
    (void) getBytesUSBUSART(&inCbuffer);
}


void cdcDevice_ExecuteCommand(void)
{
    /* if input Cbuffer is not empty, process one command  */
    (void) mqosCli_InvokeCommand(&inCbuffer, &outCbuffer);
}


tMqosConfig_BaseType cdcDevice_putchar(char c)
{
    /* No need to check if USB CDC device is ready.
     * Output data are cached in writeCbuffer until USB is ready to transmit.
     * 
     * Use mqosCbuffer_Put_NoAssert() instead of mqosCbuffer_Put() to avoid
     * "recursive function call" errors if the run-time assert handler prints
     * debug messages on a device that does not support recursion.
     */
    return mqosCbuffer_Put_NoAssert(&outCbuffer, (uint8_t) c);
}


tMqosCbuffer* cdcDevice_getWriteCbufferHandle(void)
{
    return &outCbuffer;
}


void cdcDevice_InjectHelpCommand(void)
{
    mqosCbuffer_StrcpyToCbuffer(&inCbuffer, "help\r\n");
}
