/*
 * main.c - Main source file of MQOS USB CDC demo application.
 *
 * Copyright (C) 2021 Antony Bayley.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */


/* INCLUDES ******************************************************************/
#include <stdbool.h>
#include <stdint.h>
#include "main.h"
#include "mcc.h"
#include "usb.h"
#include "usb_device.h"
#include "usb_device_cdc.h"
#include "mqos.h"
#include "cdcDevice.h"

#if(MQOS_CONFIG_STDIO_LIBRARY == MQOS_CONFIG_STANDARD_STDIO_LIBRARY)
#include <stdio.h>
#elif(MQOS_CONFIG_STDIO_LIBRARY == MQOS_CONFIG_MQOS_STDIO_LIBRARY)
#include "mqosStdio.h"
#endif


/* CONSTANTS *****************************************************************/

#define LED_D2_PERIOD   4000    // Time period of LED D2 flash in milliseconds
#define LED_D1_PERIOD   1000    // Time period of LED D1 flash in milliseconds
#define BTN_POLL_PERIOD 5       // Button S1 polling period in milliseconds
#define BOOT_TIME       500     // Time before system is ready for use


/* TYPE DEFINITIONS **********************************************************/

/*! Button pressed state */
typedef enum
{
    NO_CHANGE,
    PRESSED,
    RELEASED
} tButtonState;


/* PRIVATE VARIABLE DEFINITIONS **********************************************/

static bool enableLeds = true;          //!< LED driver enable/disable
static bool enableLedEvents = false;    //!< enable/disable CLI LED state change notifications


/* PRIVATE FUNCTION DECLARATIONS *********************************************/

static tButtonState getButtonState(void);
static void writeLedEventToCli(const char* led, uint8_t ledState);


/* PRIVATE FUNCTION DEFINITIONS **********************************************/

/*! 
 *  Return the debounced button-press value for switch S1
 * 
 *  This function is intended to be called repeatedly,  with a 5ms period.
 * 
 *  S1 GPIO input (RB4) is logic 1 when S1 is not pressed and is logic 0 when
 *  S1 is pressed. This function returns PRESSED when the state buffer contains
 *  a single 1 sample followed by 4 consecutive 0 samples. It returns RELEASED
 *  when the state buffer contains a single 0 sample followed by 4 consecutive
 *  1 samples.
 * 
 * @return  PRESSED   : Button press has occurred
 *          RELEASED  : Button release has occurred
 *          NO_CHANGE : Button state has not changed
 */
static tButtonState getButtonState(void)
{
    static uint8_t rawStateBuffer = 0;   // previous five raw switch states
    tButtonState state = NO_CHANGE;
    
    rawStateBuffer = (uint8_t) ((rawStateBuffer << 1) | SWITCH_S1_GetValue() | 0xE0);
    if(rawStateBuffer == 0xF0)
    {
        state = PRESSED;
    }
    else if(rawStateBuffer == 0xEF)
    {
        state = RELEASED;
    }
    return state;
}


/*!
 *  Write an LED state changed event to the command line interface.
 *
 *  @param  led         LED name.
 *  @param  ledState    LED state.  0: LED off, 1: LED on.
 */
static void writeLedEventToCli(const char* led, uint8_t ledState)
{
    tMqosConfig_TimerCount tickCount = mqos_getSystemTickCount();
    const char* ledStateStr = ledState ? "On" : "Off";
    
    /* print the formatted output to the CLI */
    printf("System Ticks: %5u, LED %s: %s\r\n", tickCount, led, ledStateStr);
}


/* PUBLIC FUNCTION DEFINITIONS ***********************************************/

/*!
 *  Main program entry point.
 */
void main(void)
{
    SYSTEM_Initialize(SYSTEM_STATE_USB_START);

    USBDeviceInit();
    USBDeviceAttach();

    /* Initialise two LED flash timers and a timer for button press debouncing */
    (void) mqos_MessageSendLater(mqosConfig_MSG_SYSTEM_INITIALISED, mqos_mSecToTicks(BOOT_TIME));
    (void) mqos_MessageSendLater(mqosConfig_MSG_POLL_BTN, mqos_mSecToTicks(BTN_POLL_PERIOD));
    (void) mqos_MessageSendLater(mqosConfig_MSG_TOGGLE_D2, mqos_mSecToTicks(LED_D2_PERIOD) / 2);
    (void) mqos_MessageSendLater(mqosConfig_MSG_TOGGLE_D1, mqos_mSecToTicks(LED_D1_PERIOD) / 2);

    /* Enable watchdog timer with period of 1.057 seconds */
    WDTCON = 1 << _WDTCON_SWDTEN_POSN;
    
    while(1)
    {
        #if defined(USB_POLLING)
            // Interrupt or polling method.  If using polling, must call
            // this function periodically.  This function will take care
            // of processing and responding to SETUP transactions
            // (such as during the enumeration process when you first
            // plug in).  USB hosts require that USB devices should accept
            // and process SETUP packets in a timely fashion.  Therefore,
            // when using polling, this function should be called
            // regularly (such as once every 1.8ms or faster** [see
            // inline code comments in usb_device.c for explanation when
            // "or faster" applies])  In most cases, the USBDeviceTasks()
            // function does not take very long to execute (ex: <100
            // instruction cycles) before it returns.
            USBDeviceTasks();
        #endif

        /* Handle device-to-host CDC transactions */
        CDCTxService();

        /* Execute one command line from the input Cbuffer, if it contains one  */
        cdcDevice_ExecuteCommand();
        
        /* MQOS message handler - application specific functionality */
        tMqosMessage msg = mqos_MessageReceive();
        switch(msg.msgId)
        {
            case mqosConfig_MSG_SYSTEM_INITIALISED:
            {
                /* Write user instructions to console via USB serial port */
                printf("\r\nPress hardware button to freeze LEDs, or enter a command:\r\n");
                cdcDevice_InjectHelpCommand();
            }
            break;

            case mqosConfig_MSG_TOGGLE_D2:
            {
                /* reset LED D2 flash timer */
                (void) mqos_MessageSendLater(mqosConfig_MSG_TOGGLE_D2, mqos_mSecToTicks(LED_D2_PERIOD) / 2);
                /* toggle LED D2, unless button S1 is pressed */
                if(enableLeds)
                {
                    LED_D2_Toggle();
                    if(enableLedEvents)
                    {
                        uint8_t ledState = LED_D2_GetValue();
                        writeLedEventToCli("D2", ledState);
                    }
                }
            }
            break;

            case mqosConfig_MSG_TOGGLE_D1:
            {
                /* reset LED D1 flash timer */
                (void) mqos_MessageSendLater(mqosConfig_MSG_TOGGLE_D1, mqos_mSecToTicks(LED_D1_PERIOD) / 2);
                /* toggle LED D1, unless button S1 is pressed */
                if(enableLeds)
                {
                    LED_D1_Toggle();
                    if(enableLedEvents)
                    {
                        uint8_t ledState = LED_D1_GetValue();
                        writeLedEventToCli("D1", ledState);
                    }
                }
            }
            break;

            case mqosConfig_MSG_POLL_BTN:
            {
                /* reset button press polling timer */
                (void) mqos_MessageSendLater(mqosConfig_MSG_POLL_BTN, mqos_mSecToTicks(BTN_POLL_PERIOD));
                /* Poll the input switch */
                tButtonState btnState = getButtonState();
                if(btnState == PRESSED)
                {
                    /* disable LED flashing while the button is pressed */
                    enableLeds = false;
                    /* send button pressed message over serial port to host */
                    printf("Button pressed\r\n");
                }
                else if(btnState == RELEASED)
                {
                    /* enable LED flashing while the button is not pressed */
                    enableLeds = true;
                    /* send button released message over serial port to host */
                    printf("Button released\r\n");
                }
                
                /* clear the watchdog timer */
                CLRWDT();
            }
            break;

            case mqosConfig_MSG_CDC_DATA_RECEIVED:
            {
                cdcDevice_ProcessReceivedData();
            }
            break;

            default:
            {
                /* do nothing */
            }
            break;
        }
    }
}


void main_WriteLedEventsToCli(bool enable)
{
    enableLedEvents = enable;
}
