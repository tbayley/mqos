/*
 * cdcDevice.c - CDC Device header file.
 *
 * Copyright (C) 2021 Antony Bayley.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef CDC_DEVICE_H
#define CDC_DEVICE_H

#include <stdbool.h>
#include <stddef.h>
#include "mqos.h"
#include "usb_device_cdc.h"

/* CDC operation result codes */
#define CDCDEVICE_PUTCHAR_RESULT_SUCCESS            0   //!< CDC operation succeeded
#define CDCDEVICE_PUTCHAR_RESULT_USB_NOT_CONFIGURED 1   //!< USB is not configured
#define CDCDEVICE_PUTCHAR_RESULT_USB_SUSPENDED      2   //!< USB is suspended
#define CDCDEVICE_PUTCHAR_RESULT_CBUFFER_FULL       3   //!< Output Cbuffer is full


/*!
 *  Copy CDC serial data received from the host to the input Cbuffer.
 */
void cdcDevice_ProcessReceivedData(void);


/*!
 *  Execute one command line from the input Cbuffer, if it contains one.
 */
void cdcDevice_ExecuteCommand(void);


/*!
 *  Transmit a single character over the serial port to the host.
 * 
 *  @param  c       Character to be transmitted.
 *
 *  @return         Result code: zero on success, non-zero on failure.
 */
tMqosConfig_BaseType cdcDevice_putchar(char c);


/*!
 *  Get the pointer to the CDC write circular buffer.
 * 
 *  The write circular buffer contains data to be transmitted to the USB host.
 * 
 *  @return     Pointer to write circular buffer.
 */
tMqosCbuffer* cdcDevice_getWriteCbufferHandle(void);


/*!
 *  Inject a help command line into the input Cbuffer.
 * 
 *  This is a simple way to display user instructions on the connected terminal.
 */
void cdcDevice_InjectHelpCommand(void);

#endif  /* #define CDC_DEVICE_H */
