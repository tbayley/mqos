# MQOS demo application for PIC16F1619 #

This is a MQOS demo application for the PIC16F1619 processor that runs on the
Microchip low pin-count 8-bit
[Curiosity development board](https://www.microchip.com/DevelopmentTools/ProductDetails/PartNO/DM164137),
Part Number: DM164137.  The PIC16F1619 processor is the standard part fitted to
the Curiosity development board at manufacture. The project is configured to use
the board's built-in PKOB debugger (PIC-kit on board). However, for enhanced
debugging capabilities, you can connect external Microchip debugger as described
below.

The development tool versions used to create the application are:
- [MPLAB X IDE](https://www.microchip.com/mplab/mplab-x-ide) v5.20
- [XC8 C compiler](https://www.microchip.com/mplab/compilers) v2.05

The chip peripherals were configured at design time by using Microchip's
[MPLAB Code Configurator](https://www.microchip.com/mplab/mplab-code-configurator),
which generates driver files for the clock oscillator, GPIO and peripherals in
the _mcc\_generated\_files_ subdirectory.


## Using an external debugger with the Curiosity board ##

It is possible to disconnect the Curiosity development board's built-in PKOB 
debugger (PIC-kit on board) and connect an external debugger that uses a
standard 6-pin ICSP (in-circuit serial programming) cable, such as MPLAB
ICD 3 or ICD 4, to a pin header fitted to connector position P1 on the PCB.

Refer to the Curiosity development board schematic in Appendix A.1 of the
[Curiosity Development Board User's Guide](http://ww1.microchip.com/downloads/en/DeviceDoc/40001804B.pdf).

A single hardware change is required:

- Solder a 6-pin single-in-line pin header to connector position P1 at the left
  hand side of the board.

To avoid damaging the built-in PKOB debugger, you are advised to use the
Curiosity board in 3.3V self-powered mode and not to power it from the debugger.
Power the board via a USB cable plugged into connector J2. Set the jumper link
on connector J12 to configure VDD to 3.3V.  Use the project properties dialog
to set the debugger _"Power"_ option that does not power the target device.

The PKOB circuit is powered up only when a USB cable is connected to J2.  If an
external debugger or device programmer applies programming waveforms to the
ICSP pins while the PKOB circuit is unpowered, the PKOB circuitry is likely to
be permanently damaged.

The PKOB circuit runs from a 3.3V power rail. Voltage level shifters are fitted
between the PKOB circuit and the target processor, to enable the target
processor to run at either 3.3V or 5V. However the level shifters are intended
to drive programming waveforms onto the target device ICSP pins, not to
withstand programming waveforms applied by an external device. Although it is
probably OK to configure a 5V target processor power rail, it is safer to use
3.3V if that provides fast enough processor performance for your application.


## User instructions for the MQOS demo application ##

The demo application flashes LED D7 with a period of 1 second and flashes LED D6
with a period of 4 seconds.  When push-button switch S1 is pressed and held, the
flashing LEDs freeze in their current state for the duration of the button
press.  The LEDs resume flashing when the button is released.


## Profiler ##

LED D4 is driven by the MQOS kernel profiler, which sets GPIO pin RA5 high (LED
D4 on) on entry to the task scheduler and sets it low again (LED D4 off) when
the scheduler returns.  Connecting an oscilloscope to this GPIO pin provides a
simple way to check the system clock tick period, and to assess the amount of
processor time consumed by the MQOS kernel.


## Memory usage

When the demo application is built for PIC16F1619 target device using Microchip
XC8 C compiler v2.05 in "free" mode, at optimization level 0 (no optimization),
the memory usage statistics using the FIFO order message scheduling algorithm
are:

```
Memory Summary:
    Program space        used   28Fh (   655) of  2000h words   (  8.0%)
    Data space           used    3Dh (    61) of   400h bytes   (  6.0%)
    EEPROM space         None available
    Data stack space     used     0h (     0) of   3C0h bytes   (  0.0%)
    Configuration bits   used     3h (     3) of     3h words   (100.0%)
    ID Location space    used     0h (     0) of     4h bytes   (  0.0%)
```

The memory usage statistics using the unordered message scheduling algorithm
are:

```
Memory Summary:
    Program space        used   230h (   560) of  2000h words   (  6.8%)
    Data space           used    35h (    53) of   400h bytes   (  5.2%)
    EEPROM space         None available
    Data stack space     used     0h (     0) of   3C9h bytes   (  0.0%)
    Configuration bits   used     3h (     3) of     3h words   (100.0%)
    ID Location space    used     0h (     0) of     4h bytes   (  0.0%)
```
