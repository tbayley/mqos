/*
 * tmr0.c - TMR0 module source file for PIC16F676.
 *
 * This header file provides APIs for the 8-bit timer TMR0.
 * 
 * Copyright (C) 2021 Antony Bayley.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <xc.h>
#include "tmr0.h"
#include "mqos.h"


/* PRIVATE VARIABLE DEFINITIONS **********************************************/

volatile static uint8_t timer0ReloadVal;    //!< Timer0 reload value


/* PUBLIC FUNCTION DEFINITIONS ***********************************************/

void tmr0_Initialize(void)
{
    // Configure TMR0
	
    // PSA assigned; PS 1:8; TMRSE Increment_hi_lo; mask the nWPUEN and INTEDG bits
    OPTION_REG = (uint8_t)((OPTION_REG & 0xC0) | (0xD2 & 0x3F)); 
	
    // TMR0: Set 1ms period; 
    TMR0 = 0x83;
	
    // Load the TMR value to reload variable
    timer0ReloadVal = 0x83;

    // Clear Interrupt flag before enabling the interrupt
    INTCONbits.T0IF = 0;

    // Enabling TMR0 interrupt
    INTCONbits.T0IE = 1;
}


void tmr0_ISR(void)
{
    /* Clear the TMR0 interrupt flag and reload the counter.
     * Interrupts remain disabled until TMR0_ISR returns.
     */
    INTCONbits.T0IF = 0;
    TMR0 = timer0ReloadVal;
    // call the MQOS system tick function
    mqos_Tick();
}
