/*
 * deviceConfig.c - Device configuration module source file for PIC16F676.
 *
 * This source file sets the device configuration bits.
 * 
 * Copyright (C) 2021 Antony Bayley.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

/* Configuration bits */
#pragma config BOREN = OFF      //!< Brown-out Reset Enable->Brown-out Reset disabled
#pragma config CPD = OFF        //!< EEPROM Data Memory Code Protection->Program memory code protection is disabled
#pragma config CP = OFF         //!< Flash Program Memory Code Protection->Program memory code protection is disabled
#pragma config MCLRE = ON       //!< MCLR Pin Function Select->MCLR/VPP pin function is MCLR
#pragma config PWRTE = OFF      //!< Power-up Timer Enable->PWRT disabled
#pragma config WDTE = OFF       //!< Watchdog Timer Enable->WDT disabled
#pragma config FOSC = INTRCIO   //!< INTOSC oscillator: I/O function on RA4/OSC2/CLKOUT pin, I/O function on RA5/OSC1/CLKIN
