/*
 * mqosConfig.h - mqosConfig module header file.
 *
 * Copyright (C) 2021 Antony Bayley.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef MQOSCONFIG_H
#define	MQOSCONFIG_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>
#include "mqosPort.h"
#include "pinManager.h"


/* CONSTANTS *****************************************************************/

/*! Maximum number of queued messages at any one time (the sum of messages in
 *  the ready and delayed message queues).
 */
#define MQOS_CONFIG_HEAP_SIZE 3


/*! MQOS system timer tick period, in microseconds */
#define MQOS_CONFIG_TICK_PERIOD_US  1000


/*! MQOS message scheduler algorithm.
 *
 *  Select one of the message scheduling algorithms by setting value of its
 *  preprocessor symbol 1 in this in this block of #define statements. The
 *  #define statements here can be overridden by defining the corresponding
 *  preprocessor symbols in the compiler command line.
 *
 *  MQOS_CONFIG_MSG_SCHEDULER_FIFO_ORDER delivers messages in the order that
 *  they were posted.
 *
 *  MQOS_CONFIG_MSG_SCHEDULER_UNORDERED delivers messages in the order that
 *  they appear in the heap, which may not correspond to the order in which the
 *  messages were posted. Unordered delivery uses less RAM and flash memory
 *  than the FIFO order message scheduler.
 */
#ifndef MQOS_CONFIG_MSG_SCHEDULER_FIFO_ORDER
#define MQOS_CONFIG_MSG_SCHEDULER_FIFO_ORDER    0
#endif

#ifndef MQOS_CONFIG_MSG_SCHEDULER_UNORDERED
#define MQOS_CONFIG_MSG_SCHEDULER_UNORDERED     1
#endif


/*! MQOS message payload
 *
 *  If you want the MQOS message to contain a payload, set the preprocessor
 *  symbol MQOS_CONFIG_MSG_CONTAINS_PAYLOAD to 1 and, in the type definitions
 *  section of this file, edit the tMqosConfig_MessagePayload typedef to suit
 *  your application requirements.
 * 
 *  Some simpler applications, or applications targetting devices with very
 *  little RAM, may use MQOS messages without a payload.  To achieve this set
 *  the preprocessor symbol MQOS_CONFIG_MSG_CONTAINS_PAYLOAD to 0. If this is
 *  done, the tMqosMessage structure contains just the message ID.
 */
#ifndef MQOS_CONFIG_MSG_CONTAINS_PAYLOAD
#define MQOS_CONFIG_MSG_CONTAINS_PAYLOAD        0
#endif


/*! MQOS run-time assert macro
 *
 *  If the expression 'expr' is true, do nothing. If 'expr' is false, halt the
 *  application to enable the fault to be detected.
 *
 *  Run-time assert checks are always disabled for release builds (with the
 *  preprocessor symbol NDEBUG defined).
 * 
 *  By default run-time assert checks are enabled for debug builds, where the
 *  preprocessor symbol NDEBUG is not defined. An assert failure results in the
 *  system disabling interrupts and entering an infinite loop. If you run the
 *  code in a debugger, you can detect the occurrence of assert failures by
 *  adding appropriate breakpoints.
 */
#ifdef NDEBUG
#define MQOS_CONFIG_ASSERT(expr)    ((void) (expr))
#else   /* #ifdef NDEBUG */
#define MQOS_CONFIG_ASSERT(expr)   do   \
{                                       \
    if(!(expr))                         \
    {                                   \
        mqosPort_ENTER_CRITICAL();      \
        while(1) {};                    \
    }                                   \
} while(0);
#endif  /* #ifdef NDEBUG */


/*
 * The MQOS kernel profiler sets a GPIO pin high on entry to the task scheduler
 * and sets it low again when the scheduler returns. This provides a simple way
 * to check the system clock tick period, and to assess the amount of processor
 * time consumed by the MQOS kernel.
 * 
 * To enable profiling, #define mqosConfig_profilerStart() and
 * mqosConfig_profilerStop() as target-specific functions or macros that set a
 * GPIO pin high and low respectively.  For example:
 * 
 *    #define mqosConfig_profilerStart()  LED_D4_SetHigh()
 *    #define mqosConfig_profilerStop()   LED_D4_SetLow()
 *  
 * To disable profiling, #define the macros to nothing, like this:
 * 
 *    #define mqosConfig_profilerStart()
 *    #define mqosConfig_profilerStop()
 */
    
/*! Macro to set GPIO high on entry to task scheduler function */
#define mqosConfig_profilerStart()  LED_D4_SetHigh()

/*! Macro to set GPIO low on return from task scheduler function */
#define mqosConfig_profilerStop()   LED_D4_SetLow()


/* String library
 *
 * The mqos/extra subdirectory includes the MQOS string library, with header
 * file "mqosString.h". It implements a small selection of the <string.h>
 * standard library's functions that are used by the MQOS CLI. The aim is to
 * achieve a smaller memory footprint than would be achieved using the full
 * <string.h> standard library, and to enable MQOS CLI to be used on systems
 * that do not implement the <string.h> standard library.
 *
 * If you are not using MQOS CLI and your application does not use string
 * library functions, choose MQOS_CONFIG_NO_STRING_LIBRARY. 
 */
#define MQOS_CONFIG_NO_STRING_LIBRARY           0   //!< No string library is used
#define MQOS_CONFIG_STANDARD_STRING_LIBRARY     1   //!< Use the standard string library <string.h>
#define MQOS_CONFIG_MQOS_STRING_LIBRARY         2   //!< Use the MQOS string library "mqosString.h"

/*! String library: One of MQOS_CONFIG_NO_STRING_LIBRARY, MQOS_CONFIG_STANDARD_STRING_LIBRARY, MQOS_CONFIG_MQOS_STRING_LIBRARY. */
#ifndef MQOS_CONFIG_STRING_LIBRARY
#define MQOS_CONFIG_STRING_LIBRARY      MQOS_CONFIG_NO_STRING_LIBRARY
#endif


/* Stdio library
 *
 * The mqos/extra subdirectory includes the MQOS stdio library, which implements
 * a small subset of the <stdio.h> standard library's functions. The printf()
 * function has a restricted range of format specifiers. It typically uses less
 * program memory and RAM than the <stdio.h> implementation, so may be a useful
 * alternative for some applications on memory-constrained embedded systems.
 * 
 * The Microchip PIC standard library makes use of the function putch() to
 * implement its printf() function. putch() returns void, so the calling
 * function cannot tell if printing fails due to buffer overflow. The MQOS
 * printf() function does not suffer from this problem. It returns the number of
 * characters written to stdout, so remedial action can be taken if the expected
 * number of characters are not written.
 *
 * If your application does not require standard IO functions, choose the
 * option MQOS_CONFIG_NO_STDIO_LIBRARY. If your application uses IO functions from
 * <stdio.h>,  choose MQOS_CONFIG_STANDARD_STDIO_LIBRARY. If it uses the MQOS
 * implementation, choose MQOS_CONFIG_MQOS_STDIO_LIBRARY.
 */
#define MQOS_CONFIG_NO_STDIO_LIBRARY            0   //!< No IO library is used
#define MQOS_CONFIG_STANDARD_STDIO_LIBRARY      1   //!< Use the standard IO library <stdio.h>
#define MQOS_CONFIG_MQOS_STDIO_LIBRARY          2   //!< Use the MQOS IO library "mqosStdio.h"

/*! Standard IO library: one of MQOS_CONFIG_NO_STDIO_LIBRARY, MQOS_CONFIG_STANDARD_STDIO_LIBRARY, MQOS_CONFIG_MQOS_STDIO_LIBRARY */
#ifndef MQOS_CONFIG_STDIO_LIBRARY
#define MQOS_CONFIG_STDIO_LIBRARY       MQOS_CONFIG_NO_STDIO_LIBRARY
#endif

/*! Configure printf support for long data type: e.g. printf("x = %ld\n").
 *    1: enable printf support for long data types.
 *    0: disable printf support for long data types.
 */
#ifndef MQOS_CONFIG_STDIO_LIBRARY_LONG_PRINTF_ENABLED
#define MQOS_CONFIG_STDIO_LIBRARY_LONG_PRINTF_ENABLED   0   // 1: Enable, 0: Disable.
#endif


/* TYPE DEFINITIONS **********************************************************/


/*! MQOS base type
 *
 *  The base type is int8_t for an 8-bit processor, int_16_t for a 16-bit
 *  processor and int32_t for a 32-bit processor.
 */
typedef int8_t tMqosConfig_BaseType;


/*! MQOS unsigned base type
 *
 *  The unsigned base type is uint8_t for an 8-bit processor, uint_16_t for a
 *  16-bit processor and uint32_t for a 32-bit processor.
 */
typedef uint8_t tMqosConfig_UBaseType;


/*! MQOS message ID's
 *
 *  mqosConfig_MSG_NONE must be the first value in the enum list and
 *  mqos_MSG_MAX_ID  must be the last value.  All other enum values are
 *  application-specific and may be re-defined as required.
 *
 *  Application code must never set a message's msgId to mqosConfig_MSG_NONE.
 *  Doing so disrupts the operation of the mqosMessage_Malloc() and
 *  mqosMessage_Free() functions, causing the message heap to become corrupted.
 */
typedef enum
{
    mqosConfig_MSG_NONE = 0,    //!< Setting this message ID returns a message to the heap for re-use (must be first item in enum list)

    /*------ Start of the application-specific message ID definitions ------*/
    mqosConfig_MSG_TOGGLE_D6,   //*< Toggle LED D6
    mqosConfig_MSG_TOGGLE_D7,   //*< Toggle LED D7
    mqosConfig_MSG_POLL_BTN,    //*< Poll the push-button switch input (S1)
    /*------ End of the application-specific message ID definitions --------*/

    mqos_MSG_MAX_ID             //*< Maximum message ID (must be last item in enum list)
} tMqosConfig_MessageId;


/*! MQOS message index type.
 *
 *  The message heap is an array, accessed using an integer index. The index
 *  word length must be large enough to hold the value MQOS_CONFIG_HEAP_SIZE.
 */
typedef uint8_t tMqosConfig_MessageIndex;


/*! MQOS timer count type: sets the timer word length.
 *  tMqosConfig_TimerCount must be an unsigned integer type
 */
typedef uint16_t tMqosConfig_TimerCount;


/*! MQOS message payload class */
typedef struct
{
    uint8_t data;
} tMqosConfig_MessagePayload;


#ifdef __cplusplus
}
#endif

#endif  /* MQOSCONFIG_H */
