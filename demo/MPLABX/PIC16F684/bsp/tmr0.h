/*
 * tmr0.h - TMR0 module header file for PIC16F684.
 *
 * This header file provides APIs for the 8-bit timer TMR0.
 * 
 * Copyright (C) 2021 Antony Bayley.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef TMR0_H
#define TMR0_H

#include <stdint.h>
#include <stdbool.h>

#ifdef __cplusplus  // Provide C++ Compatibility
extern "C" {
#endif

        
/* PUBLIC FUNCTION DECLARATIONS **********************************************/

/*!
 *  This function initialises the TMR0 registers.
 * 
 *  The function must be called before any other TMR0 function is called.
 */
void tmr0_Initialize(void);


/*!
 *  Timer interrupt service routine.
 * 
 *  The timer interrupt Service routine is called by the Interrupt Manager.
 */
void tmr0_ISR(void);


#ifdef __cplusplus  // Provide C++ Compatibility
}
#endif

#endif // TMR0_H
