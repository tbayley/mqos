# MQOS demo application for PIC16F684 #

This is a MQOS demo application for the PIC16F684 processor that runs on the
Microchip low pin-count 8-bit
[Curiosity development board](https://www.microchip.com/DevelopmentTools/ProductDetails/PartNO/DM164137),
Part Number: DM164137. The board's built-in PKOB debugger (PIC-kit on board) is
not designed to work with older devices such as the PIC16F684, so the board was
modified to add a 6-pin ICSP (in-circuit serial programming) header. The project
is configured to use the MPLAB ICD 3 debugger, but you can change the settings
to connect any other Microchip debugger that supports 6-pin ICSP connectivity.

The PIC16F684 is a 14-pin device.  Plug it into the Curiosity board's 20-way IC
socket with pin 1 of the chip in pin 1 of the IC socket, and pin 14 of the chip
in pin 20 of the socket.

The development tool versions used to create the application are:
- [MPLAB X IDE](https://www.microchip.com/mplab/mplab-x-ide) v5.20
- [XC8 C compiler](https://www.microchip.com/mplab/compilers) v2.05

Microchip's [MPLAB Code Configurator](https://www.microchip.com/mplab/mplab-code-configurator)
does not support older devices such as the PIC16F684.  Therefore the demo
application uses a hand-coded board support package, in the _bsp_ subdirectory,
to implement drivers for the clock oscillator, GPIO and peripherals.

The PIC16F684 has no built-in debug circuitry, so the external debugger can only
program the device, not debug it. An optional debug header board, part number
[AC162055](http://ww1.microchip.com/downloads/en/DeviceDoc/Header_Board_51292m.pdf),
is available to provide debug support. The header contains a special ICD version
of the PIC16F684, with built-in debug circuity and extra pins, which is wired up
to a 14-pin header that can be plugged into an IC socket on the target board.
Because of this, the PIC16F684 is **NOT RECOMMENDED FOR NEW DESIGNS**. Use a
newer device with built-in debug circuitry instead, like
[PIC16F18324](https://www.microchip.com/wwwproducts/en/PIC16F18324).


## Using an external debugger with the Curiosity board ##

It is possible to disconnect the Curiosity development board's built-in PKOB 
debugger (PIC-kit on board) and connect an external debugger that uses a
standard 6-pin ICSP (in-circuit serial programming) cable, such as MPLAB
ICD 3 or ICD 4, to a pin header fitted to connector position P1 on the PCB.

Refer to the Curiosity development board schematic in Appendix A.1 of the
[Curiosity Development Board User's Guide](http://ww1.microchip.com/downloads/en/DeviceDoc/40001804B.pdf).

A single hardware change is required:

- Solder a 6-pin single-in-line pin header to connector position P1 at the left
  hand side of the board.

To avoid damaging the built-in PKOB debugger, you are advised to use the
Curiosity board in 3.3V self-powered mode and not to power it from the debugger.
Power the board via a USB cable plugged into connector J2. Set the jumper link
on connector J12 to configure VDD to 3.3V.  Use the project properties dialog
to set the debugger _"Power"_ option that does not power the target device.

The PKOB circuit is powered up only when a USB cable is connected to J2.  If an
external debugger or device programmer applies programming waveforms to the
ICSP pins while the PKOB circuit is unpowered, the PKOB circuitry is likely to
be permanently damaged.

The PKOB circuit runs from a 3.3V power rail. Voltage level shifters are fitted
between the PKOB circuit and the target processor, to enable the target
processor to run at either 3.3V or 5V. However the level shifters are intended
to drive programming waveforms onto the target device ICSP pins, not to
withstand programming waveforms applied by an external device. Although it is
probably OK to configure a 5V target processor power rail, it is safer to use
3.3V if that provides fast enough processor performance for your application.


## User instructions for the MQOS demo application ##

The demo application flashes LED D7 with a period of 1 second and flashes LED D6
with a period of 4 seconds.  When push-button switch S1 is pressed and held, the
flashing LEDs freeze in their current state for the duration of the button
press.  The LEDs resume flashing when the button is released.


## Profiler ##

LED D4 is driven by the MQOS kernel profiler, which sets GPIO pin RA5 high (LED
D4 on) on entry to the task scheduler and sets it low again (LED D4 off) when
the scheduler returns.  Connecting an oscilloscope to this GPIO pin provides a
simple way to check the system clock tick period, and to assess the amount of
processor time consumed by the MQOS kernel.


## Memory usage

The MQOS demo application for PIC16F688 has been built with the following option
defined in _mqosConfig.h_, to reduce RAM usage:

    #define MQOS_CONFIG_MSG_CONTAINS_PAYLOAD        0

When the demo application is built for PIC16F684 target device using Microchip
XC8 C compiler v2.05 in "free" mode, at optimization level 0 (no optimization),
the memory usage statistics using the FIFO order message scheduling algorithm
are:

```
Memory Summary:
    Program space        used   229h (   553) of   800h words   ( 27.0%)
    Data space           used    34h (    52) of    80h bytes   ( 40.6%)
    EEPROM space         used     0h (     0) of   100h bytes   (  0.0%)
    Data stack space     used     0h (     0) of    28h bytes   (  0.0%)
    Configuration bits   used     1h (     1) of     1h word    (100.0%)
    ID Location space    used     0h (     0) of     4h bytes   (  0.0%)
```

The memory usage statistics using the unordered message scheduling algorithm
are:

```
Memory Summary:
    Program space        used   1D4h (   468) of   800h words   ( 22.9%)
    Data space           used    2Dh (    45) of    80h bytes   ( 35.2%)
    EEPROM space         used     0h (     0) of   100h bytes   (  0.0%)
    Data stack space     used     0h (     0) of    2Fh bytes   (  0.0%)
    Configuration bits   used     1h (     1) of     1h word    (100.0%)
    ID Location space    used     0h (     0) of     4h bytes   (  0.0%)
```
