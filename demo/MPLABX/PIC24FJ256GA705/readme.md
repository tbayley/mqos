# MQOS demo application for PIC24FJ256GA705 #

This is a MQOS demo application for the PIC24FJ256GA705 processor that runs on
the Microchip
[PIC24FJ256GA7 Curiosity development board](https://www.microchip.com/DevelopmentTools/ProductDetails/DM240016),
Part Number: DM240016.  The project is configured to use the board's built-in
PKOB debugger (PIC-kit on board). However, for enhanced debugging capabilities,
you can connect external Microchip debugger as described below.

The development tool versions used to create the application are:
- [MPLAB X IDE](https://www.microchip.com/mplab/mplab-x-ide) v5.20
- [XC16 C compiler](https://www.microchip.com/mplab/compilers) v1.36

The chip peripherals were configured at design time by using Microchip's
[MPLAB Code Configurator](https://www.microchip.com/mplab/mplab-code-configurator),
which generates driver files for the clock oscillator, GPIO and peripherals in
the _mcc\_generated\_files_ subdirectory.


## Using an external debugger with the PIC24FJ256GA7 Curiosity board ##

It is possible to disconnect the PIC24FJ256GA7 Curiosity development board's
built-in PKOB debugger (PIC-kit on board) and connect an external debugger that
uses a standard 6-pin ICSP (in-circuit serial programming) cable, such as MPLAB
ICD 3 or ICD 4, to a pin header fitted to connector position J2 on the PCB.

Refer to the PIC24FJ256GA7 Curiosity development board schematic in the
[PIC24FJ256GA7 Curiosity Development Board Quick Start Guide](http://ww1.microchip.com/downloads/en/DeviceDoc/50002589a.pdf).

A single hardware change is required:

- Solder a 6-pin single-in-line pin header to connector position J2 at the left
  hand side of the board.

When using the PIC24FJ256GA7 Curiosity board with an external debugger, you must
still plug in the board's USB cable to provide power.  The target processor's
3.3V power rail is derived from the 5V USB bus power via a voltage regulator.


## User instructions for the MQOS demo application ##

The demo application flashes LED D1 with a period of 1 second and flashes LED D2
with a period of 4 seconds.  When push-button switch S1 is pressed and held, the
flashing LEDs freeze in their current state for the duration of the button
press.  The LEDs resume flashing when the button is released.

The 4MHz instruction clock (Fosc/2) is output on pin RA3 of connector J4.


## Profiler ##

GPIO output pin RA10, on connector J5 pin 22, is driven by the MQOS kernel
profiler. It is set high on entry to the task scheduler and set low again when
the scheduler returns.  Connecting an oscilloscope to this GPIO pin provides a
simple way to check the system clock tick period, and to assess the amount of
processor time consumed by the MQOS kernel.


## Memory usage ##

When the demo application is built for PIC24FJ256GA705 target device using
Microchip XC16 C compiler v1.36 in "free" mode, at optimization level 0 (no
optimization), the memory usage statistics using the FIFO order message
scheduling algorithm are:

```
"program" Memory  [Origin = 0x100, Length = 0x2ae00]

section                    address   length (PC units)   length (bytes) (dec)
-------                    -------   -----------------   --------------------
.text                        0x100               0x16e           0x225  (549)
.const                       0x26e                 0x4             0x6  (6)
.text                        0x272               0x43e           0x65d  (1629)
.dinit                       0x6b0                0x36            0x51  (81)
.text                        0x6e6                0x4c            0x72  (114)

                 Total "program" memory used (bytes):          0x94b  (2379) <1%


"data" Memory  [Origin = 0x800, Length = 0x4000]

section                    address      alignment gaps    total length  (dec)
-------                    -------      --------------    -------------------
.nbss                        0x800                   0             0xc  (12)
.ndata                       0x80c                   0             0x2  (2)
.nbss                        0x80e                   0             0x2  (2)
.ndata                       0x810                   0             0x2  (2)
.bss                         0x812                   0            0x48  (72)

                 Total "data" memory used (bytes):           0x5a  (90) <1%


Dynamic Memory Usage

region                     address                      maximum length  (dec)
------                     -------                      ---------------------
heap                             0                                   0  (0)
stack                        0x85a                              0x3fa6  (16294)

                 Maximum dynamic memory (bytes):         0x3fa6  (16294)
```

The memory usage statistics using the unordered message scheduling algorithm
are:

```
"program" Memory  [Origin = 0x100, Length = 0x2ae00]

section                    address   length (PC units)   length (bytes) (dec)
-------                    -------   -----------------   --------------------
.text                        0x100               0x16e           0x225  (549)
.text                        0x26e               0x3de           0x5cd  (1485)
.dinit                       0x64c                0x36            0x51  (81)
.text                        0x682                0x4c            0x72  (114)

                 Total "program" memory used (bytes):          0x8b5  (2229) <1%


"data" Memory  [Origin = 0x800, Length = 0x4000]

section                    address      alignment gaps    total length  (dec)
-------                    -------      --------------    -------------------
.nbss                        0x800                   0             0x8  (8)
.ndata                       0x808                   0             0x2  (2)
.nbss                        0x80a                   0             0x2  (2)
.ndata                       0x80c                   0             0x2  (2)
.bss                         0x80e                   0            0x40  (64)

                 Total "data" memory used (bytes):           0x4e  (78) <1%


Dynamic Memory Usage

region                     address                      maximum length  (dec)
------                     -------                      ---------------------
heap                             0                                   0  (0)
stack                        0x84e                              0x3fb2  (16306)

                 Maximum dynamic memory (bytes):         0x3fb2  (16306)
```

The unordered message scheduler uses less program memory and RAM than the FIFO
order message scheduler.
