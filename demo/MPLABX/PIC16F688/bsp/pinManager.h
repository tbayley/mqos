/*
 * pinManager.h - Pin manager module header file for PIC16F688.
 *
 * This header file provides APIs for driver for GPIO pins. The LED and switch
 * names are for the Curiosity development board, Part Number: DM164137.
 * 
 * Copyright (C) 2021 Antony Bayley.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef PIN_MANAGER_H
#define PIN_MANAGER_H

#ifdef __cplusplus
extern "C" {
#endif

#include <xc.h>

/* PUBLIC MACROS *************************************************************/
    
#define INPUT   1
#define OUTPUT  0

#define HIGH    1
#define LOW     0

#define ANALOG      1
#define DIGITAL     0

#define PULL_UP_ENABLED      1
#define PULL_UP_DISABLED     0

/* LED_D5 aliases */
#define LED_D5_TRIS                 TRISAbits.TRISA1
#define LED_D5_PORT                 PORTAbits.RA1
#define LED_D5_WPU                  WPUAbits.WPUA1
#define LED_D5_OD                   ODCONAbits.ODA1
#define LED_D5_ANS                  ANSELAbits.ANSA1
#define LED_D5_SetHigh()            do{ PORTAbits.RA1 = 1; } while(0)
#define LED_D5_SetLow()             do{ PORTAbits.RA1 = 0; } while(0)
#define LED_D5_Toggle()             do{ PORTAbits.RA1 = ~PORTAbits.RA1; } while(0)
#define LED_D5_GetValue()           PORTAbits.RA1
#define LED_D5_SetDigitalInput()    do{ TRISAbits.TRISA1 = 1; } while(0)
#define LED_D5_SetDigitalOutput()   do{ TRISAbits.TRISA1 = 0; } while(0)
#define LED_D5_SetPullup()          do{ WPUAbits.WPUA1 = 1; } while(0)
#define LED_D5_ResetPullup()        do{ WPUAbits.WPUA1 = 0; } while(0)
#define LED_D5_SetPushPull()        do{ ODCONAbits.ODA1 = 0; } while(0)
#define LED_D5_SetOpenDrain()       do{ ODCONAbits.ODA1 = 1; } while(0)
#define LED_D5_SetAnalogMode()      do{ ANSELAbits.ANSA1 = 1; } while(0)
#define LED_D5_SetDigitalMode()     do{ ANSELAbits.ANSA1 = 0; } while(0)

/* LED_D6 aliases */
#define LED_D6_TRIS                 TRISAbits.TRISA2
#define LED_D6_PORT                 PORTAbits.RA2
#define LED_D6_WPU                  WPUAbits.WPUA2
#define LED_D6_OD                   ODCONAbits.ODA2
#define LED_D6_ANS                  ANSELAbits.ANSA2
#define LED_D6_SetHigh()            do{ PORTAbits.RA2 = 1; } while(0)
#define LED_D6_SetLow()             do{ PORTAbits.RA2 = 0; } while(0)
#define LED_D6_Toggle()             do{ PORTAbits.RA2 = ~PORTAbits.RA2; } while(0)
#define LED_D6_GetValue()           PORTAbits.RA2
#define LED_D6_SetDigitalInput()    do{ TRISAbits.TRISA2 = 1; } while(0)
#define LED_D6_SetDigitalOutput()   do{ TRISAbits.TRISA2 = 0; } while(0)
#define LED_D6_SetPullup()          do{ WPUAbits.WPUA2 = 1; } while(0)
#define LED_D6_ResetPullup()        do{ WPUAbits.WPUA2 = 0; } while(0)
#define LED_D6_SetPushPull()        do{ ODCONAbits.ODA2 = 0; } while(0)
#define LED_D6_SetOpenDrain()       do{ ODCONAbits.ODA2 = 1; } while(0)
#define LED_D6_SetAnalogMode()      do{ ANSELAbits.ANSA2 = 1; } while(0)
#define LED_D6_SetDigitalMode()     do{ ANSELAbits.ANSA2 = 0; } while(0)

/* LED_D4 aliases */
#define LED_D4_TRIS                 TRISAbits.TRISA5
#define LED_D4_PORT                 PORTAbits.RA5
#define LED_D4_WPU                  WPUAbits.WPUA5
#define LED_D4_OD                   ODCONAbits.ODA5
#define LED_D4_SetHigh()            do { PORTAbits.RA5 = 1; } while(0)
#define LED_D4_SetLow()             do { PORTAbits.RA5 = 0; } while(0)
#define LED_D4_Toggle()             do { PORTAbits.RA5 = ~PORTAbits.RA5; } while(0)
#define LED_D4_GetValue()           PORTAbits.RA5
#define LED_D4_SetDigitalInput()    do { TRISAbits.TRISA5 = 1; } while(0)
#define LED_D4_SetDigitalOutput()   do { TRISAbits.TRISA5 = 0; } while(0)
#define LED_D4_SetPullup()          do { WPUAbits.WPUA5 = 1; } while(0)
#define LED_D4_ResetPullup()        do { WPUAbits.WPUA5 = 0; } while(0)
#define LED_D4_SetPushPull()        do { ODCONAbits.ODA5 = 0; } while(0)
#define LED_D4_SetOpenDrain()       do { ODCONAbits.ODA5 = 1; } while(0)

/* SWITCH_S1 aliases */
#define SWITCH_S1_TRIS                 TRISCbits.TRISC4
#define SWITCH_S1_PORT                 PORTCbits.RC4
#define SWITCH_S1_WPU                  WPUCbits.WPUC4
#define SWITCH_S1_OD                   ODCONCbits.ODC4
#define SWITCH_S1_SetHigh()            do{ PORTCbits.RC4 = 1; } while(0)
#define SWITCH_S1_SetLow()             do{ PORTCbits.RC4 = 0; } while(0)
#define SWITCH_S1_Toggle()             do{ PORTCbits.RC4 = ~PORTCbits.RC4; } while(0)
#define SWITCH_S1_GetValue()           PORTCbits.RC4
#define SWITCH_S1_SetDigitalInput()    do{ TRISCbits.TRISC4 = 1; } while(0)
#define SWITCH_S1_SetDigitalOutput()   do{ TRISCbits.TRISC4 = 0; } while(0)
#define SWITCH_S1_SetPullup()          do{ WPUCbits.WPUC4 = 1; } while(0)
#define SWITCH_S1_ResetPullup()        do{ WPUCbits.WPUC4 = 0; } while(0)
#define SWITCH_S1_SetPushPull()        do{ ODCONCbits.ODC4 = 0; } while(0)
#define SWITCH_S1_SetOpenDrain()       do{ ODCONCbits.ODC4 = 1; } while(0)

/* LED_D7 aliases */
#define LED_D7_TRIS                 TRISCbits.TRISC5
#define LED_D7_PORT                 PORTCbits.RC5
#define LED_D7_WPU                  WPUCbits.WPUC5
#define LED_D7_OD                   ODCONCbits.ODC5
#define LED_D7_SetHigh()            do{ PORTCbits.RC5 = 1; } while(0)
#define LED_D7_SetLow()             do{ PORTCbits.RC5 = 0; } while(0)
#define LED_D7_Toggle()             do{ PORTCbits.RC5 = ~PORTCbits.RC5; } while(0)
#define LED_D7_GetValue()           PORTCbits.RC5
#define LED_D7_SetDigitalInput()    do{ TRISCbits.TRISC5 = 1; } while(0)
#define LED_D7_SetDigitalOutput()   do{ TRISCbits.TRISC5 = 0; } while(0)
#define LED_D7_SetPullup()          do{ WPUCbits.WPUC5 = 1; } while(0)
#define LED_D7_ResetPullup()        do{ WPUCbits.WPUC5 = 0; } while(0)
#define LED_D7_SetPushPull()        do{ ODCONCbits.ODC5 = 0; } while(0)
#define LED_D7_SetOpenDrain()       do{ ODCONCbits.ODC5 = 1; } while(0)


/* PUBLIC FUNCTION DECLARATIONS **********************************************/

/*!
 *  GPIO and peripheral I/O initialisation
 */
void pinManager_Initialise(void);


#ifdef __cplusplus
}
#endif

#endif // PIN_MANAGER_H
/**
 End of File
*/