/*
 * pinManager.c - Pin manager module source file for PIC16F688.
 *
 * This header file provides APIs for driver for GPIO pins. The LED and switch
 * names are for the Curiosity development board, Part Number: DM164137.
 * 
 * Copyright (C) 2021 Antony Bayley.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <xc.h>
#include "pinManager.h"


/* PUBLIC FUNCTION DEFINITIONS ***********************************************/

void pinManager_Initialise(void)
{
    /**
    TRISx registers
    */
    TRISA = 0xD9;   // RA1, RA2 and RA5 output, all other PORT A pins input
    TRISC = 0xDF;   // RC5 output, all other PORT C pins input

    /**
    ANSEL registers
    */
    ANSEL = 0xF9;   // AN1(RA1) and AN2(RA2) are digital output, all other ANx pins are analog inputs

    /**
    WPUx registers
    */
    WPUA = 0x00;
    OPTION_REGbits.nRAPU = 1;

    /* Set all output port pins low */
    PORTA = 0;
    PORTC = 0;
}
