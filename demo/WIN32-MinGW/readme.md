# MQOS demo application for Windows (MinGW) #

This is a MQOS demo application, written in C, that runs on Windows and builds
with [MinGW](http://www.mingw.org/). It uses a Makefile based build system. The
available make targets are:

    make           -   List all make targets (equivalent to make help)
    make build     -   Build MQOS demo application: $(LINK_TARGET)
    make run       -   Run MQOS demo application
    make clean     -   Delete all build artefacts
    make help      -   List all make targets and the actions performed

MQOS is intended for use on resource-constrained embedded systems. The Windows
port enables developers that do not have access to an embedded microprocessor
development board to evaluate MQOS. It is not envisaged that MQOS will be used
to develop Windows applications!


## User instructions for the MQOS demo application ##

The application runs two periodic tasks that are triggered to run by MQOS
messages. Each task prints messages to the console to identify the task ID and
how many times the task has run. A keyboard polling task, triggered by another
MQOS message, periodically checks for keyboard input and responds as follows:

    <Esc> or 'Q' key pressed    Quit the application.
    '1' key pressed             Toggle task 1 between enabled/disabled state.
    '2' key pressed             Toggle task 2 between enabled/disabled state.
